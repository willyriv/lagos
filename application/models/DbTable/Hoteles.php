<?php

class Application_Model_DbTable_Hoteles extends Zend_Db_Table_Abstract {

    protected $_name = 'hk_hotel';
    


    public function like($desc) {
        $select = $this->select()
             ->from(array('e'  => 'hk_hotel') ,array('nombre', 'id'))  
             
              ->where('e.nombre LIKE "%'.$desc.'%"');

               $select->setIntegrityCheck(false);

              //echo $select;


        return $this->fetchAll($select);
    }

  
    public function get($id) {
      

         $select = $this->select()
             ->from(array('e'  => 'hk_hotel'))  
             ->join(array('s' => 'countries'),'e.pais = s.id', array('nombre_pais'=>'name'))
             ->join(array('c' => 'states'),'e.estado = c.id', array('nombre_estado'=>'name'))
             ->where('e.id="'.$id.'"');

        $select->setIntegrityCheck(false);

        $row=$this->fetchRow($select);

        return $row->toArray();

    }

    public function gets($id) {
        $row = $this->fetchAll('id_solicitud = "' .$id.'"');
        
        return $row;
    }


     public function getAll() {


       $select = $this->select()
             ->from(array('e'  => 'hk_hotel'))  
             ->join(array('s' => 'countries'),'e.pais = s.id', array('nombre_pais'=>'name'))
             ->join(array('c' => 'states'),'e.estado = c.id', array('nombre_estado'=>'name'))
              ->order('e.id DESC');
        
       
                
        $select->setIntegrityCheck(false);

        return $this->fetchAll($select);
    }
 
    public function add($data = array()) {
        $rs = $this->insert($data);
        return $rs;
    }
    
    public function upd($id, $data = array()) {
        $rs = $this->update($data, 'id = ' . (int)$id);
        return $rs;
    }
    
    public function del($id) {
        $rs = $this->delete('id = ' . (int)$id);
        return $rs;
    }

        public function getHoteles() {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_hotel'),array('id','nombre', 'pais', 'estado', 'categoria'   ))  
             ->join(array('f' => 'hk_fotos'),'e.id = f.id_solicitud', array('foto'))
             ->group('e.id')
            ->where('f.posicion="1"')
            ->where('e.estatus="1"')
              ->order('e.nombre DESC');
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchAll($select);
        
    }

    

        public function getHotelesAleatorio() {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_hotel'),array('id','nombre', 'pais', 'estado', 'categoria'   ))  
             ->join(array('f' => 'hk_fotos'),'e.id = f.id_solicitud', array('foto'))
             ->group('e.id')
            ->where('f.posicion="1"')
            ->where('e.estatus="1"')
              ->order('rand()')
              ->limit(8,0);
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchAll($select);
        
    }

         public function getHotelesEstado($estado) {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_hotel'),array('id','nombre', 'pais', 'estado', 'categoria', 'direccion'   ))  
             ->join(array('f' => 'hk_fotos'),'e.id = f.id_solicitud', array('foto'))
             ->join(array('h' => 'hk_habitaciones'),'e.id = h.id_hotel', array('tarifa_bs', 'tarifa_usd'))
             ->group('e.id')
            ->where('f.posicion="1"')
            ->where('e.estatus="1"')
            ->where('e.estado="'.$estado.'"')
              ->order('e.nombre DESC');
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchAll($select);
        
    }

     public function getHotelesEstadoCantidad($cantidad, $estado) {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_hotel'),array('id','nombre', 'pais', 'estado', 'categoria', 'direccion'   ))  
             ->join(array('f' => 'hk_fotos'),'e.id = f.id_solicitud', array('foto'))
             ->join(array('h' => 'hk_habitaciones'),'e.id = h.id_hotel', array('id_habitacion'=>'id'))
             ->join(array('t' => 'hk_tarifa'),'e.id = t.id_hotel', array('bs', 'usd'))
             ->group('e.id')
            ->where('f.posicion="1"')
            ->where('e.estatus="1"')
            ->where('e.estado="'.$estado.'"')
              ->order('t.bs ASC')
              ->limit($cantidad, 0);
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchAll($select);
        
    }

      public function getHotelesEstadoUn($estado) {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_hotel'),array('id','nombre', 'pais', 'estado', 'categoria', 'direccion'   ))  
             ->join(array('f' => 'hk_fotos'),'e.id = f.id_solicitud', array('foto'))
             ->group('e.id')
            ->where('e.estatus="1"')
            ->where('e.estado="'.$estado.'"')
              ->order('e.nombre DESC');
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchAll($select);
        
    }

    public function getHotelesEstadoDisponible($estado) {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_hotel'),array('id','nombre', 'pais', 'estado', 'categoria', 'direccion'   ))  
             ->join(array('f' => 'hk_fotos'),'e.id = f.id_solicitud', array('foto'))
             ->join(array('h' => 'hk_tarifa'),'e.id = h.id_hotel', array('bs', 'usd'))
             ->group('e.id')
            ->where('f.posicion="1"')
            ->where('e.estatus="1"')
            ->where('e.estado="'.$estado.'"')
            ->where('h.desde<=curdate()')
            ->where('h.hasta>=curdate()')

              ->order('e.nombre DESC');
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchAll($select);
        
    }

    
    
}