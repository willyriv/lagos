<?php

class Application_Model_DbTable_Habitaciones extends Zend_Db_Table_Abstract {

    protected $_name = 'hk_habitaciones';
    

  
    public function get($id) {
        $row = $this->fetchRow('id = ' . (int)$id);
        if (!$row) {
            throw new Exception('No se encontró el registro');
        }
        return $row->toArray();
    }

    public function gets($id) {
        $row = $this->fetchAll('id_solicitud = "' .$id.'"');
        
        return $row;
    }


 
    public function add($data = array()) {
        $rs = $this->insert($data);
        return $rs;
    }
    
    public function upd($id, $data = array()) {
        $rs = $this->update($data, 'id = ' . (int)$id);
        return $rs;
    }
    
    public function del($id) {
        $rs = $this->delete('id = ' . (int)$id);
        return $rs;
    }


    public function getHabitacionesTipo($id) {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_habitaciones'))
             ->group('e.tipo')
            ->where('e.id_hotel="'.$id.'"');
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchAll($select);
        
    }

     public function getHabitacionesTipoOcupacion($id, $ocupacion) {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_habitaciones'));

            if ($ocupacion=='Sencilla') {
                $select->where('e.id_hotel="'.$id.'" and e.ocupacion="Sencilla"');

               
            }

            if ($ocupacion=='Doble') {
                $select->where('e.id_hotel="'.$id.'" and e.ocupacion="Sencilla"');
               $select ->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Doble"');
            }

            if ($ocupacion=='Triple') {
                $select->where('e.id_hotel="'.$id.'" and e.ocupacion="Sencilla"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Doble"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Triple"');


            }

            if ($ocupacion=='Cuadruple') {
                 $select->where('e.id_hotel="'.$id.'" and e.ocupacion="Sencilla"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Doble"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Triple"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Cuadruple"');

            }

             if ($ocupacion=='Quintuple') {
                 $select->where('e.id_hotel="'.$id.'" and e.ocupacion="Sencilla"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Doble"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Triple"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Cuadruple"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Quintuple"');
            }

             if ($ocupacion=='Sextuple') {
                $select->where('e.id_hotel="'.$id.'" and e.ocupacion="Sencilla"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Doble"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Triple"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Cuadruple"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Sextuple"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Quintuple"');
            }

            if ($ocupacion=='Octuple') {
                $select->where('e.id_hotel="'.$id.'" and e.ocupacion="Sencilla"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Doble"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Triple"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Cuadruple"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Sextuple"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Quintuple"');
                $select->orwhere('e.id_hotel="'.$id.'" and e.ocupacion="Octuple"');
            }
        
       
                
        $select->setIntegrityCheck(false);
         echo $select;

        return $this->fetchAll($select);
        
    }

    public function getHabitacionesPlanes($id, $tipo) {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_habitaciones'))
             ->group('e.plan')
            ->where('e.id_hotel="'.$id.'"')
             

            ->where('e.tipo="'.$tipo.'"');
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchAll($select);
        
    }

    public function getHabitacionesOcupacion($id, $tipo) {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_habitaciones'))
             ->group('e.ocupacion')
            ->where('e.id_hotel="'.$id.'"')
             

            ->where('e.tipo="'.$tipo.'"');
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchAll($select);
        
    }


    
}