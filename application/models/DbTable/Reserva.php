<?php

class Application_Model_DbTable_Reserva extends Zend_Db_Table_Abstract {

    protected $_name = 'hk_reserva';
    

  
    public function get($id) {
      

         $select = $this->select()
             ->from(array('e'  => 'hk_reserva'))  
             ->join(array('s' => 'hk_hotel'),'e.hotek = s.id', array('nombre_hotel'=>'nombre'))
             ->where('e.id="'.$id.'"');

        $select->setIntegrityCheck(false);

        $row=$this->fetchRow($select);

        return $row->toArray();

    }

    public function todas() {
      

         $select = $this->select()
             ->from(array('e'  => 'hk_reserva'))  
             ->join(array('s' => 'hk_hotel'),'e.hotel = s.id', array('nombre_hotel'=>'nombre'));

        $select->setIntegrityCheck(false);

        $row=$this->fetchAll($select);

        return $row;

    }

    public function gets($id) {
        $row = $this->fetchAll('id_solicitud = "' .$id.'"');
        
        return $row;
    }


 
    public function add($data = array()) {
        $rs = $this->insert($data);
        return $rs;
    }
    
    public function upd($id, $data = array()) {
        $rs = $this->update($data, 'id = ' . (int)$id);
        return $rs;
    }
    
    public function del($id) {
        $rs = $this->delete('id = ' . (int)$id);
        return $rs;
    }

    
}