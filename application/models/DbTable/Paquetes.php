<?php

class Application_Model_DbTable_Paquetes extends Zend_Db_Table_Abstract {

    protected $_name = 'hk_paquetes';
    

  
    public function get($id) {
      

         $select = $this->select()
             ->from(array('e'  => 'hk_paquetes'))  
             ->join(array('s' => 'countries'),'e.pais = s.id', array('nombre_pais'=>'name'))
             ->join(array('c' => 'states'),'e.estado = c.id', array('nombre_estado'=>'name'))
             ->where('e.id="'.$id.'"');

        $select->setIntegrityCheck(false);

        $row=$this->fetchRow($select);

        return $row->toArray();

    }

    public function gets($id) {
        $row = $this->fetchAll('id_solicitud = "' .$id.'"');
        
        return $row;
    }


     public function getAll() {


       $select = $this->select()
             ->from(array('e'  => 'hk_paquetes'))  
             ->join(array('s' => 'countries'),'e.pais = s.id', array('nombre_pais'=>'name'))
             ->join(array('c' => 'states'),'e.estado = c.id', array('nombre_estado'=>'name'))
              ->order('e.id DESC');
        
       
                
        $select->setIntegrityCheck(false);

        return $this->fetchAll($select);
    }
 
    public function add($data = array()) {
        $rs = $this->insert($data);
        return $rs;
    }
    
    public function upd($id, $data = array()) {
        $rs = $this->update($data, 'id = ' . (int)$id);
        return $rs;
    }
    
    public function del($id) {
        $rs = $this->delete('id = ' . (int)$id);
        return $rs;
    }

        public function getPaquetes() {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_paquetes'),array('id','titulo', 'pais', 'estado', 'larga', 'nota'   ))  
             ->join(array('f' => 'hk_fotos'),'e.id = f.id_solicitud', array('foto'))
             ->group('e.id')
            ->where('f.posicion="1"')
            ->where('e.estatus="1"')
              ->order('e.fecha DESC');
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchAll($select);
        
    }

         public function getPaquetesEstado($estado) {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_paquetes'),array('id','nombre', 'pais', 'estado', 'categoria', 'direccion'   ))  
             ->join(array('f' => 'hk_fotos'),'e.id = f.id_solicitud', array('foto'))
             ->join(array('h' => 'hk_habitaciones'),'e.id = h.id_hotel', array('tarifa_bs', 'tarifa_usd'))
             ->group('e.id')
            ->where('f.posicion="1"')
            ->where('e.estatus="1"')
            ->where('e.estado="'.$estado.'"')
              ->order('e.nombre DESC');
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchAll($select);
        
    }

    
    
}