<?php

class Application_Model_DbTable_Reservadetalle extends Zend_Db_Table_Abstract {

    protected $_name = 'hk_reserva_detalle';
    

  
    public function get($id) {
      

         $select = $this->select()
             ->from(array('e'  => 'hk_reserva_detalle'))  
             ->join(array('s' => 'countries'),'e.pais = s.id', array('nombre_pais'=>'name'))
             ->join(array('c' => 'states'),'e.estado = c.id', array('nombre_estado'=>'name'))
             ->where('e.id="'.$id.'"');

        $select->setIntegrityCheck(false);

        $row=$this->fetchRow($select);

        return $row->toArray();

    }

    public function gets($id) {
        $row = $this->fetchAll('id_solicitud = "' .$id.'"');
        
        return $row;
    }


     public function getAll() {


       $select = $this->select()
             ->from(array('e'  => 'hk_reserva_detalle'))  
             ->join(array('s' => 'countries'),'e.pais = s.id', array('nombre_pais'=>'name'))
             ->join(array('c' => 'states'),'e.estado = c.id', array('nombre_estado'=>'name'))
              ->order('e.id DESC');
        
       
                
        $select->setIntegrityCheck(false);

        return $this->fetchAll($select);
    }
 
    public function add($data = array()) {
        $rs = $this->insert($data);
        return $rs;
    }
    
    public function upd($id, $data = array()) {
        $rs = $this->update($data, 'id = ' . (int)$id);
        return $rs;
    }
    
    public function del($id) {
        $rs = $this->delete('id = ' . (int)$id);
        return $rs;
    }

        

    
    
}