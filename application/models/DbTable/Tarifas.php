<?php

class Application_Model_DbTable_Tarifas extends Zend_Db_Table_Abstract {

    protected $_name = 'hk_tarifa';
    

  
    public function get($id) {
      

         $select = $this->select()
             ->from(array('e'  => 'hk_tarifa'))
             ->where('e.id="'.$id.'"');

        $select->setIntegrityCheck(false);

        $row=$this->fetchRow($select);

        return $row->toArray();

    }

    public function gets($id) {
        $row = $this->fetchAll('id_solicitud = "' .$id.'"');
        
        return $row;
    }


     public function getAll() {


       $select = $this->select()
             ->from(array('e'  => 'hk_tarifa'))
              ->order('e.id DESC');
        
       
                
        $select->setIntegrityCheck(false);

        return $this->fetchAll($select);
    }
 
    public function add($data = array()) {
        $rs = $this->insert($data);
        return $rs;
    }
    
    public function upd($id, $data = array()) {
        $rs = $this->update($data, 'id = ' . (int)$id);
        return $rs;
    }
    
    public function del($id) {
        $rs = $this->delete('id = ' . (int)$id);
        return $rs;
    }

        public function getHoteles() {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_tarifa'),array('id','nombre', 'pais', 'estado', 'categoria'   )) 
              ->order('e.id DESC');
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchAll($select);
        
    }

      public function getTarifaHotel($hotel) {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_tarifa'),array('bs', 'usd')) 
             ->where('id_hotel="'.$hotel.'"')
              ->order('e.bs asc');
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchRow($select);
        
    }


     public function getTarifaHabitacion($habitacion) {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_tarifa')) 
             ->where('id_habitacion="'.$habitacion.'"');
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchRow($select);
        
    }

         public function getHotelesEstado($estado) {
        
        $select = $this->select()
             ->from(array('e'  => 'hk_tarifa'),array('id','nombre', 'pais', 'estado', 'categoria', 'direccion'   ))
              ->order('e.id DESC');
        
       
                
        $select->setIntegrityCheck(false);
        // echo $select;

        return $this->fetchAll($select);
        
    }

    
    
}