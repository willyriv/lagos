<?php
class AjaxController extends Zend_Controller_Action
{

    public function init()
    {
        $this->_helper->layout('layout')->disableLayout();
        date_default_timezone_set('America/Caracas');
    }

    public function busquedaAction()
    {

         $term = $this->_getParam('term');


        $ObjEstado = new Application_Model_DbTable_Estado();
        $ObjPais = new Application_Model_DbTable_Pais();
        $ObjHoteles= new Application_Model_DbTable_Hoteles();
        
        //$ObjInmueble = new Application_Model_DbTable_Inmueble();

        $res = array();

        $estados=$ObjEstado->like($term);
        
        $paises=$ObjPais->like($term);
        $hoteles=$ObjHoteles->like($term);

        
         foreach ($hoteles as $hotel) {

          $valor = array('tipo' =>'f_hotel' ,'id' =>$hotel->id , 'descripcion' =>utf8_decode($hotel->nombre.' , Hotel'));

          $res[]=$valor;
          
        }
        
        foreach ($estados as $esta) {

          $valor = array('tipo' =>'f_estado' ,'id' =>$esta->id , 'descripcion' =>utf8_decode($esta->name.' , Estado'));

          $res[]=$valor;
          
        }

        foreach ($paises as $pais) {

          $valor = array('tipo' =>'f_pais' ,'id' =>$pais->id , 'descripcion' =>utf8_decode($pais->name.' , Pais'));

          $res[]=$valor;
          

        }


        $respuesta=json_encode($res);

        $this->view->respuesta=$respuesta;


    }



    public function cargarvideosAction(){

        $id = $this->_getParam('id');

        $titulo = $this->_getParam('titulo');

        $video = $this->_getParam('video');
        
        if ($id) {

        $ObjVideo = new Application_Model_DbTable_Video();

        $data = array(
          'id_destino' =>$id,
          'titulo' =>$titulo,
          'video' =>$video,
          );

        $ObjVideo->add($data);

        $this->view->videos=$ObjVideo->fetchAll('id_destino="'.$id.'"');

        }
       
    }

    public function agregartarifaAction(){

        $id_hotel = $this->_getParam('id_hotel');
        $desde = $this->_getParam('desde');
        $hasta = $this->_getParam('hasta');
        $lun = $this->_getParam('lun');
        $mar = $this->_getParam('mar');
        $mie = $this->_getParam('mie');
        $jue = $this->_getParam('jue');
        $vie = $this->_getParam('vie');
        $sab = $this->_getParam('sab');
        $dom = $this->_getParam('dom');

        $partes=explode('-', $desde);
        $desde=$partes[2].'-'.$partes[1].'-'.$partes[0];

        $partes=explode('-', $hasta);
        $hasta=$partes[2].'-'.$partes[1].'-'.$partes[0];

       
        
        if ($id_hotel) {

        $ObjHabitaciones = new Application_Model_DbTable_Habitaciones();
        $ObjTarifa = new Application_Model_DbTable_Tarifas();

        $habitaciones=$ObjHabitaciones->fetchAll('id_hotel="'.$id_hotel.'"');
        // se envia a la vista todos los registros de usuarios

        foreach ($habitaciones as $habitacion) {
          

          $data = array(
            'id_hotel' =>$id_hotel,  
            'id_habitacion' =>$habitacion->id,  
            'plan' =>$habitacion->plan,  
            'tipo' =>$habitacion->tipo,  
            'ocupacion' =>$habitacion->ocupacion,   
            'desde' =>$desde,  
            'hasta' =>$hasta,  
            'lun' =>$lun,  
            'mar' =>$mar,  
            'mie' =>$mie,  
            'jue' =>$jue,  
            'vie' =>$vie,  
            'sab' =>$sab,  
            'dom' =>$dom,  
            'bs' =>0,  
            'usd' =>0,  
            'nino' =>0,  
            'abuelo' =>0,  
            'extra' =>0,  
          );

          


          $ObjTarifa->add($data);


        }

        

        $this->view->tarifas=$ObjTarifa->fetchAll('id_hotel="'.$id_hotel.'"');

        }
       
    }


    public function calcularreservaAction(){

        $numero_adultos = $this->_getParam('numero_adultos');
        $numero_ninos = $this->_getParam('numero_ninos');
        $checkin = $this->_getParam('checkin');
        $checkout = $this->_getParam('checkout');
        $habitacion = $this->_getParam('habitacion');
        

        $this->view->numero_adultos=$numero_adultos;
        $this->view->numero_ninos=$numero_ninos;
        $this->view->checkin=$checkin;
        $this->view->checkout=$checkout;
        $this->view->habitacion=$habitacion;


        $ObjTarifa = new Application_Model_DbTable_Tarifas();

        $ObjHabitaciones = new Application_Model_DbTable_Habitaciones();

        $ObjHoteles = new Application_Model_DbTable_Hoteles();

        $habitacion=$ObjHabitaciones->fetchRow('id="'.$habitacion.'"');



         $this->view->habitacion=$habitacion;

        $this->view->tarifas=$ObjTarifa->getTarifaHabitacion($habitacion);

       
    }


     public function generarreservaAction(){

       
        $id_reserva = $this->_getParam('id_reserva');
        $id_hotel = $this->_getParam('id_hotel');
        $idhab = $this->_getParam('idhab');
        $habitaciones = $this->_getParam('habitaciones');
        $habitacion_tipo = $this->_getParam('habitacion_tipo');
        $habitacion_ocupacion = $this->_getParam('habitacion_ocupacion');
        $habitacion_plan = $this->_getParam('habitacion_plan');
        $checkin = $this->_getParam('checkin');
        $checkout = $this->_getParam('checkout');
        $dias = $this->_getParam('dias');
        $adultos = $this->_getParam('numero_adultos');
        $ninos = $this->_getParam('numero_ninos');
        $tarifa_adulto = $this->_getParam('tarifa_adulto');
        $tarifa_nino = $this->_getParam('tarifa_nino');
        $tarifa_abuelo = $this->_getParam('tarifa_abuelo');
        $subtotal = $this->_getParam('subtotal');


        


         $ObjReservadetalle= new Application_Model_DbTable_Reservadetalle();

         $data = array(
          'id_reserva' => $id_reserva, 
          'id_hotel' => $id_hotel, 
          'id_habitacion' => $idhab, 
          'habitaciones' => $habitaciones, 
          'habitacion_tipo' => $habitacion_tipo, 
          'habitacion_ocupacion' =>$habitacion_ocupacion, 
          'habitacion_plan' => $habitacion_plan, 
          'entrada' =>$checkin , 
          'salida' =>$checkout , 
          'dias' => $dias, 
          'adultos' => $adultos, 
          'ninos' =>$ninos , 
          'abuelo' => '0', 
          'tarifa_adulto' => $tarifa_adulto, 
          'tarifa_nino' => $tarifa_nino, 
          'tarifa_abuelo' => $tarifa_abuelo, 
          'total' => $subtotal, 
          );

        // var_dump($data);


         $ObjReservadetalle->add($data);

       
    }


    public function generarprereservaAction(){

       
        $nombre = $this->_getParam('nombre');
        $cedula = $this->_getParam('cedula');
        $telefono = $this->_getParam('tlf');
        $email = $this->_getParam('email');
        $password = $this->_getParam('password');
        $repassword = $this->_getParam('repassword');
        $id_hotel = $this->_getParam('id_hotel');
        $entrada = $this->_getParam('checkin');
        $salida = $this->_getParam('checkout');
        $estadia = $this->_getParam('dias');
        $id_reserva = $this->_getParam('id_reserva');
        $total = $this->_getParam('total');
       

         $ObjReserva= new Application_Model_DbTable_Reserva();

         $data = array(
          'id' => $id_reserva, 
          'asesor' => 'Miguel Machado',  
          'agencia' => 'Conkhep', 
          'sede' => 'Aragua', 
          'nombre' =>$nombre, 
          'cedula' =>$cedula, 
          'telefono' => $telefono, 
          'email' =>$email , 
          'hotel' =>$hotel , 
          'entrada' => $entrada, 
          'salida' =>$salida , 
          'estadia' => $estadia, 
          'subtotal' => ($total/1.12), 
          'iva' => ($total*0.12), 
          'total' => $total, 
          'notas' => '', 
          'servicios' => ''
          );

        // var_dump($data);


         $ObjReserva->add($data);

         $this->_redirect('/destinos/prereserva/codigo/'.$id_reserva);

       
    }


    public function generarreservaidAction(){

       
        $id_reserva = $this->_getParam('id_reserva');
        $id_hotel = $this->_getParam('id_hotel');
        $idhab = $this->_getParam('idhab');
        $habitacion_tipo = $this->_getParam('habitacion_tipo');
        $habitacion_ocupacion = $this->_getParam('habitacion_ocupacion');
        $habitacion_plan = $this->_getParam('habitacion_plan');
        $checkin = $this->_getParam('checkin');
        $checkout = $this->_getParam('checkout');
        $dias = $this->_getParam('dias');
        $adultos = $this->_getParam('numero_adultos');
        $ninos = $this->_getParam('numero_ninos');
        $tarifa_adulto = $this->_getParam('tarifa_adulto');
        $tarifa_nino = $this->_getParam('tarifa_nino');
        $tarifa_abuelo = $this->_getParam('tarifa_abuelo');
        $subtotal = $this->_getParam('subtotal');


        


         $ObjReservadetalle= new Application_Model_DbTable_Reservadetalle();

         $data = array(
          'id_reserva' => $id_reserva, 
          'id_hotel' => $id_hotel, 
          'id_habitacion' => $idhab, 
          'habitacion_tipo' => $habitacion_tipo, 
          'habitacion_ocupacion' =>$habitacion_ocupacion, 
          'habitacion_plan' => $habitacion_plan, 
          'entrada' =>$checkin , 
          'salida' =>$checkout , 
          'dias' => $dias, 
          'adultos' => $adultos, 
          'ninos' =>$ninos , 
          'abuelo' => '0', 
          'tarifa_adulto' => $tarifa_adulto, 
          'tarifa_nino' => $tarifa_nino, 
          'tarifa_abuelo' => $tarifa_abuelo, 
          'total' => $subtotal, 
          );

        // var_dump($data);


         $ObjReservadetalle->add($data);

       
    }


    public function actualizatarifaAction(){

        $id = $this->_getParam('id');
        $tipo = $this->_getParam('tipo');
        $tarifa = $this->_getParam('tarifa');

       
        
        if ($id) {

        $ObjTarifas = new Application_Model_DbTable_Tarifas();
        // se envia a la vista todos los registros de usuarios

        if ($tipo=='bs') {

          $data = array('bs' =>$tarifa );

        }

        if ($tipo=='usd') {

          $data = array('usd' =>$tarifa );

        }

        if ($tipo=='nino') {

          $data = array('nino' =>$tarifa );

        }

        if ($tipo=='abuelo') {

          $data = array('abuelo' =>$tarifa );

        }

        if ($tipo=='extra') {

          $data = array('extra' =>$tarifa );

        }


        $ObjTarifas->upd($id, $data);

        $this->view->tarifa=$ObjTarifas->fetchRow('id="'.$id.'"');


        }

       
    }


    public function principalAction(){

        $id = $this->_getParam('id');

        $id_solicitud = $this->_getParam('id_solicitud');
        
        if ($id) {

        $ObjFotos = new Application_Model_DbTable_Fotos();

        $data = array('posicion' =>'1', );

        $ObjFotos->updSolicitud($id_solicitud);

        $ObjFotos->upd($id, $data);

        $this->view->fotos=$ObjFotos->fetchAll('id_solicitud="'.$id_solicitud.'"');

        }
       
    }
    
    public function actualizarpdfAction(){

        $id = $this->_getParam('id');

        $descripcion = $this->_getParam('descripcion');

        $id_solicitud = $this->_getParam('id_solicitud');
        
        if ($id) {

        $ObjPdf = new Application_Model_DbTable_Pdf();

        $data = array('descripcion' =>$descripcion );

        $ObjPdf->upd($id, $data);

        $this->view->pdfs=$ObjPdf->fetchAll('id_solicitud="'.$id_solicitud.'"');

        }
       
    }

      public function agregarhabitacionAction()
    {

        $id = $this->_getParam('id');

        $tipohabitacion = $this->_getParam('tipohabitacion');

        $ocupacion = $this->_getParam('ocupacion');

        $plan = $this->_getParam('planes');

        $ObjHabitaciones = new Application_Model_DbTable_Habitaciones();

        if ($id && $plan) {

        $planes=explode(",", $plan);
          
        foreach ($planes as $key => $value) {

            $data = array(
              'id_hotel' =>$id, 
              'plan' =>$value, 
              'tipo' =>$tipohabitacion, 
              'ocupacion' =>$ocupacion, 
              'tarifa_bs' =>'0', 
              'tarifa_usd' =>'0', 
              );

               $res=$ObjHabitaciones->add($data);
          }

        }

         $this->view->habitaciones=$ObjHabitaciones->fetchAll('id_hotel="'.$id.'"');
    }


         public function cargarestadoAction()
    {

        $pais = $this->_getParam('pais');
        
        if ($pais) {

        $ObjEstado = new Application_Model_DbTable_Estado();

        $estados=$ObjEstado->fetchAll('country_id="'.$pais.'"');

        $this->view->estados=$estados;

        }

    }

public function uploadAction(){

      $ObjFotos = new Application_Model_DbTable_Fotos();


      $id = $this->_getParam('id');

        $auth = Zend_Auth::getInstance();

        $this->view->auth = $auth;

        $id_user=$auth->getIdentity()->uid;
    
        $output_dir = "assets/images/";

        $fileName='';

            if(isset($_FILES["myfile"]))
            {
                $ret = array();

                $error =$_FILES["myfile"]["error"];
                //You need to handle  both cases
                //If Any browser does not support serializing of multiple files using FormData() 
                if(!is_array($_FILES["myfile"]["name"])) //single file
                {
                    $fileName = $_FILES["myfile"]["name"];

                    $code = md5(uniqid(rand(), true));

                    $nombre=substr($code, 0, 8);

                    $file=$nombre.$fileName;

                    move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$file);

                    $ret[]= $file;
                }
                else  //Multiple files, file[]
                {
                  $fileCount = count($_FILES["myfile"]["name"]);

                  for($i=0; $i < $fileCount; $i++)

                  {

                    $code = md5(uniqid(rand(), true));

                    $nombre=substr($code, 0, 8);

                    $fileName = $_FILES["myfile"]["name"][$i];

                    $file=$nombre.$fileName;

                    move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$file);
                    
                    $ret[]= $file;
                  }
                
                }
                //echo json_encode($ret);
                $data = array('id_solicitud' =>$id , 'foto' =>$file  );

                $ObjFotos->add($data);

                $this->view->fotos=$ObjFotos->fetchAll('id_solicitud="'.$id.'"');

                //echo $file;
            }
            
    }


    public function uploadpdfAction(){

      $ObjPdf = new Application_Model_DbTable_Pdf();


      $id = $this->_getParam('id');

        $auth = Zend_Auth::getInstance();
        $this->view->auth = $auth;
        $id_user=$auth->getIdentity()->uid;
    
        $output_dir = "assets/pdf/";

        $fileName='';

            if(isset($_FILES["myfile"]))
            {
                $ret = array();

                $error =$_FILES["myfile"]["error"];
                //You need to handle  both cases
                //If Any browser does not support serializing of multiple files using FormData() 
                if(!is_array($_FILES["myfile"]["name"])) //single file
                {
                    $fileName = $_FILES["myfile"]["name"];



                    //$ext=explode(".", $fileName);

                    //$ext = array_shift($ext);

                    $code = md5(uniqid(rand(), true));

                    $nombre=substr($code, 0, 8);

                    //$nombreArchivo=$id_user.$nombre.'.'.$ext;

                    $file=$nombre.$fileName;
  

                    move_uploaded_file($_FILES["myfile"]["tmp_name"],$output_dir.$file);


                    $ret[]= $file;
                }
                else  //Multiple files, file[]
                {
                  $fileCount = count($_FILES["myfile"]["name"]);
                  for($i=0; $i < $fileCount; $i++)
                  {

                    $code = md5(uniqid(rand(), true));

                    $nombre=substr($code, 0, 8);


                    $fileName = $_FILES["myfile"]["name"][$i];

                    $file=$nombre.$fileName;


                    move_uploaded_file($_FILES["myfile"]["tmp_name"][$i],$output_dir.$file);

                    
                    $ret[]= $file;
                  }
                
                }
                //echo json_encode($ret);
                $data = array('id_solicitud' =>$id , 'pdf' =>$file, 'descripcion'=>$fileName);

                $ObjPdf->add($data);

                $this->view->pdfs=$ObjPdf->fetchAll('id_solicitud="'.$id.'"');


                //echo $file;
            }
            
    }


        public function eliminarAction()
    {

        $id = $this->_getParam('id');
        $modelo = $this->_getParam('modelo');

        
        if ($id) {

          switch ($modelo) {
            case 'auditoria':
                $Obj = new Application_Model_DbTable_Auditoria();
              # code...
              break;

              case 'aerolinea':
                $Obj = new Application_Model_DbTable_Aerolineas();
              # code...
              break;

          case 'inmueble':
                $Obj = new Application_Model_DbTable_Inmueble();
              # code...
              break;

          case 'mueble':
                $Obj = new Application_Model_DbTable_Mueble();
              # code...
              break;

          case 'validador':
                $Obj = new Application_Model_DbTable_Validador();
              # code...
              break;

          case 'presupuesto':
                $Obj = new Application_Model_DbTable_Presupuesto();
              # code...
              break;

          case 'hoteles':
                $Obj = new Application_Model_DbTable_Hoteles();
              # code...
              break; 

           case 'fotos':
                $Obj = new Application_Model_DbTable_Fotos();
              # code...
              break;  

           case 'habitaciones':
                $Obj = new Application_Model_DbTable_Habitaciones();
              # code...
              break;

          case 'video':
                $Obj = new Application_Model_DbTable_Video();
              # code...
              break;

          case 'agencias':
                $Obj = new Application_Model_DbTable_Agencias();
              # code...
              break;

               
          case 'pdf':
                $Obj = new Application_Model_DbTable_Pdf();
              # code...
              break;  

          case 'tarifas':
                $Obj = new Application_Model_DbTable_Tarifas();
              break;            
            default:
              # code...
              break;
          }
          
        // se envia a la vista todos los registros de usuarios
        
           $res=$Obj->del($id);

           echo $res;

        }

        

       
    }

         public function estatusAction()
    {

        $id = $this->_getParam('id');
        $modelo = $this->_getParam('modelo');
        $estatus = $this->_getParam('estatus');

        
        if ($id) {

          switch ($modelo) {
            case 'auditoria':
                $Obj = new Application_Model_DbTable_Auditoria();
              # code...
              break;

              case 'aerolinea':
                $Obj = new Application_Model_DbTable_Aerolineas();
              # code...
              break;

          case 'inmueble':
                $Obj = new Application_Model_DbTable_Inmueble();
              # code...
              break;

          case 'mueble':
                $Obj = new Application_Model_DbTable_Mueble();
              # code...
              break;

          case 'validador':
                $Obj = new Application_Model_DbTable_Validador();
              # code...
              break;

          case 'presupuesto':
                $Obj = new Application_Model_DbTable_Presupuesto();
              # code...
              break;

          case 'pagina':
                $Obj = new Application_Model_DbTable_Paginas();
              # code...
              break;

          case 'hoteles':
                $Obj = new Application_Model_DbTable_Hoteles();
              # code...
              break;

          case 'habitaciones':
                $Obj = new Application_Model_DbTable_Habitaciones();
              # code...
              break; 

          case 'agencias':
                $Obj = new Application_Model_DbTable_Agencias();
              # code...
              break;
                        
            default:
              # code...
              break;
          }

          $data = array('estatus' => $estatus );


       
        // se envia a la vista todos los registros de usuarios
        
           $res=$Obj->upd($id, $data);

           $this->view->modelo=$modelo;
           $this->view->id=$id;
           $this->view->estatus=$estatus;

           

        }

        

       
    }

     public function savecontactoAction()
    {

        $nombre = $this->_getParam('nombre');

        $email = $this->_getParam('email');
        $mensaje = $this->_getParam('mensaje');
        $tel = $this->_getParam('tel');
        
        if ($email) {

        $ObjContacto = new Application_Model_DbTable_Contacto();
        // se envia a la vista todos los registros de usuarios
        

        $data = array(
          'nombre' =>$nombre, 
          'email' =>$email, 
          'mensaje' =>$mensaje, 
          'telefono' =>$tel, 
          );

           $res=$ObjContacto->add($data);

        }

        // $this->view->etiquetas=$ObjEtiquetas->getEtiquetasIdNoticia($id);

       
    }



    public function eliminartagAction()
    {

        $id = $this->_getParam('id');

        $tag = $this->_getParam('tag');
        
        if ($id) {

        $ObjEtiquetas = new Application_Model_DbTable_Etiquetas();
        // se envia a la vista todos los registros de usuarios
        
           $res=$ObjEtiquetas->deleteEtiqueta($tag);

        }

         $this->view->etiquetas=$ObjEtiquetas->getEtiquetasIdNoticia($id);

       
    }


    public function agregartagsAction()
    {

        $id = $this->_getParam('id');
        $etiqueta = $this->_getParam('etiqueta');
            if ($id) {

        $ObjEtiquetas = new Application_Model_DbTable_Etiquetas();
        // se envia a la vista todos los registros de usuarios
       
        
        $tags=explode(',', $etiqueta);

        foreach ($tags as $tag) {
            
             $data = array(
            'id_noticia' => $id,
            'descripcion' => trim($tag),
            'estatus' => '1'
            );

           $res=$ObjEtiquetas->addEtiqueta($data);

        }


         $this->view->etiquetas=$ObjEtiquetas->getEtiquetasIdNoticia($id);

        }
    }


/*inicio de funciones noticias */

        public function desactivarnoticiaAction()
    {

        $id = $this->_getParam('id');
            if ($id) {

                 $ObjNoticias = new Application_Model_DbTable_Noticias();
             


              $data = array(
                'estatus' => '0'
                );

              $ObjNoticias->updateNoticia($id, $data);

              $this->view->noticia=$ObjNoticias->getNoticiaUn($id);

        }
    }

      public function activarnoticiaAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                 $ObjNoticias = new Application_Model_DbTable_Noticias();
                
             


              $data = array(
                'estatus' => '1'
                );

              $ObjNoticias->updateNoticia($id, $data);

              $this->view->noticia=$ObjNoticias->getNoticiaUn($id);

        }
      
    }

     public function deletenoticiaAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                $ObjNoticias = new Application_Model_DbTable_Noticias();
                

              $res=$ObjNoticias->del($id);

              echo $res;

        }
      
    }

/*fin de funciones noticias */






    public function desactivarsliderAction()
    {

        $id = $this->_getParam('id');
            if ($id) {

                 $ObjSliders = new Application_Model_DbTable_Sliders();
             


              $data = array(
                'estatus' => '0'
                );

              $ObjSliders->updateSliders($id, $data);

              $this->view->slider=$ObjSliders->getSliders($id);

        }
    }

      public function activarsliderAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                 $ObjSliders = new Application_Model_DbTable_Sliders();
             


              $data = array(
                'estatus' => '1'
                );

              $ObjSliders->updateSliders($id, $data);

              $this->view->slider=$ObjSliders->getSliders($id);

        }
      
    }

     public function deletesliderAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                  $ObjSliders = new Application_Model_DbTable_Sliders();
                
             
                

              $res=$ObjSliders->deleteSliders($id);

              echo $res;

        }
      
    }

     public function deletesvalidadorAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                  $ObjValidador = new Application_Model_DbTable_Validador();
                
              $res=$ObjValidador->del($id);

              echo $res;
        }
      

    }

     public function deletesauditoriaAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                  $Obj = new Application_Model_DbTable_Auditoria();
                

              $res=$Obj->del($id);

              echo $res;

        }
      
    }



    

         public function desactivarpaginaAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                $ObjPaginas = new Application_Model_DbTable_Paginas();
             


              $data = array(
                'estatus' => '0'
                );

              $ObjPaginas->updatePagina($id, $data);

              $this->view->pagina=$ObjPaginas->getPagina($id);

        }
      
    }

      public function activarpaginaAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                $ObjPaginas = new Application_Model_DbTable_Paginas();
             


              $data = array(
                'estatus' => '1'
                );

              $ObjPaginas->updatePagina($id, $data);

              $this->view->pagina=$ObjPaginas->getPagina($id);

        }
      
    }

     public function deletepaginaAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                 $ObjPaginas = new Application_Model_DbTable_Paginas();
                
             
                

              $res=$ObjPaginas->deletePagina($id);

              echo $res;

        }
      
    }



        public function desactivarportafolioAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                $ObjPortafolios = new Application_Model_DbTable_Portafolios();
             


              $data = array(
                'estatus' => '0'
                );

              $ObjPortafolios->updatePortafolios($id, $data);

              $this->view->portafolio=$ObjPortafolios->getPortafolios($id);

        }
      
    }

      public function activarportafolioAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                 $ObjPortafolios = new Application_Model_DbTable_Portafolios();
             
              $data = array(
                'estatus' => '1'
                );

              $ObjPortafolios->updatePortafolios($id, $data);

              $this->view->portafolio=$ObjPortafolios->getPortafolios($id);

        }
      
    }

     public function deleteportafolioAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                 $ObjPortafolios = new Application_Model_DbTable_Portafolios();
                
             
                

              $res=$ObjPortafolios->deletePortafolios($id);

              echo $res;

        }
      
    }

    public function desactivarmoduloAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                $ObjModulos = new Application_Model_DbTable_Modulos();
             


              $data = array(
                'estatus' => '0'
                );

              $ObjModulos->updateModulos($id, $data);

              $this->view->modulo=$ObjModulos->getModulos($id);

        }
      
    }

      public function activarmoduloAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                $ObjModulos = new Application_Model_DbTable_Modulos();
             
              $data = array(
                'estatus' => '1'
                );

              $ObjModulos->updateModulos($id, $data);

              $this->view->modulo=$ObjModulos->getModulos($id);

        }
      
    }

     public function deletemoduloAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

                $ObjModulos = new Application_Model_DbTable_Modulos();
             
                

              $res=$ObjModulos->deleteModulos($id);

              echo $res;

        }
      
    }


public function desactivarserviciosAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

             $objServicios = new Application_Model_DbTable_Servicios();


              $data = array(
                'estatus' => '0'
                );

              $objServicios->upd($id, $data);

              $this->view->servicio=$objServicios->get($id);

        }
      
    }

      public function activarserviciosAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

             $objServicios = new Application_Model_DbTable_Servicios();

              $data = array(
                'estatus' => '1'
                );

              $objServicios->upd($id, $data);

              $this->view->servicio=$objServicios->get($id);

        }
      
    }

     public function deleteserviciosAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

             $objServicios = new Application_Model_DbTable_Servicios();
                

              $res=$objServicios->del($id);

              echo $res;

        }
      
    }


    public function desactivarmarcaAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

             $objMarcas = new Application_Model_DbTable_Marcas();

              $data = array(
                'estatus' => '0'
                );

              $objMarcas->upd($id, $data);

              $this->view->marca=$objMarcas->get($id);

        }
      
    }

      public function activarmarcaAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

             $objMarcas = new Application_Model_DbTable_Marcas();

              $data = array(
                'estatus' => '1'
                );

              $objMarcas->upd($id, $data);

              $this->view->marca=$objMarcas->get($id);

        }
      
    }

     public function deletemarcaAction()
    {

        $id = $this->_getParam('id');

        if ($id) {

             $objMarcas = new Application_Model_DbTable_Marcas();

              $res=$objMarcas->del($id);

              echo $res;

        }
      
    }




    
     public function pagarcajaAction()
    {

        $id = $this->_getParam('id');

        $monto = $this->_getParam('monto');

         $tipo_pago = $this->_getParam('tipo_pago');




        if ($id) {


              $objPago = new Application_Model_DbTable_Pago();

              $data = array(
                'id_pedido' => $id, 
                'tipo_pago' => $tipo_pago,
                'monto' => $monto

                );

              $objPago->add($data);

            
            $this->view->pagado=$objPago->getPagado($id);


             $objProductos = new Application_Model_DbTable_Productos();

             $this->view->productos=$objProductos->fetchAll();

             $objDetalles = new Application_Model_DbTable_Detalle();
          

             $detalles=$objDetalles->getDetallePedido($id);

             $this->view->detalles=$detalles;


              $objPedidos = new Application_Model_DbTable_Pedidos();

              $total=$objPedidos->updatePedidoTotal($id);

           

             $this->view->pedido=$objPedidos->fetchRow('id="'.$id.'"');

        }
      
    }

    public function eliminardetalleAction()
    {

        $detalle = $this->_getParam('id');

        $pedido = $this->_getParam('pedido');


        if ($detalle) {



             $objPedidos = new Application_Model_DbTable_Pedidos();

             $this->view->pedido=$objPedidos->fetchRow('id="'.$pedido.'"');


             $objProductos = new Application_Model_DbTable_Productos();

             $this->view->productos=$objProductos->fetchAll();

             $objDetalles = new Application_Model_DbTable_Detalle();
          

             $objDetalles->deleteDetalle($detalle);

             $detalles=$objDetalles->getDetallePedido($pedido);

             $this->view->detalles=$detalles;


              $objPedidos = new Application_Model_DbTable_Pedidos();

              $total=$objPedidos->updatePedidoTotal($pedido);

              


             $this->view->pedido=$objPedidos->fetchRow('id="'.$pedido.'"');

        }
      
    }




    public function agregardetalleAction()
    {

        $pedido = $this->_getParam('pedido');
        $producto = $this->_getParam('producto');
        $cantidad = $this->_getParam('cantidad');
        $precio = $this->_getParam('precio');


        if ($pedido) {



             $objPedidos = new Application_Model_DbTable_Pedidos();

             $this->view->pedido=$objPedidos->fetchRow('id="'.$pedido.'"');


             $objProductos = new Application_Model_DbTable_Productos();

             $this->view->productos=$objProductos->fetchAll();

             $objDetalles = new Application_Model_DbTable_Detalle();

             $total=$cantidad*$precio;

             $data = array(
                'id_pedido' =>$pedido , 
                'id_producto' =>$producto , 
                'cantidad' =>$cantidad , 
                'precio' => $precio, 
                'total' => $total, 

                );

             $objDetalles->addDetalle($data);

             $detalles=$objDetalles->getDetallePedido($pedido);

             $this->view->detalles=$detalles;




              $objPedidos = new Application_Model_DbTable_Pedidos();

              $total=$objPedidos->updatePedidoTotal($pedido);

              $objPago = new Application_Model_DbTable_Pago();

            $this->view->pagado=$objPago->getPagado($pedido);


             $this->view->pedido=$objPedidos->fetchRow('id="'.$pedido.'"');

        }
      
    }

    public function detallecajaAction()
    {

        $id = $this->_getParam('id');

        if ($id) {



             $objPedidos = new Application_Model_DbTable_Pedidos();

             $this->view->pedido=$objPedidos->fetchRow('id="'.$id.'"');


             $objProductos = new Application_Model_DbTable_Productos();

             $this->view->productos=$objProductos->fetchAll();

             $objDetalles = new Application_Model_DbTable_Detalle();

             $detalles=$objDetalles->getDetallePedido($id);

             $this->view->detalles=$detalles;

              $objPago = new Application_Model_DbTable_Pago();

            $this->view->pagado=$objPago->getPagado($id);

        }
      
    }


    public function crearcajaAction()
    {
        $nombre = $this->_getParam('nombre');

        $vendedor = $this->_getParam('vendedor');

        $cliente = $this->_getParam('cliente');
        
        
         $auth = Zend_Auth::getInstance();
        
        $user_id = $auth->getIdentity()->uid;

        if ($vendedor) {
           
            
            $objPedidos = new Application_Model_DbTable_Pedidos();

            $data = array(
                'id_user' =>$user_id,
                'id_cliente' =>$cliente,
                'id_empleado' =>$vendedor,
                'total' =>'0',
                'descripcion'=>$nombre
                );
           
            $addPedido=$objPedidos->addPedido($data);
            

            //$estatus=$objEstatus->fetchAll();

            $html='';
            
            $pedido=$objPedidos->fetchAll('status="1"');

                $this->view->pedido=$pedido;               
           

        }
    }


    private function generaCaja(){

         $objPedidos = new Application_Model_DbTable_Pedidos();

         $pedido=$objPedidos->fetchAll('status="1"');

         $this->view->pedido=$pedido;

 
    }


     public function cajaAction(){

         $objPedidos = new Application_Model_DbTable_Pedidos();

         $pedido=$objPedidos->fetchAll('status="1"');

         $this->view->pedido=$pedido;

    }

         public function cerrarcajaAction(){

        $id = $this->_getParam('id');

         
         $objPedidos = new Application_Model_DbTable_Pedidos();


         $data = array('status' => '2' );

         $objPedidos->updatePedido($id, $data);

         $pedido=$objPedidos->fetchAll('status="1"');

         $this->view->pedido=$pedido;

    }


      public function hakayAction()
    {

        

          $ObjResources = new Application_Model_DbTable_Resources();
                
             
              $data = array(
                'resource' => 'index'
                );
             
             $ObjResources->add($data);

       
    }


}