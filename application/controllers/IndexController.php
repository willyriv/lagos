<?php

class IndexController extends Zend_Controller_Action {
    
    
    public function init() {
         $opt=array('layout'=>'layout');

         Zend_Layout::startMvc($opt);
    }
    
    public function indexAction() {

    	$ObjSliders = new Application_Model_DbTable_Sliders();
        // se envia a la vista todos los registros de usuarios
        $this->view->sliders = $ObjSliders->fetchAll('estatus=1');


        $ObjPaginas = new Application_Model_DbTable_Paginas();
      

        $this->view->destinos = $ObjPaginas->getDestinos();

        $ObjMarcas= new Application_Model_DbTable_Marcas();

        $this->view->marcas = $ObjMarcas->fetchAll('estatus=1');

        $ObjServicios= new Application_Model_DbTable_Servicios();

        $this->view->avaluos = $ObjServicios->fetchAll('estatus=1');

        $ObjModulos= new Application_Model_DbTable_Modulos();

      
        $this->view->modulos = $ObjModulos->fetchAll('estatus=1');

        $ObjNoticias = new Application_Model_DbTable_Noticias();
        // se envia a la vista todos los registros de usuarios
        $this->view->noticias = $ObjNoticias->Aleatorio();
        
    }


}
