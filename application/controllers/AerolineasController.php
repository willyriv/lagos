<?php

class AerolineasController extends Zend_Controller_Action {
    
    protected $_flashMessenger = null;
    
    public function init() {
        
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        
    }

    public function indexAction(){
        
         $ObjAerolineas = new Application_Model_DbTable_Aerolineas();
        // se envia a la vista todos los registros de usuarios
        $this->view->aerolineas = $ObjAerolineas->fetchAll();

         $this->view->messages = $this->_flashMessenger->getMessages();
        
        $page = $this->_getParam('page', 1);
            
        $paginator = Zend_Paginator::factory($ObjAerolineas->fetchAll());
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);

        $this->view->paginator = $paginator;
        
    }

  
     public function verAction(){

        $id = $this->_getParam('id', 1);
        
        // se instancia el modelo users
        $ObjModulos= new Application_Model_DbTable_Modulos();

        $ObjEtiquetas = new Application_Model_DbTable_Etiquetas();
        // se envia a la vista todos los registros de usuarios
        $this->view->noticias = $ObjModulos->fetchAll($id);

       

        //var_dump($ObjNoticias->getNoticiaId($id))  ;      
       
        $this->view->etiquetas = $ObjEtiquetas->getEtiquetaID($id);

        $tags=$ObjEtiquetas->getEtiquetaID($id);

        $data = array();

        foreach ($tags as $key ) {
            $data[]=$key['descripcion'];
        }

        $related=$ObjNoticias->getNoticiasTags($data);

        $this->view->related=$related;
        
        
        // se envia a la vista los mensajes de acciones
        $this->view->messages = $this->_flashMessenger->getMessages();
        
       
        
    }

     private function getFileExtension($filename)
        {
            $fext_tmp = explode('.',$filename);
            return $fext_tmp[(count($fext_tmp) - 1)];
        }

     public function addAction(){


        $auth = Zend_Auth::getInstance();
        $this->view->auth = $auth;

           $ObjEtiquetas = new Application_Model_DbTable_Etiquetas();
     
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
            

      



            $data = array(
            'id' => $formData['id'],
            'aerolinea' => $formData['aerolinea']
            );
                
        $ObjAerolineas= new Application_Model_DbTable_Aerolineas();
                $ObjAerolineas->add($data);

                $this->_flashMessenger->addMessage(array('success' => 'Se ha registrado con éxito!'));
                
                $this->_redirect('/aerolineas/');
                

            
        }
        
    }

    public function editAction() {
        
        $id = $this->_getParam('id', 0);
        
        

        if ($this->getRequest()->isPost()){
            
            $formData = $this->getRequest()->getPost();
            
             $data = array(
            'aerolinea' => $formData['aerolinea']
            );

            $ObjAerolineas= new Application_Model_DbTable_Aerolineas();
            
            $ObjAerolineas->upd($formData['id'], $data);

                $this->_redirect('/aerolineas/');
            
            
            
            
        } else {
            
            if ($id > 0) {
                
                $ObjAerolineas = new Application_Model_DbTable_Aerolineas();
        
                $this->view->aerolinea=$ObjAerolineas->get($id);



            } else {
                throw new Exception('No se encontró el registro');
            }
        }
    }




}









