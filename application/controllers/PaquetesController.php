<?php

class PaquetesController extends Zend_Controller_Action {
    
    protected $_flashMessenger = null;
    
    public function init() {
        
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

    }

    public function indexAction(){

        $opt=array('layout'=>'layout');

         Zend_Layout::startMvc($opt);
       
        $ObjSliders = new Application_Model_DbTable_Sliders();
        // se envia a la vista todos los registros de usuarios
        $this->view->sliders = $ObjSliders->fetchAll('estatus=1');

        $ObjPaquetes = new Application_Model_DbTable_Paquetes();
      
        $this->view->paquetes = $ObjPaquetes->getPaquetes();

        $ObjPaginas = new Application_Model_DbTable_Paginas();
      
        $this->view->destinos = $ObjPaginas->getDestinos();

        $ObjMarcas= new Application_Model_DbTable_Marcas();

        $this->view->marcas = $ObjMarcas->fetchAll('estatus=1');

        $ObjServicios= new Application_Model_DbTable_Servicios();

        $this->view->avaluos = $ObjServicios->fetchAll('estatus=1');

        $ObjModulos= new Application_Model_DbTable_Modulos();
     
        $this->view->modulos = $ObjModulos->fetchAll('estatus=1');

        $ObjNoticias = new Application_Model_DbTable_Noticias();
        // se envia a la vista todos los registros de usuarios
        $this->view->noticias = $ObjNoticias->Aleatorio();
        
    }

    public function listAction(){
        
         $ObjPaquetes = new Application_Model_DbTable_Paquetes();
        // se envia a la vista todos los registros de usuarios
        $this->view->paquetes = $ObjPaquetes->getAll();

         $this->view->messages = $this->_flashMessenger->getMessages();
        
        $page = $this->_getParam('page', 1);
            
        $paginator = Zend_Paginator::factory($ObjPaquetes->fetchAll());
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);

        $this->view->paginator = $paginator;
        
    }

     public function tarifaAction(){

        $id = $this->_getParam('id', 0);

        $ObjPais = new Application_Model_DbTable_Pais();
        // se envia a la vista todos los registros de usuarios
        $this->view->paises = $ObjPais->fetchAll();

          $ObjAerolineas = new Application_Model_DbTable_Aerolineas();
        // se envia a la vista todos los registros de usuarios
        $this->view->aerolineas = $ObjAerolineas->fetchAll();

        $ObjFotos = new Application_Model_DbTable_Fotos();
        // se envia a la vista todos los registros de usuarios
        $this->view->fotos = $ObjFotos->fetchAll('id_solicitud="'.$id.'"');

        $ObjHabitaciones = new Application_Model_DbTable_Habitaciones();
        // se envia a la vista todos los registros de usuarios
        $this->view->habitaciones = $ObjHabitaciones->fetchAll('id_hotel="'.$id.'"');

        $auth = Zend_Auth::getInstance();
        $this->view->auth = $auth;

        $ObjHoteles = new Application_Model_DbTable_Hoteles();
        
        $hotel=$ObjHoteles->get($id);
        

        $this->view->hotel=$hotel;

        $ObjEstado = new Application_Model_DbTable_Estado();
                // se envia a la vista todos los registros de usuarios
        $this->view->estados = $ObjEstado->fetchAll('country_id="'.$hotel['pais'].'"');
        
              
    }

  
     public function verAction(){

         $opt=array('layout'=>'layout');

         Zend_Layout::startMvc($opt);

        $id = $this->_getParam('id', 1);
        
        // se instancia el modelo users
        $ObjPaquetes= new Application_Model_DbTable_Paquetes();
        

        $ObjHabitaciones= new Application_Model_DbTable_Habitaciones();

        $paquete=$ObjPaquetes->fetchRow('id="'.$id.'"');

        $this->view->paquete=$paquete;

        $ObjFotos = new Application_Model_DbTable_Fotos();
        // se envia a la vista todos los registros de usuarios
        $this->view->fotos = $ObjFotos->fetchAll('id_solicitud="'.$id.'"');


        #$this->view->hoteles=$ObjHoteles->getHotelesEstado($hotel['estado']);
        #$this->view->habitaciones=$ObjHabitaciones->getHabitacionesTipo($id);


        $ObjPaginas = new Application_Model_DbTable_Paginas();
        $this->view->destinos = $ObjPaginas->getDestinos();

        
        // se envia a la vista los mensajes de acciones
        $this->view->messages = $this->_flashMessenger->getMessages();



        $ObjHoteles = new Application_Model_DbTable_Hoteles();

        $hoteles=$ObjHoteles->getHotelesEstado($paquete['estado']);

        $this->view->hoteles=$hoteles;
        
       
        
    }

     private function getFileExtension($filename)
        {
            $fext_tmp = explode('.',$filename);
            return $fext_tmp[(count($fext_tmp) - 1)];
        }

     public function addAction(){

          $ObjPais = new Application_Model_DbTable_Pais();
        // se envia a la vista todos los registros de usuarios
        $this->view->paises = $ObjPais->fetchAll();

          $ObjAerolineas = new Application_Model_DbTable_Aerolineas();
        // se envia a la vista todos los registros de usuarios
        $this->view->aerolineas = $ObjAerolineas->fetchAll();


        $auth = Zend_Auth::getInstance();
        $this->view->auth = $auth;

        $ObjPaquetes = new Application_Model_DbTable_Paquetes();     
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
            

            $data = array(
            'id' => $formData['id'],
            'titulo' => $formData['titulo'],
            'pais' => $formData['pais'],
            'estado' => $formData['estado'],
            'larga' => $formData['larga'],
            'nota' => $formData['nota']
            );
                
        $ObjPaquetes= new Application_Model_DbTable_Paquetes();
                $ObjPaquetes->add($data);

                $this->_flashMessenger->addMessage(array('success' => 'Se ha registrado con éxito!'));
                
                $this->_redirect('/paquetes/list');
               
            
        }
        
    }

    public function editAction() {
        
        $id = $this->_getParam('id', 0);

        $ObjPais = new Application_Model_DbTable_Pais();
        // se envia a la vista todos los registros de usuarios
        $this->view->paises = $ObjPais->fetchAll();


        $ObjFotos = new Application_Model_DbTable_Fotos();
        // se envia a la vista todos los registros de usuarios
        $this->view->fotos = $ObjFotos->fetchAll('id_solicitud="'.$id.'"');

        $auth = Zend_Auth::getInstance();
        $this->view->auth = $auth;

        $ObjPaquetes = new Application_Model_DbTable_Paquetes();
        
        

        if ($this->getRequest()->isPost()){
            
            $formData = $this->getRequest()->getPost();
            
             $data = array(
            'titulo' => $formData['titulo'],
            'pais' => $formData['pais'],
            'estado' => $formData['estado'],
            'larga' => $formData['larga'],
            'nota' => $formData['nota']
            );

            
            
            $ObjPaquetes->upd($formData['id'], $data);

            $this->_redirect('/paquetes/list');
            
            
            
            
        } else {
            
          
                
           $ObjPaquetes = new Application_Model_DbTable_Paquetes();
                
        

                $paquete=$ObjPaquetes->get($id);

                $this->view->paquete=$paquete;



            $ObjEstado = new Application_Model_DbTable_Estado();
                // se envia a la vista todos los registros de usuarios
             $this->view->estados = $ObjEstado->fetchAll('country_id="'.$paquete['pais'].'"');


        }
    }




}









