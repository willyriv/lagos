<?php

class PaginaController extends Zend_Controller_Action {
    
    protected $_flashMessenger = null;
    
    public function init() {
        
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');
        
    }

    public function indexAction(){
        
        // se instancia el modelo users
        $ObjPaginas = new Application_Model_DbTable_Paginas();
        // se envia a la vista todos los registros de usuarios
        $this->view->paginas = $ObjPaginas->fetchAll();

        
        // se envia a la vista los mensajes de acciones
        $this->view->messages = $this->_flashMessenger->getMessages();
        
        $page = $this->_getParam('page', 1);
            
        $paginator = Zend_Paginator::factory($ObjPaginas->fetchAll());
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);

        $this->view->paginator = $paginator;
        
    }

  
     public function verAction(){

        $id = $this->_getParam('id', 1);
        
        // se instancia el modelo users
        $ObjNoticias = new Application_Model_DbTable_Noticias();

        $ObjEtiquetas = new Application_Model_DbTable_Etiquetas();
        // se envia a la vista todos los registros de usuarios
        $this->view->noticias = $ObjNoticias->getNoticiaId($id);

       

        //var_dump($ObjNoticias->getNoticiaId($id))  ;      
       
        $this->view->etiquetas = $ObjEtiquetas->getEtiquetaID($id);

        $tags=$ObjEtiquetas->getEtiquetaID($id);

        $data = array();

        foreach ($tags as $key ) {
            $data[]=$key['descripcion'];
        }

        $related=$ObjNoticias->getNoticiasTags($data);

        $this->view->related=$related;
        
        
        // se envia a la vista los mensajes de acciones
        $this->view->messages = $this->_flashMessenger->getMessages();
        
       
        
    }

     private function getFileExtension($filename)
        {
            $fext_tmp = explode('.',$filename);
            return $fext_tmp[(count($fext_tmp) - 1)];
        }

     public function addAction(){


        $auth = Zend_Auth::getInstance();
        $this->view->auth = $auth;

         $ObjPais = new Application_Model_DbTable_Pais();
        // se envia a la vista todos los registros de usuarios
        $this->view->paises = $ObjPais->fetchAll();


           $ObjIdioma = new Application_Model_DbTable_Idiomas();

        $this->view->idiomas=$ObjIdioma->fetchAll();

          
     
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();

            $data = array(
            'id' => $formData['id'],
            'idioma' => $formData['idioma'],
            'nombre' => $formData['nombre'],
            'pais' => $formData['pais'],
            'estado' => $formData['estado'],
            'corta' => $formData['corta'],
            'corta_ingles' => $formData['corta_ingles'],
            'larga' => $formData['larga'],
            'larga_ingles' => $formData['larga_ingles'],
            'gmaps' => $formData['gmaps']
            );
                
                $ObjPaginas = new Application_Model_DbTable_Paginas();
                $ObjPaginas->addPagina($data);

                $this->_flashMessenger->addMessage(array('success' => 'Se ha registrado con éxito!'));
                
                $this->_redirect('/pagina/add');
            
        }
        
    }

    public function editAction() {

        $id = $this->_getParam('id', 0);

          $ObjPais = new Application_Model_DbTable_Pais();
        // se envia a la vista todos los registros de usuarios
        $this->view->paises = $ObjPais->fetchAll();


         $ObjIdioma = new Application_Model_DbTable_Idiomas();

        $this->view->idiomas=$ObjIdioma->fetchAll();

        $ObjFotos = new Application_Model_DbTable_Fotos();
        // se envia a la vista todos los registros de usuarios
        $this->view->fotos = $ObjFotos->fetchAll('id_solicitud="'.$id.'"');

        $ObjPdf = new Application_Model_DbTable_Pdf();
        // se envia a la vista todos los registros de usuarios
        $this->view->pdfs = $ObjPdf->fetchAll('id_solicitud="'.$id.'"');
        
        


        if ($this->getRequest()->isPost()){
            
            $formData = $this->getRequest()->getPost();
        

            $data = array(
            'idioma' => $formData['idioma'],
            'nombre' => $formData['nombre'],
            'pais' => $formData['pais'],
            'estado' => $formData['estado'],
            'corta' => $formData['corta'],
            'corta_ingles' => $formData['corta_ingles'],
            'larga' => $formData['larga'],
            'larga_ingles' => $formData['larga_ingles'],
            'gmaps' => $formData['gmaps']
            );

       


             $id=$formData['id'];



           
                $ObjPaginas = new Application_Model_DbTable_Paginas();
                $ObjPaginas->updatePagina($id, $data);

                $this->_flashMessenger->addMessage(array('success' => 'Se ha Actualizado con éxito!'));
                
                $this->_redirect('/pagina/');
            
            // se agrega validator para campo username
            
            
            
        } else {
            
            if ($id) {
                
                $ObjPaginas = new Application_Model_DbTable_Paginas();
        
                $this->view->pagina=$ObjPaginas->getPagina($id);

                $pagina=$ObjPaginas->getPagina($id);

                $pais=$pagina['pais'];


        $ObjFotos = new Application_Model_DbTable_Fotos();
        // se envia a la vista todos los registros de usuarios
        $this->view->fotos = $ObjFotos->fetchAll('id_solicitud="'.$id.'"');

        $ObjVideos = new Application_Model_DbTable_Video();
        // se envia a la vista todos los registros de usuarios
        $this->view->videos = $ObjVideos->fetchAll('id_destino="'.$id.'"');

        $ObjPdf = new Application_Model_DbTable_Pdf();
        // se envia a la vista todos los registros de usuarios
        $this->view->pdfs = $ObjPdf->fetchAll('id_solicitud="'.$id.'"');

         $ObjEstados = new Application_Model_DbTable_Estado();
        // se envia a la vista todos los registros de usuarios
        $this->view->estados = $ObjEstados->fetchAll('country_id="'.$pais.'"');




            } else {
                throw new Exception('No se encontró el registro');
            }
        }
    }

    



     public function getallAction(){

         $this->_helper->layout('layout')->disableLayout();
        
        // se instancia el modelo users
        $ObjPaginas = new Application_Model_DbTable_Paginas();
        // se envia a la vista todos los registros de usuarios
        $Paginas=$ObjPaginas->fetchAll();

        $json = array();

    

        foreach ($Paginas as $row) {
           
           
           $json[]=$row->toArray();
           
        }
        
        echo json_encode($json);  
    }


    public function deleteAction(){

          $id = $this->_getParam('id', 0);

         $this->_helper->layout('layout')->disableLayout();
        
        // se instancia el modelo users
        $ObjPaginas = new Application_Model_DbTable_Paginas();
        // se envia a la vista todos los registros de usuarios
        $Paginas=$ObjPaginas->deleteNoticia($id);

        $json = array();

        if ($Paginas) {
           $json[]=array('res' => true );
        }else{
             $json[]=array('res' => false);
        }
        
      
        
        echo json_encode($json);  
    }


      public function getnoticiaidAction(){

        $id = $this->_getParam('id', 0);

         $this->_helper->layout('layout')->disableLayout();
        
        // se instancia el modelo users
        $ObjNoticias = new Application_Model_DbTable_Noticias();
        // se envia a la vista todos los registros de usuarios
        $noticias=$ObjNoticias->getNoticiaId($id);

        $json = array();

        $json[]=$noticias->toArray();

    
        
        echo json_encode($json);  
    }


         public function gettagsAction(){

        $idNoticia = $this->_getParam('code');

         $this->_helper->layout('layout')->disableLayout();
        
        // se instancia el modelo users
        $ObjEtiquetas= new Application_Model_DbTable_Etiquetas();
        // se envia a la vista todos los registros de usuarios
        $tags=$ObjEtiquetas->getEtiquetasIdNoticia($idNoticia);

        $json = array();

    

        foreach ($tags as $row) {
           
           $fila = array(
            'id' => $row->id,
            'descripcion' => $row->descripcion
            );

           $json[]=$fila;
           
        }
        
        echo json_encode($json);  
    }

     public function setnoticiaAction(){

         $this->_helper->layout('layout')->disableLayout();

         $auth = Zend_Auth::getInstance();

         var_dump($auth);



         $titulo = $this->_getParam('titulo');

         $noticia = $this->_getParam('noticia');
        
        // se instancia el modelo users
        $ObjNoticias = new Application_Model_DbTable_Noticias();
        // se envia a la vista todos los registros de usuarios
       
           
           $data = array(
            'id_autor' => $auth->getIdentity()->uid,
            'titulo' => $titulo,
            'noticia' => $noticia,
            'date_add' => date('Y-m-d'),
            'date_upd' => date('Y-m-d'),
            'estatus' => '1'
            );

           $res=$ObjNoticias->addNoticia($data);

           if ($res) {
               echo TRUE;
           }else{
            echo false;
           }

        
          
    }


    public function settagsAction(){

         $this->_helper->layout('layout')->disableLayout();

         $auth = Zend_Auth::getInstance();

         //var_dump($auth);



         $idNoticia = $this->_getParam('idNoticia');

         $tags = $this->_getParam('tags');
        
        // se instancia el modelo users
        $ObjEtiquetas = new Application_Model_DbTable_Etiquetas();
        // se envia a la vista todos los registros de usuarios
       
        
        $tags=explode(',', $tags);

        foreach ($tags as $tag) {
            
             $data = array(
            'id_noticia' => $idNoticia,
            'descripcion' => trim($tag),
            'estatus' => '1'
            );

           $res=$ObjEtiquetas->addEtiqueta($data);

        }


           if ($res) {
               echo TRUE;
           }else{
            echo false;
           }

        
          
    }




}







