<?php

class AgenciasController extends Zend_Controller_Action {
    
    protected $_flashMessenger = null;
    
    public function init() {
        
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        
    }

    public function indexAction(){  

        $ObjAgencias = new Application_Model_DbTable_Agencias();

        $this->view->agencias = $ObjAgencias->getAgencias();
        
    }

    public function listAction(){
        
         $ObjAgencias = new Application_Model_DbTable_Agencias();
        // se envia a la vista todos los registros de usuarios
        $this->view->agencias = $ObjAgencias->getAll();

         $this->view->messages = $this->_flashMessenger->getMessages();
        
        $page = $this->_getParam('page', 1);
            
        $paginator = Zend_Paginator::factory($ObjAgencias->fetchAll());
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);

        $this->view->paginator = $paginator;
        
    }

     public function verAction(){

         $opt=array('layout'=>'layout');

         Zend_Layout::startMvc($opt);

        $id = $this->_getParam('id', 1);
        
        // se instancia el modelo users
        $ObjHoteles= new Application_Model_DbTable_Hoteles();
        

        $ObjHabitaciones= new Application_Model_DbTable_Habitaciones();

        $hotel=$ObjHoteles->fetchRow('id="'.$id.'"');

        $this->view->hotel=$hotel;

        $ObjFotos = new Application_Model_DbTable_Fotos();
        // se envia a la vista todos los registros de usuarios
        $this->view->fotos = $ObjFotos->fetchAll('id_solicitud="'.$id.'"');


        $this->view->hoteles=$ObjHoteles->getHotelesEstado($hotel['estado']);
        $this->view->habitaciones=$ObjHabitaciones->getHabitacionesTipo($id);


        $ObjPaginas = new Application_Model_DbTable_Paginas();
        $this->view->destinos = $ObjPaginas->getDestinos();

 
        // se envia a la vista los mensajes de acciones
        $this->view->messages = $this->_flashMessenger->getMessages();
       
    }

     private function getFileExtension($filename)
        {
            $fext_tmp = explode('.',$filename);
            return $fext_tmp[(count($fext_tmp) - 1)];
        }

     public function addAction(){

          $ObjPais = new Application_Model_DbTable_Pais();
        // se envia a la vista todos los registros de usuarios
        $this->view->paises = $ObjPais->fetchAll();

          $ObjAerolineas = new Application_Model_DbTable_Aerolineas();
        // se envia a la vista todos los registros de usuarios
        $this->view->aerolineas = $ObjAerolineas->fetchAll();

        $auth = Zend_Auth::getInstance();

        $this->view->auth = $auth;


        $ObjAgencias = new Application_Model_DbTable_Agencias();
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();


               $dest_dir = "assets/img/";
            
            /* Uploading Document File on Server */
            $upload = new Zend_File_Transfer_Adapter_Http();
            $upload->setDestination($dest_dir)
                         ->addValidator('Count', false, 1)
                         ->addValidator('Size', false, 1048576)
                         ->addValidator('Extension', false, 'jpg,png,gif');
            $files = $upload->getFileInfo();
 
            echo '<hr />
                            <pre>';
            print_r($files);
            echo '  </pre>
                        <hr />';
           
               if($upload->receive()) {
           
            
            $mime_type = $upload->getMimeType('doc_path');
            $fname = $upload->getFileName('doc_path');
            $size = $upload->getFileSize('doc_path');
            $file_ext = $this->getFileExtension($fname);            
            $new_file = $dest_dir.md5(mktime()).'.'.$file_ext;
            
            $filterFileRename = new Zend_Filter_File_Rename(
                array(
                    'target' => $new_file, 'overwrite' => true
            ));
            
            $filterFileRename->filter($fname);

            if (file_exists($new_file))
            {
                $request = $this->getRequest();
                $caption = $request->getParam('caption');
                
                $html = 'Orig Filename: '.$fname.'<br />';
                $html .= 'New Filename: '.$new_file.'<br />';
                $html .= 'File Size: '.$size.'<br />';
                $html .= 'Mime Type: '.$mime_type.'<br />';
                $html .= 'Caption: '.$caption.'<br />';
            }
            else
            {
                $html = 'Unable to upload the file!';
            }

        }else{
            $new_file='assets/img/default.jpg';
        }


            

            $data = array(
            'id' => $formData['id'],
            'nombre' => $formData['nombre'],
            'pais' => $formData['pais'],
            'estado' => $formData['estado'],
            'direccion' => $formData['direccion'],
            'rif' => $formData['rif'],
            'tlf' => $formData['tlf'],
            'movil' => $formData['movil'],
            'email' => $formData['email'],
            'logo' => $new_file,
            );


                
        $ObjAgencias= new Application_Model_DbTable_Agencias();
                $ObjAgencias->add($data);

                $this->_flashMessenger->addMessage(array('success' => 'Se ha registrado con éxito!'));
                
                $this->_redirect('/agencias/');
                

            
        }
        
    }

    public function editAction() {
        
        $id = $this->_getParam('id', 0);

           $ObjPais = new Application_Model_DbTable_Pais();
        // se envia a la vista todos los registros de usuarios
        $this->view->paises = $ObjPais->fetchAll();

     

        

         $auth = Zend_Auth::getInstance();
        $this->view->auth = $auth;

           $ObjAgencias = new Application_Model_DbTable_Agencias();
        
        

        if ($this->getRequest()->isPost()){
            
            $formData = $this->getRequest()->getPost();


             $dest_dir = "assets/img/";
            
            /* Uploading Document File on Server */
            $upload = new Zend_File_Transfer_Adapter_Http();
            $upload->setDestination($dest_dir)
                         ->addValidator('Count', false, 1)
                         ->addValidator('Size', false, 1048576)
                         ->addValidator('Extension', false, 'jpg,png,gif');
            $files = $upload->getFileInfo();
 
            echo '<hr />
                            <pre>';
            print_r($files);
            echo '  </pre>
                        <hr />';
           
               if($upload->receive()) {
           
            
            $mime_type = $upload->getMimeType('doc_path');
            $fname = $upload->getFileName('doc_path');
            $size = $upload->getFileSize('doc_path');
            $file_ext = $this->getFileExtension($fname);            
            $new_file = $dest_dir.md5(mktime()).'.'.$file_ext;
            
            $filterFileRename = new Zend_Filter_File_Rename(
                array(
                    'target' => $new_file, 'overwrite' => true
            ));
            
            $filterFileRename->filter($fname);

            if (file_exists($new_file))
            {
                $request = $this->getRequest();
                $caption = $request->getParam('caption');
                
                $html = 'Orig Filename: '.$fname.'<br />';
                $html .= 'New Filename: '.$new_file.'<br />';
                $html .= 'File Size: '.$size.'<br />';
                $html .= 'Mime Type: '.$mime_type.'<br />';
                $html .= 'Caption: '.$caption.'<br />';
            }
            else
            {
                $html = 'Unable to upload the file!';
            }

             $data = array(
            'id' => $formData['id'],
            'nombre' => $formData['nombre'],
            'pais' => $formData['pais'],
            'estado' => $formData['estado'],
            'direccion' => $formData['direccion'],
            'rif' => $formData['rif'],
            'tlf' => $formData['tlf'],
            'movil' => $formData['movil'],
            'email' => $formData['email'],
            'logo' => $new_file,
            );



        }else{
            

             $data = array(
            'id' => $formData['id'],
            'nombre' => $formData['nombre'],
            'pais' => $formData['pais'],
            'estado' => $formData['estado'],
            'direccion' => $formData['direccion'],
            'rif' => $formData['rif'],
            'tlf' => $formData['tlf'],
            'movil' => $formData['movil'],
            'email' => $formData['email']
            );
            



        }


            
             
            
            $ObjAgencias->upd($formData['id'], $data);

            $this->_redirect('/agencias/');
            
            
            
            
        } else {
            
          
                
           $ObjAgencias = new Application_Model_DbTable_Agencias();
                
        

                $agencia=$ObjAgencias->get($id);

                $this->view->agencia=$agencia;



            $ObjEstado = new Application_Model_DbTable_Estado();
                // se envia a la vista todos los registros de usuarios
             $this->view->estados = $ObjEstado->fetchAll('country_id="'.$agencia['pais'].'"');



           
        }
    }




}









