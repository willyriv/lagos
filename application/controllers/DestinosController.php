<?php

class DestinosController extends Zend_Controller_Action {
    
    
    public function init() {
         $opt=array('layout'=>'layout');

         Zend_Layout::startMvc($opt);

         date_default_timezone_set('America/Caracas');
    }
    
    public function indexAction() {

    	$ObjSliders = new Application_Model_DbTable_Sliders();
        // se envia a la vista todos los registros de usuarios
        $this->view->sliders = $ObjSliders->fetchAll('estatus=1');


        $ObjPaginas = new Application_Model_DbTable_Paginas();
        // se envia a la vista todos los registros de usuarios

        $this->view->destinos = $ObjPaginas->getDestinos();
        $this->view->nacionales = $ObjPaginas->getDestinosNacionales(6);
        $this->view->todos_nacionales = $ObjPaginas->getDestinosNacionales(1000);
        $this->view->internacionales = $ObjPaginas->getDestinosInternacionales(6);
        $this->view->todos_internacionales = $ObjPaginas->getDestinosInternacionales(1000);



        $ObjMarcas= new Application_Model_DbTable_Marcas();

        $this->view->marcas = $ObjMarcas->fetchAll('estatus=1');

        $ObjServicios= new Application_Model_DbTable_Servicios();

        $this->view->avaluos = $ObjServicios->fetchAll('estatus=1');

        $ObjModulos= new Application_Model_DbTable_Modulos();

      
        $this->view->modulos = $ObjModulos->fetchAll('estatus=1');

        
        $ObjNoticias = new Application_Model_DbTable_Noticias();
        // se envia a la vista todos los registros de usuarios
        $this->view->noticias = $ObjNoticias->Aleatorio();
        
    }

        public function verAction(){

        $buscar = $this->_getParam('buscar', 0);
        

        $ObjPaginas = new Application_Model_DbTable_Paginas();


        $ObjViews = new Application_Model_DbTable_Views();

        $view = array('id_articulo' =>$buscar );

        $ObjViews->add($view);

        $ObjHoteles = new Application_Model_DbTable_Hoteles();
        // se envia a la vista todos los registros de usuarios

        $ObjFotos = new Application_Model_DbTable_Fotos();
        // se envia a la vista todos los registros de usuarios
        $this->view->fotos = $ObjFotos->fetchAll('id_solicitud="'.$buscar.'"');

         $ObjPdf = new Application_Model_DbTable_Pdf();
        // se envia a la vista todos los registros de usuarios
        $this->view->archivos = $ObjPdf->fetchAll('id_solicitud="'.$buscar.'"');

         $ObjVideos = new Application_Model_DbTable_Video();
        // se envia a la vista todos los registros de usuarios
        $this->view->videos = $ObjVideos->fetchAll('id_destino="'.$buscar.'"');

        $this->view->destinos = $ObjPaginas->getDestinos();


        $destino=$ObjPaginas->getPagina($buscar);

        $this->view->destino = $destino;

        //echo $destino['estado'];

        $hoteles=$ObjHoteles->getHotelesEstadoCantidad('8',$destino['estado']);

        //var_dump($hoteles);

       $this->view->hoteles=$hoteles;
        
    }


    public function consultarAction(){

        $ubicacion = $this->_getParam('destino', 0);
        $checkin = $this->_getParam('checkin', 0);
        $checkout = $this->_getParam('checkout', 0);
        $adultos = $this->_getParam('adultos', 0);
        $ninos = $this->_getParam('ninos', 0);

        $this->view->data_ubicacion=$ubicacion;
        $this->view->data_checkin=$checkin;
        $this->view->data_checkout=$checkout;
        $this->view->data_adultos=$adultos;
        $this->view->data_ninos=$ninos;

        
        $cin=explode('-', $checkin); $cin=$cin[2].'-'.$cin['1'].'-'.$cin[0];
        $cout=explode('-', $checkout); $cout=$cout[2].'-'.$cout['1'].'-'.$cout[0];


        $dias   = (strtotime($cin)-strtotime($cout))/86400;
        $dias   = abs($dias); $dias = floor($dias);

        echo $dias;
        

        $ObjPaginas = new Application_Model_DbTable_Paginas();


        $ObjViews = new Application_Model_DbTable_Views();

        $view = array('id_articulo' =>$ubicacion );

        $ObjViews->add($view);

        $ObjHoteles = new Application_Model_DbTable_Hoteles();
        // se envia a la vista todos los registros de usuarios

        $this->view->destinos = $ObjPaginas->getDestinos();

        $destino=$ObjPaginas->getPagina($ubicacion);

        $this->view->destino = $destino;

        $hoteles=$ObjHoteles->getHotelesEstado($destino['estado']);

        //var_dump($hoteles);

       $this->view->hoteles=$hoteles;

    }

    public function hotelAction(){

        $ubicacion = $this->_getParam('destino', 0);
        $checkin = $this->_getParam('checkin', 0);
        $checkout = $this->_getParam('checkout', 0);
        $adultos = $this->_getParam('adultos', 0);
        $ninos = $this->_getParam('ninos', 0);
        $id_hotel = $this->_getParam('hotel', 0);

        $ocupacion = array('1' => 'Sencilla','2'=>'Doble', '3'=>'Triple', '4'=>'Cuadruple', '5'=>'Quintuple', '6'=>'Sextuple', '8'=>'Octuple' );

        $personas=$adultos+$ninos;
        $requiere=$ocupacion[$personas];

        //echo $requiere;

        $datetime1 = date_create($checkin);
$datetime2 = date_create($checkout);
$interval = date_diff($datetime1, $datetime2);
echo $interval->format('%R%a días');


        $this->view->data_ubicacion=$ubicacion;
        $this->view->data_checkin=$checkin;
        $this->view->data_checkout=$checkout;
        $this->view->data_adultos=$adultos;
        $this->view->data_ninos=$ninos;
        $this->view->data_hotel=$id_hotel;
        

        $ObjPaginas = new Application_Model_DbTable_Paginas();


        $ObjViews = new Application_Model_DbTable_Views();

        $view = array('id_articulo' =>$id_hotel);

        $ObjViews->add($view);

        $ObjHoteles= new Application_Model_DbTable_Hoteles();
        
        $hotel=$ObjHoteles->fetchRow('id="'.$id_hotel.'"');

        $this->view->hotel=$hotel;

         $ObjFotos = new Application_Model_DbTable_Fotos();
        // se envia a la vista todos los registros de usuarios
        $this->view->fotos = $ObjFotos->fetchAll('id_solicitud="'.$id_hotel.'"');

        $ObjHabitaciones= new Application_Model_DbTable_Habitaciones();
       

        $this->view->habitaciones=$ObjHabitaciones->getHabitacionesTipoOcupacion($id_hotel, $requiere);


        $destino=$ObjPaginas->getPagina($ubicacion);

        $this->view->destino = $destino;

    }


    public function reservaAction(){

        
        $codigo = $this->_getParam('codigo', 0);

         $ObjAgencias= new Application_Model_DbTable_Agencias();

          $this->view->agencias=$ObjAgencias->fetchAll();



        if ($this->getRequest()->isPost()){

            
            $formData = $this->getRequest()->getPost();


            $id_reserva = $formData['id_reserva'];
            $nombre = $formData['nombre'];
            $agencia = $formData['agencia'];
            $asesor = $formData['asesor'];
            $sede = $formData['sede'];
            $cedula = $formData['cedula'];
            $telefono = $formData['tlf'];
            $email = $formData['email_reserva'];
            $password = $formData['password_reserva'];


         $ObjReservadetalle= new Application_Model_DbTable_Reservadetalle();


        $detalle=$ObjReservadetalle->fetchRow("id_reserva='".$codigo."'");


         


          $ObjReserva= new Application_Model_DbTable_Reserva();

         $data = array(
          'id' => $id_reserva, 
          'asesor' => $asesor,  
          'agencia' => $agencia, 
          'sede' => $sede, 
          'nombre' =>$nombre, 
          'cedula' =>$cedula, 
          'telefono' => $telefono, 
          'email' =>$email , 
          'hotel' =>$detalle['id_hotel'] , 
          'entrada' => $detalle['entrada'] , 
          'salida' =>$detalle['salida']  , 
          'estadia' => $detalle['dias'] , 
          'subtotal' => '0', 
          'iva' => '0',
          'total' => '0', 
          'notas' => '', 
          'servicios' => ''
          );

        //var_dump($data);

         try {
             $ObjReserva->add($data);
         } catch (Exception $e) {
             echo $e;
         }
         

         $this->_redirect('/destinos/prereserva/codigo/'.$id_reserva);
       
            
        } else {
            
          
                
        $ocupacion = array('1' => 'Sencilla','2'=>'Doble', '3'=>'Triple', '4'=>'Cuadruple', '5'=>'Quintuple', '6'=>'Sextuple', '8'=>'Octuple' );


        $ObjReservadetalle = new Application_Model_DbTable_Reservadetalle();

        $ObjHoteles = new Application_Model_DbTable_Hoteles();

        $detalles=$ObjReservadetalle->fetchAll("id_reserva='".$codigo."'");

        //var_dump($detalles);

        $detalle=$ObjReservadetalle->fetchRow("id_reserva='".$codigo."'");

        $hotel=$ObjHoteles->fetchRow("id='".$detalle['id_hotel']."'");

        $this->view->detalles=$detalles;

        $this->view->detalle_reserva=$detalle;

        $this->view->hotel=$hotel;
        
        }

    }


    public function prereservaAction(){

        
        $codigo = $this->_getParam('codigo', 0);
       

        $ocupacion = array('1' => 'Sencilla','2'=>'Doble', '3'=>'Triple', '4'=>'Cuadruple', '5'=>'Quintuple', '6'=>'Sextuple', '8'=>'Octuple' );


        $ObjReservadetalle = new Application_Model_DbTable_Reservadetalle();
        $ObjReserva = new Application_Model_DbTable_Reserva();

        $ObjHoteles = new Application_Model_DbTable_Hoteles();

        $detalles=$ObjReservadetalle->fetchAll("id_reserva='".$codigo."'");

        //var_dump($detalles);

        $detalle=$ObjReserva->fetchRow("id='".$codigo."'");

        $hotel=$ObjHoteles->fetchRow("id='".$detalle['hotel']."'");

        $this->view->detalles=$detalles;

        $this->view->detalle_reserva=$detalle;

        $this->view->hotel=$hotel;


    }




    public function listAction(){

        $opt=array('layout'=>'gentelella');

         Zend_Layout::startMvc($opt);

     

        $ObjContacto= new Application_Model_DbTable_Contacto();

      
        $this->view->contactos = $ObjContacto->fetchAll('estatus=1');
        
    }


        public function imprimirAction() {

            $codigo = $this->_getParam('reserva', 0);

        $this->_helper->layout->disableLayout();

         $ObjReserva = new Application_Model_DbTable_Reserva();
        $detalle=$ObjReserva->fetchRow("id='".$codigo."'");

        $ObjReservadetalle = new Application_Model_DbTable_Reservadetalle();
$detalles=$ObjReservadetalle->fetchAll("id_reserva='".$codigo."'");

$ObjHoteles = new Application_Model_DbTable_Hoteles();
$hotel=$ObjHoteles->fetchRow("id='".$detalle['hotel']."'");
        
             
       // $objPedidos = new Application_Model_DbTable_Pedidos();
       // $pedidos = $objPedidos->getPedidos();
        //$this->view->pedidos = $pedidos;


         require('assets/fpdf/fpdf.php');

        $pdf = new FPDF();
        $pdf->AddPage();
            //Encabezado 

        if ($detalle['agencia']=='Conkhep') {
            # code...
        

        $pdf->SetFont('Arial','B',12);
        $pdf->Image('assets/diseno/images/logo_rif.jpg',10,10,70,25);
        $pdf->Cell(110,12,'', '0');
        $pdf->SetFont('Arial','B',20);
        $pdf->Cell(80,12,'COTIZACION', '0',1,'C',false);
        
        $pdf->SetFont('Arial','',10);

        $pdf->Cell(110,5,'', '0');
        $pdf->Cell(80, 5, 'No valido como reserva', '0',1,'R',false);
        
        $pdf->Cell(110,5,'', '0', 0, 'C', false);
        $pdf->Cell(80, 5, 'Elaborado: '.$detalle['fecha'], '0',1,'R',false);
        
        $pdf->Cell(110,5,'', '0');
        $pdf->Cell(80, 5, 'CONSORCIO KHEP C.A.', '0',1,'R',false);

        $pdf->Cell(110,5,'', '0');
        $pdf->Cell(80, 5, 'Rif.  J-31307146-3', '0',1,'R',false);

        $pdf->Cell(110,5,'', '0');
        $pdf->Cell(80, 5, 'Direccion: Sabana Grande, Caracas DC 1050, Venezuela', '0',1,'R',false);

    }else{


            $ObjAgencia = new Application_Model_DbTable_Agencias();
        $agencia=$ObjAgencia->fetchRow("id='".$detalle['agencia']."'");


            $pdf->SetFont('Arial','B',12);
        $pdf->Image($agencia['logo'],10,10,70,25);
        $pdf->Cell(110,12,'', '0');
        $pdf->SetFont('Arial','B',20);
        $pdf->Cell(80,12,'COTIZACION', '0',1,'C',false);
        
        $pdf->SetFont('Arial','',10);

        $pdf->Cell(110,5,'', '0');
        $pdf->Cell(80, 5, 'No valido como reserva', '0',1,'R',false);
        
        $pdf->Cell(110,5,'', '0', 0, 'C', false);
        $pdf->Cell(80, 5, 'Elaborado: '.$detalle['fecha'], '0',1,'R',false);
        
        $pdf->Cell(110,5,'', '0');
        $pdf->Cell(80, 5, $agencia['nombre'], '0',1,'R',false);

        $pdf->Cell(110,5,'', '0');
        $pdf->Cell(80, 5, 'Rif: '.$agencia['rif'], '0',1,'R',false);

        $pdf->Cell(110,5,'', '0');
        $pdf->SetFont('Arial','',8);

        $pdf->Cell(80, 5, $agencia['direccion'], '0',1,'R',false);
        $pdf->SetFont('Arial','',10);
        



    }

        $pdf->Cell(20,2,'', '0', 1, 'L', false);

        $pdf->SetFont('Arial','',12);







        //$pdf->Cell(95, 7,'Asesor: '.$detalle['asesor'], '0');
        $pdf->Cell(95, 7,'Asesor: '.$detalle['asesor'], '0');
        $pdf->Cell(95, 7, 'Sucursal: '.$detalle['sede'], '0',1,'',false);

        $pdf->Cell(95, 7,'Cliente: '.$detalle['nombre'], '0');
        $pdf->Cell(95, 7, 'Destino: ', '0',1,'',false);

        $pdf->Cell(95, 7,'Telefono: '.$detalle['telefono'], '0');
        $pdf->Cell(95, 7, 'Hotel : '.$hotel['nombre'], '0',1,'',false);

        $pdf->Cell(95, 7,'Correo: '.$detalle['email'], '0');
        $pdf->Cell(95, 7, 'Estadia : '.$detalle['estadia'].' Noches', '0',1,'',false);

        $pdf->Cell(95, 7,'Entrada: '.str_replace('-', '/', $detalle['entrada']), '0');
        $pdf->Cell(95, 7, 'Salida : '.str_replace('-', '/', $detalle['salida']), '0',1,'',false);


        
       


        $pdf->Cell(20,2,'', '0', 1, 'L', false);
        $pdf->SetFont('Arial','B',12);
       
        $pdf->Cell(10,5,'#', '1', 0, 'C', false);
        $pdf->Cell(65,5,'Tipo de Habitacion', '1', 0, 'C', false);
        $pdf->Cell(45,5,'Numero de personas', '1', 0, 'C', false);
        $pdf->Cell(45,5,'Precio por persona', '1', 0, 'C', false);
       $pdf->Cell(20,5,'Subtotal', '1', 0, 'C', false);

        $pdf->Cell(20,5,'', '0', 1, 'L', false);
        $i=0;
        $total=0;

        $pdf->SetFont('Arial','',10);

        foreach ($detalles as $det ) {
            $i++;
            $total=$total+$det->total;
           
            $pdf->Cell(10,6, $i, '0', 0, 'L', false);
            $pdf->Cell(65,6,utf8_encode($det->habitacion_tipo), '0', 0, 'L', false);

            if ($det->ninos=='0') {
               $personas='Adultos: '.$det->adultos;
               $precio=number_format($det->tarifa_adulto, 2, ',','.');
            }else{
                $personas='Adultos: '.$det->adultos.' Niños: '.$det->ninos;
                $precio=number_format($det->tarifa_adulto, 2, ',','.').' + '.number_format($det->tarifa_nino, 2, ',','.');
            }
         
            $pdf->Cell(45,6,utf8_decode($personas), '0', 0, 'L', false);
            $pdf->Cell(45,6,$precio, '0', 0, 'L', false);
            $pdf->Cell(20,6,number_format($det->total, 2, ',','.'), '0', 1, 'L', false);

            

        }

        for ($i; $i <=10 ; $i++) { 
            $pdf->Cell(10,5, '', '0', 0, 'L', false);
            $pdf->Cell(65,5,'', '0', 0, 'L', false);
            $pdf->Cell(45,5,'', '0', 0, 'L', false);
            $pdf->Cell(45,5,'', '0', 0, 'L', false);
            $pdf->Cell(20,5,'', '0', 1, 'L', false);
        }

        $pdf->SetFont('Arial','B',18);


        $pdf->Cell(95,8,'', '0');
        $pdf->Cell(45, 8, 'Total Porcion Terrestes: ', '0',0,'R',false);
        $pdf->Cell(40, 8, number_format($total, 2, ',','.'), '0',1,'R',false);

        if($total<=2000000){

            $iva=9;
            $cuota=0.09;


        }else{
            $iva=7;
            $cuota=0.07;



        }

        $pdf->Cell(95, 8,'', '0');
        $pdf->Cell(45, 8, 'IVA '.$iva.'%:', '0',0,'R',false);
        $pdf->Cell(40, 8, number_format($total*$cuota, 2, ',','.'), '0',1,'R',false);

        $pdf->Cell(95, 8,'', '0');
        $pdf->Cell(45, 8, 'IVA sobre Service Fee:', '0',0,'R',false);
        $pdf->Cell(40, 8, ' 0', '0',1,'R',false);

        $pdf->Cell(95, 8,'', '0');
        $pdf->Cell(45, 8, 'Impuestos Aereos', '0',0,'R',false);
        $pdf->Cell(40, 8, ' 0', '0',1,'R',false);

        $pdf->Cell(95, 8,'', '0');
        $pdf->Cell(45, 8, 'Total General: ', '0',0,'R',false);
        $pdf->Cell(40, 8, number_format($total*($cuota+1), 2, ',','.'), '0',1,'R',false);











        //cuadro clientes 
         $pdf->Output();

     
    }


}
