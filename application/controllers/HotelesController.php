<?php

class HotelesController extends Zend_Controller_Action {
    
    protected $_flashMessenger = null;
    
    public function init() {
        
        $this->_flashMessenger = $this->_helper->getHelper('FlashMessenger');

        
        
    }

    public function indexAction(){

        $opt=array('layout'=>'layout');

         Zend_Layout::startMvc($opt);


        
        $ObjSliders = new Application_Model_DbTable_Sliders();
        // se envia a la vista todos los registros de usuarios
        $this->view->sliders = $ObjSliders->fetchAll('estatus=1');


        $ObjHoteles = new Application_Model_DbTable_Hoteles();
      

        $this->view->hoteles = $ObjHoteles->getHotelesAleatorio();

        $ObjPaginas = new Application_Model_DbTable_Paginas();
        // se envia a la vista todos los registros de usuarios

        $this->view->destinos = $ObjPaginas->getDestinos();


        $ObjNoticias = new Application_Model_DbTable_Noticias();
        // se envia a la vista todos los registros de usuarios
        $this->view->noticias = $ObjNoticias->Aleatorio();
        
    }

    public function listAction(){
        
         $ObjHoteles = new Application_Model_DbTable_Hoteles();
        // se envia a la vista todos los registros de usuarios
        $this->view->hoteles = $ObjHoteles->getAll();

         $this->view->messages = $this->_flashMessenger->getMessages();
        
        $page = $this->_getParam('page', 1);
            
        $paginator = Zend_Paginator::factory($ObjHoteles->fetchAll());
        $paginator->setItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);

        $this->view->paginator = $paginator;
        
    }

     public function tarifaAction(){

        $id = $this->_getParam('id', 0);

        $ObjPais = new Application_Model_DbTable_Pais();
        // se envia a la vista todos los registros de usuarios
        $this->view->paises = $ObjPais->fetchAll();

       

        

        $ObjTarifas = new Application_Model_DbTable_Tarifas();
        // se envia a la vista todos los registros de usuarios
        $this->view->tarifas = $ObjTarifas->fetchAll('id_hotel="'.$id.'"');

        $auth = Zend_Auth::getInstance();
        $this->view->auth = $auth;

        $ObjHoteles = new Application_Model_DbTable_Hoteles();
        

        $hotel=$ObjHoteles->get($id);
        

        $this->view->hotel=$hotel;

        $ObjEstado = new Application_Model_DbTable_Estado();
                // se envia a la vista todos los registros de usuarios
        $this->view->estados = $ObjEstado->fetchAll('country_id="'.$hotel['pais'].'"');
        
       
        
    }

  
     public function verAction(){

         $opt=array('layout'=>'layout');

         Zend_Layout::startMvc($opt);

        $id = $this->_getParam('id', 1);


        $ObjViews = new Application_Model_DbTable_Views();

        $view = array('id_articulo' =>$id );

        $ObjViews->add($view);
        
        // se instancia el modelo users
        $ObjHoteles= new Application_Model_DbTable_Hoteles();
        

        $ObjHabitaciones= new Application_Model_DbTable_Habitaciones();

        $hotel=$ObjHoteles->fetchRow('id="'.$id.'"');

        $this->view->hotel=$hotel;

        $ObjFotos = new Application_Model_DbTable_Fotos();
        // se envia a la vista todos los registros de usuarios
        $this->view->fotos = $ObjFotos->fetchAll('id_solicitud="'.$id.'"');


        $this->view->hoteles=$ObjHoteles->getHotelesEstadoCantidad('8', $hotel['estado']);
        $this->view->habitaciones=$ObjHabitaciones->getHabitacionesTipo($id);


        $ObjPaginas = new Application_Model_DbTable_Paginas();
        $this->view->destinos = $ObjPaginas->getDestinos();


     
        
        
        // se envia a la vista los mensajes de acciones
        $this->view->messages = $this->_flashMessenger->getMessages();
        
       
        
    }

     private function getFileExtension($filename)
        {
            $fext_tmp = explode('.',$filename);
            return $fext_tmp[(count($fext_tmp) - 1)];
        }

     public function addAction(){

          $ObjPais = new Application_Model_DbTable_Pais();
        // se envia a la vista todos los registros de usuarios
        $this->view->paises = $ObjPais->fetchAll();

          $ObjAerolineas = new Application_Model_DbTable_Aerolineas();
        // se envia a la vista todos los registros de usuarios
        $this->view->aerolineas = $ObjAerolineas->fetchAll();




        $auth = Zend_Auth::getInstance();
        $this->view->auth = $auth;

        $ObjHoteles = new Application_Model_DbTable_Hoteles();

        $ObjHabitacionesTipo = new Application_Model_DbTable_HabitacionesTipo();

        $this->view->habitacioes_tipo=$ObjHabitacionesTipo->fetchAll();

        $ObjIdioma = new Application_Model_DbTable_Idiomas();

        $this->view->idiomas=$ObjIdioma->fetchAll();


     
        
        if ($this->getRequest()->isPost()) {
            
            $formData = $this->getRequest()->getPost();
            

            $data = array(
            'id' => $formData['id'],
            'nombre' => $formData['nombre'],
            'pais' => $formData['pais'],
            'estado' => $formData['estado'],
            'larga' => $formData['larga'],
            'categoria' => $formData['categoria'],
            'direccion' => $formData['direccion'],
            'comision_local' => $formData['comision_local'],
            'iva_local' => $formData['iva_local'],
            'utilidad_local' => $formData['utilidad_local'],
            'comision_receptivo' => $formData['comision_receptivo'],
            'iva_receptivo' => $formData['iva_receptivo'],
            'utilidad_receptivo' => $formData['utilidad_receptivo'],
            'nota' => $formData['nota']
            );


                
        $ObjHoteles= new Application_Model_DbTable_Hoteles();
                $ObjHoteles->add($data);

                $this->_flashMessenger->addMessage(array('success' => 'Se ha registrado con éxito!'));
                
                $this->_redirect('/hoteles/list');
                

            
        }
        
    }

    public function editAction() {
        
        $id = $this->_getParam('id', 0);

           $ObjPais = new Application_Model_DbTable_Pais();
        // se envia a la vista todos los registros de usuarios
        $this->view->paises = $ObjPais->fetchAll();

          $ObjAerolineas = new Application_Model_DbTable_Aerolineas();
        // se envia a la vista todos los registros de usuarios
        $this->view->aerolineas = $ObjAerolineas->fetchAll();

        $ObjFotos = new Application_Model_DbTable_Fotos();
        // se envia a la vista todos los registros de usuarios
        $this->view->fotos = $ObjFotos->fetchAll('id_solicitud="'.$id.'"');

        $ObjHabitaciones = new Application_Model_DbTable_Habitaciones();
        // se envia a la vista todos los registros de usuarios
        $this->view->habitaciones = $ObjHabitaciones->fetchAll('id_hotel="'.$id.'"');

         $auth = Zend_Auth::getInstance();
        $this->view->auth = $auth;

           $ObjHoteles = new Application_Model_DbTable_Hoteles();
        
        

        if ($this->getRequest()->isPost()){
            
            $formData = $this->getRequest()->getPost();


            
             $data = array(
            'nombre' => $formData['nombre'],
            'pais' => $formData['pais'],
            'estado' => $formData['estado'],
            'larga' => $formData['larga'],
            'categoria' => $formData['categoria'],
            'direccion' => $formData['direccion'],
            'comision_local' => $formData['comision_local'],
            'iva_local' => $formData['iva_local'],
            'utilidad_local' => $formData['utilidad_local'],
            'comision_receptivo' => $formData['comision_receptivo'],
            'iva_receptivo' => $formData['iva_receptivo'],
            'utilidad_receptivo' => $formData['utilidad_receptivo'],
            'nota' => $formData['nota']
            );

            
            
            $ObjHoteles->upd($formData['id'], $data);

            $this->_redirect('/hoteles/list');
            
            
            
            
        } else {
            
          
                
           $ObjHoteles = new Application_Model_DbTable_Hoteles();
                
        

                $hotel=$ObjHoteles->get($id);

                $this->view->hotel=$hotel;



            $ObjEstado = new Application_Model_DbTable_Estado();
                // se envia a la vista todos los registros de usuarios
             $this->view->estados = $ObjEstado->fetchAll('country_id="'.$hotel['pais'].'"');



           
        }
    }




}









