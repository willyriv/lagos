
/**
*
*  Base64 encode / decode
*  http://www.webtoolkit.info/
*
**/

var Base64 = {

    // private property
    _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode : function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
            this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
            this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },

    // public method for decoding
    decode : function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    // private method for UTF-8 encoding
    _utf8_encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode : function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while ( i < utftext.length ) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

}

$(document).ready(function(){
	/* This code is executed after the DOM has been completely loaded */

	/* Changing thedefault easing effect - will affect the slideUp/slideDown methods: */
	$.easing.def = "easeOutBounce";

	/* Binding a click event handler to the links: */
	$('li.button a').click(function(e){
	
		/* Finding the drop down list that corresponds to the current section: */
		var dropDown = $(this).parent().next();
		
		/* Closing all other drop down sections, except the current one */
		$('.dropdown').not(dropDown).slideUp('slow');
		dropDown.slideToggle('slow');
		
		/* Preventing the default event (which would be to navigate the browser to the link's address) */
		e.preventDefault();
	})
	
});






/*$(window).load(function() {      
              $("#flexiselDemo1").flexisel({
              visibleItems: 4,
              animationSpeed: 1000,
              autoPlay: true,
              autoPlaySpeed: 3000,        
              pauseOnHover:true,
              enableResponsiveBreakpoints: true,
              responsiveBreakpoints: { 
                portrait: { 
                  changePoint:480,
                  visibleItems: 1
                }, 
                landscape: { 
                  changePoint:640,
                  visibleItems: 2
                },
                tablet: { 
                  changePoint:768,
                  visibleItems: 3
                }
              }
            });
            });


$(window).load(function() {
                      $('.flexslider').flexslider({
                      animation: "slide"
                      /*controlNav: "thumbnails"*/
                      /*});
                    }); */


   /* $(function(){
      SyntaxHighlighter.all();
      });*/
      /*$(window).load(function(){
      $('.flexslider').flexslider({
      animation: "slide",
      start: function(slider){
      $('body').removeClass('loading');
      }
      });
    });*/



function mostrarvideo(video){

    $('#modalVideo .modal-body').html(Base64.decode(video));

    $('#modalVideo').modal('show');

}

$( function() {

/*$( "#buscador" ).autocomplete({
      source: "/ajax/busqueda",
      minLength: 3,
      focus: function( event, ui ) {
        $( "#buscador" ).val( ui.item.descripcion );
        return false;
      },
      select: function( event, ui ) {
        /*log( "Selected: " + ui.item.descripcion + " aka " + ui.item.id );*/
       /* alert("Selected: " + ui.item.descripcion + " aka " + ui.item.id);*/
      /*  $('.filtros').val('');

        $('#'+ui.item.tipo+'').val(ui.item.id);
        $('#buscador').val(ui.item.descripcion);

        return false;

      }
    }). data("uiAutocomplete")._renderItem = function( ul, item ) {
      return $( "<li class='opts' style='z-index:1000;'>" )
        .append( "<div style='background:red;'>" + item.descripcion +  "</div>" )
       
        .appendTo( ul );
    };


    $( "#buscador" ).autocomplete({
      source: "/ajax/busqueda",
      minLength: 3,
      success: function( data ) {
            /*response( data );
            res='<ul>';
            $.each(data, function(i, item) {
                alert(item);
            });




        }
     
    });*/


var ajax = null;
   
$( "#buscador" ).keypress(function( event ) {
  if ( event.which == 13 ) {
     event.preventDefault();
  }

  if ($('#buscador').val().length>2) {

    term=$('#buscador').val();

    if(ajax){
        ajax.abort();
    }

    ajax=$.ajax({
            type: "POST",
            data:{term},
            url: "/ajax/busqueda",
                
            complete: function(datos){

              //alert(datos.responseText);
                //alert(id);
                //$("#deleteModal").modal('hide');
                  /*$('#'+habitacion+' .costo').html(datos.responseText); */

                  $('.res_busq').html(datos.responseText);

            }
        });

  }
  
});

    });


function selectbusqueda(tipo, id, descripcion){

    $('#'+tipo).val(id);
    $('#buscador').val(descripcion);
    $('.res_busq').html('');


}


