/**
*
*  Base64 encode / decode
*  http://www.webtoolkit.info/
*
**/

var Base64 = {

    // private property
    _keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

    // public method for encoding
    encode : function (input) {
        var output = "";
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;

        input = Base64._utf8_encode(input);

        while (i < input.length) {

            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);

            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;

            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }

            output = output +
            this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
            this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

        }

        return output;
    },

    // public method for decoding
    decode : function (input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;

        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

        while (i < input.length) {

            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));

            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;

            output = output + String.fromCharCode(chr1);

            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }

        }

        output = Base64._utf8_decode(output);

        return output;

    },

    // private method for UTF-8 encoding
    _utf8_encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    // private method for UTF-8 decoding
    _utf8_decode : function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while ( i < utftext.length ) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

}








$(document).ready(function(){

    $('#tb_pedidos').dataTable();

    $('.fecha').datepicker({
        format:"dd/mm/yyyy"
    });

    $('#tb_estudiantes').dataTable({
        "order":[[1, "desc"]]
    });

});




function agregarPedido(){
	$('#addCajaModal').modal('show');
}

 function agregardetalle(pedido, producto, cantidad, precio){     

    //alert('hola');

    url="/ajax/agregardetalle";

         $.post(url, {pedido, producto, cantidad, precio}, function(data) {

           /* res=JSON.parse(data);*/

            if (data) {
               
                
                $('#contenido_caja').html(data);

                     
            } else {

              
            }
         });

 }


 function cerrarcaja(id){     

    url="/ajax/cerrarcaja";

         $.post(url, {id}, function(data) {

           /* res=JSON.parse(data);*/

            if (data) {
               
                
                $('#contenido_caja').html(data);

                     
            } else {

              
            }
         });

 }

  function caja(){     

    //alert('hola');

    url="/ajax/caja";

         $.post(url, {}, function(data) {

           /* res=JSON.parse(data);*/

            if (data) {
               
                
                $('#contenido_caja').html(data);

                     
            } else {

              
            }
         });

 }

 function crearCaja(){     

    nombre=$('#nombre_modal').val();

    cliente=$('#cliente').val();

    vendedor=$('#vendedor').val();

    url="/ajax/crearcaja";

         $.post(url, {nombre, vendedor, cliente}, function(data) {

            if (data) {
                
                ///alertify.success(res2.contenido).dismissOthers();
                
                $('#contenido_caja').html(data);

               // $('.mensaje').html('<div class="alert alert-success alert-dismissable">Solicitud procesada correctamente</div>');
                
                $('#addCajaModal').modal('hide');
                     
            } else {

                $('.mensaje').html('<div class="alert alert-danger alert-dismissable">Error al procesar solicitud</div>');
                //alertify.error(res2.contenido).dismissOthers(); 

                $('#addCajaModal').modal('hide');
                    
            }

         });

 }


 function detalleCaja(id){     

    //alert('hola');

    url="/ajax/detallecaja";

         $.post(url, {id}, function(data) {

           /* res=JSON.parse(data);*/

            if (data) {
                
                ///alertify.success(res2.contenido).dismissOthers();
                
                $('#contenido_caja').html(data);

               /* $('.mensaje').html('<div class="alert alert-success alert-dismissable">Solicitud procesada correctamente</div>');
                
                $('#addCajaModal').modal('hide');*/
                     
            } else {

               /* $('.mensaje').html('<div class="alert alert-danger alert-dismissable">Error al procesar solicitud</div>');
                //alertify.error(res2.contenido).dismissOthers(); 

                $('#addCajaModal').modal('hide');*/
                     
            }




         });

 }

 function eliminardetalle(id, pedido){     

    //alert('hola');

    url="/ajax/eliminardetalle";

         $.post(url, {id, pedido}, function(data) {

           /* res=JSON.parse(data);*/

            if (data) {
               
                
                $('#contenido_caja').html(data);

                     
            } else {

              
            }
         });

 }


function pagar(id, monto){     

    $('#id_pedido').val(id);
    $('#monto_modal').val(monto);

    $('#addPagoModal').modal('show');


 }


 function pagarCaja(){     

    
    id=$('#id_pedido').val();
    monto=$('#monto_modal').val();
    tipo_pago=$('#tipo_pago').val();



    url="/ajax/pagarcaja";

         $.post(url, {id, monto, tipo_pago}, function(data) {

           /* res=JSON.parse(data);*/

            if (data) {
               
                
                $('#contenido_caja').html(data);

                $('#addPagoModal').modal('hide');

                     
            } else {

              
            }
         });

 }




function desactivarMarca(id){   

    url="/ajax/desactivarmarca";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }

 function activarMarca(id){   

    url="/ajax/activarmarca";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }

 function del(id){
    $('#del_id').val(id);
    $('#deleteModal').modal('show');
 }

  function deleteMarca(){   

    id=$('#del_id').val();

    url="/ajax/deletemarca";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).remove();
                $('#deleteModal').modal('hide');
                     
            } else {
        
            }

         });

 }

  function deleteServicios(){   

    id=$('#del_id').val();

    url="/ajax/deleteservicios";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).remove();
                $('#deleteModal').modal('hide');
                     
            } else {
        
            }

         });

 }



function desactivarServicio(id){   

    url="/ajax/desactivarservicios";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }

 function activarServicio(id){   

    url="/ajax/activarservicios";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }

   function deleteModulo(){   

    id=$('#del_id').val();

    url="/ajax/deletemodulo";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).remove();
                $('#deleteModal').modal('hide');
                     
            } else {
        
            }

         });

 }



function desactivarModulo(id){   

    url="/ajax/desactivarmodulo";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }

 function activarModulo(id){   

    url="/ajax/activarmodulo";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }


   function deletePortafolio(){   

    id=$('#del_id').val();

    url="/ajax/deleteportafolio";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).remove();
                $('#deleteModal').modal('hide');
                     
            } else {
        
            }

         });

 }



function desactivarPortafolio(id){   

    url="/ajax/desactivarportafolio";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }

 function activarPortafolio(id){   

    url="/ajax/activarportafolio";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }

  function deleteValidador(){   

    id=$('#del_id').val();

    url="/ajax/deletevalidador";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).remove();
                $('#deleteModal').modal('hide');
                     
            } else {
        
            }

         });

 }

 function deleteAuditoria(){   

    id=$('#del_id').val();

    url="/ajax/deleteauditoria";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).remove();
                $('#deleteModal').modal('hide');
                     
            } else {
        
            }

         });

 }



 



 function deletePagina(){   

    id=$('#del_id').val();

    url="/ajax/deletepagina";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).remove();
                $('#deleteModal').modal('hide');
                     
            } else {
        
            }

         });

 }



function desactivarPagina(id){   

    url="/ajax/desactivarpagina";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }

 function activarPagina(id){   

    url="/ajax/activarpagina";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }





 function deleteNoticia(){   

    id=$('#del_id').val();

    url="/ajax/deletenoticia";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).remove();
                $('#deleteModal').modal('hide');
                     
            } else {
        
            }

         });

 }



function desactivarNoticia(id){   

    url="/ajax/desactivarnoticia";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }

 function activarNoticia(id){   

    url="/ajax/activarnoticia";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }





 function deleteSlider(){   

    id=$('#del_id').val();

    url="/ajax/deleteslider";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).remove();
                $('#deleteModal').modal('hide');
                     
            } else {
        
            }

         });

 }



function desactivarSlider(id){   

    url="/ajax/desactivarslider";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }

 function activarSlider(id){   

    url="/ajax/activarslider";

         $.post(url, {id}, function(data) {

            if (data) {
                
                $('#'+id).html(data);
                     
            } else {
        
            }

         });

 }

 function agregarTags(){

        id=$('#id').val();
        etiqueta=$('#etiqueta').val();

       url="/ajax/agregartags";

         $.post(url, {id, etiqueta}, function(data) {

            if (data) {
                
                $('.contenido').html(data);
                $('#etiqueta').val('');
                     
            } else {
        
            }

         });

 }

 function eliminarTag(tag){   

    id=$('#id').val();

    
    url="/ajax/eliminartag";

         $.post(url, {tag, id}, function(data) {

            if (data) {

                $('.contenido').html(data);
                $('#etiqueta').val('');
               
                
                     
            } else {

                $('.contenido').html(data);
                $('#etiqueta').val('');
        
            }

         });

 }




function borrar(id, modelo){

    //alert(id+modelo);

    $('#del_id').val(id);
    $('#del_modelo').val(modelo);

    $('#deleteModal').modal('show');

}

function eliminar(){

    id=$('#del_id').val();
    modelo=$('#del_modelo').val();

    url="/ajax/eliminar";

     $.post(url, {id, modelo}, function(data) {

            if (data) {
                
                $('#'+id).remove();
                $('#deleteModal').modal('hide');
                     
            } else {
        
            }

         });
}

 function estatus(id, modelo, estatus){   

    url="/ajax/estatus";

         $.post(url, {id, modelo, estatus}, function(data) {

            if (data) {
                
                $('#'+id+' .estatus').html(data);
                     
            } else {
        
            }

         });

 }

 function principal(id){ 

    id_solicitud=$('#id').val();  

    url="/ajax/principal";

         $.post(url, {id, id_solicitud}, function(data) {

            if (data) {
                
                $(".carga .row").html(data);
                     
            } else {
        
            }

         });

 }

 function cargarmaps(){
    
    gmaps=$('#gmaps').val();

    $('#googlemaps').html(gmaps);

    cadena = Base64.encode(gmaps);

    $('#gmaps').val('');
    $('#gmaps').val(cadena);
 }



  function actualizarpdf(id){ 

    id_solicitud=$('#id').val();

    descripcion=$('#descripcion'+id).val();    

    url="/ajax/actualizarpdf";

         $.post(url, {id, descripcion, id_solicitud }, function(data) {

            if (data) {
                
                $(".cargapdf .row").html(data);
                     
            } else {
        
            }

         });

 }




function cargarEstado(){

    pais=$('#pais').val();
   // modelo=$('#del_modelo').val();

    url="/ajax/cargarestado";

     $.post(url, {pais}, function(data) {

            if (data) {
                
                //$('#'+id).remove();
                $('#estado').html(data);
                     
            } else {
        
            }

         });

}

function agregarHabitacion(){

  plan='';

  if ($('#EP').is(':checked') ) { plan=plan+'EP,';  }
  if ($('#AP').is(':checked') ) { plan=plan+'AP,';  }
  if ($('#MAP').is(':checked') ) { plan=plan+'MAP,';  }
  if ($('#FAP').is(':checked') ) { plan=plan+'FAP,';  }
  if ($('#AI').is(':checked') ) { plan=plan+'AI,';  }

  planes = plan.slice(0,-1);

  //alert(planes);

    tipohabitacion=$('#tipohabitacion').val();
    ocupacion=$('#ocupacion').val();
    id=$('#id').val();
   // modelo=$('#del_modelo').val();

    url="/ajax/agregarhabitacion";

     $.post(url, {planes, tipohabitacion, ocupacion, id}, function(data) {

            if (data) {
                
                //$('#'+id).remove();
                $('.respuesta_habitaciones').html(data);
                     
            } else {
        
            }

         });

}


function actualizaTarifa(id, tipo){

  tarifa=$('#'+tipo+id).val();


    url="/ajax/actualizatarifa";

        $(".respuesta"+id).html("<img title='cargando'  src='/assets/img/loading.gif'>");

    $.post(url, {tarifa, id, tipo}, function(data) {

        if (data) {
            
            $('#'+id).html(data);

            $(".respuesta"+id).html("");

                 
        } else {
        
          $(".respuesta"+id).html("");
    
        }

    });

}


function agregarTarifa(){

  id_hotel=$('#id_hotel').val();
  desde=$('#desde').val();
  hasta=$('#hasta').val();
  lun=$('#lun:checked').val();
  mar=$('#mar:checked').val();
  mie=$('#mie:checked').val();
  jue=$('#jue:checked').val();
  vie=$('#vie:checked').val();
  sab=$('#sab:checked').val();
  dom=$('#dom:checked').val();
  

  if (lun==undefined) { lun='0';}
  if (mar==undefined) { mar='0';}
  if (mie==undefined) { mie='0';}
  if (jue==undefined) { jue='0';}
  if (vie==undefined) { vie='0';}
  if (sab==undefined) { sab='0';}
  if (dom==undefined) { dom='0';}
  //alert('lun:'+lun+' mar:'+mar+' mie:'+mie+' jue:'+jue+' vie:'+vie+' sab:'+sab+' dom:'+dom);

  if(desde=='' || hasta==''  ){


    alert('Debe llenar los campos Desde y Hasta');

  }else{


 url="/ajax/agregartarifa";

       $(".respuesta"+id).html("<img title='cargando'  src='/assets/img/loading.gif'>");

    $.post(url, {id_hotel, desde, hasta, lun, mar, mie, jue, vie, sab, dom}, function(data) {

        if (data) {
            
            //$('#'+id).html(data);

            $(".respuesta_habitaciones").html(data);
          $('#desde').val('');
        $('#hasta').val('');

                 
        } else {
        
          $(".respuesta_habitaciones").html(data);
    
        }

    });

  }

   

}


function cargarvideos(){

    youtube=$('#youtube').val();

    video = Base64.encode(youtube);

    id=$('#id').val();

    titulo=$('#descripcion').val();

    url="/ajax/cargarvideos";

     $.post(url, {video, titulo, id}, function(data) {

            if (data) {
                
                //$('#'+id).remove();
                $('#videos').html(data);
                     
            } else {
        
            }

         });

}


  function actualizarvideos(id){ 

    id_solicitud=$('#id').val();

    descripcion=$('#descripcion'+id).val();    

    url="/ajax/actualizarpdf";

         $.post(url, {id, descripcion, id_solicitud }, function(data) {

            if (data) {
                
                $(".cargapdf .row").html(data);
                     
            } else {
        
            }

         });

 }




