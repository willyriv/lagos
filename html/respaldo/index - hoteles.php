   <!-- banner -->
  <div class="banner">
    <!-- container -->
    <div class="container">
      <div class="col-md-4 banner-left">
        <section class="slider2">
          <div class="flexslider">
            <ul class="slides">

            <?php foreach ($this->sliders as $slider): ?>

              <li>
                <div class="slider-info">
                  <img src="<?php echo $this->baseUrl($slider->imagen); ?>" alt="<?php echo $slider->titulo; ?>">
                </div>
              </li>

            <?php endforeach ?>
              
              
            </ul>
          </div>
        </section>
        <!--FlexSlider-->
      </div>
      <div class="col-md-8 banner-right">
        <div class="sap_tabs">  
          <div class="booking-info">
            <h2>Selecciona tu Paquete de Viajes</h2>
          </div>
           <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
              <ul class="resp-tabs-list">
                <li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>Tralasdos Aereos</span></li>
                <li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span>Hospedaje</span></li>
                <div class="clearfix"></div>
              </ul>   
              <!---->        
             <div class="resp-tabs-container">
              <div class="tab-1 resp-tab-content" aria-labelledby="tab_item-0">
                  <div class="facts">
                    <div class="booking-form">
                      <!---strat-date-piker---->
                      <script>
                        $(function() {
                          $( "#datepicker,#datepicker1" ).datepicker();
                        });
                      </script>
                      <!---/End-date-piker---->
                      <!-- Set here the key for your domain in order to hide the watermark on the web server -->
                      
                      <div class="online_reservation">
                          <div class="b_room">
                            <div class="booking_room">
                              <div class="reservation">
                                <ul>    
                                  <li  class="span1_of_1 desti">
                                     <h5>Destino</h5>
                                     <div class="book_date">
                                       <form>
                                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                        <input type="text" placeholder="Destino" class="typeahead1 input-md form-control tt-input" required="">
                                       </form>
                                     </div>         
                                   </li>
                                   <li  class="span1_of_1 left desti">
                                     <h5>Origen</h5>
                                     <div class="book_date">
                                     <form>
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <input type="text" placeholder="Origen" class="typeahead1 input-md form-control tt-input" required="">
                                     </form>
                                     </div>   
                                   </li>
                                   <div class="clearfix"></div>
                                </ul>
                              </div>
                              <div class="reservation">
                                <ul>  
                                   <li  class="span1_of_1">
                                     <h5>Desde</h5>
                                     <div class="book_date">
                                     <form>
                                      <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                      <input type="date" value="Select date" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Select date';}">
                                     </form>
                                     </div>   
                                   </li>
                                   <li  class="span1_of_1 left">
                                     <h5>Hasta</h5>
                                     <div class="book_date">
                                       <form>
                                        <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                        <input type="date" value="Select date" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Select date';}">
                                      </form>
                                     </div>         
                                   </li>
                                   <li class="span1_of_1 left adult">
                                     <h5>Adultos (18+)</h5>
                                     <div class="section_room">
                                        <select id="country" onchange="change_country(this.value)" class="frm-field required">
                                          <option value="null">1</option>
                                          <option value="null">2</option>         
                                          <option value="AX">3</option>
                                          <option value="AX">4</option>
                                          <option value="AX">5</option>
                                          <option value="AX">6</option>
                                        </select>
                                     </div> 
                                  </li>
                                  <li class="span1_of_1 left children">
                                     <h5>Niños (0-17)</h5>
                                     <div class="section_room">
                                        <select id="country" onchange="change_country(this.value)" class="frm-field required">
                                          <option value="null">1</option>
                                          <option value="null">2</option>         
                                          <option value="AX">3</option>
                                          <option value="AX">4</option>
                                          <option value="AX">5</option>
                                          <option value="AX">6</option>
                                        </select>
                                     </div> 
                                  </li>
                                  <li class="span1_of_1 economy">
                                     <h5>Clase</h5>
                                   
                                     <div class="section_room">
                                        <select id="country" onchange="change_country(this.value)" class="frm-field required">
                                          <option value="null">Economy</option>
                                          <option value="null">Business</option>     
                                        </select>
                                     </div> 
                                  </li>
                                   <div class="clearfix"></div>
                                </ul>
                              </div>
                              <div class="reservation">
                                <ul>  
                                   <li class="span1_of_3">
                                      <div class="date_btn">
                                        <form>
                                          <input type="submit" value="Buscar" />
                                        </form>
                                      </div>
                                   </li>
                                   <div class="clearfix"></div>
                                </ul>
                              </div>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                      </div>
                      <!---->
                    </div>  
                  </div>
              </div>    
              <div class="tab-2 resp-tab-content" aria-labelledby="tab_item-1">
                <div class="facts">
                    <div class="booking-form">




                            <script>
                                $(function() {
                                $( "#datepicker,#datepicker1" ).datepicker();
                                });
                            </script>

                      <!---/End-date-piker---->
                      <div class="online_reservation">
                          <div class="b_room">
                            <div class="booking_room">
                              <div class="reservation">
                                <ul>    
                                  <li  class="span1_of_1 desti">
                                     <h5>Flying from</h5>
                                     <div class="book_date">
                                       <form>
                                        <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                        <input type="text" placeholder="Type Departure City" class="typeahead1 input-md form-control tt-input" required="">
                                       </form>
                                     </div>         
                                   </li>
                                   <li  class="span1_of_1 left desti">
                                     <h5>Flying to</h5>
                                     <div class="book_date">
                                     <form>
                                      <span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span>
                                      <input type="text" placeholder="Type Destination City" class="typeahead1 input-md form-control tt-input" required="">
                                     </form>
                                     </div>   
                                   </li>
                                   <div class="clearfix"></div>
                                </ul>
                              </div>
                              <div class="reservation">
                                <ul>  
                                   <li  class="span1_of_1">
                                     <h5>Departure</h5>
                                     <div class="book_date">
                                     <form>
                                      <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span>
                                      <input type="date" value="Select date" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Select date';}">
                                    </form>
                                     </div>   
                                   </li>
                                   <li class="span1_of_1 left">
                                     <h5>Adults (18+)</h5>
                                     <div class="section_room">
                                        <select id="country" onchange="change_country(this.value)" class="frm-field required">
                                          <option value="null">1</option>
                                          <option value="null">2</option>         
                                          <option value="AX">3</option>
                                          <option value="AX">4</option>
                                          <option value="AX">5</option>
                                          <option value="AX">6</option>
                                        </select>
                                     </div> 
                                  </li>
                                  <li class="span1_of_1 left tab-children">
                                     <h5>Children (0-17)</h5>
                                     <div class="section_room">
                                        <select id="country" onchange="change_country(this.value)" class="frm-field required">
                                          <option value="null">1</option>
                                          <option value="null">2</option>         
                                          <option value="AX">3</option>
                                          <option value="AX">4</option>
                                          <option value="AX">5</option>
                                          <option value="AX">6</option>
                                        </select>
                                     </div> 
                                  </li>
                                  <li class="span1_of_1 economy">
                                     <h5>Class</h5>
                                     <div class="section_room">
                                        <select id="country" onchange="change_country(this.value)" class="frm-field required">
                                          <option value="null">Economy</option>
                                          <option value="null">Business</option>     
                                        </select>
                                     </div> 
                                  </li>
                                   <div class="clearfix"></div>
                                </ul>
                              </div>
                              <div class="reservation">
                                <ul>  
                                   <li class="span1_of_3">
                                      <div class="date_btn">
                                        <form>
                                          <input type="submit" value="Search" />
                                        </form>
                                      </div>
                                   </li>
                                   <div class="clearfix"></div>
                                </ul>
                              </div>
                            </div>
                            <div class="clearfix"></div>
                          </div>
                      </div>
                      <!---->
                    </div>  
                </div>
              </div>                                            
              </div>  
            </div>  
        </div>
      </div>
      <div class="clearfix"> </div>
    </div>
    <!-- //container -->
  </div>
  <!-- //banner -->

  <div class="move-text">
    <div class="marquee">Registra tu Hotel en nuestro sitio gratis.<a href="signup.html">Aqui</a></div>
    
        <script>
      $('.marquee').marquee({ pauseOnHover: true });
      //@ sourceURL=pen.js
    </script>
  </div>
  <!-- banner-bottom -->
  <div class="banner-bottom">
    <!-- container -->
<div class="container">
      <!--<div class="banner-bottom-info">
        <h3>Oferta del Mes</h3>
      </div>-->
      <div class="banner-bottom-grids" style="    margin: 2em 0 0 0;">
        <div class="col-md-4 banner-bottom-grid">
          <!--<div class="banner-bottom-right">
            <a href="products.html">
              <img src="images/o1.jpg" alt="Paquete a Orlando" />
              <div class="destinations-grid-info tours">
                  <h5>Aprovecha!!! Nuestro Paquete a Orlando </h5>
                  <p>Hospedaje, traslados y comidas incluidas</p>
                  <p class="b-period">5 Dias y 4 Noches en el Hotel de tu Preferencia </p>
              </div>
            </a>
          </div>-->
          <div class="top-destinations-grids" style="margin: 0em 0;">
            <div class="top-destinations-info">
              <h4>Paquetes</h4>
            </div>
            <div class="top-destinations-bottom">
              <div class="td-grids">
                <div class="col-xs-3 td-left">
                  <img src="images/x1.jpg" alt="">
                </div>
                <div class="col-xs-7 td-middle">
                  <a href="<?php echo $this->baseUrl('/contacto'); ?>">Ciudad</a>
                  <p>Caracas para todo el mundo En un valle</p>
                  <span class="glyphicon glyphicon-star" aria-hidden="true"></span> <span class="glyphicon glyphicon-star" aria-hidden="true"></span> <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                </div>
                <div class="col-xs-2 td-right">
                  <p>$190</p>
                </div>
                <div class="clearfix"> </div>
              </div>
              <div class="td-grids">
                <div class="col-xs-3 td-left">
                  <img src="images/x2.png" alt="">
                </div>
                <div class="col-xs-7 td-middle">
                  <a href="<?php echo $this->baseUrl('/contacto'); ?>">Amazonas</a>
                  <p>Aquí nace el Orinoco</p>
                  <span class="glyphicon glyphicon-star" aria-hidden="true"></span> <span class="glyphicon glyphicon-star" aria-hidden="true"></span> <span class="glyphicon glyphicon-star" aria-hidden="true"></span> <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                </div>
                <div class="col-xs-2 td-right">
                  <p>$213</p>
                </div>
                <div class="clearfix"> </div>
              </div>
              <div class="td-grids">
                <div class="col-xs-3 td-left">
                  <img src="images/x3.jpg" alt="">
                </div>
                <div class="col-xs-7 td-middle">
                  <a href="<?php echo $this->baseUrl('/contacto'); ?>">Llanos</a>
                  <p>Excursionismo, ecoturismo, agroturismo</p>
                  <span class="glyphicon glyphicon-star" aria-hidden="true"></span> <span class="glyphicon glyphicon-star" aria-hidden="true"></span> <span class="glyphicon glyphicon-star" aria-hidden="true"></span> <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                </div>
                <div class="col-xs-2 td-right">
                  <p>$176</p>
                </div>
                <div class="clearfix"> </div>
              </div>
              <div class="td-grids">
                <div class="col-xs-3 td-left">
                  <img src="images/x4.jpg" alt="">
                </div>
                <div class="col-xs-7 td-middle">
                  <a href="<?php echo $this->baseUrl('/contacto'); ?>">Sol y Playa</a>
                  <p>Playas de arenas blancas y cristalinas aguas</p>
                  <span class="glyphicon glyphicon-star" aria-hidden="true"></span> <span class="glyphicon glyphicon-star" aria-hidden="true"></span> <span class="glyphicon glyphicon-star" aria-hidden="true"></span>
                </div>
                <div class="col-xs-2 td-right">
                  <p>$490</p>
                </div>
                <div class="clearfix"> </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-8 banner-bottom-grid holidays-bottom-grid">
          <div class="holidays-grids">
            <div class="holidays-info">
              <h4>Hoteles Mas Visitados </h4>
            </div>
            
          </div>


          

          <?php $ban=1; ?>
          <?php foreach ($this->hoteles as $hotel): ?>

            <?php if ($ban==1): ?>

              <div class="holidays-top-grids">
              <div class="col-md-6 holidays-top-grid">
              <a href="<?php echo $this->baseUrl('hoteles/ver/id/'.$hotel->id); ?>"><img src="<?php echo $this->baseUrl('assets/images/'.$hotel->foto); ?>" alt="" /></a>
              <h2><span><?php echo $hotel->nombre; ?></span></h2>
              

            </div>
            <?php $ban=2; ?>
            <?php continue; ?>
              
            <?php endif ?>



            <?php if ($ban==2): ?>

              <div class="col-md-6 holidays-top-grid">
              <a href="<?php echo $this->baseUrl('hoteles/ver/id/'.$hotel->id); ?>"><img src="<?php echo $this->baseUrl('assets/images/'.$hotel->foto); ?>" alt="" /></a>
              <h2><span><?php echo $hotel->nombre; ?></span></h2>
            </div>

            <div class="clearfix"> </div>

            </div>

             <?php $ban=1; ?>

            <?php continue; ?>
              
            <?php endif ?>
            
          <?php endforeach ?>

          <?php if ($ban=2): ?>

            <div class="clearfix"> </div>
          </div>
            
          <?php endif ?>

          
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
    <!-- //container -->
  </div>
  <!-- //banner-bottom -->
  <!-- popular-grids -->
  <div class="popular-grids">
    <!-- container -->
    <div class="container">
      <div class="popular-info">
        <h3>Todos Nuestros Destinos</h3>
      </div>
      <!-- slider -->
      <div class="slider">
        <div class="arrival-grids">      
           <ul id="flexiselDemo1">
              <?php foreach ($this->destinos as $destino): ?>

                <li>
               <a href="<?php echo $this->baseUrl('destinos/ver/buscar/'.$destino->id); ?>"><img src="<?php echo $this->baseUrl('assets/images/'.$destino->foto); ?>" alt=""/>
               </a>
               <h4><span><?php echo $destino->nombre; ?></span></h4>
             </li>
                
              <?php endforeach ?>
             
            </ul>
          
        </div>
      </div>
      <!-- //slider -->
    </div>
    <!-- //container -->
  </div>
  <!-- popular-grids -->
  <!-- footer -->
