-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-01-2020 a las 13:18:25
-- Versión del servidor: 10.1.21-MariaDB
-- Versión de PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `conkhcom_cms`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--
-- en uso(#1932 - Table 'conkhcom_cms.cities' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`cities`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--
-- en uso(#1932 - Table 'conkhcom_cms.countries' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`countries`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_access`
--

CREATE TABLE `hk_access` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `resource_id` int(11) NOT NULL,
  `privilege_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_access`
--

INSERT INTO `hk_access` (`id`, `role_id`, `resource_id`, `privilege_id`) VALUES
(1, 2, 1, 1),
(2, 2, 3, 1),
(8, 2, 3, 4),
(3, 2, 3, 5),
(4, 2, 3, 6),
(5, 2, 3, 7),
(6, 2, 3, 8),
(7, 2, 3, 9),
(9, 3, 1, 1),
(10, 3, 3, 1),
(11, 3, 3, 4),
(12, 3, 4, 1),
(13, 3, 4, 2),
(14, 3, 4, 3),
(15, 3, 4, 4),
(16, 3, 4, 5),
(17, 3, 4, 6),
(18, 3, 4, 7),
(19, 3, 4, 8),
(20, 3, 4, 9),
(21, 3, 5, 1),
(22, 3, 5, 2),
(23, 3, 5, 3),
(24, 3, 5, 4),
(25, 3, 5, 5),
(26, 3, 5, 6),
(27, 3, 5, 7),
(28, 3, 5, 8),
(29, 3, 5, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_admin_users`
--

CREATE TABLE `hk_admin_users` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 NOT NULL,
  `lastname` varchar(100) CHARACTER SET latin1 NOT NULL,
  `state_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `city_id` varchar(100) CHARACTER SET latin1 NOT NULL,
  `username` varchar(100) CHARACTER SET latin1 NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 NOT NULL,
  `salt` varchar(100) CHARACTER SET latin1 NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `role_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_admin_users`
--

INSERT INTO `hk_admin_users` (`id`, `name`, `lastname`, `state_id`, `city_id`, `username`, `password`, `salt`, `created`, `modified`, `role_id`, `status`) VALUES
(7, 'miguel', 'machado', '1', '', 'miguelmachadoaa', '715f310ced30f55b015d80d74b0f2e90', '', '2014-09-04 09:16:51', '0000-00-00 00:00:00', 1, 1),
(8, 'Secretario', 'Secretario', '4', '', 'secretario', 'a0a475cf454cf9a06979034098167b9e', '', '2016-11-25 15:05:28', '2017-02-21 13:18:32', 2, 1),
(9, 'Conkhep', 'Corporacion', '24', '', 'conkhep', 'a0a475cf454cf9a06979034098167b9e', '', '2017-02-21 13:18:10', '2017-09-26 15:49:12', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_aerolineas`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_aerolineas' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_aerolineas`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_agencia`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_agencia' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_agencia`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_asistencia`
--

CREATE TABLE `hk_asistencia` (
  `id` int(11) NOT NULL,
  `id_empleado` varchar(128) CHARACTER SET latin1 NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_asistencia`
--

INSERT INTO `hk_asistencia` (`id`, `id_empleado`, `fecha`, `estatus`) VALUES
(1, '1', '2016-10-25 05:41:44', 1),
(2, '1', '2016-10-25 05:43:02', 1),
(3, '1', '2016-10-25 05:43:34', 1),
(4, '2', '2016-10-25 05:43:56', 1),
(5, '1', '2016-10-24 06:05:47', 1),
(6, '2', '2016-10-23 06:05:58', 1),
(7, '1', '2016-10-22 06:06:21', 1),
(8, '1', '2016-10-21 06:06:29', 1),
(9, '1', '2016-10-20 06:06:41', 1),
(10, '2', '2016-10-20 06:06:44', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_auditoria`
--

CREATE TABLE `hk_auditoria` (
  `id` int(22) NOT NULL,
  `nombres` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `apellidos` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `tlf` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `tlf_movil` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Mayor de solicitudes de Auditoria de Tasaciones';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_audit_trail`
--

CREATE TABLE `hk_audit_trail` (
  `id` int(11) NOT NULL,
  `message` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL,
  `user_ip` varchar(100) CHARACTER SET latin1 NOT NULL,
  `data` longtext CHARACTER SET latin1 NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_audit_trail`
--

INSERT INTO `hk_audit_trail` (`id`, `message`, `user_id`, `user_ip`, `data`, `timestamp`) VALUES
(1, 'administrador:users/edit', 1, '192.168.0.104', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\"}', '2014-06-11 11:29:34'),
(2, 'administrador:users/add', 1, '192.168.0.104', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-06-11 11:29:41'),
(3, 'administrador:members/export', 1, '192.168.0.101', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 11:33:24'),
(4, 'administrador:members/approve', 1, '192.168.0.101', '{\"controller\":\"members\",\"action\":\"approve\",\"module\":\"default\",\"mid\":\"1\"}', '2014-06-11 11:40:17'),
(5, 'administrador:members/export', 1, '192.168.0.101', '{\"controller\":\"members\",\"action\":\"export\",\"f_status\":\"\",\"f_memberType\":\"2\",\"module\":\"default\"}', '2014-06-11 11:40:27'),
(6, 'administrador:members/export', 1, '192.168.0.101', '{\"controller\":\"members\",\"action\":\"export\",\"f_status\":\"\",\"f_memberType\":\"\",\"module\":\"default\"}', '2014-06-11 11:40:37'),
(7, 'administrador:members/export', 1, '192.168.0.101', '{\"controller\":\"members\",\"action\":\"export\",\"f_status\":\"\",\"f_memberType\":\"\",\"module\":\"default\"}', '2014-06-11 11:40:54'),
(8, 'administrador:members/export', 1, '192.168.0.101', '{\"controller\":\"members\",\"action\":\"export\",\"f_status\":\"\",\"f_memberType\":\"\",\"f_state\":\"4\",\"module\":\"default\"}', '2014-06-11 11:41:40'),
(9, 'administrador:users/add', 1, '192.168.0.101', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-06-11 11:44:17'),
(10, 'administrador:users/add', 1, '192.168.0.101', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\",\"name\":\"Orangel\",\"lastname\":\"Barrera\",\"state_id\":\"4\",\"username\":\"obarrera\",\"role_id\":\"2\",\"password\":\"H3ur3k@\",\"password_conf\":\"H3ur3k@\",\"btn_submit\":\"Guardar\"}', '2014-06-11 11:44:49'),
(11, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 11:53:25'),
(12, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 12:48:10'),
(13, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 12:50:01'),
(14, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 12:50:22'),
(15, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 12:52:15'),
(16, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 12:58:48'),
(17, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 13:01:13'),
(18, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 13:01:42'),
(19, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 13:04:10'),
(20, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 13:04:35'),
(21, 'administrador:members/export', 1, '192.168.0.101', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 13:06:27'),
(22, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 13:07:51'),
(23, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 13:08:30'),
(24, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 13:10:46'),
(25, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 13:11:00'),
(26, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 13:11:37'),
(27, 'administrador:members/export', 1, '192.168.0.101', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 14:26:51'),
(28, 'administrador:users/add', 1, '192.168.0.104', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-06-11 14:33:50'),
(29, 'administrador:members/export', 1, '192.168.0.104', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 14:34:20'),
(30, 'administrador:members/export', 1, '192.168.0.101', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-06-11 14:37:14'),
(31, 'administrador:users/add', 1, '192.168.0.104', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-06-11 15:04:15'),
(32, 'administrador:users/add', 1, '192.168.0.104', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\",\"name\":\"Rosmy\",\"lastname\":\"Rodriguez\",\"state_id\":\"4\",\"username\":\"raynexus\",\"role_id\":\"1\",\"password\":\"Rrz2398f+\",\"password_conf\":\"Rrz2398f+\",\"btn_submit\":\"Guardar\"}', '2014-06-11 15:04:41'),
(33, 'administrador:members/reject', 1, '192.168.0.12', '{\"controller\":\"members\",\"action\":\"reject\",\"module\":\"default\",\"mid\":\"1\"}', '2014-06-27 12:08:50'),
(34, 'administrador:members/del', 1, '192.168.0.12', '{\"controller\":\"members\",\"action\":\"del\",\"module\":\"default\",\"mid\":\"1\"}', '2014-06-27 12:08:57'),
(35, 'administrador:members/reject', 1, '192.168.0.12', '{\"controller\":\"members\",\"action\":\"reject\",\"module\":\"default\",\"mid\":\"8\"}', '2014-06-27 12:09:05'),
(36, 'administrador:members/del', 1, '192.168.0.12', '{\"controller\":\"members\",\"action\":\"del\",\"module\":\"default\",\"mid\":\"8\"}', '2014-06-27 12:09:09'),
(37, 'administrador:members/reject', 1, '192.168.0.12', '{\"controller\":\"members\",\"action\":\"reject\",\"module\":\"default\",\"mid\":\"2\"}', '2014-06-27 12:09:16'),
(38, 'administrador:members/del', 1, '192.168.0.12', '{\"controller\":\"members\",\"action\":\"del\",\"module\":\"default\",\"mid\":\"2\"}', '2014-06-27 12:09:21'),
(39, 'administrador:users/edit', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\"}', '2014-06-27 12:09:30'),
(40, 'administrador:users/edit', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\",\"name\":\"Rosmy\",\"lastname\":\"Rodriguez\",\"state_id\":\"4\",\"username\":\"raynexus\",\"role_id\":\"2\",\"password\":\"\",\"password_conf\":\"\",\"btn_submit\":\"Guardar\"}', '2014-06-27 12:09:39'),
(41, 'secretario:members/reject', 4, '192.168.0.12', '{\"controller\":\"members\",\"action\":\"reject\",\"module\":\"default\",\"mid\":\"3\"}', '2014-06-27 12:10:01'),
(42, 'secretario:members/del', 4, '192.168.0.12', '{\"controller\":\"members\",\"action\":\"del\",\"module\":\"default\",\"mid\":\"3\"}', '2014-06-27 12:10:07'),
(43, 'secretario:members/del', 4, '192.168.0.12', '{\"controller\":\"members\",\"action\":\"del\",\"module\":\"default\",\"mid\":\"3\"}', '2014-06-27 12:12:54'),
(44, 'administrador:members/export', 1, '192.168.0.12', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-10 08:38:39'),
(45, 'administrador:members/export', 1, '192.168.0.12', '{\"controller\":\"members\",\"action\":\"export\",\"f_profession\":\"\",\"f_entretainment\":\"\",\"f_state\":\"\",\"f_memberType\":\"\",\"f_status\":\"N\",\"module\":\"default\"}', '2014-07-10 08:40:45'),
(46, 'administrador:users/add', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-10 08:51:03'),
(47, 'administrador:users/add', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-10 09:22:51'),
(48, 'administrador:users/edit', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\"}', '2014-07-10 09:22:59'),
(49, 'administrador:users/add', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-10 09:23:15'),
(50, 'administrador:users/add', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-10 13:14:17'),
(51, 'administrador:users/edit', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\"}', '2014-07-10 13:14:22'),
(52, 'administrador:users/add', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-11 09:45:59'),
(53, 'administrador:users/add', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-11 09:46:25'),
(54, 'administrador:users/edit', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2014-07-25 15:00:38'),
(55, 'administrador:users/edit', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\",\"name\":\"Orangel\",\"lastname\":\"Barrera\",\"state_id\":\"4\",\"username\":\"obarrera\",\"role_id\":\"2\",\"password\":\"Hala1505\",\"password_conf\":\"Hala1505\",\"btn_submit\":\"Guardar\"}', '2014-07-25 15:01:08'),
(56, 'administrador:members/export', 1, '192.168.0.12', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-25 15:34:58'),
(57, 'administrador:cursos/add', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:05:09'),
(58, 'administrador:cursos/add', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:05:31'),
(59, 'administrador:cursos/add', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\",\"codigo\":\"MT\",\"descripcion\":\"Montacarguista\",\"duracion\":\"120\",\"btn_submit\":\"Guardar\"}', '2014-07-28 09:05:44'),
(60, 'administrador:cursos/edit', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\"}', '2014-07-28 09:05:48'),
(61, 'administrador:cursos/edit', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\",\"codigo\":\"MT\",\"descripcion\":\"Montacarguista\",\"duracion\":\"100\",\"btn_submit\":\"Guardar\"}', '2014-07-28 09:05:52'),
(62, 'administrador:cursos/export', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 09:05:54'),
(63, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:06:57'),
(64, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:07:13'),
(65, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:07:47'),
(66, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:07:50'),
(67, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:10:26'),
(68, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:11:33'),
(69, 'administrador:certificados/export', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 09:11:37'),
(70, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:12:07'),
(71, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:13:16'),
(72, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:13:33'),
(73, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:13:37'),
(74, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:13:39'),
(75, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:14:20'),
(76, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:14:22'),
(77, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:14:24'),
(78, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:14:44'),
(79, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:15:37'),
(80, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:15:41'),
(81, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:17:52'),
(82, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:19:14'),
(83, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:19:18'),
(84, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:19:23'),
(85, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:19:45'),
(86, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:20:27'),
(87, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:21:37'),
(88, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:25:18'),
(89, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:37:36'),
(90, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:37:41'),
(91, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:37:51'),
(92, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:38:05'),
(93, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:38:11'),
(94, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:38:15'),
(95, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:38:19'),
(96, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:38:52'),
(97, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:38:56'),
(98, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:39:01'),
(99, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:39:04'),
(100, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:39:16'),
(101, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:39:45'),
(102, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:39:48'),
(103, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:39:53'),
(104, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:40:02'),
(105, 'administrador:cursos/add', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:40:10'),
(106, 'administrador:cursos/add', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:40:18'),
(107, 'administrador:cursos/add', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:40:22'),
(108, 'administrador:cursos/add', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:40:28'),
(109, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 09:40:38'),
(110, 'administrador:certificados/export', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"f_status\":\"2\",\"f_curso\":\"1\",\"f_state\":\"17\",\"f_city\":\"342\",\"module\":\"default\"}', '2014-07-28 09:50:27'),
(111, 'administrador:certificados/export', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"f_status\":\"2\",\"f_curso\":\"1\",\"f_state\":\"17\",\"f_city\":\"342\",\"module\":\"default\"}', '2014-07-28 09:54:42'),
(112, 'administrador:certificados/export', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"f_status\":\"2\",\"f_curso\":\"1\",\"f_state\":\"17\",\"f_city\":\"342\",\"module\":\"default\"}', '2014-07-28 09:54:57'),
(113, 'administrador:certificados/export', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"f_status\":\"2\",\"f_curso\":\"1\",\"f_state\":\"17\",\"f_city\":\"342\",\"module\":\"default\"}', '2014-07-28 09:55:04'),
(114, 'administrador:certificados/export', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"f_status\":\"2\",\"f_curso\":\"1\",\"f_state\":\"17\",\"f_city\":\"342\",\"module\":\"default\"}', '2014-07-28 09:55:18'),
(115, 'administrador:certificados/export', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"f_status\":\"2\",\"f_curso\":\"1\",\"f_state\":\"17\",\"f_city\":\"342\",\"module\":\"default\"}', '2014-07-28 09:55:35'),
(116, 'administrador:certificados/export', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"f_status\":\"2\",\"f_curso\":\"1\",\"f_state\":\"17\",\"f_city\":\"342\",\"module\":\"default\"}', '2014-07-28 09:58:58'),
(117, 'administrador:certificados/add', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 10:19:32'),
(118, 'administrador:members/export', 1, '192.168.0.9', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 10:19:43'),
(119, 'administrador:users/del', 1, '192.168.0.9', '{\"controller\":\"users\",\"action\":\"del\",\"module\":\"default\",\"uid\":\"4\"}', '2014-07-28 10:20:01'),
(120, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 10:58:02'),
(121, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 10:58:59'),
(122, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 10:59:08'),
(123, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 10:59:55'),
(124, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:01:19'),
(125, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:02:11'),
(126, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\",\"cedula\":\"18083878\",\"nombre\":\"aaaaaaaaa\",\"apellido\":\"martinez\",\"email\":\"angelm3achado@hotmail.com\",\"fecha_nac\":\"15-05-1986\",\"estado\":\"3\",\"ciudad\":\"46\",\"direccion\":\"Urb la barraca av. 99 este no. 139\",\"tlf\":\"09871234567\",\"grado\":\"2\",\"profesion\":\"4\",\"MAX_FILE_SIZE\":\"2097152\",\"btn_submit\":\"Guardar\"}', '2014-07-28 11:02:43'),
(127, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:15:04'),
(128, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:16:14'),
(129, 'administrador:estudiantes/edit', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\"}', '2014-07-28 11:16:37'),
(130, 'administrador:estudiantes/edit', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\"}', '2014-07-28 11:17:09'),
(131, 'administrador:estudiantes/edit', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\"}', '2014-07-28 11:17:32'),
(132, 'administrador:estudiantes/edit', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\"}', '2014-07-28 11:18:23'),
(133, 'administrador:estudiantes/edit', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"module\":\"default\",\"id\":\"1\",\"cedula\":\"18083878\",\"nombre\":\"Rosa\",\"apellido\":\"Mela Cabeza\",\"email\":\"angelm3achado@hotmail.com\",\"fecha_nac\":\"15-05-1986\",\"estado\":\"3\",\"ciudad\":\"46\",\"direccion\":\"Urb la barraca av. 99 este no. 139\",\"tlf\":\"09871234567\",\"grado\":\"2\",\"profesion\":\"4\",\"MAX_FILE_SIZE\":\"2097152\",\"btn_submit\":\"Guardar\"}', '2014-07-28 11:19:07'),
(134, 'administrador:estudiantes/edit', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\"}', '2014-07-28 11:19:20'),
(135, 'administrador:estudiantes/edit', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"module\":\"default\",\"id\":\"1\",\"cedula\":\"18083878\",\"nombre\":\"Rosa\",\"apellido\":\"Mela Cabeza\",\"email\":\"angelm3achado@hotmail.com\",\"fecha_nac\":\"15-05-1986\",\"estado\":\"3\",\"ciudad\":\"46\",\"direccion\":\"Urb la barraca av. 99 este no. 139\",\"tlf\":\"09871234567\",\"grado\":\"2\",\"profesion\":\"4\",\"MAX_FILE_SIZE\":\"2097152\",\"btn_submit\":\"Guardar\"}', '2014-07-28 11:19:27'),
(136, 'administrador:estudiantes/export', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 11:19:35'),
(137, 'administrador:estudiantes/export', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 11:20:39'),
(138, 'administrador:estudiantes/export', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 11:20:48'),
(139, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:21:42'),
(140, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:22:10'),
(141, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:23:10'),
(142, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\",\"estudiante\":\"18083878\",\"curso\":\"1\",\"fecha_cap\":\"15-03-2011\",\"fecha_ven\":\"30-07-2014\",\"btn_submit\":\"Guardar\"}', '2014-07-28 11:23:50'),
(143, 'administrador:certificados/export', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 11:31:29'),
(144, 'administrador:certificados/export', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"f_status\":\"2\",\"f_curso\":\"1\",\"f_state\":\"1\",\"f_city\":\"2\",\"module\":\"default\"}', '2014-07-28 11:31:40'),
(145, 'administrador:estudiantes/export', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 11:35:36'),
(146, 'administrador:estudiantes/export', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 11:38:21'),
(147, 'administrador:cursos/export', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 11:38:29'),
(148, 'administrador:estudiantes/export', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 11:38:37'),
(149, 'administrador:users/add', 1, '192.168.0.9', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:40:46'),
(150, 'administrador:cursos/add', 1, '192.168.0.9', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:41:11'),
(151, 'administrador:members/export', 1, '192.168.0.9', '{\"controller\":\"members\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 11:41:21'),
(152, 'administrador:estudiantes/add', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:41:33'),
(153, 'administrador:cursos/add', 1, '192.168.0.9', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:42:03'),
(154, 'administrador:estudiantes/edit', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\"}', '2014-07-28 11:44:14'),
(155, 'administrador:estudiantes/edit', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"module\":\"default\",\"id\":\"1\",\"cedula\":\"18083878\",\"nombre\":\"Rosa\",\"apellido\":\"Mela Cabeza\",\"email\":\"angelm3achado@hotmail.com\",\"fecha_nac\":\"15-05-1986\",\"estado\":\"3\",\"ciudad\":\"46\",\"direccion\":\"Urb la barraca av. 99 este no. 139\",\"tlf\":\"09871234567\",\"grado\":\"2\",\"profesion\":\"4\",\"MAX_FILE_SIZE\":\"2097152\",\"btn_submit\":\"Guardar\"}', '2014-07-28 11:44:35'),
(156, 'administrador:certificados/add', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:44:58'),
(157, 'administrador:estudiantes/export', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 11:46:09'),
(158, 'administrador:estudiantes/export', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 11:48:09'),
(159, 'administrador:estudiantes/export', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 11:49:27'),
(160, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:53:18'),
(161, 'administrador:estudiantes/edit', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\"}', '2014-07-28 11:53:29'),
(162, 'administrador:cursos/edit', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\"}', '2014-07-28 11:53:50'),
(163, 'administrador:cursos/add', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:53:58'),
(164, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 11:54:05'),
(165, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 13:52:34'),
(166, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\",\"cedula\":\"17985998\",\"nombre\":\"Alan\",\"apellido\":\"Brito  Delgado\",\"email\":\"angelm3achaddo@hotmail.com\",\"fecha_nac\":\"04-07-2014\",\"estado\":\"14\",\"ciudad\":\"273\",\"direccion\":\"Barrio san carlos\",\"tlf\":\"09871234567\",\"grado\":\"8\",\"profesion\":\"16\",\"MAX_FILE_SIZE\":\"2097152\",\"btn_submit\":\"Guardar\"}', '2014-07-28 13:53:17'),
(167, 'administrador:estudiantes/export', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 13:57:48'),
(168, 'administrador:cursos/export', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 13:58:11'),
(169, 'administrador:certificados/export', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 13:58:19'),
(170, 'administrador:users/add', 1, '192.168.0.9', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 15:11:18'),
(171, 'administrador:estudiantes/add', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 15:12:09'),
(172, 'administrador:estudiantes/add', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\",\"cedula\":\"18083878\",\"nombre\":\"Orangel\",\"apellido\":\"Barrera\",\"email\":\"obarrerafranco@gmail.com\",\"fecha_nac\":\"01-08-2014\",\"estado\":\"4\",\"ciudad\":\"64\",\"direccion\":\"Urb. la barraca av. 99 este no. 139\",\"tlf\":\"584165431435\",\"grado\":\"6\",\"profesion\":\"16\",\"MAX_FILE_SIZE\":\"2097152\",\"btn_submit\":\"Guardar\"}', '2014-07-28 15:13:52'),
(173, 'administrador:estudiantes/edit', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"f_profession\":\"\",\"f_grado\":\"\",\"f_state\":\"\",\"f_city\":\"64\",\"id\":\"3\",\"module\":\"default\"}', '2014-07-28 15:14:58'),
(174, 'administrador:estudiantes/edit', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"module\":\"default\",\"id\":\"3\",\"cedula\":\"18083878\",\"nombre\":\"Orangel\",\"apellido\":\"Barrera\",\"email\":\"obarrerafranco@gmail.com\",\"fecha_nac\":\"27-02-1990\",\"estado\":\"4\",\"ciudad\":\"64\",\"direccion\":\"Urb. la barraca av. 99 este no. 139\",\"tlf\":\"584165431435\",\"grado\":\"6\",\"profesion\":\"16\",\"MAX_FILE_SIZE\":\"2097152\",\"btn_submit\":\"Guardar\"}', '2014-07-28 15:15:13'),
(175, 'administrador:estudiantes/add', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 15:16:08'),
(176, 'administrador:estudiantes/export', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 15:16:57'),
(177, 'administrador:estudiantes/export', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 15:17:38'),
(178, 'administrador:cursos/export', 1, '192.168.0.9', '{\"controller\":\"cursos\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 15:17:46'),
(179, 'administrador:cursos/add', 1, '192.168.0.9', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 15:17:56'),
(180, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 15:18:55'),
(181, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\",\"estudiante\":\"17985998\",\"curso\":\"1\",\"fecha_cap\":\"09-05-2012\",\"fecha_ven\":\"02-05-2012\",\"btn_submit\":\"Guardar\"}', '2014-07-28 15:19:19'),
(182, 'administrador:cursos/add', 1, '192.168.0.9', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\",\"codigo\":\"MT\",\"descripcion\":\"curso de montacarguistas\",\"duracion\":\"200\",\"btn_submit\":\"Guardar\"}', '2014-07-28 15:19:19'),
(183, 'administrador:cursos/export', 1, '192.168.0.9', '{\"controller\":\"cursos\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 15:19:41'),
(184, 'administrador:cursos/edit', 1, '192.168.0.9', '{\"controller\":\"cursos\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\"}', '2014-07-28 15:19:48'),
(185, 'administrador:cursos/edit', 1, '192.168.0.9', '{\"controller\":\"cursos\",\"action\":\"edit\",\"id\":\"1\",\"module\":\"default\",\"codigo\":\"MT\",\"descripcion\":\"Montacarguista\",\"duracion\":\"200\",\"btn_submit\":\"Guardar\"}', '2014-07-28 15:19:53'),
(186, 'administrador:certificados/add', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 15:20:52'),
(187, 'administrador:certificados/add', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\",\"estudiante\":\"18083878\",\"curso\":\"1\",\"fecha_cap\":\"30-06-2014\",\"fecha_ven\":\"03-07-2014\",\"btn_submit\":\"Guardar\"}', '2014-07-28 15:21:11'),
(188, 'administrador:certificados/add', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"add\",\"f_status\":\"1\",\"f_curso\":\"\",\"f_state\":\"\",\"module\":\"default\"}', '2014-07-28 15:21:28'),
(189, 'administrador:certificados/add', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-28 15:22:15'),
(190, 'administrador:certificados/add', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\",\"estudiante\":\"18083878\",\"curso\":\"1\",\"fecha_cap\":\"28-07-2014\",\"fecha_ven\":\"24-10-2014\",\"btn_submit\":\"Guardar\"}', '2014-07-28 15:22:37'),
(191, 'administrador:estudiantes/export', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-28 15:24:03'),
(192, 'administrador:certificados/edit', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"2\",\"module\":\"default\"}', '2014-07-28 15:28:14'),
(193, 'administrador:certificados/edit', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"2\",\"module\":\"default\",\"estudiante\":\"17985998\",\"curso\":\"1\",\"fecha_cap\":\"09-05-2012\",\"fecha_ven\":\"30-04-2012\",\"btn_submit\":\"Guardar\"}', '2014-07-28 15:28:20'),
(194, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 11:43:43'),
(195, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 11:43:56'),
(196, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 11:44:05'),
(197, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 14:52:27'),
(198, 'administrador:cursos/add', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 15:01:14'),
(199, 'administrador:cursos/add', 1, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\",\"codigo\":\"EM\",\"descripcion\":\"Experto Montacarguista\",\"duracion\":\"280\",\"btn_submit\":\"Guardar\"}', '2014-07-29 15:01:30'),
(200, 'administrador:certificados/edit', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\"}', '2014-07-29 15:08:06'),
(201, 'administrador:certificados/edit', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\",\"estudiante\":\"18083878\",\"curso\":\"1\",\"fecha_cap\":\"28-07-2014\",\"fecha_ven\":\"24-07-2014\",\"btn_submit\":\"Guardar\"}', '2014-07-29 15:08:19'),
(202, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 15:12:11'),
(203, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 15:13:33'),
(204, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 15:14:20'),
(205, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 15:15:32'),
(206, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 15:20:15'),
(207, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 15:21:37'),
(208, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 15:25:44'),
(209, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 15:26:20'),
(210, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 15:28:17'),
(211, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 15:58:03'),
(212, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 15:59:26'),
(213, 'administrador:estudiantes/add', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 15:59:55'),
(214, 'administrador:estudiantes/add', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\",\"cedula\":\"18083878\",\"nombre\":\"Orangel\",\"apellido\":\"Barrera\",\"email\":\"obarrerafranco@gmail.com\",\"fecha_nac\":\"15-05-1986\",\"estado\":\"4\",\"ciudad\":\"64\",\"direccion\":\"Urb. la barraca av. 99 este no. 139\",\"tlf\":\"04121574646\",\"grado\":\"6\",\"profesion\":\"38\",\"MAX_FILE_SIZE\":\"2097152\",\"btn_submit\":\"Guardar\"}', '2014-07-29 16:01:13'),
(215, 'administrador:estudiantes/edit', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\"}', '2014-07-29 16:01:47'),
(216, 'administrador:estudiantes/edit', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"module\":\"default\",\"id\":\"4\",\"cedula\":\"18083878\",\"nombre\":\"Orangel\",\"apellido\":\"Barrera Franco\",\"email\":\"obarrerafranco@gmail.com\",\"fecha_nac\":\"15-05-1986\",\"estado\":\"4\",\"ciudad\":\"64\",\"direccion\":\"Urb. la barraca av. 99 este no. 139\",\"tlf\":\"04121574646\",\"grado\":\"6\",\"profesion\":\"38\",\"MAX_FILE_SIZE\":\"2097152\",\"btn_submit\":\"Guardar\"}', '2014-07-29 16:01:56'),
(217, 'administrador:cursos/add', 1, '192.168.0.9', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 16:03:08'),
(218, 'administrador:cursos/add', 1, '192.168.0.9', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\",\"codigo\":\"Montacarguista 2\",\"descripcion\":\"curso de montacarguistas nivel 2\",\"duracion\":\"200\",\"btn_submit\":\"Guardar\"}', '2014-07-29 16:03:26'),
(219, 'administrador:cursos/edit', 1, '192.168.0.9', '{\"controller\":\"cursos\",\"action\":\"edit\",\"id\":\"2\",\"module\":\"default\"}', '2014-07-29 16:03:39'),
(220, 'administrador:cursos/edit', 1, '192.168.0.9', '{\"controller\":\"cursos\",\"action\":\"edit\",\"id\":\"2\",\"module\":\"default\",\"codigo\":\"EM\",\"descripcion\":\"Experto Montacarguista 1\",\"duracion\":\"280\",\"btn_submit\":\"Guardar\"}', '2014-07-29 16:03:44'),
(221, 'administrador:cursos/export', 1, '192.168.0.9', '{\"controller\":\"cursos\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-29 16:03:47'),
(222, 'administrador:certificados/add', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 16:04:45'),
(223, 'administrador:certificados/add', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\",\"estudiante\":\"18083878\",\"curso\":\"2\",\"fecha_cap\":\"10-07-2014\",\"fecha_ven\":\"19-07-2014\",\"btn_submit\":\"Guardar\"}', '2014-07-29 16:05:05'),
(224, 'administrador:certificados/export', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-07-29 16:06:06'),
(225, 'administrador:certificados/export', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"export\",\"f_status\":\"\",\"f_curso\":\"\",\"f_state\":\"4\",\"module\":\"default\"}', '2014-07-29 16:06:19'),
(226, 'administrador:certificados/edit', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"edit\",\"f_status\":\"\",\"f_curso\":\"\",\"f_state\":\"4\",\"f_city\":\"\",\"id\":\"5\",\"module\":\"default\"}', '2014-07-29 16:06:41'),
(227, 'administrador:certificados/edit', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"edit\",\"f_status\":\"\",\"f_curso\":\"\",\"f_state\":\"4\",\"f_city\":\"\",\"id\":\"5\",\"module\":\"default\",\"estudiante\":\"18083878\",\"curso\":\"2\",\"fecha_cap\":\"10-07-2014\",\"fecha_ven\":\"13-09-2014\",\"btn_submit\":\"Guardar\"}', '2014-07-29 16:06:46'),
(228, 'administrador:certificados/edit', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"5\",\"module\":\"default\"}', '2014-07-29 16:06:57'),
(229, 'administrador:certificados/edit', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"5\",\"module\":\"default\",\"estudiante\":\"18083878\",\"curso\":\"2\",\"fecha_cap\":\"10-07-2014\",\"fecha_ven\":\"09-08-2014\",\"btn_submit\":\"Guardar\"}', '2014-07-29 16:07:05'),
(230, 'administrador:certificados/edit', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"5\",\"module\":\"default\"}', '2014-07-29 16:07:14'),
(231, 'administrador:certificados/edit', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"5\",\"module\":\"default\",\"estudiante\":\"18083878\",\"curso\":\"2\",\"fecha_cap\":\"10-07-2014\",\"fecha_ven\":\"10-10-2014\",\"btn_submit\":\"Guardar\"}', '2014-07-29 16:07:19'),
(232, 'administrador:certificados/edit', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"5\",\"module\":\"default\"}', '2014-07-29 16:07:30'),
(233, 'administrador:certificados/edit', 1, '192.168.0.9', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"5\",\"module\":\"default\",\"estudiante\":\"18083878\",\"curso\":\"2\",\"fecha_cap\":\"10-07-2014\",\"fecha_ven\":\"07-01-2015\",\"btn_submit\":\"Guardar\"}', '2014-07-29 16:07:37'),
(234, 'administrador:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-29 16:10:15'),
(235, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-30 10:07:15'),
(236, 'administrador:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-30 10:07:23'),
(237, 'administrador:estudiantes/add', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-30 16:00:58'),
(238, 'administrador:estudiantes/add', 1, '192.168.0.9', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\",\"cedula\":\"18083871\",\"nombre\":\"Reinaldo\",\"apellido\":\"P\\u00e9rez\",\"email\":\"obarrera@heureka.com.ve\",\"fecha_nac\":\"15-01-1980\",\"estado\":\"16\",\"ciudad\":\"325\",\"direccion\":\"8536 NW 66TH ST\",\"tlf\":\"17864862928\",\"grado\":\"7\",\"profesion\":\"15\",\"MAX_FILE_SIZE\":\"2097152\",\"btn_submit\":\"Guardar\"}', '2014-07-30 16:01:46'),
(239, 'administrador:users/add', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-31 14:01:16'),
(240, 'administrador:users/edit', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2014-07-31 14:01:28'),
(241, 'superadmin:users/add', 1, '192.168.0.9', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-31 14:28:03'),
(242, 'superadmin:users/add', 1, '192.168.0.9', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\",\"name\":\"Orangel\",\"lastname\":\"Franco\",\"state_id\":\"4\",\"username\":\"agenciahk\",\"role_id\":\"1\",\"password\":\"H3ur3k@\",\"password_conf\":\"H3ur3k@\",\"btn_submit\":\"Guardar\"}', '2014-07-31 14:28:32'),
(243, 'administrador:users/add', 4, '192.168.0.9', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-31 14:29:50'),
(244, 'administrador:users/add', 4, '192.168.0.9', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-31 14:38:42'),
(245, 'administrador:users/add', 4, '192.168.0.9', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-31 14:38:49'),
(246, 'administrador:users/edit', 4, '192.168.0.9', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2014-07-31 14:38:57'),
(247, 'superadmin:estudiantes/export', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"f_state\":\"24\",\"module\":\"default\"}', '2014-07-31 16:47:35'),
(248, 'superadmin:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-31 16:47:41'),
(249, 'superadmin:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\",\"cedula\":\"17985798\",\"nombre\":\"miguel angel\",\"apellido\":\"martinez\",\"email\":\"angelm3achado@hotmail.com\",\"fecha_nac\":\"08-07-2014\",\"estado\":\"17\",\"ciudad\":\"351\",\"direccion\":\"san carlos \",\"tlf\":\"09871234567\",\"grado\":\"9\",\"profesion\":\"15\",\"MAX_FILE_SIZE\":\"2097152\",\"btn_submit\":\"Guardar\"}', '2014-07-31 16:48:29'),
(250, 'superadmin:certificados/export', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"f_state\":\"24\",\"module\":\"default\"}', '2014-07-31 16:56:25'),
(251, 'superadmin:estudiantes/add', 1, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-07-31 16:58:53'),
(252, 'superadmin:users/add', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-01 09:28:13'),
(253, 'superadmin:users/add', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\",\"name\":\"Miguel\",\"lastname\":\"Machado\",\"state_id\":\"4\",\"username\":\"miguelmachadoaa\",\"role_id\":\"1\",\"password\":\"v16339893V\",\"password_conf\":\"v16339893V\",\"btn_submit\":\"Guardar\"}', '2014-08-01 09:28:56'),
(254, 'administrador:estudiantes/add', 5, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-01 09:30:57'),
(255, 'administrador:cursos/add', 5, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-01 09:31:04'),
(256, 'administrador:certificados/add', 5, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-01 09:31:09'),
(257, 'administrador:users/add', 5, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-01 09:34:12'),
(258, 'administrador:estudiantes/add', 5, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-01 09:34:20'),
(259, 'administrador:cursos/add', 5, '192.168.0.12', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-01 09:34:24'),
(260, 'administrador:certificados/add', 5, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"f_status\":\"\",\"f_curso\":\"\",\"f_state\":\"\",\"module\":\"default\"}', '2014-08-01 09:34:30'),
(261, 'administrador:estudiantes/add', 5, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-01 11:03:21');
INSERT INTO `hk_audit_trail` (`id`, `message`, `user_id`, `user_ip`, `data`, `timestamp`) VALUES
(262, 'administrador:estudiantes/add', 5, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\",\"cedula\":\"32435465\",\"nombre\":\"Michael \",\"apellido\":\"Jordan \",\"email\":\"michael@jordan.com\",\"fecha_nac\":\"06\\/08\\/2014\",\"estado\":\"16\",\"ciudad\":\"332\",\"direccion\":\"turmero \",\"tlf\":\"02432345465\",\"grado\":\"9\",\"profesion\":\"9\",\"MAX_FILE_SIZE\":\"2097152\",\"btn_submit\":\"Guardar\"}', '2014-08-01 11:04:41'),
(263, 'superadmin:users/add', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-22 09:34:21'),
(264, 'superadmin:users/add', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\",\"name\":\"capacitacion\",\"lastname\":\"capacitacion\",\"state_id\":\"4\",\"username\":\"Capacitacion\",\"role_id\":\"1\",\"password\":\"Cc123456\",\"password_conf\":\"Cc123456\",\"btn_submit\":\"Guardar\"}', '2014-08-22 09:35:10'),
(265, 'superadmin:users/edit', 1, '192.168.0.10', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2014-08-22 10:12:02'),
(266, 'superadmin:users/edit', 1, '192.168.0.10', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\",\"name\":\"Orangel\",\"lastname\":\"Barrera\",\"state_id\":\"4\",\"username\":\"obarrera\",\"role_id\":\"2\",\"password\":\"H3ur3k@\",\"password_conf\":\"H3ur3k@\",\"btn_submit\":\"Guardar\"}', '2014-08-22 10:12:17'),
(267, 'superadmin:users/edit', 1, '192.168.0.10', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2014-08-22 10:12:53'),
(268, 'superadmin:users/edit', 1, '192.168.0.10', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\",\"name\":\"Orangel\",\"lastname\":\"Barrera\",\"state_id\":\"4\",\"username\":\"obarrera\",\"role_id\":\"1\",\"password\":\"\",\"password_conf\":\"\",\"btn_submit\":\"Guardar\"}', '2014-08-22 10:12:58'),
(269, 'administrador:estudiantes/add', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"f_profession\":\"\",\"f_grado\":\"\",\"f_state\":\"\",\"module\":\"default\"}', '2014-08-22 11:39:56'),
(270, 'administrador:estudiantes/add', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-22 11:45:01'),
(271, 'administrador:estudiantes/add', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-22 11:49:55'),
(272, 'administrador:estudiantes/add', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-22 12:01:43'),
(273, 'administrador:estudiantes/add', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-22 12:55:33'),
(274, 'administrador:estudiantes/edit', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"7\",\"module\":\"default\"}', '2014-08-22 12:56:23'),
(275, 'administrador:estudiantes/export', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"f_profession\":\"\",\"f_grado\":\"\",\"f_state\":\"16\",\"module\":\"default\"}', '2014-08-22 12:56:49'),
(276, 'administrador:estudiantes/export', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-08-22 12:57:09'),
(277, 'administrador:estudiantes/edit', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"7\",\"module\":\"default\"}', '2014-08-22 13:55:16'),
(278, 'administrador:estudiantes/export', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-08-22 13:57:59'),
(279, 'administrador:estudiantes/add', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-22 14:03:16'),
(280, 'administrador:estudiantes/add', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-22 14:49:51'),
(281, 'administrador:cursos/add', 3, '192.168.0.10', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-22 14:59:35'),
(282, 'administrador:cursos/add', 3, '192.168.0.10', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-22 15:05:02'),
(283, 'administrador:certificados/add', 3, '192.168.0.10', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-25 10:16:27'),
(284, 'administrador:certificados/edit', 3, '192.168.0.10', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\"}', '2014-08-25 10:44:35'),
(285, 'administrador:certificados/add', 3, '192.168.0.10', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-25 10:44:44'),
(286, 'administrador:certificados/add', 3, '192.168.0.10', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-08-25 11:21:37'),
(287, 'superadmin:users/del', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"del\",\"module\":\"default\",\"uid\":\"5\"}', '2014-09-04 09:16:12'),
(288, 'superadmin:users/add', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-04 09:16:17'),
(289, 'superadmin:users/add', 1, '192.168.0.12', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\",\"name\":\"miguel\",\"lastname\":\"machado\",\"state_id\":\"1\",\"username\":\"miguelmachadoaa\",\"role_id\":\"1\",\"password\":\"v16339893V\",\"password_conf\":\"v16339893V\",\"btn_submit\":\"Guardar\"}', '2014-09-04 09:16:51'),
(290, 'administrador:certificados/add', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-04 09:17:44'),
(291, 'administrador:certificados/add', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-04 09:19:34'),
(292, 'administrador:certificados/edit', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\"}', '2014-09-04 09:19:42'),
(293, 'administrador:certificados/edit', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\"}', '2014-09-04 09:24:22'),
(294, 'administrador:certificados/edit', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\"}', '2014-09-04 09:29:46'),
(295, 'administrador:certificados/edit', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\"}', '2014-09-04 09:31:27'),
(296, 'administrador:certificados/edit', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\"}', '2014-09-04 09:31:31'),
(297, 'administrador:certificados/edit', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\"}', '2014-09-04 09:31:38'),
(298, 'administrador:certificados/edit', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\"}', '2014-09-04 09:35:03'),
(299, 'administrador:certificados/add', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-04 09:35:20'),
(300, 'administrador:certificados/add', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-04 09:35:54'),
(301, 'administrador:certificados/add', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\",\"codigo\":\"mt00012\",\"estudiante\":\"32435465\",\"curso\":\"3\",\"fecha_cap\":\"01\\/09\\/2014\",\"fecha_ven\":\"01\\/09\\/2017\",\"btn_submit\":\"Guardar\"}', '2014-09-04 09:36:54'),
(302, 'administrador:certificados/edit', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\"}', '2014-09-04 09:38:46'),
(303, 'administrador:estudiantes/add', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-04 09:50:53'),
(304, 'administrador:estudiantes/edit', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"7\",\"module\":\"default\"}', '2014-09-04 09:51:00'),
(305, 'superadmin:certificados/add', 1, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-05 11:20:40'),
(306, 'administrador:estudiantes/add', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-05 13:17:32'),
(307, 'administrador:estudiantes/edit', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"6\",\"module\":\"default\"}', '2014-09-05 13:32:40'),
(308, 'administrador:estudiantes/add', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-08 08:41:45'),
(309, 'administrador:cursos/add', 3, '192.168.0.10', '{\"controller\":\"cursos\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-08 08:45:35'),
(310, 'administrador:certificados/add', 3, '192.168.0.10', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-08 08:49:05'),
(311, 'administrador:estudiantes/add', 3, '192.168.0.10', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-08 09:23:50'),
(312, 'administrador:estudiantes/add', 2, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-18 14:17:35'),
(313, 'administrador:estudiantes/add', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-29 10:57:32'),
(314, 'administrador:certificados/add', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-29 10:57:39'),
(315, 'administrador:certificados/add', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"add\",\"module\":\"default\"}', '2014-09-29 15:43:28'),
(316, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 16:01:41'),
(317, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 16:02:15'),
(318, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 16:04:44'),
(319, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 16:07:19'),
(320, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 16:07:38'),
(321, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 16:10:15'),
(322, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 16:10:26'),
(323, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 16:50:09'),
(324, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 16:51:02'),
(325, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 16:51:52'),
(326, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 16:52:23'),
(327, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 16:52:37'),
(328, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 16:53:37'),
(329, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 17:00:29'),
(330, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-29 17:00:52'),
(331, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-30 09:22:27'),
(332, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-30 09:38:40'),
(333, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-30 09:49:15'),
(334, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-30 09:52:22'),
(335, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-30 12:02:13'),
(336, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-30 12:02:22'),
(337, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"f_status\":\"2\",\"f_curso\":\"\",\"f_state\":\"\",\"module\":\"default\"}', '2014-09-30 12:04:16'),
(338, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-30 12:04:53'),
(339, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-30 12:05:26'),
(340, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-30 12:14:12'),
(341, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-09-30 12:14:32'),
(342, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-10-01 12:53:22'),
(343, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-10-01 12:54:23'),
(344, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-10-01 12:58:00'),
(345, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-10-02 15:20:30'),
(346, 'administrador:certificados/edit', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2014-10-02 15:20:38'),
(347, 'administrador:estudiantes/export', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2014-10-02 15:20:45'),
(348, 'administrador:estudiantes/edit', 7, '192.168.0.12', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"4\",\"module\":\"default\"}', '2014-10-02 15:20:52'),
(349, 'administrador:certificados/export', 7, '192.168.0.12', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2014-10-03 16:59:47'),
(350, 'administrador:certificados/export', 7, '::1', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2015-07-29 05:10:28'),
(351, 'administrador:estudiantes/edit', 7, '::1', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"5\",\"module\":\"default\"}', '2015-07-29 05:11:46'),
(352, 'administrador:estudiantes/edit', 7, '::1', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"module\":\"default\"}', '2015-07-29 05:12:21'),
(353, 'administrador:estudiantes/edit', 7, '::1', '{\"controller\":\"estudiantes\",\"action\":\"edit\",\"id\":\"5\",\"module\":\"default\"}', '2015-07-29 05:12:29'),
(354, 'administrador:estudiantes/export', 7, '::1', '{\"controller\":\"estudiantes\",\"action\":\"export\",\"module\":\"default\"}', '2015-12-15 01:42:40'),
(355, 'administrador:certificados/export', 7, '::1', '{\"controller\":\"certificados\",\"action\":\"export\",\"module\":\"default\"}', '2015-12-15 01:43:16'),
(356, 'administrador:users/add', 7, '::1', '{\"controller\":\"users\",\"action\":\"add\",\"module\":\"default\"}', '2016-02-05 04:53:08'),
(357, 'administrador:productos/add', 7, '::1', '{\"controller\":\"productos\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-17 03:55:25'),
(358, 'administrador:productos/add', 7, '::1', '{\"controller\":\"productos\",\"action\":\"add\",\"module\":\"default\",\"codigo\":\"001\",\"descripcion\":\"Cerveza\",\"precio\":\"400\",\"btn_submit\":\"Guardar\"}', '2016-10-17 03:56:05'),
(359, 'administrador:productos/edit', 7, '::1', '{\"controller\":\"productos\",\"action\":\"edit\",\"id\":\"6\",\"module\":\"default\"}', '2016-10-17 03:56:24'),
(360, 'administrador:productos/edit', 7, '::1', '{\"controller\":\"productos\",\"action\":\"edit\",\"id\":\"6\",\"module\":\"default\"}', '2016-10-17 03:57:10'),
(361, 'administrador:productos/edit', 7, '::1', '{\"controller\":\"productos\",\"action\":\"edit\",\"module\":\"default\",\"id\":\"6\",\"codigo\":\"001\",\"descripcion\":\"Cerveza Peque\\u00f1a\",\"precio\":\"400.00\",\"btn_submit\":\"Guardar\"}', '2016-10-17 03:57:32'),
(362, 'administrador:clientes/add', 7, '::1', '{\"controller\":\"clientes\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-17 04:17:54'),
(363, 'administrador:clientes/add', 7, '::1', '{\"controller\":\"clientes\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-17 04:18:59'),
(364, 'administrador:clientes/add', 7, '::1', '{\"controller\":\"clientes\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-17 04:19:20'),
(365, 'administrador:clientes/add', 7, '::1', '{\"controller\":\"clientes\",\"action\":\"add\",\"module\":\"default\",\"cedula\":\"3454234324234234\",\"nombre\":\"test\",\"email\":\"test@test.com\",\"fecha_nac\":\"11\\/11\\/1986\",\"estado\":\"3\",\"direccion\":\"dsfsfafsdf\",\"tlf\":\"04121574646\",\"btn_submit\":\"Guardar\"}', '2016-10-17 04:19:55'),
(366, 'administrador:clientes/edit', 7, '::1', '{\"controller\":\"clientes\",\"action\":\"edit\",\"id\":\"7\",\"module\":\"default\"}', '2016-10-17 04:22:18'),
(367, 'administrador:clientes/edit', 7, '::1', '{\"controller\":\"clientes\",\"action\":\"edit\",\"module\":\"default\",\"id\":\"7\",\"cedula\":\"2147483647\",\"nombre\":\"test2\",\"email\":\"test@test.com\",\"fecha_nac\":\"11\\/11\\/1986\",\"estado\":\"3\",\"direccion\":\"dsfsfafsdf\",\"tlf\":\"04121574646\",\"btn_submit\":\"Guardar\"}', '2016-10-17 04:22:37'),
(368, 'administrador:productos/add', 7, '::1', '{\"controller\":\"productos\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-22 10:59:45'),
(369, 'administrador:productos/add', 7, '::1', '{\"controller\":\"productos\",\"action\":\"add\",\"module\":\"default\",\"codigo\":\"hw\",\"descripcion\":\"Hora Wake\",\"precio\":\"4000\",\"btn_submit\":\"Guardar\"}', '2016-10-22 11:00:15'),
(370, 'administrador:productos/add', 7, '::1', '{\"controller\":\"productos\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-22 11:18:26'),
(371, 'administrador:productos/add', 7, '::1', '{\"controller\":\"productos\",\"action\":\"add\",\"module\":\"default\",\"codigo\":\"001\",\"descripcion\":\"Vaso con Hielo\",\"precio\":\"100\",\"btn_submit\":\"Guardar\"}', '2016-10-22 11:19:13'),
(372, 'administrador:productos/edit', 7, '::1', '{\"controller\":\"productos\",\"action\":\"edit\",\"id\":\"8\",\"module\":\"default\"}', '2016-10-22 11:19:22'),
(373, 'administrador:productos/edit', 7, '::1', '{\"controller\":\"productos\",\"action\":\"edit\",\"module\":\"default\",\"id\":\"8\",\"codigo\":\"002\",\"descripcion\":\"Vaso con Hielo\",\"precio\":\"100.00\",\"btn_submit\":\"Guardar\"}', '2016-10-22 11:19:32'),
(374, 'administrador:productos/add', 7, '::1', '{\"controller\":\"productos\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-22 11:19:35'),
(375, 'administrador:productos/add', 7, '::1', '{\"controller\":\"productos\",\"action\":\"add\",\"module\":\"default\",\"codigo\":\"003\",\"descripcion\":\"Refresco\",\"precio\":\"500\",\"btn_submit\":\"Guardar\"}', '2016-10-22 11:19:46'),
(376, 'administrador:productos/add', 7, '::1', '{\"controller\":\"productos\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-22 11:19:52'),
(377, 'administrador:productos/add', 7, '::1', '{\"controller\":\"productos\",\"action\":\"add\",\"module\":\"default\",\"codigo\":\"004\",\"descripcion\":\"Perro Caliente\",\"precio\":\"1000\",\"btn_submit\":\"Guardar\"}', '2016-10-22 11:20:04'),
(378, 'administrador:pedidos/export', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"export\",\"module\":\"default\"}', '2016-10-23 20:05:50'),
(379, 'administrador:pedidos/export', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"export\",\"module\":\"default\"}', '2016-10-23 20:43:29'),
(380, 'administrador:pedidos/edit', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"edit\",\"id\":\"20\",\"module\":\"default\"}', '2016-10-23 20:55:14'),
(381, 'administrador:users/edit', 7, '::1', '{\"controller\":\"users\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2016-10-23 22:05:06'),
(382, 'administrador:clientes/edit', 7, '::1', '{\"controller\":\"clientes\",\"action\":\"edit\",\"id\":\"7\",\"module\":\"default\"}', '2016-10-23 22:24:37'),
(383, 'administrador:clientes/edit', 7, '::1', '{\"controller\":\"clientes\",\"action\":\"edit\",\"id\":\"7\",\"module\":\"default\"}', '2016-10-23 22:25:28'),
(384, 'administrador:clientes/add', 7, '::1', '{\"controller\":\"clientes\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-23 23:22:33'),
(385, 'administrador:clientes/add', 7, '::1', '{\"controller\":\"clientes\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-23 23:30:17'),
(386, 'administrador:empleados/add', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-23 23:31:53'),
(387, 'administrador:empleados/add', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-23 23:33:22'),
(388, 'administrador:empleados/add', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"add\",\"module\":\"default\",\"cedula\":\"16339893\",\"nombre\":\"Miguel Machado \",\"email\":\"miguelmachadoaa@gmail.com\",\"fecha_nac\":\"13\\/05\\/1984\",\"estado\":\"4\",\"direccion\":\"Maracauy\",\"tlf\":\"04121574646\",\"btn_submit\":\"Guardar\"}', '2016-10-23 23:34:07'),
(389, 'administrador:clientes/add', 7, '::1', '{\"controller\":\"clientes\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-23 23:34:08'),
(390, 'administrador:clientes/add', 7, '::1', '{\"controller\":\"clientes\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-23 23:34:45'),
(391, 'administrador:empleados/add', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-23 23:34:50'),
(392, 'administrador:empleados/add', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-23 23:35:00'),
(393, 'administrador:empleados/add', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-23 23:40:54'),
(394, 'administrador:empleados/add', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"add\",\"module\":\"default\",\"cedula\":\"16339893\",\"nombre\":\"Miguel Machado\",\"email\":\"miguelmachadoaa@gmail.com\",\"fecha_nac\":\"13\\/05\\/1984\",\"estado\":\"4\",\"direccion\":\"Maracauy\",\"tlf\":\"04121574646\",\"btn_submit\":\"Guardar\"}', '2016-10-23 23:41:31'),
(395, 'administrador:empleados/add', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-23 23:41:31'),
(396, 'administrador:empleados/add', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-23 23:44:22'),
(397, 'administrador:empleados/add', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"add\",\"module\":\"default\",\"cedula\":\"16339893\",\"nombre\":\"Miguel Machado\",\"email\":\"miguelmachadoaa@gmail.com\",\"fecha_nac\":\"13\\/05\\/1984\",\"estado\":\"\",\"direccion\":\"Maracauy\",\"tlf\":\"04121574646\",\"btn_submit\":\"Guardar\"}', '2016-10-23 23:44:49'),
(398, 'administrador:empleados/add', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-23 23:49:05'),
(399, 'administrador:empleados/add', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"add\",\"module\":\"default\",\"cedula\":\"123456\",\"nombre\":\"empleado \",\"email\":\"empleado@empleado.com\",\"fecha_nac\":\"08\\/10\\/2016\",\"estado\":\"4\",\"direccion\":\"Maracauy\",\"tlf\":\"04121574646\",\"btn_submit\":\"Guardar\"}', '2016-10-23 23:49:50'),
(400, 'administrador:empleados/edit', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"edit\",\"id\":\"2\",\"module\":\"default\"}', '2016-10-23 23:49:56'),
(401, 'administrador:empleados/edit', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"edit\",\"id\":\"2\",\"module\":\"default\"}', '2016-10-23 23:51:04'),
(402, 'administrador:empleados/edit', 7, '::1', '{\"controller\":\"empleados\",\"action\":\"edit\",\"module\":\"default\",\"id\":\"2\",\"cedula\":\"123456\",\"nombre\":\"empleado \",\"email\":\"empleado@empleado.com\",\"fecha_nac\":\"08\\/10\\/2016\",\"estado\":\"4\",\"direccion\":\"Maracauy\",\"tlf\":\"04121574646\",\"btn_submit\":\"Guardar\"}', '2016-10-23 23:51:11'),
(403, 'administrador:pedidos/edit', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2016-10-25 02:32:21'),
(404, 'administrador:pedidos/edit', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2016-10-25 02:33:21'),
(405, 'administrador:pedidos/edit', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2016-10-25 02:34:19'),
(406, 'administrador:pedidos/edit', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2016-10-25 02:35:03'),
(407, 'administrador:pedidos/edit', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2016-10-25 11:08:20'),
(408, 'administrador:pedidos/edit', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2016-10-26 10:12:51'),
(409, 'administrador:pedidos/edit', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"edit\",\"id\":\"30\",\"module\":\"default\"}', '2016-10-26 10:13:48'),
(410, 'administrador:productos/add', 7, '::1', '{\"controller\":\"productos\",\"action\":\"add\",\"module\":\"default\"}', '2016-10-26 10:14:17'),
(411, 'administrador:pedidos/edit', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2016-10-26 10:17:04'),
(412, 'administrador:pedidos/edit', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2016-10-26 10:48:18'),
(413, 'administrador:pedidos/edit', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2016-10-26 10:59:43'),
(414, 'administrador:pedidos/edit', 7, '::1', '{\"controller\":\"pedidos\",\"action\":\"edit\",\"id\":\"3\",\"module\":\"default\"}', '2016-10-26 10:59:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_bienes`
--

CREATE TABLE `hk_bienes` (
  `id` int(11) NOT NULL,
  `id_mueble` varchar(128) NOT NULL,
  `tipo_bien` varchar(512) NOT NULL,
  `cantidad_bien` varchar(128) NOT NULL,
  `marca` varchar(128) NOT NULL,
  `modelo` varchar(128) NOT NULL,
  `edad_bien` varchar(128) NOT NULL,
  `conservacion_bien` varchar(128) NOT NULL,
  `tecnologia_bien` varchar(128) NOT NULL,
  `mantenimiento_bien` varchar(128) NOT NULL,
  `trabajo_bien` varchar(128) NOT NULL,
  `observacion_bien` varchar(4096) NOT NULL,
  `imagen` varchar(1024) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` varchar(128) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hk_bienes`
--

INSERT INTO `hk_bienes` (`id`, `id_mueble`, `tipo_bien`, `cantidad_bien`, `marca`, `modelo`, `edad_bien`, `conservacion_bien`, `tecnologia_bien`, `mantenimiento_bien`, `trabajo_bien`, `observacion_bien`, `imagen`, `fecha`, `estatus`) VALUES
(1, '1479968746', 'vehiculo', '1', '1', '1', '1', 'Bueno', 'Cambios en Tecnologia', 'Normal', '', 'Observaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y ComentariosObservaciones y Comentarios', '0', '2016-11-24 02:25:50', '1'),
(2, '1480534922', 'carro ', '1', 'fiat', 'palio weedken ', '6', 'Regular', 'Cambios en Produccion', 'Normal', '', 'fiat palio año 2006', '0', '2016-11-30 15:45:08', '1'),
(3, '1480535765', 'aragua', 'aragua', 'aragua', 'aragua', '11', 'Excelente', 'Misma Tecnologia', 'Riguroso', '', 'araguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaraguaaragua', '0', '2016-11-30 15:59:34', '1'),
(4, '1480539090', '0000028', '0000028', '0000028', '0000028', '5', 'Muy Bueno', 'Algunos Cambios', 'Riguroso', '', '0000028000002800000280000028000002800000280000028000002800000280000028000002800000280000028000002800000280000028000002800000280000028000002800028028', '0', '2016-11-30 16:52:36', '1'),
(5, '1480560962', '', '', '', '', '', '', '', '', '', '', '0', '2016-11-30 22:56:35', '1'),
(6, '1480561188', '', '', '', '', '', '', '', '', '', '', '0', '2016-11-30 23:00:07', '1'),
(7, '1485457477', 'Carro', '1', 'fiat ', 'palio wekend', '17', 'Excelente', 'Misma Tecnologia', 'Perfecto', '', 'fefsaf fdasfafaffaf asf asff af', '1', '2017-01-26 15:05:14', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_categoria`
--

CREATE TABLE `hk_categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(128) CHARACTER SET latin1 NOT NULL,
  `descripcion` varchar(128) CHARACTER SET latin1 NOT NULL,
  `estatus` varchar(12) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_categoria`
--

INSERT INTO `hk_categoria` (`id`, `nombre`, `descripcion`, `estatus`) VALUES
(1, 'Tecnologia', 'tecnologia', '1'),
(2, 'Ciencia', 'ciencia', '1'),
(3, 'Sistemas Operativos', 'sistemas-operativos', '1'),
(4, 'Juegos', 'juegos', '1'),
(5, 'Maracay', 'maracay', '1'),
(6, 'Turmero', 'turmero', '1'),
(7, 'Victoria', 'victoria', '1'),
(8, 'Cagua', 'cagua', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_cities`
--

CREATE TABLE `hk_cities` (
  `id` int(11) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city` varchar(200) CHARACTER SET utf8 NOT NULL,
  `capital` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_cities`
--

INSERT INTO `hk_cities` (`id`, `state_id`, `city`, `capital`) VALUES
(1, 1, 'Maroa', 0),
(2, 1, 'Puerto Ayacucho', 1),
(3, 1, 'San Fernando de Atabapo', 0),
(4, 2, 'Anaco', 0),
(5, 2, 'Aragua de Barcelona', 0),
(6, 2, 'Barcelona', 1),
(7, 2, 'Boca de Uchire', 0),
(8, 2, 'Cantaura', 0),
(9, 2, 'Clarines', 0),
(10, 2, 'El Chaparro', 0),
(11, 2, 'El Pao Anzoátegui', 0),
(12, 2, 'El Tigre', 0),
(13, 2, 'El Tigrito', 0),
(14, 2, 'Guanape', 0),
(15, 2, 'Guanta', 0),
(16, 2, 'Lechería', 0),
(17, 2, 'Onoto', 0),
(18, 2, 'Pariaguán', 0),
(19, 2, 'Píritu', 0),
(20, 2, 'Puerto La Cruz', 0),
(21, 2, 'Puerto Píritu', 0),
(22, 2, 'Sabana de Uchire', 0),
(23, 2, 'San Mateo Anzoátegui', 0),
(24, 2, 'San Pablo Anzoátegui', 0),
(25, 2, 'San Tomé', 0),
(26, 2, 'Santa Ana de Anzoátegui', 0),
(27, 2, 'Santa Fe Anzoátegui', 0),
(28, 2, 'Santa Rosa', 0),
(29, 2, 'Soledad', 0),
(30, 2, 'Urica', 0),
(31, 2, 'Valle de Guanape', 0),
(43, 3, 'Achaguas', 0),
(44, 3, 'Biruaca', 0),
(45, 3, 'Bruzual', 0),
(46, 3, 'El Amparo', 0),
(47, 3, 'El Nula', 0),
(48, 3, 'Elorza', 0),
(49, 3, 'Guasdualito', 0),
(50, 3, 'Mantecal', 0),
(51, 3, 'Puerto Páez', 0),
(52, 3, 'San Fernando de Apure', 1),
(53, 3, 'San Juan de Payara', 0),
(54, 4, 'Barbacoas', 0),
(55, 4, 'Cagua', 0),
(56, 4, 'Camatagua', 0),
(58, 4, 'Choroní', 0),
(59, 4, 'Colonia Tovar', 0),
(60, 4, 'El Consejo', 0),
(61, 4, 'La Victoria', 0),
(62, 4, 'Las Tejerías', 0),
(63, 4, 'Magdaleno', 0),
(64, 4, 'Maracay', 1),
(65, 4, 'Ocumare de La Costa', 0),
(66, 4, 'Palo Negro', 0),
(67, 4, 'San Casimiro', 0),
(68, 4, 'San Mateo', 0),
(69, 4, 'San Sebastián', 0),
(70, 4, 'Santa Cruz de Aragua', 0),
(71, 4, 'Tocorón', 0),
(72, 4, 'Turmero', 0),
(73, 4, 'Villa de Cura', 0),
(74, 4, 'Zuata', 0),
(75, 5, 'Barinas', 1),
(76, 5, 'Barinitas', 0),
(77, 5, 'Barrancas', 0),
(78, 5, 'Calderas', 0),
(79, 5, 'Capitanejo', 0),
(80, 5, 'Ciudad Bolivia', 0),
(81, 5, 'El Cantón', 0),
(82, 5, 'Las Veguitas', 0),
(83, 5, 'Libertad de Barinas', 0),
(84, 5, 'Sabaneta', 0),
(85, 5, 'Santa Bárbara de Barinas', 0),
(86, 5, 'Socopó', 0),
(87, 6, 'Caicara del Orinoco', 0),
(88, 6, 'Canaima', 0),
(89, 6, 'Ciudad Bolívar', 1),
(90, 6, 'Ciudad Piar', 0),
(91, 6, 'El Callao', 0),
(92, 6, 'El Dorado', 0),
(93, 6, 'El Manteco', 0),
(94, 6, 'El Palmar', 0),
(95, 6, 'El Pao', 0),
(96, 6, 'Guasipati', 0),
(97, 6, 'Guri', 0),
(98, 6, 'La Paragua', 0),
(99, 6, 'Matanzas', 0),
(100, 6, 'Puerto Ordaz', 0),
(101, 6, 'San Félix', 0),
(102, 6, 'Santa Elena de Uairén', 0),
(103, 6, 'Tumeremo', 0),
(104, 6, 'Unare', 0),
(105, 6, 'Upata', 0),
(106, 7, 'Bejuma', 0),
(107, 7, 'Belén', 0),
(108, 7, 'Campo de Carabobo', 0),
(109, 7, 'Canoabo', 0),
(110, 7, 'Central Tacarigua', 0),
(111, 7, 'Chirgua', 0),
(112, 7, 'Ciudad Alianza', 0),
(113, 7, 'El Palito', 0),
(114, 7, 'Guacara', 0),
(115, 7, 'Guigue', 0),
(116, 7, 'Las Trincheras', 0),
(117, 7, 'Los Guayos', 0),
(118, 7, 'Mariara', 0),
(119, 7, 'Miranda', 0),
(120, 7, 'Montalbán', 0),
(121, 7, 'Morón', 0),
(122, 7, 'Naguanagua', 0),
(123, 7, 'Puerto Cabello', 0),
(124, 7, 'San Joaquín', 0),
(125, 7, 'Tocuyito', 0),
(126, 7, 'Urama', 0),
(127, 7, 'Valencia', 1),
(128, 7, 'Vigirimita', 0),
(129, 8, 'Aguirre', 0),
(130, 8, 'Apartaderos Cojedes', 0),
(131, 8, 'Arismendi', 0),
(132, 8, 'Camuriquito', 0),
(133, 8, 'El Baúl', 0),
(134, 8, 'El Limón', 0),
(135, 8, 'El Pao Cojedes', 0),
(136, 8, 'El Socorro', 0),
(137, 8, 'La Aguadita', 0),
(138, 8, 'Las Vegas', 0),
(139, 8, 'Libertad de Cojedes', 0),
(140, 8, 'Mapuey', 0),
(141, 8, 'Piñedo', 0),
(142, 8, 'Samancito', 0),
(143, 8, 'San Carlos', 1),
(144, 8, 'Sucre', 0),
(145, 8, 'Tinaco', 0),
(146, 8, 'Tinaquillo', 0),
(147, 8, 'Vallecito', 0),
(148, 9, 'Tucupita', 1),
(149, 24, 'Caracas', 1),
(150, 24, 'El Junquito', 0),
(151, 10, 'Adícora', 0),
(152, 10, 'Boca de Aroa', 0),
(153, 10, 'Cabure', 0),
(154, 10, 'Capadare', 0),
(155, 10, 'Capatárida', 0),
(156, 10, 'Chichiriviche', 0),
(157, 10, 'Churuguara', 0),
(158, 10, 'Coro', 1),
(159, 10, 'Cumarebo', 0),
(160, 10, 'Dabajuro', 0),
(161, 10, 'Judibana', 0),
(162, 10, 'La Cruz de Taratara', 0),
(163, 10, 'La Vela de Coro', 0),
(164, 10, 'Los Taques', 0),
(165, 10, 'Maparari', 0),
(166, 10, 'Mene de Mauroa', 0),
(167, 10, 'Mirimire', 0),
(168, 10, 'Pedregal', 0),
(169, 10, 'Píritu Falcón', 0),
(170, 10, 'Pueblo Nuevo Falcón', 0),
(171, 10, 'Puerto Cumarebo', 0),
(172, 10, 'Punta Cardón', 0),
(173, 10, 'Punto Fijo', 0),
(174, 10, 'San Juan de Los Cayos', 0),
(175, 10, 'San Luis', 0),
(176, 10, 'Santa Ana Falcón', 0),
(177, 10, 'Santa Cruz De Bucaral', 0),
(178, 10, 'Tocopero', 0),
(179, 10, 'Tocuyo de La Costa', 0),
(180, 10, 'Tucacas', 0),
(181, 10, 'Yaracal', 0),
(182, 11, 'Altagracia de Orituco', 0),
(183, 11, 'Cabruta', 0),
(184, 11, 'Calabozo', 0),
(185, 11, 'Camaguán', 0),
(196, 11, 'Chaguaramas Guárico', 0),
(197, 11, 'El Socorro', 0),
(198, 11, 'El Sombrero', 0),
(199, 11, 'Las Mercedes de Los Llanos', 0),
(200, 11, 'Lezama', 0),
(201, 11, 'Onoto', 0),
(202, 11, 'Ortíz', 0),
(203, 11, 'San José de Guaribe', 0),
(204, 11, 'San Juan de Los Morros', 1),
(205, 11, 'San Rafael de Laya', 0),
(206, 11, 'Santa María de Ipire', 0),
(207, 11, 'Tucupido', 0),
(208, 11, 'Valle de La Pascua', 0),
(209, 11, 'Zaraza', 0),
(210, 12, 'Aguada Grande', 0),
(211, 12, 'Atarigua', 0),
(212, 12, 'Barquisimeto', 1),
(213, 12, 'Bobare', 0),
(214, 12, 'Cabudare', 0),
(215, 12, 'Carora', 0),
(216, 12, 'Cubiro', 0),
(217, 12, 'Cují', 0),
(218, 12, 'Duaca', 0),
(219, 12, 'El Manzano', 0),
(220, 12, 'El Tocuyo', 0),
(221, 12, 'Guaríco', 0),
(222, 12, 'Humocaro Alto', 0),
(223, 12, 'Humocaro Bajo', 0),
(224, 12, 'La Miel', 0),
(225, 12, 'Moroturo', 0),
(226, 12, 'Quíbor', 0),
(227, 12, 'Río Claro', 0),
(228, 12, 'Sanare', 0),
(229, 12, 'Santa Inés', 0),
(230, 12, 'Sarare', 0),
(231, 12, 'Siquisique', 0),
(232, 12, 'Tintorero', 0),
(233, 13, 'Apartaderos Mérida', 0),
(234, 13, 'Arapuey', 0),
(235, 13, 'Bailadores', 0),
(236, 13, 'Caja Seca', 0),
(237, 13, 'Canaguá', 0),
(238, 13, 'Chachopo', 0),
(239, 13, 'Chiguara', 0),
(240, 13, 'Ejido', 0),
(241, 13, 'El Vigía', 0),
(242, 13, 'La Azulita', 0),
(243, 13, 'La Playa', 0),
(244, 13, 'Lagunillas Mérida', 0),
(245, 13, 'Mérida', 1),
(246, 13, 'Mesa de Bolívar', 0),
(247, 13, 'Mucuchíes', 0),
(248, 13, 'Mucujepe', 0),
(249, 13, 'Mucuruba', 0),
(250, 13, 'Nueva Bolivia', 0),
(251, 13, 'Palmarito', 0),
(252, 13, 'Pueblo Llano', 0),
(253, 13, 'Santa Cruz de Mora', 0),
(254, 13, 'Santa Elena de Arenales', 0),
(255, 13, 'Santo Domingo', 0),
(256, 13, 'Tabáy', 0),
(257, 13, 'Timotes', 0),
(258, 13, 'Torondoy', 0),
(259, 13, 'Tovar', 0),
(260, 13, 'Tucani', 0),
(261, 13, 'Zea', 0),
(262, 14, 'Araguita', 0),
(263, 14, 'Carrizal', 0),
(264, 14, 'Caucagua', 0),
(265, 14, 'Chaguaramas Miranda', 0),
(266, 14, 'Charallave', 0),
(267, 14, 'Chirimena', 0),
(268, 14, 'Chuspa', 0),
(269, 14, 'Cúa', 0),
(270, 14, 'Cupira', 0),
(271, 14, 'Curiepe', 0),
(272, 14, 'El Guapo', 0),
(273, 14, 'El Jarillo', 0),
(274, 14, 'Filas de Mariche', 0),
(275, 14, 'Guarenas', 0),
(276, 14, 'Guatire', 0),
(277, 14, 'Higuerote', 0),
(278, 14, 'Los Anaucos', 0),
(279, 14, 'Los Teques', 1),
(280, 14, 'Ocumare del Tuy', 0),
(281, 14, 'Panaquire', 0),
(282, 14, 'Paracotos', 0),
(283, 14, 'Río Chico', 0),
(284, 14, 'San Antonio de Los Altos', 0),
(285, 14, 'San Diego de Los Altos', 0),
(286, 14, 'San Fernando del Guapo', 0),
(287, 14, 'San Francisco de Yare', 0),
(288, 14, 'San José de Los Altos', 0),
(289, 14, 'San José de Río Chico', 0),
(290, 14, 'San Pedro de Los Altos', 0),
(291, 14, 'Santa Lucía', 0),
(292, 14, 'Santa Teresa', 0),
(293, 14, 'Tacarigua de La Laguna', 0),
(294, 14, 'Tacarigua de Mamporal', 0),
(295, 14, 'Tácata', 0),
(296, 14, 'Turumo', 0),
(297, 15, 'Aguasay', 0),
(298, 15, 'Aragua de Maturín', 0),
(299, 15, 'Barrancas del Orinoco', 0),
(300, 15, 'Caicara de Maturín', 0),
(301, 15, 'Caripe', 0),
(302, 15, 'Caripito', 0),
(303, 15, 'Chaguaramal', 0),
(305, 15, 'Chaguaramas Monagas', 0),
(307, 15, 'El Furrial', 0),
(308, 15, 'El Tejero', 0),
(309, 15, 'Jusepín', 0),
(310, 15, 'La Toscana', 0),
(311, 15, 'Maturín', 1),
(312, 15, 'Miraflores', 0),
(313, 15, 'Punta de Mata', 0),
(314, 15, 'Quiriquire', 0),
(315, 15, 'San Antonio de Maturín', 0),
(316, 15, 'San Vicente Monagas', 0),
(317, 15, 'Santa Bárbara', 0),
(318, 15, 'Temblador', 0),
(319, 15, 'Teresen', 0),
(320, 15, 'Uracoa', 0),
(321, 16, 'Altagracia', 0),
(322, 16, 'Boca de Pozo', 0),
(323, 16, 'Boca de Río', 0),
(324, 16, 'El Espinal', 0),
(325, 16, 'El Valle del Espíritu Santo', 0),
(326, 16, 'El Yaque', 0),
(327, 16, 'Juangriego', 0),
(328, 16, 'La Asunción', 1),
(329, 16, 'La Guardia', 0),
(330, 16, 'Pampatar', 0),
(331, 16, 'Porlamar', 0),
(332, 16, 'Puerto Fermín', 0),
(333, 16, 'Punta de Piedras', 0),
(334, 16, 'San Francisco de Macanao', 0),
(335, 16, 'San Juan Bautista', 0),
(336, 16, 'San Pedro de Coche', 0),
(337, 16, 'Santa Ana de Nueva Esparta', 0),
(338, 16, 'Villa Rosa', 0),
(339, 17, 'Acarigua', 0),
(340, 17, 'Agua Blanca', 0),
(341, 17, 'Araure', 0),
(342, 17, 'Biscucuy', 0),
(343, 17, 'Boconoito', 0),
(344, 17, 'Campo Elías', 0),
(345, 17, 'Chabasquén', 0),
(346, 17, 'Guanare', 1),
(347, 17, 'Guanarito', 0),
(348, 17, 'La Aparición', 0),
(349, 17, 'La Misión', 0),
(350, 17, 'Mesa de Cavacas', 0),
(351, 17, 'Ospino', 0),
(352, 17, 'Papelón', 0),
(353, 17, 'Payara', 0),
(354, 17, 'Pimpinela', 0),
(355, 17, 'Píritu de Portuguesa', 0),
(356, 17, 'San Rafael de Onoto', 0),
(357, 17, 'Santa Rosalía', 0),
(358, 17, 'Turén', 0),
(359, 18, 'Altos de Sucre', 0),
(360, 18, 'Araya', 0),
(361, 18, 'Cariaco', 0),
(362, 18, 'Carúpano', 0),
(363, 18, 'Casanay', 0),
(364, 18, 'Cumaná', 1),
(365, 18, 'Cumanacoa', 0),
(366, 18, 'El Morro Puerto Santo', 0),
(367, 18, 'El Pilar', 0),
(368, 18, 'El Poblado', 0),
(369, 18, 'Guaca', 0),
(370, 18, 'Guiria', 0),
(371, 18, 'Irapa', 0),
(372, 18, 'Manicuare', 0),
(373, 18, 'Mariguitar', 0),
(374, 18, 'Río Caribe', 0),
(375, 18, 'San Antonio del Golfo', 0),
(376, 18, 'San José de Aerocuar', 0),
(377, 18, 'San Vicente de Sucre', 0),
(378, 18, 'Santa Fe de Sucre', 0),
(379, 18, 'Tunapuy', 0),
(380, 18, 'Yaguaraparo', 0),
(381, 18, 'Yoco', 0),
(382, 19, 'Abejales', 0),
(383, 19, 'Borota', 0),
(384, 19, 'Bramon', 0),
(385, 19, 'Capacho', 0),
(386, 19, 'Colón', 0),
(387, 19, 'Coloncito', 0),
(388, 19, 'Cordero', 0),
(389, 19, 'El Cobre', 0),
(390, 19, 'El Pinal', 0),
(391, 19, 'Independencia', 0),
(392, 19, 'La Fría', 0),
(393, 19, 'La Grita', 0),
(394, 19, 'La Pedrera', 0),
(395, 19, 'La Tendida', 0),
(396, 19, 'Las Delicias', 0),
(397, 19, 'Las Hernández', 0),
(398, 19, 'Lobatera', 0),
(399, 19, 'Michelena', 0),
(400, 19, 'Palmira', 0),
(401, 19, 'Pregonero', 0),
(402, 19, 'Queniquea', 0),
(403, 19, 'Rubio', 0),
(404, 19, 'San Antonio del Tachira', 0),
(405, 19, 'San Cristobal', 1),
(406, 19, 'San José de Bolívar', 0),
(407, 19, 'San Josecito', 0),
(408, 19, 'San Pedro del Río', 0),
(409, 19, 'Santa Ana Táchira', 0),
(410, 19, 'Seboruco', 0),
(411, 19, 'Táriba', 0),
(412, 19, 'Umuquena', 0),
(413, 19, 'Ureña', 0),
(414, 20, 'Batatal', 0),
(415, 20, 'Betijoque', 0),
(416, 20, 'Boconó', 0),
(417, 20, 'Carache', 0),
(418, 20, 'Chejende', 0),
(419, 20, 'Cuicas', 0),
(420, 20, 'El Dividive', 0),
(421, 20, 'El Jaguito', 0),
(422, 20, 'Escuque', 0),
(423, 20, 'Isnotú', 0),
(424, 20, 'Jajó', 0),
(425, 20, 'La Ceiba', 0),
(426, 20, 'La Concepción de Trujllo', 0),
(427, 20, 'La Mesa de Esnujaque', 0),
(428, 20, 'La Puerta', 0),
(429, 20, 'La Quebrada', 0),
(430, 20, 'Mendoza Fría', 0),
(431, 20, 'Meseta de Chimpire', 0),
(432, 20, 'Monay', 0),
(433, 20, 'Motatán', 0),
(434, 20, 'Pampán', 0),
(435, 20, 'Pampanito', 0),
(436, 20, 'Sabana de Mendoza', 0),
(437, 20, 'San Lázaro', 0),
(438, 20, 'Santa Ana de Trujillo', 0),
(439, 20, 'Tostós', 0),
(440, 20, 'Trujillo', 1),
(441, 20, 'Valera', 0),
(442, 21, 'Carayaca', 0),
(443, 21, 'Litoral', 0),
(444, 25, 'Archipiélago Los Roques', 0),
(445, 22, 'Aroa', 0),
(446, 22, 'Boraure', 0),
(447, 22, 'Campo Elías de Yaracuy', 0),
(448, 22, 'Chivacoa', 0),
(449, 22, 'Cocorote', 0),
(450, 22, 'Farriar', 0),
(451, 22, 'Guama', 0),
(452, 22, 'Marín', 0),
(453, 22, 'Nirgua', 0),
(454, 22, 'Sabana de Parra', 0),
(455, 22, 'Salom', 0),
(456, 22, 'San Felipe', 1),
(457, 22, 'San Pablo de Yaracuy', 0),
(458, 22, 'Urachiche', 0),
(459, 22, 'Yaritagua', 0),
(460, 22, 'Yumare', 0),
(461, 23, 'Bachaquero', 0),
(462, 23, 'Bobures', 0),
(463, 23, 'Cabimas', 0),
(464, 23, 'Campo Concepción', 0),
(465, 23, 'Campo Mara', 0),
(466, 23, 'Campo Rojo', 0),
(467, 23, 'Carrasquero', 0),
(468, 23, 'Casigua', 0),
(469, 23, 'Chiquinquirá', 0),
(470, 23, 'Ciudad Ojeda', 0),
(471, 23, 'El Batey', 0),
(472, 23, 'El Carmelo', 0),
(473, 23, 'El Chivo', 0),
(474, 23, 'El Guayabo', 0),
(475, 23, 'El Mene', 0),
(476, 23, 'El Venado', 0),
(477, 23, 'Encontrados', 0),
(478, 23, 'Gibraltar', 0),
(479, 23, 'Isla de Toas', 0),
(480, 23, 'La Concepción del Zulia', 0),
(481, 23, 'La Paz', 0),
(482, 23, 'La Sierrita', 0),
(483, 23, 'Lagunillas del Zulia', 0),
(484, 23, 'Las Piedras de Perijá', 0),
(485, 23, 'Los Cortijos', 0),
(486, 23, 'Machiques', 0),
(487, 23, 'Maracaibo', 1),
(488, 23, 'Mene Grande', 0),
(489, 23, 'Palmarejo', 0),
(490, 23, 'Paraguaipoa', 0),
(491, 23, 'Potrerito', 0),
(492, 23, 'Pueblo Nuevo del Zulia', 0),
(493, 23, 'Puertos de Altagracia', 0),
(494, 23, 'Punta Gorda', 0),
(495, 23, 'Sabaneta de Palma', 0),
(496, 23, 'San Francisco', 0),
(497, 23, 'San José de Perijá', 0),
(498, 23, 'San Rafael del Moján', 0),
(499, 23, 'San Timoteo', 0),
(500, 23, 'Santa Bárbara Del Zulia', 0),
(501, 23, 'Santa Cruz de Mara', 0),
(502, 23, 'Santa Cruz del Zulia', 0),
(503, 23, 'Santa Rita', 0),
(504, 23, 'Sinamaica', 0),
(505, 23, 'Tamare', 0),
(506, 23, 'Tía Juana', 0),
(507, 23, 'Villa del Rosario', 0),
(508, 21, 'La Guaira', 1),
(509, 21, 'Catia La Mar', 0),
(510, 21, 'Macuto', 0),
(511, 21, 'Naiguatá', 0),
(512, 25, 'Archipiélago Los Monjes', 0),
(513, 25, 'Isla La Tortuga y Cayos adyacentes', 0),
(514, 25, 'Isla La Sola', 0),
(515, 25, 'Islas Los Testigos', 0),
(516, 25, 'Islas Los Frailes', 0),
(517, 25, 'Isla La Orchila', 0),
(518, 25, 'Archipiélago Las Aves', 0),
(519, 25, 'Isla de Aves', 0),
(520, 25, 'Isla La Blanquilla', 0),
(521, 25, 'Isla de Patos', 0),
(522, 25, 'Islas Los Hermanos', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_clientes`
--

CREATE TABLE `hk_clientes` (
  `id` int(11) NOT NULL,
  `cedula` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tlf` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `estado` int(11) NOT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_nac` date NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='mayor de clientes ';

--
-- Volcado de datos para la tabla `hk_clientes`
--

INSERT INTO `hk_clientes` (`id`, `cedula`, `nombre`, `tlf`, `estado`, `direccion`, `fecha_nac`, `email`) VALUES
(0, 123456, 'cliente', 'telefono', 1, 'marcay', '2016-07-01', 'localhos@localhost.com'),
(1, 16339893, 'miguel machado Alvarado', '04121574646', 4, 'maracay ', '1984-05-13', 'miguelmachadoaa@gmail.com'),
(2, 17985998, 'marylin martinez', '04123429572', 2, 'maracay ', '1986-11-11', 'marylin_1111@hotmail.com'),
(3, 12, 'juan ', '04246589712', 1, 'maracay', '1896-11-11', 'juam@juan.com'),
(4, 7267245, 'Elizabeth Alvarado ', '04125054366', 4, 'maracay ', '1965-10-31', 'elizabeth@gmail.com'),
(5, 7265299, 'angel machado', '04120579865', 4, 'maracay ', '1955-05-31', 'angel@gmail.com'),
(6, 1234564, 'daniel ', '04243570548', 2, 'fasfsdfsdfsdfsdfsdf', '2016-06-01', 'daniel@daniel.com'),
(7, 2147483647, 'test2', '04121574646', 3, 'dsfsfafsdf', '1986-11-11', 'test@test.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_contacto`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_contacto' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_contacto`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_cursos`
--

CREATE TABLE `hk_cursos` (
  `id` int(11) NOT NULL,
  `codigo` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `duracion` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='mayor de cursos';

--
-- Volcado de datos para la tabla `hk_cursos`
--

INSERT INTO `hk_cursos` (`id`, `codigo`, `descripcion`, `duracion`) VALUES
(2, 'EM', 'Experto Montacarguista 1', 280),
(3, 'Montacarguista 2', 'curso de montacarguistas nivel 2', 200);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_detalle_pedido`
--

CREATE TABLE `hk_detalle_pedido` (
  `id` int(11) NOT NULL,
  `id_pedido` varchar(12) CHARACTER SET latin1 NOT NULL,
  `id_producto` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` float(12,2) NOT NULL,
  `total` float(12,2) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_detalle_pedido`
--

INSERT INTO `hk_detalle_pedido` (`id`, `id_pedido`, `id_producto`, `cantidad`, `precio`, `total`, `fecha`, `status`) VALUES
(18, '4', 8, 1, 100.00, 100.00, '2016-10-22 08:27:59', 1),
(19, '4', 9, 1, 500.00, 500.00, '2016-10-22 08:27:59', 1),
(20, '4', 6, 1, 400.00, 400.00, '2016-10-22 08:28:01', 1),
(21, '4', 6, 1, 400.00, 400.00, '2016-10-22 08:28:02', 1),
(22, '4', 6, 1, 400.00, 400.00, '2016-10-22 08:28:02', 1),
(23, '5', 6, 1, 400.00, 400.00, '2016-10-22 08:28:06', 1),
(28, '6', 6, 1, 400.00, 400.00, '2016-10-22 08:28:11', 1),
(29, '6', 10, 1, 1000.00, 1000.00, '2016-10-22 08:28:11', 1),
(30, '6', 7, 1, 4000.00, 4000.00, '2016-10-22 08:28:12', 1),
(31, '6', 8, 1, 100.00, 100.00, '2016-10-22 08:28:12', 1),
(32, '6', 10, 1, 1000.00, 1000.00, '2016-10-22 08:28:13', 1),
(35, '7', 6, 1, 400.00, 400.00, '2016-10-22 08:39:22', 1),
(36, '7', 6, 1, 400.00, 400.00, '2016-10-22 08:40:38', 1),
(37, '3', 6, 1, 400.00, 400.00, '2016-10-22 08:40:48', 1),
(38, '8', 6, 1, 400.00, 400.00, '2016-10-22 08:42:26', 1),
(39, '8', 7, 1, 4000.00, 4000.00, '2016-10-22 08:42:55', 1),
(40, '8', 6, 1, 400.00, 400.00, '2016-10-22 08:45:24', 1),
(41, '8', 10, 1, 1000.00, 1000.00, '2016-10-22 08:45:58', 1),
(42, '8', 10, 1, 1000.00, 1000.00, '2016-10-22 08:46:26', 1),
(43, '9', 6, 1, 400.00, 400.00, '2016-10-22 08:46:46', 1),
(44, '9', 7, 1, 4000.00, 4000.00, '2016-10-22 08:46:50', 1),
(45, '10', 6, 1, 400.00, 400.00, '2016-10-22 08:48:26', 1),
(46, '3', 6, 1, 400.00, 400.00, '2016-10-22 08:50:36', 1),
(47, '4', 6, 1, 400.00, 400.00, '2016-10-22 08:50:42', 1),
(49, '6', 6, 1, 400.00, 400.00, '2016-10-22 08:50:53', 1),
(50, '7', 6, 1, 400.00, 400.00, '2016-10-22 08:50:56', 1),
(52, '9', 6, 1, 400.00, 400.00, '2016-10-22 08:51:03', 1),
(53, '10', 6, 1, 400.00, 400.00, '2016-10-22 08:51:07', 1),
(54, '11', 10, 1, 1000.00, 1000.00, '2016-10-22 08:51:12', 1),
(55, '11', 10, 1, 1000.00, 1000.00, '2016-10-22 08:51:12', 1),
(56, '11', 9, 1, 500.00, 500.00, '2016-10-22 08:51:13', 1),
(57, '11', 9, 1, 500.00, 500.00, '2016-10-22 08:51:13', 1),
(58, '11', 7, 1, 4000.00, 4000.00, '2016-10-22 08:51:15', 1),
(59, '12', 6, 1, 400.00, 400.00, '2016-10-22 08:51:20', 1),
(65, '12', 7, 1, 4000.00, 4000.00, '2016-10-22 08:51:24', 1),
(66, '12', 8, 1, 100.00, 100.00, '2016-10-22 08:51:25', 1),
(67, '12', 8, 1, 100.00, 100.00, '2016-10-22 08:51:25', 1),
(68, '12', 9, 1, 500.00, 500.00, '2016-10-22 08:51:26', 1),
(69, '3', 6, 1, 400.00, 400.00, '2016-10-22 11:34:27', 1),
(70, '3', 7, 1, 4000.00, 4000.00, '2016-10-22 11:34:29', 1),
(71, '3', 8, 1, 100.00, 100.00, '2016-10-22 11:34:30', 1),
(72, '3', 9, 1, 500.00, 500.00, '2016-10-22 11:34:30', 1),
(73, '3', 10, 1, 1000.00, 1000.00, '2016-10-22 11:34:31', 1),
(74, '3', 10, 1, 1000.00, 1000.00, '2016-10-22 11:34:33', 1),
(75, '8', 6, 1, 400.00, 400.00, '2016-10-22 11:35:15', 1),
(76, '13', 6, 1, 400.00, 400.00, '2016-10-22 16:48:48', 1),
(77, '13', 6, 1, 400.00, 400.00, '2016-10-22 16:48:49', 1),
(78, '13', 7, 1, 4000.00, 4000.00, '2016-10-22 16:48:51', 1),
(79, '13', 10, 1, 1000.00, 1000.00, '2016-10-22 16:48:53', 1),
(80, '13', 6, 1, 400.00, 400.00, '2016-10-22 16:49:00', 1),
(81, '13', 6, 1, 400.00, 400.00, '2016-10-22 16:49:00', 1),
(82, '13', 10, 1, 1000.00, 1000.00, '2016-10-22 16:49:01', 1),
(83, '14', 6, 1, 400.00, 400.00, '2016-10-22 19:25:09', 1),
(84, '14', 7, 1, 4000.00, 4000.00, '2016-10-22 19:25:14', 1),
(85, '14', 8, 1, 100.00, 100.00, '2016-10-22 19:25:15', 1),
(86, '14', 10, 1, 1000.00, 1000.00, '2016-10-22 19:25:16', 1),
(87, '14', 9, 1, 500.00, 500.00, '2016-10-22 19:25:17', 1),
(88, '3', 6, 1, 400.00, 400.00, '2016-10-23 13:33:26', 1),
(89, '11', 6, 1, 400.00, 400.00, '2016-10-23 13:58:19', 1),
(90, '13', 6, 1, 400.00, 400.00, '2016-10-23 13:58:42', 1),
(91, '13', 6, 1, 400.00, 400.00, '2016-10-23 13:58:43', 1),
(92, '13', 7, 1, 4000.00, 4000.00, '2016-10-23 13:58:43', 1),
(93, '13', 7, 1, 4000.00, 4000.00, '2016-10-23 13:58:43', 1),
(94, '13', 10, 1, 1000.00, 1000.00, '2016-10-23 13:58:45', 1),
(95, '13', 9, 1, 500.00, 500.00, '2016-10-23 13:58:45', 1),
(96, '13', 9, 1, 500.00, 500.00, '2016-10-23 13:58:46', 1),
(97, '13', 9, 1, 500.00, 500.00, '2016-10-23 13:58:46', 1),
(98, '15', 6, 1, 400.00, 400.00, '2016-10-23 13:59:12', 1),
(99, '15', 6, 1, 400.00, 400.00, '2016-10-23 13:59:12', 1),
(100, '15', 7, 1, 4000.00, 4000.00, '2016-10-23 13:59:13', 1),
(101, '15', 7, 1, 4000.00, 4000.00, '2016-10-23 13:59:14', 1),
(102, '15', 9, 1, 500.00, 500.00, '2016-10-23 13:59:17', 1),
(103, '15', 10, 1, 1000.00, 1000.00, '2016-10-23 13:59:17', 1),
(104, '15', 10, 1, 1000.00, 1000.00, '2016-10-23 13:59:18', 1),
(105, '15', 6, 1, 400.00, 400.00, '2016-10-23 13:59:24', 1),
(106, '15', 6, 1, 400.00, 400.00, '2016-10-23 13:59:24', 1),
(107, '15', 7, 1, 4000.00, 4000.00, '2016-10-23 13:59:25', 1),
(108, '15', 7, 1, 4000.00, 4000.00, '2016-10-23 13:59:25', 1),
(109, '15', 10, 1, 1000.00, 1000.00, '2016-10-23 13:59:26', 1),
(110, '16', 6, 1, 400.00, 400.00, '2016-10-23 15:14:51', 1),
(111, '16', 6, 1, 400.00, 400.00, '2016-10-23 15:14:51', 1),
(112, '16', 7, 1, 4000.00, 4000.00, '2016-10-23 15:14:52', 1),
(113, '17', 6, 1, 400.00, 400.00, '2016-10-23 15:15:01', 1),
(114, '17', 6, 1, 400.00, 400.00, '2016-10-23 15:15:02', 1),
(115, '17', 10, 1, 1000.00, 1000.00, '2016-10-23 15:15:02', 1),
(116, '17', 9, 1, 500.00, 500.00, '2016-10-23 15:15:04', 1),
(117, '17', 9, 1, 500.00, 500.00, '2016-10-23 15:15:04', 1),
(119, '19', 10, 1, 1000.00, 1000.00, '2016-10-23 15:15:26', 1),
(120, '19', 10, 1, 1000.00, 1000.00, '2016-10-23 15:15:26', 1),
(121, '19', 6, 1, 400.00, 400.00, '2016-10-23 15:15:27', 1),
(122, '19', 6, 1, 400.00, 400.00, '2016-10-23 15:15:27', 1),
(123, '19', 7, 1, 4000.00, 4000.00, '2016-10-23 15:15:29', 1),
(124, '16', 10, 1, 1000.00, 1000.00, '2016-10-23 15:15:35', 1),
(125, '16', 10, 1, 1000.00, 1000.00, '2016-10-23 15:15:36', 1),
(126, '16', 6, 1, 400.00, 400.00, '2016-10-23 15:15:37', 1),
(127, '16', 6, 1, 400.00, 400.00, '2016-10-23 15:15:37', 1),
(128, '17', 6, 1, 400.00, 400.00, '2016-10-23 15:15:43', 1),
(129, '17', 6, 1, 400.00, 400.00, '2016-10-23 15:15:43', 1),
(130, '17', 10, 1, 1000.00, 1000.00, '2016-10-23 15:15:45', 1),
(131, '17', 10, 1, 1000.00, 1000.00, '2016-10-23 15:15:45', 1),
(132, '18', 6, 1, 400.00, 400.00, '2016-10-23 15:15:50', 1),
(133, '18', 6, 1, 400.00, 400.00, '2016-10-23 15:15:50', 1),
(134, '18', 6, 1, 400.00, 400.00, '2016-10-23 15:15:50', 1),
(136, '18', 7, 1, 4000.00, 4000.00, '2016-10-23 15:16:01', 1),
(137, '18', 6, 1, 400.00, 400.00, '2016-10-23 15:16:40', 1),
(138, '18', 6, 1, 400.00, 400.00, '2016-10-23 15:16:40', 1),
(139, '18', 6, 1, 400.00, 400.00, '2016-10-23 15:16:43', 1),
(140, '18', 6, 1, 400.00, 400.00, '2016-10-23 15:16:44', 1),
(141, '18', 6, 1, 400.00, 400.00, '2016-10-23 15:16:50', 1),
(142, '18', 6, 1, 400.00, 400.00, '2016-10-23 15:16:50', 1),
(143, '18', 6, 1, 400.00, 400.00, '2016-10-23 15:16:53', 1),
(144, '18', 6, 1, 400.00, 400.00, '2016-10-23 15:16:53', 1),
(145, '19', 6, 1, 400.00, 400.00, '2016-10-23 15:16:56', 1),
(146, '19', 6, 1, 400.00, 400.00, '2016-10-23 15:16:57', 1),
(147, '20', 8, 1, 100.00, 100.00, '2016-10-23 15:17:07', 1),
(148, '20', 8, 1, 100.00, 100.00, '2016-10-23 15:17:07', 1),
(149, '20', 9, 1, 500.00, 500.00, '2016-10-23 15:17:08', 1),
(150, '20', 9, 1, 500.00, 500.00, '2016-10-23 15:17:08', 1),
(151, '21', 6, 1, 400.00, 400.00, '2016-10-23 16:52:15', 1),
(152, '21', 7, 1, 4000.00, 4000.00, '2016-10-23 16:52:18', 1),
(153, '21', 8, 1, 100.00, 100.00, '2016-10-23 16:52:21', 1),
(154, '21', 9, 1, 500.00, 500.00, '2016-10-23 16:52:26', 1),
(155, '21', 10, 1, 1000.00, 1000.00, '2016-10-23 16:52:33', 1),
(156, '21', 6, 1, 400.00, 400.00, '2016-10-23 16:52:37', 1),
(157, '21', 7, 1, 4000.00, 4000.00, '2016-10-23 16:52:39', 1),
(160, '22', 7, 1, 4000.00, 4000.00, '2016-10-23 17:08:22', 1),
(161, '22', 8, 1, 100.00, 100.00, '2016-10-23 17:08:23', 1),
(162, '22', 8, 1, 100.00, 100.00, '2016-10-23 17:08:24', 1),
(163, '22', 9, 1, 500.00, 500.00, '2016-10-23 17:08:24', 1),
(164, '22', 9, 1, 500.00, 500.00, '2016-10-23 17:08:25', 1),
(165, '22', 10, 1, 1000.00, 1000.00, '2016-10-23 17:08:25', 1),
(166, '22', 6, 1, 400.00, 400.00, '2016-10-23 17:08:27', 1),
(167, '23', 6, 1, 400.00, 400.00, '2016-10-23 17:27:46', 1),
(168, '23', 7, 1, 4000.00, 4000.00, '2016-10-23 17:27:47', 1),
(169, '23', 8, 1, 100.00, 100.00, '2016-10-23 17:27:48', 1),
(170, '23', 9, 1, 500.00, 500.00, '2016-10-23 17:27:48', 1),
(171, '23', 8, 1, 100.00, 100.00, '2016-10-23 17:27:48', 1),
(172, '23', 6, 1, 400.00, 400.00, '2016-10-23 17:27:49', 1),
(173, '23', 7, 1, 4000.00, 4000.00, '2016-10-23 17:27:50', 1),
(174, '23', 6, 1, 400.00, 400.00, '2016-10-23 17:27:51', 1),
(175, '23', 8, 1, 100.00, 100.00, '2016-10-23 17:27:52', 1),
(176, '23', 9, 1, 500.00, 500.00, '2016-10-23 17:27:52', 1),
(177, '23', 10, 1, 1000.00, 1000.00, '2016-10-23 17:27:53', 1),
(178, '23', 10, 1, 1000.00, 1000.00, '2016-10-23 17:27:53', 1),
(179, '23', 6, 1, 400.00, 400.00, '2016-10-23 17:27:54', 1),
(180, '23', 6, 1, 400.00, 400.00, '2016-10-23 17:27:54', 1),
(181, '24', 6, 1, 400.00, 400.00, '2016-10-23 17:27:59', 1),
(182, '24', 7, 1, 4000.00, 4000.00, '2016-10-23 17:28:02', 1),
(183, '25', 6, 1, 400.00, 400.00, '2016-10-23 17:28:06', 1),
(184, '25', 7, 1, 4000.00, 4000.00, '2016-10-23 17:28:07', 1),
(185, '25', 8, 1, 100.00, 100.00, '2016-10-23 17:28:08', 1),
(186, '25', 9, 1, 500.00, 500.00, '2016-10-23 17:28:08', 1),
(187, '25', 10, 1, 1000.00, 1000.00, '2016-10-23 17:28:08', 1),
(188, '27', 6, 1, 400.00, 400.00, '2016-10-23 17:28:12', 1),
(189, '27', 7, 1, 4000.00, 4000.00, '2016-10-23 17:28:13', 1),
(190, '27', 7, 1, 4000.00, 4000.00, '2016-10-23 17:28:14', 1),
(191, '27', 6, 1, 400.00, 400.00, '2016-10-23 17:28:22', 1),
(192, '28', 6, 1, 400.00, 400.00, '2016-10-23 17:28:25', 1),
(194, '28', 6, 1, 400.00, 400.00, '2016-10-23 17:28:27', 1),
(195, '28', 6, 1, 400.00, 400.00, '2016-10-23 17:28:27', 1),
(197, '26', 7, 1, 4000.00, 4000.00, '2016-10-23 17:55:28', 1),
(198, '26', 8, 1, 100.00, 100.00, '2016-10-23 17:55:29', 1),
(199, '26', 9, 1, 500.00, 500.00, '2016-10-23 17:55:29', 1),
(200, '26', 10, 1, 1000.00, 1000.00, '2016-10-23 17:55:32', 1),
(201, '28', 7, 1, 4000.00, 4000.00, '2016-10-23 22:35:26', 1),
(202, '28', 8, 1, 100.00, 100.00, '2016-10-23 22:35:28', 1),
(203, '28', 10, 1, 1000.00, 1000.00, '2016-10-23 22:35:30', 1),
(204, '29', 6, 1, 400.00, 400.00, '2016-10-23 22:40:37', 1),
(205, '29', 8, 1, 100.00, 100.00, '2016-10-23 22:40:37', 1),
(206, '29', 9, 1, 500.00, 500.00, '2016-10-23 22:40:38', 1),
(207, '29', 8, 1, 100.00, 100.00, '2016-10-23 22:40:41', 1),
(208, '29', 10, 1, 1000.00, 1000.00, '2016-10-23 22:40:42', 1),
(209, '30', 6, 1, 400.00, 400.00, '2016-10-24 21:22:52', 1),
(210, '30', 8, 1, 100.00, 100.00, '2016-10-24 21:22:52', 1),
(211, '30', 7, 1, 4000.00, 4000.00, '2016-10-24 21:22:59', 1),
(212, '30', 9, 1, 500.00, 500.00, '2016-10-24 21:23:01', 1),
(213, '31', 6, 1, 400.00, 400.00, '2016-10-24 21:25:13', 1),
(214, '31', 6, 1, 400.00, 400.00, '2016-10-24 21:25:14', 1),
(215, '31', 6, 1, 400.00, 400.00, '2016-10-24 21:25:14', 1),
(216, '31', 6, 1, 400.00, 400.00, '2016-10-24 21:25:15', 1),
(217, '31', 10, 1, 1000.00, 1000.00, '2016-10-24 21:25:17', 1),
(218, '31', 10, 1, 1000.00, 1000.00, '2016-10-24 21:25:17', 1),
(219, '31', 7, 1, 4000.00, 4000.00, '2016-10-24 21:25:19', 1),
(220, '32', 6, 1, 400.00, 400.00, '2016-10-26 08:25:49', 1),
(221, '32', 7, 1, 4000.00, 4000.00, '2016-10-26 08:25:50', 1),
(222, '32', 8, 1, 100.00, 100.00, '2016-10-26 08:25:51', 1),
(223, '32', 9, 1, 500.00, 500.00, '2016-10-26 08:25:52', 1),
(224, '33', 10, 1, 1000.00, 1000.00, '2016-10-26 08:27:42', 1),
(225, '33', 6, 1, 400.00, 400.00, '2016-10-26 08:27:43', 1),
(226, '33', 7, 1, 4000.00, 4000.00, '2016-10-26 08:27:44', 1),
(227, '33', 8, 1, 100.00, 100.00, '2016-10-26 08:27:44', 1),
(228, '33', 9, 1, 500.00, 500.00, '2016-10-26 08:27:45', 1),
(229, '33', 6, 1, 400.00, 400.00, '2016-10-26 08:27:47', 1),
(230, '34', 6, 1, 400.00, 400.00, '2016-10-26 10:02:38', 1),
(231, '34', 6, 1, 400.00, 400.00, '2016-10-26 10:02:39', 1),
(232, '34', 6, 1, 400.00, 400.00, '2016-10-26 10:02:39', 1),
(233, '34', 6, 1, 400.00, 400.00, '2016-10-26 10:02:40', 1),
(234, '34', 6, 1, 400.00, 400.00, '2016-10-26 10:02:40', 1),
(235, '34', 6, 1, 400.00, 400.00, '2016-10-26 10:02:41', 1),
(236, '34', 6, 1, 400.00, 400.00, '2016-10-26 10:02:42', 1),
(237, '34', 6, 1, 400.00, 400.00, '2016-10-26 10:02:42', 1),
(238, '34', 6, 1, 400.00, 400.00, '2016-10-26 10:02:43', 1),
(239, '34', 6, 1, 400.00, 400.00, '2016-10-26 10:02:44', 1),
(240, '34', 6, 1, 400.00, 400.00, '2016-10-26 10:02:44', 1),
(241, '34', 6, 1, 400.00, 400.00, '2016-10-26 10:02:45', 1),
(242, '34', 7, 1, 4000.00, 4000.00, '2016-10-26 10:02:46', 1),
(243, '34', 7, 1, 4000.00, 4000.00, '2016-10-26 10:02:47', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_empleados`
--

CREATE TABLE `hk_empleados` (
  `id` int(11) NOT NULL,
  `cedula` int(11) NOT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `tlf` varchar(12) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` int(11) NOT NULL,
  `direccion` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `fecha_nac` date DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci COMMENT='mayor de empleados ';

--
-- Volcado de datos para la tabla `hk_empleados`
--

INSERT INTO `hk_empleados` (`id`, `cedula`, `nombre`, `tlf`, `estado`, `direccion`, `fecha_nac`, `email`) VALUES
(1, 16339893, 'Miguel Machado', '04121574646', 1, 'Maracauy', '1984-05-13', 'miguelmachadoaa@gmail.com'),
(2, 123456, 'empleado ', '04121574646', 4, 'Maracauy', '2016-10-08', 'empleado@empleado.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_estatus_pedido`
--

CREATE TABLE `hk_estatus_pedido` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(128) CHARACTER SET latin1 NOT NULL,
  `clase` varchar(128) CHARACTER SET latin1 NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_estatus_pedido`
--

INSERT INTO `hk_estatus_pedido` (`id`, `descripcion`, `clase`, `estatus`) VALUES
(1, 'Recibido', 'primary', 0),
(2, 'Pagado', 'success', 0),
(3, 'Cancelado', 'danger', 0),
(4, 'Entregado', 'warning', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_estudiantes`
--

CREATE TABLE `hk_estudiantes` (
  `id` int(11) NOT NULL,
  `cedula` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tlf` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `grado` int(11) NOT NULL,
  `profesion` int(11) NOT NULL,
  `estado` int(11) NOT NULL,
  `ciudad` int(11) NOT NULL,
  `direccion` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_nac` date NOT NULL,
  `foto` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'img',
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='mayor de estudiantes ';

--
-- Volcado de datos para la tabla `hk_estudiantes`
--

INSERT INTO `hk_estudiantes` (`id`, `cedula`, `nombre`, `apellido`, `tlf`, `grado`, `profesion`, `estado`, `ciudad`, `direccion`, `fecha_nac`, `foto`, `email`) VALUES
(4, 18083878, 'Orangel', 'Barrera Franco', '04121574646', 6, 38, 4, 64, 'Urb. la barraca av. 99 este no. 139', '1986-05-15', 'assets/img/5be36a981fc01e5882c42f7e8b088134.jpg', 'obarrerafranco@gmail.com'),
(5, 18083871, 'Reinaldo', 'PÃ©rez', '17864862928', 7, 15, 16, 325, '8536 NW 66TH ST', '1980-01-15', 'assets/img/9f3db797b5f613c639207731112e41ec.jpg', 'obarrera@heureka.com.ve'),
(6, 17985798, 'miguel angel', 'martinez', '09871234567', 9, 15, 17, 351, 'san carlos ', '2014-07-08', 'assets/img/c046f1b7fe7173eece2e7862e9f07e5d.jpg', 'angelm3achado@hotmail.com'),
(7, 32435465, 'Michael ', 'Jordan ', '02432345465', 9, 9, 16, 332, 'turmero ', '2014-08-06', 'assets/img/bf0293ab089636b58b96470836187dcd.jpg', 'michael@jordan.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_estudiante_curso`
--

CREATE TABLE `hk_estudiante_curso` (
  `id` int(11) NOT NULL,
  `codigo` varchar(128) CHARACTER SET latin1 NOT NULL,
  `id_estudiante` int(11) DEFAULT NULL,
  `id_curso` int(11) DEFAULT NULL,
  `fecha_cap` date DEFAULT NULL,
  `fecha_ven` date DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_estudiante_curso`
--

INSERT INTO `hk_estudiante_curso` (`id`, `codigo`, `id_estudiante`, `id_curso`, `fecha_cap`, `fecha_ven`, `status`) VALUES
(1, 'mt0001', 6, 3, '2011-03-15', '2014-07-30', 2),
(3, 'mt0003', 5, 3, '2014-07-28', '2014-07-24', 2),
(4, 'mt0004', 4, 2, '2014-07-10', '2015-01-07', 2),
(5, 'mt0005', 4, 3, '2014-07-10', '2014-07-19', 2),
(6, 'mt00012', 7, 3, '2014-09-01', '2017-09-01', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_etiquetas`
--

CREATE TABLE `hk_etiquetas` (
  `id` int(11) NOT NULL,
  `id_noticia` varchar(12) CHARACTER SET latin1 NOT NULL,
  `descripcion` varchar(256) CHARACTER SET latin1 NOT NULL,
  `estatus` varchar(12) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_etiquetas`
--

INSERT INTO `hk_etiquetas` (`id`, `id_noticia`, `descripcion`, `estatus`) VALUES
(136, '1480958958', 'Inmobiliario', '1'),
(135, '1480958958', 'Avaluos', '1'),
(137, '1480958958', 'inmubele', '1'),
(138, '1480958958', 'mobiliario', '1'),
(139, '1481410021', 'Avaluos', '1'),
(140, '1481410021', 'Personal', '1'),
(141, '1481410021', 'Mobiliario', '1'),
(142, '1481410021', 'Inmobiliario', '1'),
(143, '1481411638', 'Plusvalia', '1'),
(144, '1481411638', 'Inmueble', '1'),
(145, '1481411638', 'Factores', '1'),
(146, '1481411638', 'Claves', '1'),
(147, '1482283485', 'Ã‰xito', '1'),
(148, '1482283485', 'tirex2021', '1'),
(150, '1482283485', 'avalÃºo', '1'),
(151, '1482283485', 'tasaciÃ³n', '1'),
(152, '1483377884', 'TasaciÃ³n', '1'),
(153, '1483377884', 'AvalÃºos', '1'),
(154, '1483377884', 'Activos', '1'),
(159, '1483377884', 'Inmuebles', '1'),
(156, '1483377884', 'Muebles', '1'),
(157, '1483377884', 'Peritos', '1'),
(160, '1483377884', 'Tasador', '1'),
(161, '1483377884', 'Inmuebles', '1'),
(162, '1483377884', 'Tirex 2021', '1'),
(163, '1483640375', 'Crisis', '1'),
(164, '1483640375', 'inmobiliaria', '1'),
(165, '1483640375', 'inmuebles', '1'),
(166, '1483640375', 'avalÃºo', '1'),
(167, '1483640375', 'construcciÃ³n', '1'),
(168, '1483640375', 'proyectos', '1'),
(169, '1483640375', 'vivienda', '1'),
(170, '1483640375', 'instituciones', '1'),
(171, '1483640375', 'tirex2021', '1'),
(172, '1483973995', 'AvalÃºo', '1'),
(173, '1483973995', 'Tasador', '1'),
(174, '1483973995', 'Perito', '1'),
(175, '1483973995', 'Avaluador', '1'),
(176, '1483973995', 'Activos', '1'),
(177, '1483973995', 'Inmuebles', '1'),
(178, '1483973995', 'Inmobiliaria', '1'),
(179, '1483973995', 'Hipoteca', '1'),
(180, '1483973995', 'Siniestros', '1'),
(182, '1483973995', 'Inventario', '1'),
(183, '1483973995', 'CrÃ©ditos', '1'),
(185, '1481410021', 'Ã‰xito', '1'),
(186, '1481410021', 'Prosperidad', '1'),
(187, '1481410021', 'Salud', '1'),
(188, '1481410021', 'Felicidad', '1'),
(189, '1481410021', 'Paz', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_fotos`
--

CREATE TABLE `hk_fotos` (
  `id` int(11) NOT NULL,
  `id_solicitud` varchar(50) NOT NULL DEFAULT '0',
  `foto` varchar(512) NOT NULL DEFAULT '0',
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` varchar(50) NOT NULL DEFAULT '1',
  `posicion` varchar(50) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hk_fotos`
--

INSERT INTO `hk_fotos` (`id`, `id_solicitud`, `foto`, `fecha`, `estatus`, `posicion`) VALUES
(1, '1479967196', 'assets/img/c08fd0f2f0c073f2c8d2c2cc175d4232.png', '2016-11-24 02:01:01', '1', '0'),
(2, '1480534922', '806588f611951712_130956900583267_7470651693371000704_o.jpg', '2016-11-30 15:43:13', '1', '0'),
(3, '1480534922', '8c8c226f(9).jpg', '2016-11-30 15:43:35', '1', '0'),
(4, '1480534922', '9dd861c7(10).jpg', '2016-11-30 15:43:43', '1', '0'),
(5, '1480535765', 'af185c2aagencias_reembolso.jpg', '2016-11-30 15:57:40', '1', '0'),
(6, '1480539090', '08138be0(6).jpg', '2016-11-30 16:52:17', '1', '0'),
(7, '1480539090', '63741501(5).jpg', '2016-11-30 16:52:18', '1', '0'),
(8, '1480539090', 'eaa028e3(7).jpg', '2016-11-30 16:52:21', '1', '0'),
(9, '1480539090', '30f3f087(10).jpg', '2016-11-30 16:52:21', '1', '0'),
(10, '1480539090', '498a1c70(8).jpg', '2016-11-30 16:52:23', '1', '0'),
(11, '1480539090', '1d99cd0c(9).jpg', '2016-11-30 16:52:24', '1', '0'),
(12, '1480554912', 'ced1c1197pDoP2C.png', '2016-11-30 21:16:40', '1', '0'),
(13, '1480555029', '711e2ec5Puzzle-Dragons-Z-Puzzle-Dragons-Super-Mario-Bros.-Edition-art.jpg', '2016-11-30 21:18:36', '1', '0'),
(14, '1480571254', '0cad7425210 marye.pdf', '2016-12-01 01:48:43', '1', '0'),
(15, '1480571635', 'c17f633f6tag-239123205-1141393837704485533_239123205.jpg', '2016-12-01 01:55:11', '1', '0'),
(16, '1480595503', '9b147f91(2).jpg', '2016-12-01 08:35:23', '1', '0'),
(17, '1480595503', 'b1ac5ba3(5).jpg', '2016-12-01 08:35:23', '1', '0'),
(18, '1480595503', '1da0ff2e(4).jpg', '2016-12-01 08:35:23', '1', '0'),
(19, '1480595503', '23a76e5b(6).jpg', '2016-12-01 08:35:23', '1', '0'),
(20, '1480595503', '68c623a8(1).jpg', '2016-12-01 08:35:24', '1', '0'),
(21, '1480595503', '0bcfdeff(3).jpg', '2016-12-01 08:35:24', '1', '0'),
(22, '1480595503', 'eb4e2132(7).jpg', '2016-12-01 08:35:26', '1', '0'),
(23, '1480595503', 'b8d7984b(10).jpg', '2016-12-01 08:35:28', '1', '0'),
(24, '1480595503', '02b877e0(8).jpg', '2016-12-01 08:35:29', '1', '0'),
(25, '1480595503', 'ca2fd1f2(9).jpg', '2016-12-01 08:35:30', '1', '0'),
(26, '1480605152', 'c2615fc89.661.186.pdf', '2016-12-01 11:13:44', '1', '0'),
(27, '1480620386', '4a278a1510000 marye.pdf', '2016-12-01 15:27:08', '1', '0'),
(28, '1480623434', 'ae2d099eDSC05138.JPG', '2016-12-01 16:21:36', '1', '0'),
(29, '1480623938', '62293e92DSC05264.JPG', '2016-12-01 16:29:07', '1', '0'),
(30, '1480624427', '5bd7cec8DSC05285.JPG', '2016-12-01 16:35:56', '1', '0'),
(31, '1480984021', '19df2344oO7NS.jpg', '2016-12-05 20:31:35', '1', '0'),
(32, '1481759796', '45fadad7Codigo de procedimiento para la industria de la construccion_ La ___.jpg', '2016-12-14 19:57:23', '1', '0'),
(33, '1481759796', '6e68f66eMaquina De Soldadura Electrica Lincoln Welder Soldar Daa - $ 8,999_00 ___.jpg', '2016-12-14 19:57:24', '1', '0'),
(34, '1481759796', '19ca9d5fMateriales y Equipos __ PORTONES AUTOMATICOS Y HERRERIA.jpg', '2016-12-14 19:57:24', '1', '0'),
(35, '1481816482', '9d44e1bc1s.jpg', '2016-12-15 11:41:40', '1', '0'),
(36, '1481816692', '45ae8c131s.jpg', '2016-12-15 11:45:08', '1', '0'),
(37, '1481851244', 'c16849f8inmobiliaria-oikos.jpg', '2016-12-15 21:22:17', '1', '0'),
(38, '1484762135', '9972c14e2.pdf', '2017-01-18 13:56:11', '1', '0'),
(39, '1484762191', 'd2eced88casa5-con-logo-en-pared2.jpg', '2017-01-18 13:57:30', '1', '0'),
(40, '1484762191', '4d2216aacasa8-con-logo-en-tv.jpg', '2017-01-18 13:57:31', '1', '0'),
(41, '1484762191', '80af40d4casa2-con-logo.jpg', '2017-01-18 13:57:32', '1', '0'),
(42, '1485451379', 'a9cbcf86m1.jpg', '2017-01-26 13:24:31', '1', '0'),
(43, '1485451379', '1fe8bd3cm2.jpg', '2017-01-26 13:24:32', '1', '0'),
(44, '1485451379', 'f7c758a5m3.jpg', '2017-01-26 13:24:32', '1', '0'),
(45, '1485451379', 'ff543cadm4.jpg', '2017-01-26 13:24:32', '1', '0'),
(46, '1485452045', '76679775env.jpg', '2017-01-26 13:35:11', '1', '0'),
(47, '1485452045', 'b8219920codigoPostalGuia01.png', '2017-01-26 13:35:14', '1', '0'),
(48, '1485452045', 'ecf67f68cajas2-f5c81f3a9a884c709fe54c48ad4ece8c.png', '2017-01-26 13:35:29', '1', '0'),
(49, '1485452182', 'c0ac4202footer4_logo.png', '2017-01-26 13:36:50', '1', '0'),
(50, '1485452182', '8d347bf7logo.png', '2017-01-26 13:36:51', '1', '0'),
(51, '1485452182', 'fcfa1124codigoPostalGuia01.png', '2017-01-26 13:36:51', '1', '0'),
(52, '1485452182', '41607940logo_300.png', '2017-01-26 13:36:52', '1', '0'),
(53, '1485452182', 'b6e998c0m1.jpg', '2017-01-26 13:36:52', '1', '0'),
(54, '1485452182', '592f7ab3m2.jpg', '2017-01-26 13:36:53', '1', '0'),
(55, '1485452182', '4dfc08f1m3.jpg', '2017-01-26 13:36:54', '1', '0'),
(56, '1485452182', '9b5de531m4.jpg', '2017-01-26 13:36:54', '1', '0'),
(57, '1485452182', '28367623q1.jpg', '2017-01-26 13:36:56', '1', '0'),
(58, '1485452182', '71595f6aenv.jpg', '2017-01-26 13:36:57', '1', '0'),
(59, '1485452182', '6653642flogo_cargo.png', '2017-01-26 13:36:58', '1', '0'),
(60, '1485452182', 'ed91a9d4pro-ruta.png', '2017-01-26 13:37:01', '1', '0'),
(61, '1485452182', 'a490943acajas2-f5c81f3a9a884c709fe54c48ad4ece8c.png', '2017-01-26 13:37:08', '1', '0'),
(62, '1485455438', '25f67fcd2.pdf', '2017-01-26 14:31:28', '1', '0'),
(63, '1485455634', '8d714779casa8-con-logo-en-tv.jpg', '2017-01-26 14:34:44', '1', '0'),
(64, '1485455634', '9bbb7b31casa5-con-logo-en-pared2.jpg', '2017-01-26 14:34:44', '1', '0'),
(65, '1485455634', 'd04b2165casa2-con-logo.jpg', '2017-01-26 14:34:45', '1', '0'),
(66, '1485456143', 'f7f7a77e28126331.jpg', '2017-01-26 14:51:56', '1', '0'),
(67, '1485456143', '8c3555e128126340.jpg', '2017-01-26 14:51:57', '1', '0'),
(68, '1485456143', '52120ed628126338.jpg', '2017-01-26 14:51:58', '1', '0'),
(69, '1485456143', '9064332028126339.jpg', '2017-01-26 14:51:58', '1', '0'),
(70, '1485456143', '87c5993d28126329.jpg', '2017-01-26 14:51:59', '1', '0'),
(71, '1485456143', '7482122c28126342.jpg', '2017-01-26 14:51:59', '1', '0'),
(72, '1485456143', '82d7a00328126344.jpg', '2017-01-26 14:51:59', '1', '0'),
(73, '1485456143', '636daf1228126336.jpg', '2017-01-26 14:52:00', '1', '0'),
(74, '1485456143', 'e2e9650c28126347.jpg', '2017-01-26 14:52:01', '1', '0'),
(75, '1485456143', 'b0b7ec6228126334.jpg', '2017-01-26 14:52:03', '1', '0'),
(76, '1485456143', '36fd038d28126348.jpg', '2017-01-26 14:52:05', '1', '0'),
(77, '1485456143', 'd7bc934751870441.jpg', '2017-01-26 14:52:12', '1', '0'),
(78, '1485457030', '895966da38709940.jpg', '2017-01-26 15:00:23', '1', '0'),
(79, '1485457030', 'f3a3778038709939.jpg', '2017-01-26 15:00:23', '1', '0'),
(80, '1485457030', '81066c4a38709945.jpg', '2017-01-26 15:00:24', '1', '0'),
(81, '1485457030', '7598d4e938709938.jpg', '2017-01-26 15:00:24', '1', '0'),
(82, '1485457030', '47d5e28a38709944.jpg', '2017-01-26 15:00:25', '1', '0'),
(83, '1485457030', 'be14043338709946.jpg', '2017-01-26 15:00:25', '1', '0'),
(84, '1485457030', '9a95491838709947.jpg', '2017-01-26 15:00:26', '1', '0'),
(85, '1485457030', '566e414138709948.jpg', '2017-01-26 15:00:26', '1', '0'),
(86, '1485457030', '5dfbe14b38709943.jpg', '2017-01-26 15:00:27', '1', '0'),
(87, '1485457030', 'aef2e60138709941.jpg', '2017-01-26 15:00:28', '1', '0'),
(88, '1485457030', '81e0d64238709942.jpg', '2017-01-26 15:00:32', '1', '0'),
(89, '1485457382', '584328e038709961.jpg', '2017-01-26 15:03:34', '1', '0'),
(90, '1485457477', 'aca858f238709960.jpg', '2017-01-26 15:05:12', '1', '0'),
(91, '1486886295', 'ccf9e3e0a1.jpg', '2017-02-12 07:58:39', '1', '0'),
(92, '1486886767', '57f890c9a1.jpg', '2017-02-12 08:06:21', '1', '0'),
(93, '1486886920', 'd1b3a448a1.jpg', '2017-02-12 08:08:55', '1', '0'),
(94, '1486886953', '700b6851a1.jpg', '2017-02-12 08:09:24', '1', '0'),
(95, '1486886953', 'a15fba5ca2.jpg', '2017-02-12 08:09:40', '1', '0'),
(96, '1486886953', '72dea1b4a3.jpg', '2017-02-12 08:09:40', '1', '0'),
(97, '1486886953', 'd04d2c23a5.jpg', '2017-02-12 08:09:40', '1', '0'),
(98, '1486886953', 'a52d447ea4.jpg', '2017-02-12 08:09:41', '1', '0'),
(99, '1486887040', 'e95f81bfa2.jpg', '2017-02-12 08:11:05', '1', '0'),
(100, '1486887040', '8115c679a4.jpg', '2017-02-12 08:11:06', '1', '1'),
(101, '1486887040', '73eae7d1a1.jpg', '2017-02-12 08:11:06', '1', '0'),
(102, '1486887040', '9ec1b4a5a3.jpg', '2017-02-12 08:11:06', '1', '0'),
(103, '1486887071', '7427bad1a2.jpg', '2017-02-12 08:11:23', '1', '0'),
(104, '1486887071', 'd3a32aefa1.jpg', '2017-02-12 08:11:23', '1', '0'),
(105, '1486887071', '8023c021a3.jpg', '2017-02-12 08:11:24', '1', '0'),
(106, '1486887071', 'a6a6f4f9a4.jpg', '2017-02-12 08:11:25', '1', '0'),
(107, '1486887114', 'ca3f87e9a2.jpg', '2017-02-12 08:12:04', '1', '0'),
(108, '1486887114', '5ac002bda4.jpg', '2017-02-12 08:12:05', '1', '0'),
(109, '1486887114', '893e81eca3.jpg', '2017-02-12 08:12:05', '1', '0'),
(110, '1486887114', 'c52a55c5a1.jpg', '2017-02-12 08:12:06', '1', '0'),
(111, '1486887114', '7f6ac57fcabo1.jpg', '2017-02-12 08:13:36', '1', '0'),
(112, '1486887114', 'd27933a6buenosaires1.jpg', '2017-02-12 08:13:36', '1', '0'),
(113, '1486887114', 'd4b46ae3canaima1.JPG', '2017-02-12 08:13:38', '1', '0'),
(114, '1486887114', '50e07b89chile.jpg', '2017-02-12 08:13:38', '1', '0'),
(115, '1486887646', '16431c55a1.jpg', '2017-02-12 08:21:09', '1', '0'),
(116, '1486887708', 'e88763b8a1.jpg', '2017-02-12 08:22:03', '1', '0'),
(117, '1486887776', '7113cba9a1.jpg', '2017-02-12 08:23:10', '1', '0'),
(118, '1486887865', '2b43dbaea1.jpg', '2017-02-12 08:24:36', '1', '0'),
(119, '1486887984', '6ae338cea1.jpg', '2017-02-12 08:26:35', '1', '0'),
(120, '1486888101', '26e62a92a1.jpg', '2017-02-12 08:28:33', '1', '0'),
(121, '1486888165', 'cd4d3d5fbuenosaires1.jpg', '2017-02-12 08:29:38', '1', '0'),
(122, '1486888360', '810f61e4a1.jpg', '2017-02-12 08:33:00', '1', '0'),
(123, '1486888435', '18759054a10.jpg', '2017-02-12 08:34:05', '1', '0'),
(124, '1486888858', '0d4a03f6a1.jpg', '2017-02-12 08:41:12', '1', '0'),
(125, '1486888922', '105e8701a10.jpg', '2017-02-12 08:42:12', '1', '0'),
(126, '1486888966', 'b2f2db52a8.jpg', '2017-02-12 08:43:43', '1', '0'),
(127, '1486889194', '26b77246a9.jpg', '2017-02-12 08:46:45', '1', '0'),
(128, '1486889728', 'a49a3242a10.jpg', '2017-02-12 08:55:49', '1', '0'),
(129, '1486890071', '468988dea9.jpg', '2017-02-12 09:01:24', '1', '0'),
(130, '1486890203', '38ee7e981.jpg', '2017-02-12 09:04:01', '1', '0'),
(131, '1486890962', 'e1b3cc8fa6.jpg', '2017-02-12 09:17:45', '1', '0'),
(132, '1486892020', '48e23f83m3.jpg', '2017-02-12 09:40:46', '1', '0'),
(133, '1486892020', '42b5cb26m2.jpg', '2017-02-12 09:40:46', '1', '0'),
(134, '1486892020', '2635b0bem4.jpg', '2017-02-12 09:40:46', '1', '0'),
(135, '1486892020', 'f3cbdb14m1.jpg', '2017-02-12 09:40:46', '1', '0'),
(136, '1486892020', '7d22d62em5.jpg', '2017-02-12 09:40:47', '1', '0'),
(257, '1488791761', '00f5169fm2.jpg', '2017-03-06 09:16:18', '1', '0'),
(256, '1488791761', '6c6c9a85m4.jpg', '2017-03-06 09:16:18', '1', '0'),
(255, '1488791761', 'a97074a8m5.jpg', '2017-03-06 09:16:18', '1', '0'),
(254, '1488791761', 'aea1d0f8m3.jpg', '2017-03-06 09:16:17', '1', '0'),
(253, '1488791761', '14a531c3m1.jpg', '2017-03-06 09:16:17', '1', '0'),
(142, '1486892806', '6e58c079b2.jpg', '2017-02-12 09:49:23', '1', '0'),
(143, '1486892806', '64ab26b1b4.jpg', '2017-02-12 09:49:23', '1', '0'),
(144, '1486892806', '3eb0e5bab3.jpg', '2017-02-12 09:49:26', '1', '1'),
(145, '1486892806', '940c7bf4b1.jpg', '2017-02-12 09:49:28', '1', '0'),
(146, '1486892971', 'd9fda9fcc1.JPG', '2017-02-12 09:50:56', '1', '0'),
(147, '1486892971', '0bf24fbbc4.jpg', '2017-02-12 09:50:56', '1', '0'),
(148, '1486892971', 'b90eba61c2.jpg', '2017-02-12 09:50:57', '1', '0'),
(149, '1486892971', '01b91366c5.JPG', '2017-02-12 09:50:58', '1', '0'),
(150, '1486892971', '8d1546a5c3.jpg', '2017-02-12 09:50:58', '1', '0'),
(247, '1486892594', '0810e40cMaracay 4.jpg', '2017-02-25 21:30:46', '1', '0'),
(246, '1486892594', '4c64bb51Maracay 3.jpg', '2017-02-25 21:30:38', '1', '0'),
(245, '1486892594', '62892e22Maracay 2.jpg', '2017-02-25 21:30:31', '1', '1'),
(156, '1487403037', '72bd7214hesperia playa el agua 9.jpg', '2017-02-18 07:38:50', '1', '0'),
(157, '1487403037', 'dd5b9967habitacion_doble_premium_plus_hotel_hesperia_playa_el_agua_isla_de_margarita.jpg', '2017-02-18 07:38:52', '1', '0'),
(158, '1487403037', '2e5c8ce3foto6.jpg', '2017-02-18 07:38:52', '1', '0'),
(159, '1487403037', '57d5b510hotel-hesperia-playa-el-agua-habitacion-35f7645.jpg', '2017-02-18 07:38:55', '1', '0'),
(160, '1487403037', '3d0c06a5hotel-hesperia-playa-el-agua-isla-margarita-040.jpg', '2017-02-18 07:38:58', '1', '0'),
(161, '1487403037', 'ae0df2453626_Fotos_HPA_0011_Brightness_Contrast_2.jpg', '2017-02-18 07:39:00', '1', '0'),
(162, '1487403037', '30aa1937hesperia-playa-el-agua.jpg', '2017-02-18 07:39:11', '1', '0'),
(163, '1487403037', '2284c04dhotel_marriott_los_suenos_costa_rica_11.jpg', '2017-02-18 07:39:12', '1', '0'),
(164, '1487403795', 'ff46185e5.jpg', '2017-02-18 07:45:51', '1', '0'),
(165, '1487403795', '0df1426f7.jpg', '2017-02-18 07:45:51', '1', '0'),
(166, '1487403795', '747cfac86.jpg', '2017-02-18 07:45:51', '1', '1'),
(167, '1487403795', 'd60d4a178.jpg', '2017-02-18 07:45:52', '1', '0'),
(168, '1487403795', '4ff92af83.jpg', '2017-02-18 07:45:53', '1', '0'),
(169, '1487403795', 'f45f37109.jpg', '2017-02-18 07:45:54', '1', '0'),
(170, '1487403795', '273cc87b4.jpg', '2017-02-18 07:45:56', '1', '0'),
(171, '1487561151', '7bbaeb6cchoroni2.jpg', '2017-02-20 03:26:37', '1', '0'),
(172, '1487561151', 'e5c3c431choroni4.JPG', '2017-02-20 03:26:37', '1', '0'),
(173, '1487561151', '42d500c2choroni3.jpg', '2017-02-20 03:26:38', '1', '0'),
(174, '1487561151', '086c8663choroni1.jpg', '2017-02-20 03:26:39', '1', '0'),
(175, '1487561558', '125c5107choroni2.jpg', '2017-02-20 03:33:22', '1', '0'),
(176, '1487561558', '7b71db38choroni4.JPG', '2017-02-20 03:33:24', '1', '0'),
(177, '1487561558', '546217e5choroni3.jpg', '2017-02-20 03:33:25', '1', '0'),
(178, '1487561558', '542a8508choroni1.jpg', '2017-02-20 03:33:26', '1', '0'),
(244, '1486892594', 'f9d6b1d4Maracay 1.jpg', '2017-02-25 21:30:22', '1', '0'),
(181, '1487570597', '780f6afa63366585.jpg', '2017-02-20 06:04:08', '1', '0'),
(182, '1487570597', 'b1a0ff62descarga.jpg', '2017-02-20 06:04:09', '1', '1'),
(183, '1487570597', 'e98ed8f5choroni2.jpg', '2017-02-20 06:04:09', '1', '0'),
(184, '1487570597', 'dc0cd8c8images.jpg', '2017-02-20 06:04:10', '1', '0'),
(185, '1487570597', '073b6dcb0000051992.jpg', '2017-02-20 06:04:10', '1', '0'),
(186, '1487570597', 'b7127b6bchoroni3.jpg', '2017-02-20 06:04:10', '1', '0'),
(187, '1487570597', '195f5c91choroni1.jpg', '2017-02-20 06:04:13', '1', '0'),
(188, '1487570597', '5fad8a00choroni4.JPG', '2017-02-20 06:04:14', '1', '0'),
(333, '1487595535', 'd5251a46IMG-20170426-WA0008.jpg', '2017-05-11 19:39:26', '1', '0'),
(334, '1487595535', 'bf4704daIMG-20170426-WA0009.jpg', '2017-05-11 19:39:42', '1', '0'),
(190, '1487571309', 'ff7ddd8fhotel_hesperia_playa_el_agua (38).JPG', '2017-02-20 06:14:46', '1', '1'),
(1096, '1487595535', '3ae9bcb411885106_10207501468794156_7125521018832625511_n.jpg', '2018-01-17 17:36:25', '1', '0'),
(1094, '1487595535', '355fdcaf1964793_10154645793615361_7060090923124699501_n.jpg', '2018-01-17 17:36:07', '1', '0'),
(194, '1487571309', '7a0873b1hotel_hesperia_playa_el_agua (35).JPG', '2017-02-20 06:14:49', '1', '0'),
(195, '1487573259', '751fc180choroni2.jpg', '2017-02-20 06:49:09', '1', '1'),
(196, '1487573259', 'c6089fb063366585.jpg', '2017-02-20 06:49:09', '1', '0'),
(197, '1487573259', '0129fbfcimages.jpg', '2017-02-20 06:49:10', '1', '0'),
(198, '1487573259', '41a21f69descarga.jpg', '2017-02-20 06:49:10', '1', '0'),
(199, '1487573259', 'e09dfc1cchoroni3.jpg', '2017-02-20 06:49:10', '1', '0'),
(200, '1487573259', '1fcae07b0000051992.jpg', '2017-02-20 06:49:10', '1', '0'),
(201, '1487573259', '83f7394bchoroni1.jpg', '2017-02-20 06:49:12', '1', '0'),
(202, '1487573259', '22ee475cchoroni4.JPG', '2017-02-20 06:49:12', '1', '0'),
(203, '1487586047', '4388cb610955973963.jpg', '2017-02-20 10:19:08', '1', '0'),
(204, '1487586047', '1f8853b0mochima-2_med.jpeg', '2017-02-20 10:19:09', '1', '0'),
(205, '1487586047', '524c3f9dmochima.jpg', '2017-02-20 10:19:09', '1', '1'),
(206, '1487586047', 'b974325e3091231437.jpg', '2017-02-20 10:19:10', '1', '0'),
(207, '1487586047', '1019b4b0mochima-14-640x480.jpg', '2017-02-20 10:19:12', '1', '0'),
(208, '1487586047', '4ab755c6mochima-agosto-019-640x480.jpg', '2017-02-20 10:19:13', '1', '0'),
(209, '1487586047', '615dd85e7337349ba016b28d824b54d0b96dc82e.jpg', '2017-02-20 10:19:13', '1', '0'),
(210, '1487586047', '9de91649POSADA-MOCHIMA-LOGDE-1.jpg', '2017-02-20 10:19:14', '1', '0'),
(211, '1487594507', '9079f259apartamento-venta-higuerote-higuerote-miranda-rah-13-9066-41.jpg', '2017-02-20 12:53:09', '1', '0'),
(212, '1487594507', '0b86fa30Higuerote_2000_064.JPG', '2017-02-20 12:53:09', '1', '0'),
(213, '1487594507', '6e2d572f20141028142901492895000000-o.jpg', '2017-02-20 12:53:09', '1', '1'),
(214, '1487594507', '4757e22120150615210746081453000000-o.jpg', '2017-02-20 12:53:09', '1', '0'),
(215, '1487594507', '9a8c99daboca-caiman-higuerote-estado-miranda.jpg', '2017-02-20 12:53:10', '1', '0'),
(216, '1487594507', 'cdbbd2abdesde_la_capitania_bahia_caraenero_higueroteonline.jpg', '2017-02-20 12:53:10', '1', '0'),
(217, '1487595202', '13fde94a20141028142901492895000000-o.jpg', '2017-02-20 12:54:58', '1', '0'),
(218, '1487595202', '25209683Higuerote_2000_064.JPG', '2017-02-20 12:54:58', '1', '0'),
(219, '1487595202', 'ece591ab20150615210746081453000000-o.jpg', '2017-02-20 12:54:58', '1', '0'),
(220, '1487595202', '0cd1f259apartamento-venta-higuerote-higuerote-miranda-rah-13-9066-41.jpg', '2017-02-20 12:54:58', '1', '0'),
(221, '1487595202', 'aa19aacbdesde_la_capitania_bahia_caraenero_higueroteonline.jpg', '2017-02-20 12:54:59', '1', '1'),
(222, '1487595202', 'ed881f7dboca-caiman-higuerote-estado-miranda.jpg', '2017-02-20 12:54:59', '1', '0'),
(223, '1487595474', '07a05296GRAN SABANA01 020.jpg', '2017-02-20 13:03:17', '1', '0'),
(224, '1487595474', '5f485c4fgra_0001.jpg', '2017-02-20 13:03:17', '1', '0'),
(225, '1487595474', 'e1a935e1137_9234.jpg', '2017-02-20 13:03:17', '1', '0'),
(226, '1487595474', 'bc80978a11952471uB.jpg', '2017-02-20 13:03:17', '1', '0'),
(227, '1487595474', '6021b929FOTOSdelprofesorpararevisardic2008407_opt.jpg', '2017-02-20 13:03:17', '1', '1'),
(228, '1487595474', 'eb3a1920ENTRADA A JASPE.jpg', '2017-02-20 13:03:18', '1', '0'),
(229, '1487595474', 'f1765dabS3010069.jpg', '2017-02-20 13:03:18', '1', '0'),
(1092, '1487595535', '1c0c0e2bIMG-20171129-WA0010.jpg', '2018-01-17 17:35:27', '1', '0'),
(1095, '1487595535', 'd8ba2f3610702195_10154645794455361_5830656051628730204_n.jpg', '2018-01-17 17:36:15', '1', '1'),
(280, '1487595535', 'e8424745DSC08718.JPG', '2017-03-07 21:34:10', '1', '0'),
(281, '1487595535', '39992d7fDSC08731.JPG', '2017-03-07 21:38:04', '1', '0'),
(282, '1487595535', '9e3b4fe8DSC08732.JPG', '2017-03-07 21:38:15', '1', '0'),
(283, '1487595535', '31197ad2DSC08794.JPG', '2017-03-07 21:38:26', '1', '0'),
(236, '1487739453', 'a3323338hesperia playa el agua 9.jpg', '2017-02-22 05:30:26', '1', '0'),
(237, '1487739453', '043751afhotel_marriott_los_suenos_costa_rica_11.jpg', '2017-02-22 05:30:26', '1', '0'),
(238, '1487739453', 'cf3324fchotel-hesperia-playa-el-agua-isla-margarita-040.jpg', '2017-02-22 05:30:28', '1', '0'),
(239, '1487739453', '44f3efa1habitacion_doble_premium_plus_hotel_hesperia_playa_el_agua_isla_de_margarita.jpg', '2017-02-22 05:30:28', '1', '0'),
(240, '1487739453', '7b2693a13626_Fotos_HPA_0011_Brightness_Contrast_2.jpg', '2017-02-22 05:30:29', '1', '1'),
(241, '1487739453', '62c9619bhesperia-playa-el-agua.jpg', '2017-02-22 05:30:30', '1', '0'),
(242, '1487739453', 'e683c30efoto6.jpg', '2017-02-22 05:30:31', '1', '0'),
(243, '1487739453', '61aaecc4hotel-hesperia-playa-el-agua-habitacion-35f7645.jpg', '2017-02-22 05:30:33', '1', '0'),
(1084, '1486892621', '49961efbvalencia2.jpg', '2018-01-16 21:05:37', '1', '0'),
(1083, '1486892621', 'db8fa34fvalencia3.jpg', '2018-01-16 21:05:36', '1', '0'),
(1082, '1486892621', 'adc044d1valencia1.jpg', '2018-01-16 21:05:29', '1', '0'),
(263, '1488920253', '95b368d14609f45a_37.jpg', '2017-03-07 21:02:19', '1', '0'),
(264, '1488920253', 'e8543068cuba.jpg', '2017-03-07 21:02:21', '1', '1'),
(265, '1488920253', '65c2144ccatedral_de_santiago_de_cuba_4.jpg', '2017-03-07 21:02:23', '1', '0'),
(266, '1488920253', '59af933c312_12b176988e3d057.jpg', '2017-03-07 21:02:24', '1', '0'),
(267, '1488920253', '4295b034vacaciones.en_.cuba_ (1).jpg', '2017-03-07 21:02:24', '1', '0'),
(268, '1488920253', 'e8f75e200000663124.jpg', '2017-03-07 21:02:26', '1', '0'),
(269, '1488920698', '8b21b6ea0cabine18.jpg', '2017-03-07 21:08:24', '1', '0'),
(270, '1488920698', '5d4cca4ew10.jpg', '2017-03-07 21:08:24', '1', '0'),
(271, '1488920698', '22141ff0ExcursiÃ³n en Bus a Camboriu Salida de Montevideo  2012 ExcursiÃ³n a Camboriu Pasaje y Hotel en Semana de Turismo 2012 Salida desde Uruguay.jpg', '2017-03-07 21:08:25', '1', '1'),
(272, '1488920698', '569e1c23Turismo en Punta del Este, Uruguay.jpg', '2017-03-07 21:08:26', '1', '0'),
(273, '1488920698', '57c5be70Avda._Libertador,_Montevideo.jpg', '2017-03-07 21:08:27', '1', '0'),
(274, '1488920698', '8d904860000736411-uruguay-extiende-los-beneficios-fiscales-a-turistas.jpg', '2017-03-07 21:08:30', '1', '0'),
(285, '1488922929', '8862fc52Mrd1.jpg', '2017-03-07 22:08:38', '1', '1'),
(286, '1488922929', '02156bd8Mrd2.jpg', '2017-03-07 22:08:51', '1', '0'),
(287, '1488922929', 'c968ce74Mrd3.jpg', '2017-03-07 22:09:01', '1', '0'),
(288, '1488922929', 'a63c198dMrd5.jpg', '2017-03-07 22:09:39', '1', '0'),
(289, '1488922929', '1075cd7bMrd4.jpg', '2017-03-07 22:09:53', '1', '0'),
(290, '1488922929', 'c6aec718Mrd6.jpg', '2017-03-07 22:10:07', '1', '0'),
(291, '1488922929', 'afe6b46aMrd7.jpg', '2017-03-07 22:10:16', '1', '0'),
(292, '1488922929', 'd4719087Mrd8.jpg', '2017-03-07 22:10:28', '1', '0'),
(293, '1488922929', 'd627a303Mrd9.jpg', '2017-03-07 22:10:34', '1', '0'),
(294, '1488922929', '7ff73a3cMrd11.JPG', '2017-03-07 22:10:54', '1', '0'),
(303, '1489011244', 'b1791161hotel-104-art-suite-bluedoors.jpg.694x348_default.jpg', '2017-03-08 22:13:14', '1', '0'),
(296, '1489008119', '0b6cc582Bog1.jpg', '2017-03-08 21:39:28', '1', '1'),
(297, '1489008119', '26af0b39Bog2.jpg', '2017-03-08 21:39:40', '1', '0'),
(298, '1489008119', '70cf2049Bog3.jpg', '2017-03-08 21:39:53', '1', '0'),
(299, '1489008119', '2732d232Bog4.jpg', '2017-03-08 21:39:59', '1', '0'),
(300, '1489008119', '37507d0cBog5.jpg', '2017-03-08 21:40:14', '1', '0'),
(301, '1489008119', '46e3baeaBog6.jpg', '2017-03-08 21:40:22', '1', '0'),
(302, '1489008119', '1df74b10Bog7.jpg', '2017-03-08 21:40:40', '1', '0'),
(304, '1489011244', 'f784023cexecutive-suite-021.jpg.483x241_default.jpg', '2017-03-08 22:13:15', '1', '0'),
(305, '1489011244', 'd8dc8ba4business-suite-011.jpg.483x241_default.jpg', '2017-03-08 22:13:16', '1', '0'),
(306, '1489011244', 'e35dd309hotel-93-luxury.jpg.694x348_default.jpg', '2017-03-08 22:13:17', '1', '0'),
(307, '1489011244', '577aaa96junior_suite_02.jpg.483x241_default.jpg', '2017-03-08 22:13:18', '1', '0'),
(311, '1489092186', '526c70b1Margarita 1.jpg', '2017-03-09 20:55:29', '1', '1'),
(309, '1489011244', 'f7e7d689Continental-All-Suites-Hotel-Bogota-Cundinamarca-11.jpg', '2017-03-08 22:13:23', '1', '1'),
(310, '1489011244', '8807f19bhotel-celebrities-suite-bluedoors.jpg.694x348_default.jpg', '2017-03-08 22:13:30', '1', '0'),
(312, '1489092186', '7204da2aMargarita 2.jpg', '2017-03-09 20:55:53', '1', '0'),
(313, '1489092186', 'c3297f50Margarita 3.jpg', '2017-03-09 20:56:02', '1', '0'),
(314, '1491875517', '61fd10c8Canaima1.jpg', '2017-04-11 02:51:05', '1', '1'),
(315, '1491875517', '6085220aCanaima2.jpg', '2017-04-11 02:51:18', '1', '0'),
(316, '1491875517', '9c49811bCanaima3.jpg', '2017-04-11 02:51:45', '1', '0'),
(317, '1491875517', '7f255a69Canaima4.jpg', '2017-04-11 02:51:59', '1', '0'),
(318, '1491875517', '524cddceCanaima5.jpg', '2017-04-11 02:52:13', '1', '0'),
(319, '1491875517', '5f278f6eCanaima7.jpg', '2017-04-11 02:52:22', '1', '0'),
(320, '1491875517', '25d40d9aCanaimam6.jpg', '2017-04-11 02:52:40', '1', '0'),
(321, '1493912286', '27def36e13.-Canaima-1-138.jpg', '2017-05-04 15:41:07', '1', '0'),
(322, '1493912286', '86225b29canaima998xgh7.jpg', '2017-05-04 15:41:09', '1', '1'),
(323, '1493912286', '9b697022149_4983.jpg', '2017-05-04 15:41:11', '1', '0'),
(324, '1493912286', 'c4ed4408Daytour_Canaima_Pictures_090_02.jpg', '2017-05-04 15:41:18', '1', '0'),
(325, '1493912660', '4efc9f98149_4983.jpg', '2017-05-04 15:43:45', '1', '1'),
(326, '1493912660', '3d669e9a13.-Canaima-1-138.jpg', '2017-05-04 15:43:49', '1', '0'),
(327, '1493912660', '4a844ef7canaima998xgh7.jpg', '2017-05-04 15:44:02', '1', '0'),
(328, '1493912660', 'c5fe6d9aDaytour_Canaima_Pictures_090_02.jpg', '2017-05-04 15:44:03', '1', '0'),
(1093, '1487595535', '983a4231IMG-20171129-WA0008.jpg', '2018-01-17 17:35:37', '1', '0'),
(336, '1494532418', 'fabce0acimagereader.jpg', '2017-05-11 19:55:26', '1', '1'),
(337, '1494532418', '3aa947a0localimagereader (1).jpg', '2017-05-11 19:55:50', '1', '0'),
(338, '1494533819', '38aba0b3D-43.jpg', '2017-05-11 20:14:06', '1', '1'),
(339, '1494533819', 'bc678ae4Dynasty 01.jpg', '2017-05-11 20:48:28', '1', '0'),
(340, '1494533819', 'b0be5481Dynasty 04.jpg', '2017-05-11 20:48:36', '1', '0'),
(341, '1494533819', '1d3e80daHab Estudio.jpg', '2017-05-11 20:48:46', '1', '0'),
(342, '1494533819', '263c441bRest. Saturdays.jpg', '2017-05-11 20:48:54', '1', '0'),
(343, '1494533819', '8ed1f0abSuite.jpg', '2017-05-11 20:49:00', '1', '0'),
(344, '1494629683', '3b69ba08aquarius2.jpg', '2017-05-12 23:03:18', '1', '1'),
(345, '1494629683', '2f13a2e8_MG_4235.JPG', '2017-05-12 23:05:39', '1', '0'),
(346, '1494629683', 'edc84875aquarius4.jpg', '2017-05-12 23:10:09', '1', '0'),
(347, '1494629683', '60567bedMG_4195.jpg', '2017-05-12 23:10:32', '1', '0'),
(348, '1494629683', 'a3a0431cMG_4202.jpg', '2017-05-12 23:10:51', '1', '0'),
(349, '1494629683', '70ff8cfcpiscina.aquarius.jpg', '2017-05-12 23:11:09', '1', '0'),
(350, '1494631156', '736ec6edbella1.jpg', '2017-05-12 23:39:06', '1', '1'),
(351, '1494631156', '77b38ec8bella2.jpg', '2017-05-12 23:39:19', '1', '0'),
(352, '1494631156', '33fef4a7bella3.jpg', '2017-05-12 23:39:26', '1', '0'),
(353, '1494631156', '09f4e62bbella4.jpg', '2017-05-12 23:39:33', '1', '0'),
(354, '1494631156', 'fedd7e7fbella7.jpg', '2017-05-12 23:39:49', '1', '0'),
(355, '1494631156', '7895b9a0bella6.jpg', '2017-05-12 23:40:03', '1', '0'),
(356, '1494684495', 'f8dbe44dCali 1.jpg', '2017-05-13 14:27:20', '1', '1'),
(357, '1494684495', '7a7ef166Cali 2.jpg', '2017-05-13 14:27:36', '1', '0'),
(358, '1494684495', '2ab63aa2Cali 3.jpg', '2017-05-13 14:27:41', '1', '0'),
(359, '1494684495', '88f15cf0Cali 4.jpg', '2017-05-13 14:27:46', '1', '0'),
(360, '1494685731', 'da4056c3coche 1.jpg', '2017-05-13 14:44:24', '1', '1'),
(361, '1494685731', '4ada5815coche p 1.jpg', '2017-05-13 14:44:48', '1', '0'),
(362, '1494685731', '5cdd44d2coche p 2.jpg', '2017-05-13 14:44:55', '1', '0'),
(363, '1494685731', 'b1a9cf2dcoche p 3.jpg', '2017-05-13 14:45:01', '1', '0'),
(364, '1494703731', 'be02e6e4costa 1.jpg', '2017-05-13 19:34:01', '1', '1'),
(365, '1494703731', 'b3508a5bcosta 2.jpg', '2017-05-13 19:34:22', '1', '0'),
(366, '1494703731', '0e9833c8costa 4.jpg', '2017-05-13 19:34:30', '1', '0'),
(367, '1494703731', '4f5a8726costa 3.jpg', '2017-05-13 19:34:35', '1', '0'),
(368, '1494704320', '5a1ac529dunes1.jpg', '2017-05-13 20:13:01', '1', '0'),
(369, '1494704320', 'db789966dunes2.jpg', '2017-05-13 20:13:04', '1', '1'),
(370, '1494704320', '2f5164d7dunes3.jpg', '2017-05-13 20:13:09', '1', '0'),
(371, '1494704320', '9275bc1ddunes4.jpg', '2017-05-13 20:13:14', '1', '0'),
(372, '1494704320', '3374e9d1dunes5.jpg', '2017-05-13 20:13:26', '1', '0'),
(373, '1494704320', '0159f31bdunes6.jpg', '2017-05-13 20:13:32', '1', '0'),
(374, '1494706446', '6fb36be3heden2.jpg', '2017-05-13 23:47:16', '1', '1'),
(375, '1494706446', '71d9fe02heden1.jpg', '2017-05-13 23:47:21', '1', '0'),
(376, '1494706446', '06f8d6f6heden3.jpg', '2017-05-13 23:47:31', '1', '0'),
(377, '1494706446', '1a363823heden4.jpg', '2017-05-13 23:47:39', '1', '0'),
(378, '1494706446', '5c25b3b3heden5.jpg', '2017-05-13 23:47:42', '1', '0'),
(379, '1494719542', 'b5f631b6hisla3.jpg', '2017-05-14 00:07:27', '1', '1'),
(380, '1494719542', 'b3f1eadfhisla2.jpg', '2017-05-14 00:07:31', '1', '0'),
(381, '1494719542', '497ccc9chisla1.jpg', '2017-05-14 00:07:40', '1', '0'),
(382, '1494719542', '9c056804hisla4.jpg', '2017-05-14 00:07:45', '1', '0'),
(383, '1494719542', 'a701db42hisla5.jpg', '2017-05-14 00:07:48', '1', '0'),
(384, '1494719542', '20ff1258hisla6.jpg', '2017-05-14 00:07:57', '1', '0'),
(385, '1494719542', '4210fc6ahisla7.jpg', '2017-05-14 00:07:59', '1', '0'),
(386, '1494719542', '7b880137hisla8.jpg', '2017-05-14 00:08:01', '1', '0'),
(387, '1494721200', '8008262ahplaya1.jpg', '2017-05-14 00:26:16', '1', '1'),
(388, '1494721200', 'dc5322a9hplaya2.jpg', '2017-05-14 00:26:20', '1', '0'),
(389, '1494721200', 'e47807bfhplaya3.jpg', '2017-05-14 00:26:26', '1', '0'),
(390, '1494721200', '7445a498hplaya4.jpg', '2017-05-14 00:26:33', '1', '0'),
(391, '1494721200', '662425e0hplaya5.jpg', '2017-05-14 00:26:39', '1', '0'),
(392, '1494721795', 'c5175e50_MG_2005 copy.jpg', '2017-05-14 00:42:53', '1', '0'),
(393, '1494721795', 'da3d6c9b_MG_0160 copy.JPG', '2017-05-14 00:43:46', '1', '0'),
(394, '1494721795', 'fa37424c_MG_0250 copy.jpg', '2017-05-14 00:44:44', '1', '0'),
(395, '1494721795', '5d241f09_MG_9994 copy.jpg', '2017-05-14 00:45:06', '1', '0'),
(396, '1494721795', '74eea98a_MG_2256 copy.jpg', '2017-05-14 00:45:39', '1', '1'),
(397, '1494721795', '3bed8fbc_MG_0125 copy.jpg', '2017-05-14 00:46:04', '1', '0'),
(398, '1494727308', '20b1ca82isabel2.jpg', '2017-05-14 02:09:59', '1', '0'),
(399, '1494727308', 'd04b8ea8isabel1.jpg', '2017-05-14 02:10:04', '1', '1'),
(400, '1494727308', 'e813ee9bIMG_0890.jpg', '2017-05-14 02:10:10', '1', '0'),
(401, '1494727308', 'b3f40375isabel3.jpg', '2017-05-14 02:10:13', '1', '0'),
(402, '1494727308', 'c8c48943isabel4.jpg', '2017-05-14 02:10:22', '1', '0'),
(403, '1494727308', '09bea019Isabellacatolica_01.jpg', '2017-05-14 02:10:30', '1', '0'),
(404, '1494727308', 'c2006bd6Isabellacatolica_29.jpg', '2017-05-14 02:10:34', '1', '0'),
(405, '1494727308', 'fb247294IMG_0905.jpg', '2017-05-14 02:11:16', '1', '0'),
(406, '1494728021', '7455a692kokobay 2.jpg', '2017-05-14 02:19:17', '1', '1'),
(407, '1494728021', '47dbb970kokobay 3.jpg', '2017-05-14 02:19:22', '1', '0'),
(408, '1494728021', '4c2ccec7kokobay 4.jpg', '2017-05-14 02:19:27', '1', '0'),
(409, '1494728021', '7efd836akokobay 1.jpg', '2017-05-14 02:19:31', '1', '0'),
(410, '1494856204', 'b7230f03Plazuela1.JPG', '2017-05-15 13:56:46', '1', '1'),
(411, '1494856204', '543305b9Plazuela3.JPG', '2017-05-15 13:56:50', '1', '0'),
(412, '1494856204', '42242907Plazuela2.JPG', '2017-05-15 13:56:52', '1', '0'),
(413, '1494856204', '3f32c1ffPlazuela3.JPG', '2017-05-15 13:56:56', '1', '0'),
(414, '1494856204', '38cb839fPlazuela4.JPG', '2017-05-15 13:57:01', '1', '0'),
(415, '1494856204', '1e799715plazuela5.jpg', '2017-05-15 14:00:48', '1', '0'),
(416, '1494857639', '588d0357samanna1.jpg', '2017-05-15 14:24:22', '1', '0'),
(417, '1494857639', '75148255samanna2.jpg', '2017-05-15 14:24:52', '1', '0'),
(418, '1494857639', 'e7322adbsamanna4.jpg', '2017-05-15 14:25:02', '1', '0'),
(419, '1494857639', 'b1bdb4c7samanna5.jpg', '2017-05-15 14:25:06', '1', '0'),
(420, '1494857639', '4e1f4446samanna3.jpg', '2017-05-15 14:25:09', '1', '0'),
(421, '1494857639', '1b64ee9bsamanna6.jpg', '2017-05-15 14:25:09', '1', '0'),
(422, '1494884723', 'd46cdab0lagunamar5.jpg', '2017-05-15 22:43:46', '1', '1'),
(423, '1494884723', 'e36703bflagunamar 3.jpg', '2017-05-15 22:43:54', '1', '0'),
(424, '1494884723', 'f2d3a4b3Lagunamar 2.jpg', '2017-05-15 22:43:55', '1', '0'),
(425, '1494884723', '9f1f8669lagunamar 4.jpg', '2017-05-15 22:43:59', '1', '0'),
(426, '1494884723', '6acca660lagunamar 1.jpg', '2017-05-15 22:44:15', '1', '0'),
(427, '1494884723', '3015274elagunamar7.jpg', '2017-05-15 22:44:21', '1', '0'),
(428, '1494884723', 'fe03a6c6lagunamar6.jpg', '2017-05-15 22:44:35', '1', '0'),
(429, '1494889535', '0d3bdc32Panoramica (2).JPG', '2017-05-15 23:10:09', '1', '0'),
(430, '1494889535', '7b701182Piscina - restautant.JPG', '2017-05-15 23:10:19', '1', '0'),
(431, '1494889535', 'c70fe600BaÃ±os.JPG', '2017-05-15 23:10:24', '1', '0'),
(432, '1494889535', '48e4d996Decaracion Habitacion.JPG', '2017-05-15 23:10:30', '1', '0'),
(433, '1494889535', '724bd902Habitacion (2).JPG', '2017-05-15 23:10:34', '1', '0'),
(434, '1494889535', '3b8b9047Terraza Hamaca.JPG', '2017-05-15 23:10:58', '1', '0'),
(435, '1494889906', '57496ee2Flamboyant01.jpg', '2017-05-15 23:15:41', '1', '1'),
(436, '1494889906', '0a3e2422Flamboyant02.jpg', '2017-05-15 23:15:49', '1', '0'),
(437, '1494889906', '313d1843Flamboyant03.jpg', '2017-05-15 23:15:55', '1', '0'),
(438, '1494889906', '9cae8f90Flamboyant10.jpg', '2017-05-15 23:16:03', '1', '0'),
(439, '1494889906', '55acc7afFlamboyant08.jpg', '2017-05-15 23:16:15', '1', '0'),
(440, '1494889906', 'a413ca3dFlamboyant05.jpg', '2017-05-15 23:16:18', '1', '0'),
(441, '1494890200', '4c9bbefcpalm1.jpg', '2017-05-15 23:41:20', '1', '0'),
(442, '1494890200', 'f0724ecbpalm2.jpg', '2017-05-15 23:41:28', '1', '0'),
(443, '1494890200', '892d58b0palm3.jpg', '2017-05-15 23:41:33', '1', '1'),
(444, '1494890200', '0b8a0505HABITACION.jpg', '2017-05-15 23:41:36', '1', '0'),
(445, '1494890200', 'ea6fe458LUIS026.jpg', '2017-05-15 23:41:40', '1', '0'),
(446, '1494890200', '4c5bc796LUIS035.jpg', '2017-05-15 23:41:51', '1', '0'),
(447, '1494890200', 'd12828bePLAYAS.jpg', '2017-05-15 23:42:00', '1', '0'),
(448, '1494891767', '6d955051Club 1.jpg', '2017-05-16 00:47:58', '1', '0'),
(449, '1494891767', 'ba3d9688Club Punta 2.jpg', '2017-05-16 00:48:16', '1', '0'),
(450, '1494891767', 'bac548b2Club Punta 1.jpg', '2017-05-16 00:48:17', '1', '0'),
(451, '1494891767', '8188777cDORMITORIO PEQUEÃ‘O HAB. SUITE JUNIOR (4PAX).jpg', '2017-05-16 00:48:38', '1', '0'),
(452, '1494891767', '6a5ba255Club Punta 4.jpg', '2017-05-16 00:48:45', '1', '0'),
(453, '1494891767', '64944fb6HAB. SUITE PREMIUM 6 PAX.jpg', '2017-05-16 00:49:19', '1', '0'),
(454, '1494891767', 'b22c6809Club Punta 3.JPG', '2017-05-16 00:49:44', '1', '0'),
(455, '1494891767', '497d68f8Suite Premium.jpg', '2017-05-16 00:50:34', '1', '0'),
(456, '1494891767', '9c1b2ea1_MG_1012 copy.jpg', '2017-05-16 00:52:19', '1', '1'),
(457, '1494891767', '8bf405c9_MG_1088 Hab. ppal. Suite Junior.jpg', '2017-05-16 00:52:29', '1', '0'),
(458, '1494896193', 'c6059a71agua 1.jpg', '2017-05-16 01:02:27', '1', '0'),
(459, '1494896193', '33271de7agua 2.jpg', '2017-05-16 01:02:30', '1', '0'),
(460, '1494896193', 'f5e601aeagua 3.jpg', '2017-05-16 01:02:38', '1', '0'),
(461, '1494896193', 'fc7e07e6agua 4.jpg', '2017-05-16 01:02:41', '1', '1'),
(462, '1494896193', '6e604606Hab.jpg', '2017-05-16 01:02:46', '1', '0'),
(463, '1494896758', '53e61ea1_MG_0869.jpg', '2017-05-16 01:09:27', '1', '1'),
(464, '1494896758', '2711b59a_MG_1066.jpg', '2017-05-16 01:09:33', '1', '0'),
(465, '1494896758', 'c16ab1b84.jpg', '2017-05-16 01:09:40', '1', '0'),
(466, '1494896758', 'e38d669eIMG_5955.jpg', '2017-05-16 01:09:47', '1', '0'),
(467, '1494897035', 'f81caf5fMaloka1.jpg', '2017-05-16 01:15:00', '1', '0'),
(468, '1494897035', 'd6d8d64aMaloka2.jpg', '2017-05-16 01:15:03', '1', '0'),
(469, '1494897035', '7a9287ebMaloka3.jpg', '2017-05-16 01:15:06', '1', '0'),
(470, '1494897035', '09b12323Maloka4.jpg', '2017-05-16 01:15:12', '1', '1'),
(471, '1494898123', '329a64b0Margabella 3.jpg', '2017-05-16 01:34:19', '1', '0'),
(472, '1494898123', '45a09ee2Margabella 2.jpg', '2017-05-16 01:34:19', '1', '0'),
(473, '1494898123', '8b2e92a5Margabella 1.jpg', '2017-05-16 01:34:28', '1', '0'),
(474, '1494898123', '354d18d6Margabella4.jpg', '2017-05-16 01:35:57', '1', '1'),
(475, '1494978123', 'bafe1d85concorde1.jpg', '2017-05-17 00:03:50', '1', '1'),
(476, '1494978123', '9280de0b_IFM9525.jpg', '2017-05-17 00:03:56', '1', '0'),
(477, '1494978123', '0a9ae7f6_IFM9413.jpg', '2017-05-17 00:04:04', '1', '0'),
(478, '1494978123', '5bbb2eb0_IFM9455.jpg', '2017-05-17 00:04:11', '1', '0'),
(479, '1494978123', 'b522c364_IFM9470.jpg', '2017-05-17 00:04:16', '1', '0'),
(480, '1494978123', '42257718_IFM9472.jpg', '2017-05-17 00:04:21', '1', '0'),
(481, '1494978123', '20b1b8b6concorde3.jpg', '2017-05-17 00:04:27', '1', '0'),
(482, '1494978123', 'a6fdae4bconcorde2.jpg', '2017-05-17 00:04:32', '1', '0'),
(483, '1494978123', 'de306659Foto Ajustada-new.jpg', '2017-05-17 00:05:06', '1', '0'),
(1088, '1489092186', '8babc4dfMargarita 4.jpg', '2018-01-16 21:16:17', '1', '0'),
(485, '1495043214', '57047aa2Cayo en los Roques.jpg', '2017-05-17 18:12:10', '1', '0'),
(486, '1495043214', 'e5e55a92los roques 3.jpg', '2017-05-17 18:12:26', '1', '0'),
(487, '1495043214', '976787dfroques 16.jpg', '2017-05-17 18:12:28', '1', '1'),
(488, '1495043214', '081b5d17Roques2.jpg', '2017-05-17 18:12:29', '1', '0'),
(489, '1495043214', 'e249c04eRoques3.jpg', '2017-05-17 18:12:43', '1', '0'),
(490, '1495043214', '815f2acaRoques13.jpg', '2017-05-17 18:13:03', '1', '0'),
(491, '1495043214', 'd835187fRoques10.jpg', '2017-05-17 18:13:22', '1', '0'),
(492, '1495043214', '59448bcbRoques8.jpg', '2017-05-17 18:13:35', '1', '0'),
(493, '1495044967', '7e39eb07botuto 4.JPG', '2017-05-17 18:32:41', '1', '0'),
(494, '1495044967', 'e16501c4botuto 1.JPG', '2017-05-17 18:32:54', '1', '1'),
(495, '1495044967', '91497b83botuto 2.jpg', '2017-05-17 18:32:55', '1', '0'),
(496, '1495044967', 'a75a0946posada-el-botuto-8.jpg', '2017-05-17 18:32:56', '1', '0'),
(497, '1495044967', 'e30bc03cposada-el-botuto (1).jpg', '2017-05-17 18:33:39', '1', '0'),
(498, '1495046142', 'd8a3f5e9chez1.JPG', '2017-05-17 19:10:16', '1', '0'),
(499, '1495046142', '6c3bb2af1504_posada_turistica_chez_judith_1001.jpg', '2017-05-17 19:13:27', '1', '0'),
(500, '1495046142', '2671ae8bposada_chez_judith_044.jpg', '2017-05-17 19:16:39', '1', '0'),
(501, '1495046142', '9df82924POSADA-CHEZ-JUDITH-10.jpg', '2017-05-17 19:16:51', '1', '0'),
(504, '1495046142', '6ac7f71bGEDC0658.JPG', '2017-05-17 19:23:20', '1', '0'),
(505, '1495046142', '5790a334chezjudith.jpg', '2017-05-17 19:24:37', '1', '1'),
(506, '1495049445', '8992959flos_roques_posada_la_terraza.jpg', '2017-05-18 13:19:20', '1', '0'),
(507, '1495049445', '8654972clos_roques_posada_la_terraza (2).jpg', '2017-05-18 13:19:34', '1', '0'),
(508, '1495049445', 'd72ee796posada_la_terraza_014.jpg', '2017-05-18 13:19:43', '1', '0'),
(509, '1495049445', '945e1e41posada_la_terraza_018.jpg', '2017-05-18 13:19:58', '1', '0'),
(510, '1495049445', '6d12ba8eposada_la_terraza_los_roques (16).jpg', '2017-05-18 13:20:10', '1', '0'),
(512, '1495049445', 'e38b08e5posada_la_terraza_los_roques (26).JPG', '2017-05-18 13:20:57', '1', '0'),
(513, '1495049445', 'e53f9f08posada_la_terraza_los_roques (31).jpg', '2017-05-18 13:21:00', '1', '0'),
(514, '1495049445', 'a62a4da6posada_la_terraza_los_roques (32).jpg', '2017-05-18 13:21:11', '1', '0'),
(515, '1495049445', 'a9fcc84dt10.jpg', '2017-05-18 13:21:17', '1', '1'),
(516, '1495114360', '2369047fcasaturquesa01.jpg', '2017-05-18 14:26:24', '1', '0'),
(517, '1495114360', 'b64327f2casaturquesa06.jpg', '2017-05-18 14:26:29', '1', '0'),
(518, '1495114360', '7c0fa633chilax-area.jpg', '2017-05-18 14:26:35', '1', '0'),
(519, '1495114360', '9723c67dIMG_21182-1024x768.jpg', '2017-05-18 14:26:42', '1', '0'),
(520, '1495114360', '5a6fa46bposada-casa-turquesa (1).jpg', '2017-05-18 14:26:47', '1', '0'),
(521, '1495114360', 'defbf892posada-casa-turquesa (2).jpg', '2017-05-18 14:26:57', '1', '0'),
(522, '1495114360', '4a6e67d9posada-casa-turquesa.jpg', '2017-05-18 14:27:05', '1', '0'),
(523, '1495114360', 'd5907e23Posada-casa-turquesa-2.jpg', '2017-05-18 14:27:12', '1', '1'),
(525, '1495119300', '4edfd025cayoluna08.jpg', '2017-05-18 15:25:01', '1', '1'),
(526, '1495119300', '40f82d6cposada_cayo_luna_011.jpg', '2017-05-18 15:25:18', '1', '0'),
(527, '1495119300', 'd78bc341los-roques-posada-cayo-luna-sep-dic.jpg', '2017-05-18 15:25:52', '1', '0'),
(528, '1495119300', '87f71515posada_cayo_luna_009.jpg', '2017-05-18 15:26:10', '1', '0'),
(529, '1495119300', '89888c40cayoluna08 (1).jpg', '2017-05-18 15:26:23', '1', '0'),
(530, '1495119300', '54b2c5abcayoluna01.jpg', '2017-05-18 15:26:30', '1', '0'),
(531, '1495119300', 'fafeb2d4img_intro03.jpg', '2017-05-18 15:26:47', '1', '0'),
(532, '1495119300', '350282a1img_intro02.jpg', '2017-05-18 15:27:05', '1', '0'),
(533, '1495122472', '23c16184logo.png', '2017-05-18 15:48:34', '1', '0'),
(538, '1495122183', '676b48f4IMG_1284 - copia.jpg', '2017-05-18 16:02:47', '1', '0'),
(539, '1495122183', '5a65c847paraiso 1.jpg', '2017-05-18 16:02:59', '1', '0'),
(536, '1495122183', 'ee51f71dIMG_1283 - copia.jpg', '2017-05-18 16:01:58', '1', '0'),
(540, '1495122183', '6b8316b9IMG_1277.jpg', '2017-05-18 16:03:10', '1', '0'),
(541, '1495122183', 'f2893041paraiso 4.jpg', '2017-05-18 16:03:16', '1', '0'),
(542, '1495122183', '972539e6paraiso 3.jpg', '2017-05-18 16:03:29', '1', '1'),
(543, '1495122183', '54e0052fIMG_1281.jpg', '2017-05-18 16:03:41', '1', '0'),
(544, '1495122183', 'cd4e926fIMG_1280.jpg', '2017-05-18 16:03:56', '1', '0'),
(545, '1495122183', 'e50a0ac5paraiso 2.jpg', '2017-05-18 16:04:04', '1', '0'),
(546, '1495123567', 'cfc07881La Gaviota1.JPG', '2017-05-18 16:18:37', '1', '0'),
(547, '1495123567', '1877fa0aLa Gaviota2.JPG', '2017-05-18 16:18:53', '1', '0'),
(548, '1495123567', 'fecf440bLa Gaviota3.JPG', '2017-05-18 16:19:02', '1', '0'),
(549, '1495123567', '067fdb08La Gaviota4.JPG', '2017-05-18 16:19:04', '1', '0'),
(550, '1495123567', '366fedffimage4.jpg', '2017-05-18 16:19:29', '1', '0'),
(551, '1495123567', '8d802a5btimthumb.jpg', '2017-05-18 16:24:00', '1', '0'),
(552, '1495123567', 'ea3af22e1206_posada_la_gaviota_6095.jpg', '2017-05-18 16:24:10', '1', '1'),
(553, '1495123567', 'd725028921774Posada La Gaviota5.jpg', '2017-05-18 16:24:20', '1', '0'),
(554, '1495123567', '37b623faimage4.jpg', '2017-05-18 16:24:41', '1', '0'),
(555, '1495124754', 'ec8a03adIMG_1222.JPG', '2017-05-18 16:38:58', '1', '0'),
(556, '1495124754', '0d3f2619terramar 1.jpg', '2017-05-18 16:39:01', '1', '0'),
(557, '1495124754', 'b5dd242eterramar 2.jpg', '2017-05-18 16:39:14', '1', '0'),
(558, '1495124754', '18e34039IMG_1230.JPG', '2017-05-18 16:39:27', '1', '1'),
(559, '1495124754', 'c7e496dcIMG_1269.JPG', '2017-05-18 16:39:41', '1', '0'),
(560, '1495124754', '839274d7terramar 4.jpg', '2017-05-18 16:39:43', '1', '0'),
(561, '1495124754', '27270468IMG_1229.JPG', '2017-05-18 16:39:59', '1', '0'),
(563, '1495124754', 'eea751ddterramar 3.jpg', '2017-05-18 16:40:38', '1', '0'),
(564, '1495124754', 'ab8cc7d1Tudescuenton-614-Los-Roques-por-persona-Alojamiento-2-dias-y-1-nocheBoletoDesayunoCenaTraslado.jpeg', '2017-05-18 16:42:55', '1', '0'),
(565, '1495125967', '0351a076dsc-01601.jpg', '2017-05-18 17:04:03', '1', '0'),
(566, '1495125967', '69962fbbdsc-01891.jpg.1140x481_0_236_11400-1.jpg', '2017-05-18 17:09:13', '1', '0'),
(576, '1495125967', '6c89ebd3fed-60061.JPG.1024x0.JPG', '2017-05-18 17:12:05', '1', '0'),
(568, '1495125967', '11c3977bfed_6825.JPG.1024x0.JPG', '2017-05-18 17:09:31', '1', '0'),
(569, '1495125967', '0260d9ecfed_6927.JPG.1024x0.JPG', '2017-05-18 17:09:42', '1', '0'),
(570, '1495125967', 'e7a9d7d9fed-61681.JPG.1024x0.JPG', '2017-05-18 17:09:55', '1', '0'),
(571, '1495125967', 'd27c7f97fed-88981.JPG.1024x0.JPG', '2017-05-18 17:10:06', '1', '0'),
(572, '1495125967', '27570ad8fed_6798.JPG.1024x0.JPG', '2017-05-18 17:10:19', '1', '1'),
(573, '1495125967', '6b23ef3efed-63911.JPG.1024x0.JPG', '2017-05-18 17:10:30', '1', '0'),
(574, '1495125967', '4593a0f2fed-63931.JPG.1024x0.JPG', '2017-05-18 17:10:36', '1', '0'),
(575, '1495125967', '57f69d15dsc-01771-1.jpg.1024x0.jpg', '2017-05-18 17:10:55', '1', '0'),
(577, '1495125967', 'dd873a78fed-61171.JPG.1920x807_43_484_17439.JPG', '2017-05-18 17:13:39', '1', '0'),
(578, '1495146931', 'a11f61c3boda-masaje-pareja.jpg', '2017-05-18 22:38:08', '1', '1'),
(579, '1495127712', '01de028dgalapagos 1.jpg', '2017-05-19 12:46:27', '1', '0'),
(580, '1495127712', '3f5c7d4cgalapagos 2.jpg', '2017-05-19 12:46:28', '1', '0'),
(581, '1495127712', '7f6ce1e8galapagos 3.jpg', '2017-05-19 12:46:37', '1', '0'),
(582, '1495127712', 'a045dcf3galapagos 4.jpg', '2017-05-19 12:46:41', '1', '0'),
(583, '1495127712', '43cccd7805_Posada_Galapagos.jpg', '2017-05-19 12:49:00', '1', '0'),
(584, '1495127712', '087f071924_chana.jpg', '2017-05-19 12:49:19', '1', '0'),
(585, '1495127712', '6ca5d32d114_chana.jpg', '2017-05-19 12:49:31', '1', '0'),
(586, '1495127712', '6cae5284129_chana.jpg', '2017-05-19 12:49:40', '1', '0'),
(587, '1495127712', '562e0f52135_chana.jpg', '2017-05-19 12:49:51', '1', '1'),
(588, '1495198355', '03c75aa9natura 2.jpg', '2017-05-19 13:12:13', '1', '0'),
(589, '1495198355', 'e17560a1natura 1.jpg', '2017-05-19 13:12:14', '1', '0'),
(590, '1495198355', 'ef274c62natura 3.JPG', '2017-05-19 13:12:15', '1', '0'),
(591, '1495198355', '07285d63natura 4.jpg', '2017-05-19 13:12:15', '1', '0'),
(592, '1495198355', 'dd62cf731-posada-natura-viva-893x528.jpg', '2017-05-19 13:13:37', '1', '1'),
(595, '1495198355', 'c0c58545ro648_g2.jpg', '2017-05-19 13:14:00', '1', '0'),
(594, '1495198355', '6b0bfb66maxresdefault.jpg', '2017-05-19 13:13:48', '1', '0'),
(596, '1495198355', '75f53625terrazza di notte 1.jpg', '2017-05-19 13:14:10', '1', '0'),
(597, '1495198355', 'f71966a33-posada-natura-viva-893x528.jpg', '2017-05-19 13:14:19', '1', '0'),
(598, '1495198355', 'e4368b0e2-posada-natura-viva-893x528.jpg', '2017-05-19 13:14:23', '1', '0'),
(599, '1495198355', 'f7adee3b4-posada-natura-viva-893x528.jpg', '2017-05-19 13:14:29', '1', '0'),
(600, '1495199725', 'dada549emediterraneo 1.jpg', '2017-05-19 14:06:38', '1', '0'),
(601, '1495199725', '75b8fea8mediterraneo 2.jpg', '2017-05-19 14:06:41', '1', '0'),
(602, '1495199725', 'a2d8dfadmediterraneo 3.jpg', '2017-05-19 14:06:48', '1', '0'),
(603, '1495199725', 'c29dbe5emediterraneo 4.jpg', '2017-05-19 14:06:49', '1', '1'),
(604, '1495199725', '6f95bad1l41.jpg', '2017-05-19 14:08:56', '1', '0'),
(605, '1495199725', 'd286ad4f052150156172188228224180029002201190113213043004.jpg', '2017-05-19 14:09:08', '1', '0'),
(606, '1495199725', '274f7169medrestaurant.jpg', '2017-05-19 14:10:31', '1', '0'),
(607, '1495199725', '05a32db4posada_mediterraneo_los_roques.JPG', '2017-05-19 14:11:05', '1', '0'),
(608, '1495203085', '9e5f1b96acuarela 2.jpg', '2017-05-19 14:27:57', '1', '0'),
(609, '1495203085', 'be17cf1facuarela 1.jpg', '2017-05-19 14:28:00', '1', '0'),
(610, '1495203085', 'a467dabbacuarela 3.jpg', '2017-05-19 14:28:05', '1', '0'),
(611, '1495203085', 'df96ddb6acuarela 4.jpg', '2017-05-19 14:28:08', '1', '0'),
(612, '1495203085', '345c59ceposada-acuarela-los-roques-venezuela (8).JPG', '2017-05-19 14:31:42', '1', '0'),
(613, '1495203085', 'bd115b40posada-acuarela-los-roques-venezuela (9).JPG', '2017-05-19 14:31:47', '1', '0'),
(614, '1495203085', '41c1c6f1posada-acuarela-los-roques-venezuela (18).JPG', '2017-05-19 14:31:56', '1', '0'),
(615, '1495203085', 'fa5b68baposada-acuarela-los-roques-venezuela (38).JPG', '2017-05-19 14:32:31', '1', '0'),
(616, '1495203085', '4dec3fedposada-acuarela-los-roques-venezuela (34).JPG', '2017-05-19 14:32:53', '1', '0'),
(619, '1495203085', '9bb8a671posada-acuarela-los-roques-venezuela (35).JPG', '2017-05-19 14:33:44', '1', '1'),
(618, '1495203085', 'e7f3a92eposada-acuarela-los-roques-venezuela (22).JPG', '2017-05-19 14:33:00', '1', '0'),
(620, '1495204458', '5665a32bmacanao 1.jpg', '2017-05-19 14:44:21', '1', '0'),
(621, '1495204458', 'fc7f339fmacanao 2.jpg', '2017-05-19 14:44:25', '1', '0'),
(622, '1495204458', '2ce86e4fmacanao 3.jpg', '2017-05-19 14:44:29', '1', '0'),
(623, '1495204458', '479b6aaamacanao 4.jpg', '2017-05-19 14:44:34', '1', '0'),
(624, '1495204458', 'b1127242posada_macanao_lodge_los_roques_venezuela (9).JPG', '2017-05-19 14:46:08', '1', '0'),
(625, '1495204458', '87c75994posada_macanao_lodge_fotos_022.jpg', '2017-05-19 14:48:12', '1', '0'),
(626, '1495204458', 'a5e8c086Posada-Macanao-2.jpg', '2017-05-19 14:48:16', '1', '0'),
(628, '1495204458', 'f45ffa319279084.jpg', '2017-05-19 14:48:49', '1', '1'),
(629, '1495204458', '6f0d36c7Macanao_Lodge_Room__1_.jpg', '2017-05-19 14:49:29', '1', '0'),
(630, '1495204458', 'd1373276DSC00052.JPG', '2017-05-19 14:49:34', '1', '0'),
(631, '1495205450', '06eecbecTsunami1.JPG', '2017-05-19 15:02:52', '1', '1'),
(632, '1495205450', '28ea05fftsunami2.JPG', '2017-05-19 15:02:57', '1', '0'),
(633, '1495205450', '1690d2f5tsunami3.JPG', '2017-05-19 15:02:58', '1', '0'),
(634, '1495205450', '1b6955dbtsunami4.JPG', '2017-05-19 15:02:58', '1', '0'),
(635, '1495205450', '0377697bposada-arrecife.jpg', '2017-05-19 15:08:04', '1', '0'),
(636, '1495205450', '92a2218cPOSADA-TSUNAMI-03.jpg', '2017-05-19 15:08:20', '1', '0'),
(637, '1495205450', '1992a46bposada-tsunami-los-roques-venezuela (4).JPG', '2017-05-19 15:08:21', '1', '0'),
(638, '1495205450', '6df056datimthumb.jpg', '2017-05-19 15:08:22', '1', '0'),
(639, '1495206553', '6307cf8emalibu 1.jpg', '2017-05-19 15:18:26', '1', '0'),
(640, '1495206553', '956564b4malibu 3.jpg', '2017-05-19 15:18:26', '1', '0'),
(641, '1495206553', 'ca3fdd05malibu 2.jpg', '2017-05-19 15:18:26', '1', '0'),
(642, '1495206553', '387b78f7malibu 4.jpg', '2017-05-19 15:18:28', '1', '0'),
(643, '1495206553', 'ee2eb22eposada-malibu.jpg', '2017-05-19 15:22:03', '1', '0'),
(644, '1495206553', '6ea967c3comedor.jpg', '2017-05-19 15:22:10', '1', '0'),
(645, '1495206553', '1b6fa4a2posada-malibu (1).jpg', '2017-05-19 15:23:00', '1', '1'),
(646, '1495206553', 'a251f83aMalibu-500x300.jpg', '2017-05-19 15:24:47', '1', '0'),
(647, '1495206553', '48efb9fdposada-malibu (2).jpg', '2017-05-19 15:24:53', '1', '0'),
(648, '1495207520', '4d57d972bequeve 1.jpg', '2017-05-19 15:52:36', '1', '1'),
(649, '1495207520', '9a1e0646Foto 2 sala FC.jpg', '2017-05-19 15:52:41', '1', '0'),
(650, '1495207520', '6418443dFoto lancha 2 FC.jpg', '2017-05-19 15:52:53', '1', '0'),
(651, '1495207520', '82b192aaLRS fotos Sala 013.jpg', '2017-05-19 15:53:47', '1', '0'),
(652, '1495207520', 'd3eef92dLRS fotos 002.jpg', '2017-05-19 15:53:59', '1', '0'),
(653, '1495207520', 'eaa47603LRS fotos 007.jpg', '2017-05-19 15:54:00', '1', '0'),
(654, '1495207520', '25f0b522hab 13.jpg', '2017-05-19 15:54:09', '1', '0'),
(655, '1495207520', '5aedc21ahab 16.jpg', '2017-05-19 15:54:16', '1', '0'),
(656, '1495207520', '1321fd11LRS fotos 014.jpg', '2017-05-19 15:55:01', '1', '0'),
(657, '1495218645', '18b9b92creal 4.jpg', '2017-05-19 19:19:20', '1', '1'),
(658, '1495218645', '12af77c4real 3.JPG', '2017-05-19 19:19:34', '1', '0'),
(659, '1495218645', 'ea00bd40real 2.jpg', '2017-05-19 19:20:17', '1', '0'),
(660, '1495218645', '6d1204610_13_g.jpg', '2017-05-19 19:24:55', '1', '0'),
(661, '1495218645', '90329bad0_25_g.jpg', '2017-05-19 19:25:01', '1', '0'),
(662, '1495218645', '073f22171_22_g.jpg', '2017-05-19 19:25:02', '1', '0'),
(663, '1495218645', 'f3d7937aMR-203.jpg', '2017-05-19 19:25:16', '1', '0'),
(664, '1495218645', '70cc15b6habitacion003.jpg', '2017-05-19 19:25:17', '1', '0');
INSERT INTO `hk_fotos` (`id`, `id_solicitud`, `foto`, `fecha`, `estatus`, `posicion`) VALUES
(665, '1495218645', 'da464f98MR-76.jpg', '2017-05-19 19:25:19', '1', '0'),
(666, '1495218645', 'c7031609habitacion.jpg', '2017-05-19 19:25:37', '1', '0'),
(667, '1495218645', 'ef067d11MR-2771.jpg', '2017-05-19 19:25:53', '1', '0'),
(668, '1495222741', 'ff46ec57village 4.jpg', '2017-05-19 19:41:02', '1', '0'),
(669, '1495222741', 'ccdc8899village 1.jpg', '2017-05-19 19:41:03', '1', '0'),
(670, '1495222741', 'dbb64402village 2.jpg', '2017-05-19 19:41:08', '1', '0'),
(671, '1495222741', 'c78d3b9dvillage 3.jpg', '2017-05-19 19:41:34', '1', '0'),
(672, '1495222741', 'f88a65ccPiscina Jacuzzi.JPG', '2017-05-19 19:41:44', '1', '0'),
(673, '1495222741', '7dc2ec56HabitaciÃ³n modelo.jpg', '2017-05-19 19:41:54', '1', '0'),
(674, '1495222741', '1b5d1885foto piscina.jpg', '2017-05-19 19:42:50', '1', '1'),
(675, '1495223285', '0cd193adCaribb 1.jpg', '2017-05-19 19:56:06', '1', '0'),
(676, '1495223285', '61dca1ceCaribb 2.jpg', '2017-05-19 19:56:10', '1', '0'),
(679, '1495223285', 'e0449b9caparthotel-sunsol-caribbean-beach-general-a6763c.jpg', '2017-05-19 19:59:25', '1', '1'),
(678, '1495223285', '874582b1Caribb 4.jpg', '2017-05-19 19:56:22', '1', '0'),
(680, '1495223285', 'e379f709aparthotel-sunsol-caribbean-beach-general-a67633.jpg', '2017-05-19 19:59:31', '1', '0'),
(681, '1495223285', '4d0d97edaparthotel-sunsol-caribbean-beach-general-a67637.jpg', '2017-05-19 19:59:33', '1', '0'),
(682, '1495223285', '2d6db2fdaparthotel-sunsol-caribbean-beach-general-a67645.jpg', '2017-05-19 19:59:40', '1', '0'),
(683, '1495223285', 'ad167a71aparthotel-sunsol-caribbean-beach-general-a67649.jpg', '2017-05-19 19:59:46', '1', '0'),
(684, '1495223285', 'ec055dd6aparthotel-sunsol-caribbean-beach-habitacion-3f436c.jpg', '2017-05-19 19:59:49', '1', '0'),
(685, '1495223285', '8dcc920daparthotel-sunsol-caribbean-beach-habitacion-3751d20.jpg', '2017-05-19 19:59:52', '1', '0'),
(686, '1495290985', '37d17486Caracas3.jpg', '2017-05-20 15:32:31', '1', '0'),
(687, '1495290985', 'a7d7e994Caracas4.jpg', '2017-05-20 15:32:36', '1', '0'),
(688, '1495290985', '4014c0c2Caracas5.jpg', '2017-05-20 15:32:58', '1', '0'),
(689, '1495290985', '0248f29aCaracas9.jpg', '2017-05-20 15:32:59', '1', '0'),
(690, '1495290985', '9aa85530Caracas7.jpg', '2017-05-20 15:33:01', '1', '0'),
(691, '1495290985', '3a61f270Caracas6.jpg', '2017-05-20 15:33:02', '1', '0'),
(692, '1495290985', '068ce3deCaracas11.jpg', '2017-05-20 15:35:02', '1', '0'),
(693, '1495290985', '757e06f1Caracas10.jpg', '2017-05-20 15:35:03', '1', '1'),
(694, '1495290985', 'dc268a0bCaracas12.jpg', '2017-05-20 15:35:15', '1', '0'),
(695, '1495410798', '4301660dCaracas3.jpg', '2017-05-22 00:08:50', '1', '0'),
(696, '1495410798', '0be38cc0Caracas4.jpg', '2017-05-22 00:09:05', '1', '0'),
(697, '1495410798', 'eb23eb8fCaracas5.jpg', '2017-05-22 00:09:11', '1', '0'),
(698, '1495410798', 'e35e3c57Caracas7.jpg', '2017-05-22 00:09:20', '1', '0'),
(699, '1495410798', '89576659Caracas6.jpg', '2017-05-22 00:09:22', '1', '0'),
(700, '1495410798', '64f62184Caracas9.jpg', '2017-05-22 00:09:23', '1', '1'),
(701, '1495224123', 'f917fd02icaribe1.jpg', '2017-05-22 13:50:24', '1', '0'),
(702, '1495224123', '0fd74e76icaribe 4.jpg', '2017-05-22 13:50:29', '1', '0'),
(703, '1495224123', 'efd43de5icaribe 3.jpg', '2017-05-22 13:50:32', '1', '0'),
(704, '1495224123', '7ed934abicaribe 2.jpg', '2017-05-22 13:50:40', '1', '0'),
(705, '1495224123', '1de890bcpiscinatarde.JPG', '2017-05-22 13:51:49', '1', '0'),
(706, '1495224123', '402b3fbdrestaurante2.JPG', '2017-05-22 13:51:56', '1', '0'),
(707, '1495224123', '1d18ed9fvideojuegos.JPG', '2017-05-22 13:51:59', '1', '0'),
(708, '1495461261', '8fb8c3feicaribe1.jpg', '2017-05-22 13:57:00', '1', '1'),
(709, '1495461261', '2bdb2749icaribe 4.jpg', '2017-05-22 13:57:04', '1', '0'),
(710, '1495461261', '0a7e7598icaribe 3.jpg', '2017-05-22 13:57:08', '1', '0'),
(711, '1495461261', '69261a55icaribe 2.jpg', '2017-05-22 13:57:12', '1', '0'),
(712, '1495461261', 'e7cbb26fcocteleria.JPG', '2017-05-22 13:58:52', '1', '0'),
(713, '1495461261', '1257657bpiscinatarde.JPG', '2017-05-22 13:58:58', '1', '0'),
(714, '1495461261', '08a759f8restaurante2.JPG', '2017-05-22 13:59:03', '1', '0'),
(715, '1495461261', 'be6e8bc7tumbonasenfila.JPG', '2017-05-22 13:59:09', '1', '0'),
(716, '1495461585', '0a01e555temp-bajaIC.jpg', '2017-05-22 14:11:27', '1', '0'),
(717, '1495461585', 'cdd31f84sunsol_punta_blanca.jpg', '2017-05-22 14:14:24', '1', '0'),
(718, '1495461585', 'ad204b8fsunsol punta blanca serviwebtravel 05.jpg', '2017-05-22 14:14:34', '1', '1'),
(719, '1495461585', '92c03105punta-blanca-isla-coche-venezuela.jpg', '2017-05-22 14:14:47', '1', '0'),
(720, '1495461585', '2fb438a9punta blanca 7.jpg', '2017-05-22 14:14:51', '1', '0'),
(721, '1495461585', 'c3e81fbc688317_image3918527_0.jpg', '2017-05-22 14:14:57', '1', '0'),
(722, '1495461585', 'cc4966e2habitacion_6_sunsol_isla_caribe.jpg', '2017-05-22 14:15:00', '1', '0'),
(723, '1495461585', '3afe9a5036_premiumcJ.jpg', '2017-05-22 14:15:06', '1', '0'),
(724, '1495462637', '4771cb40surf4.jpg', '2017-05-22 14:39:50', '1', '1'),
(725, '1495462637', 'c9dd8420surf2.jpg', '2017-05-22 14:39:53', '1', '0'),
(726, '1495462637', '69c16139surf3.jpg', '2017-05-22 14:39:55', '1', '0'),
(727, '1495462637', '71dfd0b7surf1.jpg', '2017-05-22 14:40:01', '1', '0'),
(728, '1495462637', '7d45be31playa_hotel_surf_paradise_el_yaque_isla_de_margarita.jpg', '2017-05-22 14:42:17', '1', '0'),
(729, '1495462637', '7f9ce6a9playa-el-yaque3.jpg', '2017-05-22 14:42:18', '1', '0'),
(730, '1495462637', 'ce55761bfoto1.JPG', '2017-05-22 14:43:26', '1', '0'),
(731, '1495462637', 'cb7c2f892.jpg', '2017-05-22 14:43:35', '1', '0'),
(732, '1495464296', '6bd17c9fHotel Tibisay  PIN (3).jpg', '2017-05-22 15:12:35', '1', '1'),
(733, '1495464296', '926e8fefHotel Tibisay (011).jpg', '2017-05-22 15:12:50', '1', '0'),
(734, '1495464296', '3f68151bHotel Tibisay (076).JPG', '2017-05-22 15:12:51', '1', '0'),
(735, '1495464296', 'd6624d57Hotel Tibisay (001).jpg', '2017-05-22 15:12:54', '1', '0'),
(736, '1495464296', '7ee0afa6Hotel Tibisay (019).jpg', '2017-05-22 15:13:04', '1', '0'),
(737, '1495464296', '533f645fHotel Tibisay (015).jpg', '2017-05-22 15:13:07', '1', '0'),
(738, '1495464296', '00676a5aHotel Tibisay (027).jpg', '2017-05-22 15:13:14', '1', '0'),
(739, '1495464296', '84868dfcHotel Tibisay (043).JPG', '2017-05-22 15:13:27', '1', '0'),
(740, '1495464296', '75543299Hotel Tibisay (046).JPG', '2017-05-22 15:13:35', '1', '0'),
(741, '1495464296', '431a7974Hotel Tibisay (070).JPG', '2017-05-22 15:13:43', '1', '0'),
(742, '1495464296', 'fe394f15Hotel Tibisay (055).JPG', '2017-05-22 15:13:47', '1', '0'),
(743, '1495464296', '9d24ac70Hotel Tibisay (070-1).JPG', '2017-05-22 15:13:49', '1', '0'),
(744, '1495464296', 'b8c48988Hotel Tibisay (079).JPG', '2017-05-22 15:13:53', '1', '0'),
(745, '1495466055', '5225ded0tropical 3.jpg', '2017-05-22 15:42:09', '1', '0'),
(746, '1495466055', '53ed124cArea Villas.jpg', '2017-05-22 15:42:14', '1', '0'),
(747, '1495466055', 'd60b1df7Area Piscina Central.jpg', '2017-05-22 15:42:17', '1', '0'),
(748, '1495466055', 'a6233155Entrada Principal.jpg', '2017-05-22 15:42:18', '1', '0'),
(749, '1495466055', '424d898aGarzas con Balcon.jpg', '2017-05-22 15:42:29', '1', '0'),
(750, '1495466055', '9c859e94Lobby.jpg', '2017-05-22 15:42:34', '1', '0'),
(751, '1495466055', '1a8b6427Piscina Central.jpg', '2017-05-22 15:42:55', '1', '1'),
(752, '1495466055', 'd5e2ac92Pisina Relax.jpg', '2017-05-22 15:43:03', '1', '0'),
(753, '1495466055', '192e40baRestaurant La Canoa.jpg', '2017-05-22 15:43:10', '1', '0'),
(754, '1495466055', '41fd90bcHotel Tropical Refuge (42).jpg', '2017-05-22 15:44:07', '1', '0'),
(755, '1495466055', 'c27ce154Hotel Tropical Refuge (10).jpg', '2017-05-22 15:44:17', '1', '0'),
(756, '1495466055', '6c0e6fb3Hotel Tropical Refuge (31).jpg', '2017-05-22 15:44:28', '1', '0'),
(757, '1495468005', '7287702b10417604_7_z.jpg', '2017-05-22 15:55:47', '1', '0'),
(758, '1495468005', '72ca724f8ec1ff39416d0855ade69558ab7480bc.jpg', '2017-05-22 15:55:47', '1', '1'),
(759, '1495468005', '6619a4ebvista_hotel_margarita_real_isla_margarita-420x280.jpg', '2017-05-22 15:56:18', '1', '0'),
(760, '1495468005', '6bb7e532unikhotel06.jpg', '2017-05-22 15:56:19', '1', '0'),
(761, '1495468005', '8059d160Unik-Hotel-Margarita-Restaurante-OZ-2.jpg', '2017-05-22 15:56:20', '1', '0'),
(762, '1495468005', '12976238UNIK_Hotel_IslaMargarita__10_.jpg', '2017-05-22 15:56:30', '1', '0'),
(763, '1495468005', '7de9f967habitaciontriple.jpg', '2017-05-22 15:56:46', '1', '0'),
(764, '1495468005', 'f7be7d02de5470d05f641a0422888de6bcdd4642.jpg', '2017-05-22 15:57:54', '1', '0'),
(765, '1495468005', '49b22a34HOTEL-UNIK-02.jpg', '2017-05-22 15:57:57', '1', '0'),
(766, '1495468005', '834bd2abHOTEL-UNIK-06.jpg', '2017-05-22 15:58:07', '1', '0'),
(767, '1495468992', '59fcc052IMG-20160929-WA003.jpg', '2017-05-22 16:07:13', '1', '0'),
(768, '1495468992', 'a249aef8IMG-20160929-WA004.jpg', '2017-05-22 16:07:22', '1', '0'),
(769, '1495468992', '3d1b0a0d_MG_1780.jpg', '2017-05-22 16:07:31', '1', '0'),
(770, '1495468992', '3f54f9e8IMG-20160929-WA008.jpg', '2017-05-22 16:07:39', '1', '0'),
(777, '1495468992', 'dc43e3063823_yaque_beach_hotel_5873.jpg', '2017-05-22 16:11:24', '1', '0'),
(772, '1495468992', 'b7a3b1d5IMG_7429.jpg', '2017-05-22 16:07:56', '1', '0'),
(773, '1495468992', '905dec1c_MG_1513.jpg', '2017-05-22 16:08:21', '1', '0'),
(774, '1495468992', '7fc479f900025.jpg', '2017-05-22 16:08:31', '1', '0'),
(775, '1495468992', 'ab23d5ceIMG_7084.jpg', '2017-05-22 16:08:40', '1', '1'),
(776, '1495468992', '248a628dIMG_7457.jpg', '2017-05-22 16:08:43', '1', '0'),
(778, '1495469584', 'aa8e465cyaque2.JPG', '2017-05-22 16:14:35', '1', '0'),
(779, '1495469584', '34c78a69yaque3.jpg', '2017-05-22 16:14:36', '1', '0'),
(780, '1495469584', 'aae9d242yaque1.jpg', '2017-05-22 16:14:39', '1', '0'),
(781, '1495469584', 'b8419a8fyaque4.jpg', '2017-05-22 16:14:42', '1', '0'),
(782, '1495469584', 'd54d07bab37129a47bf82df22083443c61e5ea10.jpg', '2017-05-22 16:16:33', '1', '0'),
(783, '1495469584', '70d1a31fpiscina2.jpg', '2017-05-22 16:16:34', '1', '1'),
(784, '1495469584', '8ceb4eabpiscinanocturna.jpg', '2017-05-22 16:16:36', '1', '0'),
(785, '1495469584', 'd022d33cmargarita_hotel_el_yaque_paradise.JPG', '2017-05-22 16:16:36', '1', '0'),
(786, '1495469584', '75a4e413playa_hotel_yaque_paradise.jpg', '2017-05-22 16:16:41', '1', '0'),
(787, '1495469584', 'effbbcc4restaurante_playa_hotel_yaque_paradise_isla_de_margarita.jpg', '2017-05-22 16:16:44', '1', '0'),
(788, '1495469584', '9e322e2bhabsaten.jpg', '2017-05-22 16:17:48', '1', '0'),
(789, '1495488069', '9c0c1b1bboda-masaje-pareja.jpg', '2017-05-22 21:24:50', '1', '0'),
(790, '1495493938', 'bb73a49720160707_074508.jpg', '2017-05-22 23:00:05', '1', '0'),
(791, '1495493938', 'fde768ad20160707_074504.jpg', '2017-05-22 23:00:06', '1', '0'),
(792, '1495493938', 'dd6082a520160707_074515.jpg', '2017-05-22 23:00:40', '1', '0'),
(793, '1495546546', '9857baf9Embassy 3.jpg', '2017-05-23 13:42:00', '1', '1'),
(794, '1495546546', 'a8263a61Embassy 4.jpg', '2017-05-23 13:42:08', '1', '0'),
(795, '1495546546', '00fb1606Embassy1.jpg', '2017-05-23 13:42:31', '1', '0'),
(796, '1495546546', '26cd88b0Embassy 2.jpg', '2017-05-23 13:42:34', '1', '0'),
(797, '1495546546', '7a8481a2CCSVZES_rooms_full_livingtdbs.jpg', '2017-05-23 13:44:04', '1', '0'),
(798, '1495546546', '50e36337CCSVZES_meetings_full_banquet6.jpg', '2017-05-23 13:44:06', '1', '0'),
(799, '1495546546', '3797b8afES_daypool_23_712x342_FitToBoxSmallDimension_Center.jpg', '2017-05-23 13:44:13', '1', '0'),
(800, '1495546546', '54839d46CCSVZES_meetings_full_smmeetrm03.jpg', '2017-05-23 13:44:22', '1', '0'),
(979, '1495547103', 'ac672caeeuroccs4.jpg', '2017-06-19 22:47:15', '1', '0'),
(980, '1495547103', '2f06ef23euroccs3.jpg', '2017-06-19 22:47:16', '1', '0'),
(978, '1495547103', 'f53f623feuroccs2.jpg', '2017-06-19 22:47:14', '1', '0'),
(977, '1495547103', 'dbf78a3aeuroccs1.jpg', '2017-06-19 22:47:05', '1', '0'),
(976, '1495547103', '5be17986euroccs6.jpg', '2017-06-19 22:46:53', '1', '1'),
(819, '1495549361', 'cf4544eepestana-caracas-overview-5-635742786415166697.jpg', '2017-05-23 14:45:16', '1', '0'),
(818, '1495549361', 'c1c3bd62pestana-caracas-overview-6-635742786418645469.jpg', '2017-05-23 14:44:38', '1', '0'),
(817, '1495549361', 'd1c44638pestana4.jpg', '2017-05-23 14:43:38', '1', '1'),
(820, '1495549361', 'fc2bb847pestana1.jpg', '2017-05-23 14:45:56', '1', '0'),
(821, '1495549361', 'a41df915pestana3.jpg', '2017-05-23 14:46:43', '1', '0'),
(822, '1495549361', 'c1131cb6pestana2.jpg', '2017-05-23 14:47:49', '1', '0'),
(823, '1495549361', 'e4a31200pestana-caracas-king-junior-suite-635742786493762756.jpg', '2017-05-23 14:50:58', '1', '0'),
(824, '1495549361', '42157676pestana-caracas-twin-superior-635742786461304304.jpg', '2017-05-23 14:51:18', '1', '0'),
(825, '1495549361', 'd5fbea59pestana-caracas-overview-2-635742786408533394.jpg', '2017-05-23 14:52:03', '1', '0'),
(826, '1495551264', '8fcb7c0apresident2.jpg', '2017-05-23 15:25:22', '1', '0'),
(827, '1495551264', '73f947daDSC08415.JPG', '2017-05-23 15:25:28', '1', '0'),
(828, '1495551264', '79178d89DSC08421.JPG', '2017-05-23 15:25:56', '1', '0'),
(829, '1495551264', '231dce2bDSC08406.JPG', '2017-05-23 15:25:59', '1', '0'),
(830, '1495551264', 'cd144e08president 1.jpg', '2017-05-23 15:26:01', '1', '0'),
(831, '1495551264', '881a7a71DSC08409.JPG', '2017-05-23 15:26:08', '1', '0'),
(832, '1495551264', '8948c456DSC08424.JPG', '2017-05-23 15:26:10', '1', '0'),
(833, '1495551264', '896a1bc2DSC08410.JPG', '2017-05-23 15:26:15', '1', '0'),
(835, '1495551264', '772e9c57FachadaPlaza.jpg', '2017-05-23 15:32:48', '1', '1'),
(863, '1495556450', '59296d4elobbybar1.JPG', '2017-05-24 12:07:56', '1', '0'),
(870, '1495561725', '759f0cf2paseoccs3.jpg', '2017-05-24 12:14:42', '1', '0'),
(855, '1495553606', '8765b01d473.jpg', '2017-05-23 20:10:59', '1', '1'),
(871, '1495561725', 'c5d49e6apaseoccs4.jpg', '2017-05-24 12:14:46', '1', '0'),
(846, '1495562798', 'bb13263bAllsuites2.JPG', '2017-05-23 18:10:33', '1', '0'),
(847, '1495563354', 'c483baffAllsuites2.JPG', '2017-05-23 18:48:46', '1', '0'),
(848, '1495563354', '5e89b81eallsuites1.JPG', '2017-05-23 18:48:54', '1', '0'),
(849, '1495563354', '43cfd7b5Allsuites3.JPG', '2017-05-23 18:48:57', '1', '0'),
(850, '1495563354', '4ccedd62Allsuites4.JPG', '2017-05-23 18:48:58', '1', '0'),
(851, '1495568127', '2d03ec52allsuites1.JPG', '2017-05-23 19:37:06', '1', '0'),
(852, '1495568127', 'b90c48afAllsuites2.JPG', '2017-05-23 19:37:14', '1', '1'),
(853, '1495568127', '9194b480Allsuites3.JPG', '2017-05-23 19:37:19', '1', '0'),
(854, '1495568127', '68d94067Allsuites4.JPG', '2017-05-23 19:37:22', '1', '0'),
(856, '1495553606', 'fc38de3c448.jpg', '2017-05-23 20:12:55', '1', '0'),
(857, '1495553606', '949b311d449.jpg', '2017-05-23 20:12:57', '1', '0'),
(858, '1495553606', '4c97dcc6450.jpg', '2017-05-23 20:13:00', '1', '0'),
(859, '1495553606', '4c5cd351454.jpg', '2017-05-23 20:13:03', '1', '0'),
(860, '1495553606', '3d65079c456.jpg', '2017-05-23 20:13:08', '1', '0'),
(861, '1495553606', '2a569213368685_fotpe2_ROOM.jpg', '2017-05-23 20:13:16', '1', '0'),
(862, '1495553606', 'd35ede09526005_168_z.jpg', '2017-05-23 20:13:16', '1', '0'),
(864, '1495556450', '07044c97suite_plus1.JPG', '2017-05-24 12:08:02', '1', '0'),
(865, '1495556450', '53fe174esuite_plus4.JPG', '2017-05-24 12:08:08', '1', '0'),
(866, '1495556450', '6fd5b686doble2.JPG', '2017-05-24 12:08:12', '1', '0'),
(867, '1495556450', '0d7ac6aahotel-continental-altamira-general-1411aab.jpg', '2017-05-24 12:11:52', '1', '0'),
(868, '1495556450', 'c8bea1f2continental-altamira2.jpg', '2017-05-24 12:12:01', '1', '0'),
(869, '1495556450', '9671be8ahotel-continental-altamira.jpg', '2017-05-24 12:12:45', '1', '1'),
(872, '1495561725', '994214b2paseoccs2.jpg', '2017-05-24 12:14:48', '1', '0'),
(873, '1495561725', '2378a8f4paseoccs1.jpg', '2017-05-24 12:14:49', '1', '0'),
(874, '1495561725', '0c1d350bpaseo11.520.360.jpg', '2017-05-24 12:16:26', '1', '1'),
(875, '1495561725', '1a02b803pis-y-rest.jpg', '2017-05-24 12:16:33', '1', '0'),
(876, '1495561725', 'a9623d78524_hotel_paseo_las_mercedes_6011.jpg', '2017-05-24 12:16:37', '1', '0'),
(877, '1495561725', 'e3e31543hotel-paseo-las-mercedes.jpg', '2017-05-24 12:16:38', '1', '0'),
(879, '1495628280', '42544d166237_16061411580043530988.jpg', '2017-05-24 12:41:58', '1', '1'),
(880, '1495628280', '1cfe89416237_16061411580043531004.jpg', '2017-05-24 12:42:01', '1', '0'),
(881, '1495628280', '9d3eaf5dgran-melia-caracas-hotel.jpg', '2017-05-24 12:42:04', '1', '0'),
(882, '1495628280', '9a626727gran-melia-caracas-hotel (1).jpg', '2017-05-24 12:42:08', '1', '0'),
(883, '1495628280', '7b3758aahotel-gran-melia-caracas-habitacion-209f57.jpg', '2017-05-24 12:42:18', '1', '0'),
(884, '1495628280', '386d7383104237a_hb_w_005.jpg', '2017-05-24 12:42:22', '1', '0'),
(885, '1495628280', 'c89ec65e47449822.jpg', '2017-05-24 12:42:24', '1', '0'),
(886, '1495628280', '755c27a5granmeliacaracas-carrusel3.jpg', '2017-05-24 12:42:29', '1', '0'),
(887, '1495628280', '627c4c29hotel-gran-melia-caracas-servicios-209f55.jpg', '2017-05-24 12:42:35', '1', '0'),
(888, '1495630010', '5293aeaa17076400_228867030913080_3779918150758301696_n.jpg', '2017-05-24 12:57:32', '1', '0'),
(890, '1495630010', '69f35aac301204.jpg', '2017-05-24 12:58:10', '1', '0'),
(891, '1495630010', 'bdb667bfF9a-Sidorkovs-hotel-waldorf.jpg', '2017-05-24 12:58:14', '1', '0'),
(893, '1495630010', 'c9e6f02e1485471291739.jpg', '2017-05-24 12:59:59', '1', '1'),
(894, '1496867624', 'cc4420eaaruba2.jpg', '2017-06-07 20:45:58', '1', '1'),
(895, '1496867624', '33fb598earuba3.jpg', '2017-06-07 20:46:02', '1', '0'),
(896, '1496867624', 'c66a2b32aruba1.JPG', '2017-06-07 20:47:02', '1', '0'),
(897, '1497379662', '30a8bc91Cristal1.jpg', '2017-06-13 18:59:31', '1', '1'),
(898, '1497379662', 'f6e50e8fCristal2.jpg', '2017-06-13 18:59:36', '1', '0'),
(899, '1497379662', 'c3b77cc8Doble Matrimonial.jpg', '2017-06-13 18:59:45', '1', '0'),
(900, '1497379662', '53e53db5Hab. King.jpg', '2017-06-13 18:59:51', '1', '0'),
(901, '1497379662', '8c12f520HabitaciÃ³n Multifamiliar.jpg', '2017-06-13 18:59:55', '1', '0'),
(902, '1497379662', 'b81a064dMatrimonial.jpg', '2017-06-13 19:00:05', '1', '0'),
(903, '1497379662', '2f38870eSalÃ³n Prebo 2.jpg', '2017-06-13 19:00:11', '1', '0'),
(904, '1497379662', 'b86ade76SalÃ³n Tarbes.jpg', '2017-06-13 19:00:18', '1', '0'),
(905, '1497383096', 'fdd65eaabelensate1.jpg', '2017-06-13 19:52:39', '1', '1'),
(906, '1497383096', '75294b39belensate2.jpg', '2017-06-13 19:52:42', '1', '0'),
(907, '1497383096', 'cb2636ecbelensate3.jpg', '2017-06-13 19:52:49', '1', '0'),
(908, '1497383096', 'abdf71b0belensate4.jpg', '2017-06-13 19:52:53', '1', '0'),
(909, '1497383588', '6adf830aserrano1.jpg', '2017-06-13 19:57:05', '1', '1'),
(910, '1497383588', '097f759bserrano2.jpg', '2017-06-13 19:57:07', '1', '0'),
(911, '1497383588', '70aa6520serrano3.jpg', '2017-06-13 19:57:12', '1', '0'),
(912, '1497383588', '7b8308a3serrano 4.jpg', '2017-06-13 19:57:16', '1', '0'),
(913, '1497384193', '6996df31terraza1.jpg', '2017-06-13 20:04:32', '1', '0'),
(914, '1497384193', '371df7e4terraza2.jpg', '2017-06-13 20:04:36', '1', '1'),
(915, '1497384193', 'a4155b6cterraza3.jpg', '2017-06-13 20:04:41', '1', '0'),
(916, '1497384193', 'ace9a623terraza4.jpg', '2017-06-13 20:04:53', '1', '0'),
(917, '1497384700', '87becd9dmistafi 2.jpg', '2017-06-13 20:13:05', '1', '1'),
(918, '1497384700', 'c9d5d260mistafi 3.jpg', '2017-06-13 20:13:09', '1', '0'),
(919, '1497384700', 'a3ec430emistafi 4.jpg', '2017-06-13 20:13:14', '1', '0'),
(920, '1497384700', 'a33b18e2mistafi.JPG', '2017-06-13 20:13:18', '1', '0'),
(921, '1497384876', 'a4dc157bpark1.jpg', '2017-06-13 20:17:04', '1', '1'),
(922, '1497384876', 'ea9bc711park4.jpg', '2017-06-13 20:17:10', '1', '0'),
(923, '1497384876', '5367ef61park3.jpg', '2017-06-13 20:17:16', '1', '0'),
(924, '1497384876', '0dacd379park2.jpg', '2017-06-13 20:17:25', '1', '0'),
(925, '1497385460', '6904cccfla pedregosa 7.JPG', '2017-06-13 20:29:06', '1', '1'),
(926, '1497385460', 'ff440983la pedregosa4.jpg', '2017-06-13 20:29:17', '1', '0'),
(927, '1497385460', '994cdf3ela pedregosa 8.JPG', '2017-06-13 20:29:20', '1', '0'),
(928, '1497385460', 'e9cde224la pedregosa 6.JPG', '2017-06-13 20:29:25', '1', '0'),
(929, '1497385777', 'bd9a6236tibisay1.jpg', '2017-06-13 20:37:13', '1', '1'),
(930, '1497385777', '6722d455tibisay4.jpg', '2017-06-13 20:38:16', '1', '0'),
(931, '1497385777', '1feea754tibisay 3.jpg', '2017-06-13 20:38:30', '1', '0'),
(932, '1497385777', '458189b1tibisay2.jpg', '2017-06-13 20:38:32', '1', '0'),
(933, '1497385777', '33275e6dtibisay5.jpg', '2017-06-13 20:38:33', '1', '0'),
(934, '1497385777', 'a63537fbtibisay7.jpg', '2017-06-13 20:38:38', '1', '0'),
(940, '1497386497', '514ada41convencion5.jpg', '2017-06-13 20:47:30', '1', '0'),
(936, '1497386497', 'c668b4f3Convencion 2.jpg', '2017-06-13 20:42:54', '1', '0'),
(937, '1497386497', '33a8e8c0convencion 3.jpg', '2017-06-13 20:42:59', '1', '0'),
(938, '1497386497', 'cf9ae107convencion 4.jpg', '2017-06-13 20:43:03', '1', '0'),
(939, '1497386497', '86e57394convencion5.jpg', '2017-06-13 20:46:09', '1', '1'),
(941, '1497387319', '9284e428abadia1.jpg', '2017-06-13 20:59:02', '1', '1'),
(942, '1497387319', '8f995e69abadia2.jpg', '2017-06-13 20:59:06', '1', '0'),
(943, '1497387319', '92e353e1abadia5.jpg', '2017-06-13 20:59:12', '1', '0'),
(944, '1497387319', 'aa4f5402abadia3.jpg', '2017-06-13 20:59:18', '1', '0'),
(945, '1497905159', 'febe89c1tropi4.JPG', '2017-06-19 21:34:22', '1', '0'),
(946, '1497905159', '95f6bca7tropi1.jpg', '2017-06-19 21:34:31', '1', '1'),
(947, '1497905159', '17d263cetropi2.jpg', '2017-06-19 21:34:54', '1', '0'),
(948, '1497905159', '7b3b05d8tropi3.jpg', '2017-06-19 21:35:03', '1', '0'),
(949, '1497908153', 'cfcf3cc9mill1.jpg', '2017-06-19 21:49:16', '1', '0'),
(950, '1497908153', '3b57322emill2.JPG', '2017-06-19 21:49:19', '1', '0'),
(951, '1497908153', 'b3eb8e16mill3.JPG', '2017-06-19 21:49:24', '1', '1'),
(952, '1497908153', '71ce9b9fmill4.JPG', '2017-06-19 21:49:28', '1', '0'),
(953, '1497909011', 'b9ccba01euroccs6.jpg', '2017-06-19 22:05:35', '1', '1'),
(954, '1497909011', '83944e26euroccs1.jpg', '2017-06-19 22:05:48', '1', '0'),
(955, '1497909011', 'bd037799euroccs4.jpg', '2017-06-19 22:05:55', '1', '0'),
(959, '1497909011', 'ae996337euroccs3.jpg', '2017-06-19 22:06:28', '1', '0'),
(957, '1497909011', '4f430c41euroccs2.jpg', '2017-06-19 22:06:00', '1', '0'),
(958, '1497909011', '433556f0euroccs5.jpg', '2017-06-19 22:06:01', '1', '0'),
(960, '1497910019', 'e2857897euromiq4.jpg', '2017-06-19 22:13:42', '1', '1'),
(961, '1497910019', 'c30cce85euromiq1.jpg', '2017-06-19 22:13:45', '1', '0'),
(962, '1497910019', '09db6ac7euromiq2.jpg', '2017-06-19 22:13:49', '1', '0'),
(963, '1497910019', '909b0dcbeuromiq3.jpg', '2017-06-19 22:13:55', '1', '0'),
(964, '1497910486', '4295a7a5euromcy1.jpg', '2017-06-19 22:20:43', '1', '1'),
(965, '1497910486', 'c13c4986euromcy2.jpg', '2017-06-19 22:20:47', '1', '0'),
(966, '1497910486', 'cc85e1c8euromcy4.jpg', '2017-06-19 22:20:55', '1', '0'),
(967, '1497910486', '154003f5euromcy3.jpg', '2017-06-19 22:20:59', '1', '0'),
(968, '1497911400', '53f6918beuroet1.jpg', '2017-06-19 22:36:38', '1', '1'),
(969, '1497911400', '737e8147euroet2.jpg', '2017-06-19 22:36:39', '1', '0'),
(970, '1497911400', 'd986ef7ceuroet3.jpg', '2017-06-19 22:36:44', '1', '0'),
(971, '1497911400', 'd804a79ceuroet4.jpg', '2017-06-19 22:36:51', '1', '0'),
(972, '1497911878', '1532ed64eurog1.jpg', '2017-06-19 22:44:39', '1', '1'),
(973, '1497911878', 'c968f3f9eurog2.jpg', '2017-06-19 22:44:49', '1', '0'),
(974, '1497911878', 'a6c0abcfeurog3.jpg', '2017-06-19 22:44:53', '1', '0'),
(975, '1497911878', 'd3fdfde6euro4.jpg', '2017-06-19 22:44:58', '1', '0'),
(981, '1495547103', '5f9977c4euroccs5.jpg', '2017-06-19 22:47:19', '1', '0'),
(982, '1497914805', '0b7fc929eurob1.jpg', '2017-06-19 23:32:12', '1', '1'),
(983, '1497914805', 'f847b8f5eurob2.jpg', '2017-06-19 23:32:12', '1', '0'),
(984, '1497914805', 'abce75cceurob3.jpg', '2017-06-19 23:32:16', '1', '0'),
(985, '1497914805', '49c078a3eurob4.jpg', '2017-06-19 23:32:21', '1', '0'),
(986, '1497915182', '6ba53b88eurocoro1.jpg', '2017-06-20 00:05:17', '1', '1'),
(987, '1497915182', 'aee6069aeurocoro2.jpg', '2017-06-20 00:05:21', '1', '0'),
(988, '1497915182', '8ef2f7b1eurocoro3.jpg', '2017-06-20 00:05:23', '1', '0'),
(989, '1497915182', '68c3ba17eurocoro4.jpg', '2017-06-20 00:05:27', '1', '0'),
(990, '1497917185', '791b8172euroba1.jpg', '2017-06-20 00:14:21', '1', '1'),
(991, '1497917185', 'bb357f47euroba2.jpg', '2017-06-20 00:14:24', '1', '0'),
(992, '1497917185', '6801588beuroba3.jpg', '2017-06-20 00:14:30', '1', '0'),
(993, '1497917185', '756fe5ffeuroba4.jpg', '2017-06-20 00:14:34', '1', '0'),
(994, '1497917185', 'eb038d25euroba5.jpg', '2017-06-20 00:14:38', '1', '0'),
(995, '1497917185', '1d637195euroba6.JPEG', '2017-06-20 00:14:42', '1', '0'),
(996, '1497918224', '2b514eb2euromia1.jpg', '2017-06-20 00:30:42', '1', '1'),
(997, '1497918224', 'c18ad414euro mia2.jpg', '2017-06-20 00:30:48', '1', '0'),
(998, '1497918224', '8efe4720euromia4.jpg', '2017-06-20 00:30:53', '1', '0'),
(999, '1497918224', '248afae4euromia5.jpg', '2017-06-20 00:30:55', '1', '0'),
(1000, '1497918224', '6c0b1b88euromia6.jpg', '2017-06-20 00:30:57', '1', '0'),
(1001, '1497918224', 'adbb6f69euromia3.jpg', '2017-06-20 00:31:01', '1', '0'),
(1002, '1498771664', '3a870ce47mares1.JPG', '2017-06-29 21:38:42', '1', '1'),
(1003, '1498771664', 'c084e86b7mares2.JPG', '2017-06-29 21:38:45', '1', '0'),
(1004, '1498771664', '40755c0f7mares3.JPG', '2017-06-29 21:38:54', '1', '0'),
(1005, '1498771664', '0646ccac7mares4.JPG', '2017-06-29 21:38:58', '1', '0'),
(1006, '1498771664', '30e4cf127mares5.JPG', '2017-06-29 21:39:02', '1', '0'),
(1007, '1498771664', 'dfcaaed97MARES6.JPG', '2017-06-29 21:39:07', '1', '0'),
(1008, '1498771664', 'cff5e12b7mares7.JPG', '2017-06-29 21:39:13', '1', '0'),
(1009, '1498771664', '35fd0fb07mares8.JPG', '2017-06-29 21:39:18', '1', '0'),
(1010, '1498771664', 'd402e72a7mares9.JPG', '2017-06-29 21:39:25', '1', '0'),
(1011, '1498772411', 'ce54d8f8Vargas 1.jpeg', '2017-06-29 22:10:29', '1', '0'),
(1012, '1498772411', '89f83f11vargas1.jpg', '2017-06-29 22:10:34', '1', '1'),
(1013, '1498772411', '7ddd7fadvargas3.jpg', '2017-06-29 22:10:44', '1', '0'),
(1014, '1498772411', '0e0f79ebVargas4.jpg', '2017-06-29 22:10:46', '1', '0'),
(1015, '1498772411', 'a0c97e37vargas7.jpg', '2017-06-29 22:10:54', '1', '0'),
(1016, '1498772411', '656b118evargas5.jpg', '2017-06-29 22:11:06', '1', '0'),
(1017, '1498772411', 'c408d525vargas6.jpg', '2017-06-29 22:11:09', '1', '0'),
(1018, '1498772411', 'dfbaadc7vargas2.jpg', '2017-06-29 22:11:20', '1', '0'),
(1019, '1498775087', '1b377d2fVargas 1.jpeg', '2017-06-29 22:29:43', '1', '0'),
(1020, '1498775087', 'ce6d8ac2vargas1.jpg', '2017-06-29 22:29:48', '1', '1'),
(1021, '1498775087', '1e03dd25vargas3.jpg', '2017-06-29 22:29:57', '1', '0'),
(1022, '1498775087', '42db6dceVargas4.jpg', '2017-06-29 22:30:00', '1', '0'),
(1023, '1498775087', '63eb6e07vargas7.jpg', '2017-06-29 22:30:10', '1', '0'),
(1024, '1498775087', 'c064a757vargas5.jpg', '2017-06-29 22:30:24', '1', '0'),
(1025, '1498775087', 'fd26fcd8vargas6.jpg', '2017-06-29 22:30:24', '1', '0'),
(1026, '1498775087', 'e3f1f921vargas2.jpg', '2017-06-29 22:30:35', '1', '0'),
(1027, '1499371034', '182d6b63portofino 1.jpg', '2017-07-06 20:12:17', '1', '0'),
(1028, '1499371034', 'e15f9ac6portofino 2.jpg', '2017-07-06 20:12:25', '1', '1'),
(1029, '1499371034', '6ae5f771portofino 3.jpg', '2017-07-06 20:12:31', '1', '0'),
(1030, '1499371034', 'cc58af01portofino 4.jpg', '2017-07-06 20:12:47', '1', '0'),
(1031, '1499372021', 'dd855102portofino 1.jpg', '2017-07-06 20:16:22', '1', '0'),
(1032, '1499372021', '04098fbdportofino 2.jpg', '2017-07-06 20:16:25', '1', '1'),
(1033, '1499372021', '023d9490portofino 3.jpg', '2017-07-06 20:16:29', '1', '0'),
(1034, '1499372021', 'cb7f5d66portofino 4.jpg', '2017-07-06 20:16:33', '1', '0'),
(1035, '1499456338', 'b1c32d58peru1.jpg', '2017-07-07 20:18:52', '1', '1'),
(1036, '1499456338', 'd4d6dd77peru2.jpg', '2017-07-07 20:18:56', '1', '0'),
(1037, '1499456338', '43a37a6cperu3.jpg', '2017-07-07 20:19:08', '1', '0'),
(1038, '1499456338', '459b777cperu4.jpg', '2017-07-07 20:19:09', '1', '0'),
(1039, '1499456338', 'b41cd8f3peru5.jpg', '2017-07-07 20:19:09', '1', '0'),
(1040, '1499456338', '99de276aperu6.jpg', '2017-07-07 20:19:18', '1', '0'),
(1041, '1499456338', '6975ec13peru7.JPG', '2017-07-07 20:19:22', '1', '0'),
(1042, '1499457565', '79c557c818893471_1601618849848382_7867868307945176235_n.jpg', '2017-07-07 20:21:57', '1', '0'),
(1043, '1499457565', '832f509718953100_1327763420610170_4431369773515009212_n.jpg', '2017-07-07 20:22:01', '1', '1'),
(1044, '1499457565', 'de7e0e1419029196_774925102677169_3302561017636484293_n.jpg', '2017-07-07 20:22:07', '1', '0'),
(1045, '1499874286', 'a98b8bffel-yaque.jpg', '2017-07-12 16:01:45', '1', '1'),
(1046, '1499874286', 'ea43a204G_266917_00501rdcho2zu.jpg', '2017-07-12 16:01:52', '1', '0'),
(1047, '1499874286', '9d9adcf9Venta-Hotel-Casa-Viento-Playa-El-Yaque-Margarita_6.jpg', '2017-07-12 16:01:57', '1', '0'),
(1048, '1499874286', 'aa4ca544images.jpg', '2017-07-12 16:03:32', '1', '0'),
(1049, '1499976136', '11468bfbfranjas.png', '2017-07-13 20:03:31', '1', '1'),
(1050, '1501252635', 'e0377f6avenetur 01.jpg', '2017-07-28 14:52:45', '1', '1'),
(1051, '1501252635', '7156c3c3venetur 02.jpg', '2017-07-28 14:53:06', '1', '0'),
(1052, '1501252635', '24f233bevenetur 2.png', '2017-07-28 14:53:12', '1', '0'),
(1053, '1501252635', 'b551b78avenetur 3.png', '2017-07-28 14:53:20', '1', '0'),
(1054, '1501252635', '9019ab77venetur 04.jpg', '2017-07-28 14:53:22', '1', '0'),
(1055, '1501252635', '37530ce4venetur 03.jpg', '2017-07-28 14:53:35', '1', '0'),
(1056, '1501252635', 'fa4a941avenetur 4.png', '2017-07-28 14:53:37', '1', '0'),
(1057, '1501252635', 'd806eed5venetur 1.png', '2017-07-28 14:54:09', '1', '0'),
(1058, '1515679755', '016c1f3arosabela1.jpg', '2018-01-11 14:21:43', '1', '1'),
(1059, '1515679755', '32169067rosabela2.jpg', '2018-01-11 14:21:51', '1', '0'),
(1060, '1515679755', '1f50ce9frosabela3.jpg', '2018-01-11 14:21:56', '1', '0'),
(1061, '1515679755', '49b269f3rosabela4.jpg', '2018-01-11 14:22:01', '1', '0'),
(1062, '1515679755', '191b08a8rosabela5.jpg', '2018-01-11 14:22:11', '1', '0'),
(1063, '1515679755', '42687e0drosabela6.jpg', '2018-01-11 14:22:36', '1', '0'),
(1064, '1515679755', 'e528158crosabela8.jpg', '2018-01-11 14:22:38', '1', '0'),
(1065, '1515679755', 'cbf58b5arosdabela7.jpg', '2018-01-11 14:22:39', '1', '0'),
(1066, '1515706291', 'e3db53fdBolivar 1.png', '2018-01-12 11:54:10', '1', '0'),
(1067, '1515706291', '447cc8acpzo1.gif', '2018-01-12 11:54:11', '1', '0'),
(1068, '1515706291', 'b8005c5fpzo2.jpg', '2018-01-12 11:54:34', '1', '0'),
(1069, '1515706291', '3b7aab72pzo3.gif', '2018-01-12 11:54:37', '1', '0'),
(1070, '1515706291', 'eb588e52pzo4.jpg', '2018-01-12 11:54:47', '1', '0'),
(1071, '1515706291', '5641d61bpzo3.jpg', '2018-01-12 11:54:49', '1', '0'),
(1072, '1515706291', 'f0b61524pzo1.jpg', '2018-01-12 11:55:27', '1', '0'),
(1081, '1515758186', 'd10b1e18pzo7.jpg', '2018-01-12 12:05:08', '1', '1'),
(1080, '1515758186', 'cc4062a8pzo6.jpg', '2018-01-12 12:05:01', '1', '0'),
(1075, '1515758186', '2ba924b1pzo2.jpg', '2018-01-12 11:59:44', '1', '0'),
(1076, '1515758186', 'ae8bb0e5pzo3.gif', '2018-01-12 11:59:50', '1', '0'),
(1077, '1515758186', '26f2f94cpzo3.jpg', '2018-01-12 11:59:58', '1', '0'),
(1078, '1515758186', 'aaaa2516pzo4.jpg', '2018-01-12 12:00:08', '1', '0'),
(1079, '1515758186', '89d7be09pzo1.jpg', '2018-01-12 12:00:43', '1', '0'),
(1085, '1486892621', '615aba55valencia4.jpg', '2018-01-16 21:05:42', '1', '0'),
(1086, '1486892621', 'fb4d3db0valencia6.jpg', '2018-01-16 21:05:55', '1', '1'),
(1087, '1486892621', '043474bcvalencia5.JPG', '2018-01-16 21:06:00', '1', '0'),
(1089, '1489092186', 'ec8166b5Margarita 5.jpg', '2018-01-16 21:16:18', '1', '0'),
(1090, '1489092186', 'd297d4baMargarita 6.jpg', '2018-01-16 21:16:23', '1', '0'),
(1091, '1489092186', '3a648b26Margarita 7.jpg', '2018-01-16 21:16:32', '1', '0'),
(1097, '1516219178', 'e47b2b18Piso1.jpg', '2018-01-17 20:06:09', '1', '0'),
(1098, '1516219178', 'dbad304ePipo2.jpg', '2018-01-17 20:06:17', '1', '0'),
(1099, '1516219178', 'a843bf06Pipo3.JPG', '2018-01-17 20:06:20', '1', '1'),
(1100, '1516219178', 'b604bb8dPipo4.JPG', '2018-01-17 20:06:27', '1', '0'),
(1101, '1516219680', 'a3931c22ara1.jpg', '2018-01-17 20:19:03', '1', '0'),
(1102, '1516219680', '1d2aa05eara2.jpg', '2018-01-17 20:19:06', '1', '0'),
(1103, '1516219680', 'b59f6efcara3.jpg', '2018-01-17 20:19:14', '1', '1'),
(1104, '1516219680', '637c3474ara5.jpg', '2018-01-17 20:19:22', '1', '0'),
(1105, '1516219680', '54a0ad96ara4.jpg', '2018-01-17 20:19:22', '1', '0'),
(1114, '1516220448', 'a2399adfWAKU4.jpg', '2018-01-17 20:37:48', '1', '0'),
(1113, '1516220448', 'd43dd77aWAKU2.jpg', '2018-01-17 20:37:44', '1', '0'),
(1112, '1516220448', '3a7800cdWAKU3.jpg', '2018-01-17 20:37:44', '1', '1'),
(1111, '1516220448', '487f84ecWAKU1.jpg', '2018-01-17 20:37:36', '1', '0'),
(1115, '1516221660', '7fed98e6catimar1.jpg', '2018-01-17 20:45:59', '1', '0'),
(1116, '1516221660', 'c1885e5ecatimar2.jpg', '2018-01-17 20:46:04', '1', '0'),
(1117, '1516221660', 'c54642d8catimar3.jpg', '2018-01-17 20:46:10', '1', '0'),
(1118, '1516221660', 'ed97a2f1catimar4.jpg', '2018-01-17 20:46:15', '1', '0'),
(1119, '1516221660', 'e39d2190CATIMAR5.jpg', '2018-01-17 20:47:43', '1', '1'),
(1120, '1516221660', 'e5c60ccaCATIMAR6.jpg', '2018-01-17 20:47:45', '1', '0'),
(1121, '1516292997', '60212dc5Baires1.jpg', '2018-01-18 16:45:18', '1', '1'),
(1122, '1516292997', '44d57daeBaires2.jpg', '2018-01-18 16:45:21', '1', '0'),
(1123, '1516292997', '078b9ca7Baires3.jpg', '2018-01-18 16:45:30', '1', '0'),
(1124, '1516292997', '737625a0Baires4.jpg', '2018-01-18 16:45:35', '1', '0'),
(1125, '1516292997', '7e17ebd0bariloche1.jpg', '2018-01-18 16:45:46', '1', '0'),
(1126, '1516292997', 'cf7ad763glaciar1.jpg', '2018-01-18 16:46:02', '1', '0'),
(1127, '1516292997', '4a3e37b6Pampas1.jpg', '2018-01-18 16:46:08', '1', '0'),
(1128, '1516292997', 'aba94cc4igua1.jpg', '2018-01-18 16:46:08', '1', '0'),
(1129, '1516292997', 'b2c3156etigre1.jpg', '2018-01-18 16:46:22', '1', '0'),
(1130, '1516292997', 'ff535414bombonera1.jpg', '2018-01-18 16:46:22', '1', '0'),
(1131, '1516292997', '6a91c409Salta BÃ¡sico 3D-2N.png', '2018-01-18 16:48:23', '1', '0'),
(1132, '1516296163', 'e6d59267morro1.jpg', '2018-01-18 18:01:21', '1', '0'),
(1133, '1516296163', 'af869708morro2.jpg', '2018-01-18 18:01:25', '1', '1'),
(1134, '1516296163', '9b891859morro3.jpg', '2018-01-18 18:01:33', '1', '0'),
(1135, '1516296163', 'e9a18b7fmorro4.jpg', '2018-01-18 18:01:42', '1', '0'),
(1136, '1516296163', 'b1adf831morro5.jpg', '2018-01-18 18:01:42', '1', '0'),
(1137, '1516296163', '3f175990morro6.jpg', '2018-01-18 18:01:44', '1', '0'),
(1138, '1516296163', 'e3d6ea0dMorrocoy1.jpg', '2018-01-18 18:01:56', '1', '0'),
(1139, '1516296163', 'be083842Morrocoy2.jpg', '2018-01-18 18:01:59', '1', '0'),
(1140, '1516296163', '0b7e0f63Morrocoy4.jpg', '2018-01-18 18:02:13', '1', '0'),
(1141, '1516296163', 'b880bab8Morrocoy3.jpg', '2018-01-18 18:02:26', '1', '0'),
(1142, '1516300024', '572a7ebbTOLEDO1.jpg', '2018-01-18 18:40:55', '1', '0'),
(1143, '1516300024', '2487de3aTOLEDO2.jpg', '2018-01-18 18:40:59', '1', '1'),
(1144, '1516300024', '1bf5f005TOLEDO3.jpg', '2018-01-18 18:41:11', '1', '0'),
(1145, '1516300024', 'daf89fc6TOLEDO4.jpg', '2018-01-18 18:41:15', '1', '0'),
(1146, '1516300024', '7e622c28TOLEDO5.jpg', '2018-01-18 18:41:18', '1', '0'),
(1147, '1516300938', '6a212f47Mara1.JPG', '2018-01-18 18:50:25', '1', '0'),
(1148, '1516300938', '03e5e348Mara2.jpg', '2018-01-18 18:50:27', '1', '0'),
(1149, '1516300938', '598fd9b6Mara3.jpg', '2018-01-18 18:50:33', '1', '0'),
(1150, '1516300938', '78423879Mara4.jpg', '2018-01-18 18:50:38', '1', '0'),
(1152, '1516300938', '906249c8mARA5.JPG', '2018-01-18 18:55:38', '1', '0'),
(1153, '1516300938', '8585cb56Mara6.jpg', '2018-01-18 18:55:41', '1', '1'),
(1154, '1516301802', '8d05132cpalmas1.jpg', '2018-01-18 19:02:35', '1', '1'),
(1155, '1516301802', '7928bf6dpalmas2.jpg', '2018-01-18 19:02:41', '1', '0'),
(1156, '1516301802', 'cdd6f0d1palmas3.jpg', '2018-01-18 19:02:45', '1', '0'),
(1157, '1516301802', 'd748ef64palmas4.jpg', '2018-01-18 19:02:48', '1', '0'),
(1158, '1516302196', '6a331c811.jpg', '2018-01-18 19:20:30', '1', '1'),
(1159, '1516302196', '493d4bec2.jpg', '2018-01-18 19:20:37', '1', '0'),
(1160, '1516302196', 'e559be4c3.jpg', '2018-01-18 19:20:42', '1', '0'),
(1161, '1516302196', 'fb86f0384.jpg', '2018-01-18 19:20:46', '1', '0'),
(1162, '1516369201', '1da0073aPC1.jpg', '2018-01-19 13:49:23', '1', '0'),
(1163, '1516369201', '704e886cPC2.JPG', '2018-01-19 13:49:24', '1', '0'),
(1164, '1516369201', 'dcc75fb8PC3.jpg', '2018-01-19 13:49:24', '1', '1'),
(1165, '1516369201', '51fc3933PC4.jpg', '2018-01-19 13:49:27', '1', '0'),
(1166, '1516369201', '469c432aPC5.jpg', '2018-01-19 13:49:33', '1', '0'),
(1167, '1516369201', '2bd0b363PC6.jpg', '2018-01-19 13:49:43', '1', '0'),
(1168, '1516370159', 'da0b723esunf1.jpg', '2018-01-19 14:05:39', '1', '0'),
(1169, '1516370159', 'a3fd87d3sunf2.jpg', '2018-01-19 14:05:43', '1', '0'),
(1170, '1516370159', 'e217a249sunf3.jpg', '2018-01-19 14:05:51', '1', '0'),
(1171, '1516370159', 'f8cf453csunf4.jpg', '2018-01-19 14:05:55', '1', '0'),
(1172, '1516370159', 'b8499aefsunf5.jpg', '2018-01-19 14:06:01', '1', '1'),
(1173, '1516370868', 'ee63c785maturin1.jpg', '2018-01-19 14:14:02', '1', '0'),
(1174, '1516370868', 'acf140b3maturin2.jpg', '2018-01-19 14:14:07', '1', '1'),
(1175, '1516370868', '40047284maturin3.jpg', '2018-01-19 14:14:16', '1', '0'),
(1176, '1516370868', '0058c393maturin4.jpg', '2018-01-19 14:14:21', '1', '0'),
(1177, '1516371443', '13cf644cluciano1.jpg', '2018-01-19 14:25:58', '1', '0'),
(1178, '1516371443', '2d10e49eluciano2.jpg', '2018-01-19 14:25:59', '1', '0'),
(1179, '1516371443', 'c6031cc8luciano3.jpg', '2018-01-19 14:26:02', '1', '1'),
(1180, '1516371443', 'aaeb672aluciano4.jpg', '2018-01-19 14:27:20', '1', '0'),
(1181, '1516372358', 'b137e60ftrinitarias1.jpg', '2018-01-19 14:39:41', '1', '1'),
(1182, '1516372358', '3c616470trinitarias2.JPG', '2018-01-19 14:39:45', '1', '0'),
(1183, '1516372358', 'd95c3186trinitarias3.jpg', '2018-01-19 14:39:48', '1', '0'),
(1184, '1516372358', 'f1bbc29dtrinitarias4.jpg', '2018-01-19 14:39:55', '1', '0'),
(1185, '1516373148', '03603e12chacao1.JPG', '2018-01-19 14:52:47', '1', '0'),
(1186, '1516373148', '9f863184chacao2.JPG', '2018-01-19 14:52:52', '1', '0'),
(1187, '1516373148', 'a86c5893chacao3.JPG', '2018-01-19 14:53:01', '1', '0'),
(1188, '1516373148', '9ea8b3b1chacao4.JPG', '2018-01-19 14:53:02', '1', '0'),
(1189, '1516373148', 'b8fe2350CHACAO5.jpg', '2018-01-19 14:54:27', '1', '1'),
(1190, '1516394576', '4426fb28puntof1.jpg', '2018-01-19 21:06:47', '1', '0'),
(1191, '1516394576', 'e1b32cb1puntof2.jpg', '2018-01-19 21:06:55', '1', '0'),
(1192, '1516394576', 'f514bb96puntof3.jpg', '2018-01-19 21:07:05', '1', '0'),
(1193, '1516394576', '9a85bf81puntof4.jpg', '2018-01-19 21:09:08', '1', '0'),
(1194, '1516394576', '59d66562puntof5.jpg', '2018-01-19 21:09:12', '1', '0'),
(1195, '1516396222', 'db152b161.jpg', '2018-01-19 21:20:12', '1', '0'),
(1196, '1516396222', 'd0b673322.jpg', '2018-01-19 21:20:17', '1', '0'),
(1197, '1516396222', 'ea5526c13.jpg', '2018-01-19 21:20:19', '1', '0'),
(1198, '1516396222', 'f44e60394.jpg', '2018-01-19 21:20:24', '1', '0'),
(1199, '1516396222', '232368c15.jpg', '2018-01-19 21:21:53', '1', '1'),
(1201, '1516396998', 'ebb6a225Habitaciones Superiores.jpg', '2018-01-19 21:25:56', '1', '0'),
(1202, '1516396998', '47feb9f9habitaciones.jpg', '2018-01-19 21:26:00', '1', '0'),
(1203, '1516396998', 'bcec2017parakaupa.jpg', '2018-01-19 21:26:16', '1', '1'),
(1204, '1516396998', '72101937Recepcion y hab standart.jpg', '2018-01-19 21:26:24', '1', '0'),
(1205, '1516396998', 'bb408fb6Parakaupa1.jpg', '2018-01-19 21:26:25', '1', '0'),
(1206, '1516396998', 'f8a2cec2Parakaupa4.jpg', '2018-01-19 21:26:30', '1', '0'),
(1207, '1516396998', 'fe01348cYV-1748 Avioneta modelo cessna con capacidad para 5 personas.jpg', '2018-01-19 21:27:04', '1', '0'),
(1208, '1516396998', '7a7c98e1Canaima Falls from Canaima Lagoon.JPG', '2018-01-19 21:27:54', '1', '0'),
(1209, '1516396998', 'e4b88bd0DSC00656.JPG', '2018-01-19 21:28:09', '1', '0'),
(1210, '1516396998', '67e873b0Angel Fall 17.JPG', '2018-01-19 21:28:22', '1', '0'),
(1211, '1516396998', 'c22a7ab0DSC04643.JPG', '2018-01-19 21:28:30', '1', '0'),
(1212, '1516397394', 'cb944039esme1.jpg', '2018-01-19 21:40:13', '1', '1'),
(1213, '1516397394', '3a7d9535esme3.jpg', '2018-01-19 21:40:18', '1', '0'),
(1214, '1516397394', '6c9cc3c4esme4.jpg', '2018-01-19 21:40:19', '1', '0'),
(1215, '1516397394', '6fc5150besme2.jpg', '2018-01-19 21:40:24', '1', '0'),
(1216, '1516397394', '904fa29desme5.jpg', '2018-01-19 21:40:24', '1', '0'),
(1217, '1516397394', 'f50f323fesme6.jpg', '2018-01-19 21:40:28', '1', '0'),
(1218, '1516397394', '57334e1aesme7.jpg', '2018-01-19 21:40:39', '1', '0'),
(1219, '1516398308', 'ecc620f4AB1.jpg', '2018-01-19 21:49:01', '1', '0'),
(1220, '1516398308', '8d7d6b41AB2.jpg', '2018-01-19 21:49:06', '1', '0'),
(1221, '1516398308', 'b13ccd16AB3.jpg', '2018-01-19 21:49:13', '1', '0'),
(1222, '1516398308', 'f39f2f10AB4.jpg', '2018-01-19 21:49:16', '1', '1'),
(1223, '1516398607', '19b51749hippo1.jpg', '2018-01-19 21:54:38', '1', '0'),
(1224, '1516398607', '07acbd5ahippo2.jpg', '2018-01-19 21:54:42', '1', '0'),
(1225, '1516398607', 'badc67f1hippo3.jpg', '2018-01-19 21:54:46', '1', '0'),
(1226, '1516398607', 'bb10653ehippo4.jpg', '2018-01-19 21:54:50', '1', '0'),
(1227, '1516398607', '0afded3fhippo5.jpg', '2018-01-19 21:56:47', '1', '1'),
(1228, '1516398607', '6963c736hippo6.jpg', '2018-01-19 21:56:51', '1', '0'),
(1229, '1516399109', 'cad8ededMIKKOS 7.jpg', '2018-01-19 22:02:47', '1', '0'),
(1230, '1516399109', '1facbe55MIKKOS 9.png', '2018-01-19 22:02:53', '1', '0'),
(1231, '1516399109', 'e2a8da0dMIKKOS 11.jpg', '2018-01-19 22:02:55', '1', '1'),
(1232, '1516399109', 'b7cf4b07mikkos paradise 21.jpg', '2018-01-19 22:03:14', '1', '0'),
(1233, '1516399109', 'a7a9761fIMG_20150404_130105_edit.jpg', '2018-01-19 22:03:35', '1', '0'),
(1234, '1516399109', '56449d95MIKKOS 15.png', '2018-01-19 22:03:43', '1', '0'),
(1235, '1516399109', '98ef9f54mikkos paradise 20.jpg', '2018-01-19 22:03:54', '1', '0'),
(1236, '1517920149', '88fa978ekangrejo1.jpg', '2018-02-06 12:39:50', '1', '0'),
(1237, '1517920149', '0c328a7ekangrejo2.jpg', '2018-02-06 12:39:58', '1', '0'),
(1238, '1517920149', 'e175a3d2kangrejo3.jpg', '2018-02-06 12:40:03', '1', '0'),
(1239, '1517920149', '86a393a6kangrejo4.jpg', '2018-02-06 12:40:08', '1', '0'),
(1249, '1518012191', '72917765yara1.jpg', '2018-02-07 14:21:48', '1', '0'),
(1241, '1517920149', '0bf344b2kangrejo5.jpg', '2018-02-06 12:40:25', '1', '1'),
(1242, '1517920149', '15d992a2kangrejo6.jpg', '2018-02-06 12:40:28', '1', '0'),
(1243, '1517922222', '805762371.jpg', '2018-02-06 13:13:53', '1', '1'),
(1244, '1517922222', 'c546fb542.jpg', '2018-02-06 13:14:00', '1', '0'),
(1245, '1517922222', 'd3b1b8613.jpg', '2018-02-06 13:14:07', '1', '0'),
(1246, '1517922222', '37f1c7d24.jpg', '2018-02-06 13:14:11', '1', '0'),
(1247, '1517922222', '1852ef495.jpg', '2018-02-06 13:14:23', '1', '0'),
(1248, '1517922222', '483688436.jpg', '2018-02-06 13:14:30', '1', '0'),
(1250, '1518012191', '0094fa38yara2.jpg', '2018-02-07 14:21:52', '1', '0'),
(1251, '1518012191', '0414a6c9yara3.jpg', '2018-02-07 14:22:06', '1', '0'),
(1252, '1518012191', '247d4d6fyara4.jpg', '2018-02-07 14:22:26', '1', '0'),
(1253, '1518012191', 'eb3d4d9dyara5.jpg', '2018-02-07 14:22:31', '1', '1'),
(1254, '1518013391', '032b5dc0granja1.jpg', '2018-02-07 14:34:28', '1', '0'),
(1255, '1518013391', '69fe29fagranja2.jpg', '2018-02-07 14:34:33', '1', '0'),
(1256, '1518013391', '1c652097granja3.jpg', '2018-02-07 14:34:36', '1', '0'),
(1257, '1518013391', 'c3e2a133granja4.jpg', '2018-02-07 14:34:40', '1', '0'),
(1258, '1518013391', 'a9be3d6fgranja6.jpg', '2018-02-07 14:34:54', '1', '0'),
(1259, '1518013391', 'f9ce72f1granja5.jpg', '2018-02-07 14:34:56', '1', '1'),
(1260, '1518710392', '9f88880bpuj1.jpg', '2018-02-15 16:18:16', '1', '0'),
(1261, '1518710392', '4b19ec15puj2.jpg', '2018-02-15 16:18:20', '1', '0'),
(1262, '1518710392', 'c55529c3puj3.jpg', '2018-02-15 16:18:27', '1', '1'),
(1263, '1518710392', '8729235bpuj4.jpg', '2018-02-15 16:18:33', '1', '0'),
(1264, '1518710392', '3a79a5fapuj5.jpg', '2018-02-15 16:18:36', '1', '0'),
(1265, '1518710392', '11b17e9cpuj6.jpg', '2018-02-15 16:18:48', '1', '0'),
(1266, '1518710392', '7ad6e36epuj7.jpg', '2018-02-15 16:18:53', '1', '0'),
(1267, '1518711617', '3ae2af5fBambu1.JPG', '2018-02-15 16:27:52', '1', '1'),
(1268, '1518711617', 'ffdafde8Bambu2.JPG', '2018-02-15 16:27:56', '1', '0'),
(1269, '1518711617', '9aae10f8Bambu3.JPG', '2018-02-15 16:28:01', '1', '0'),
(1270, '1518711617', '1b0f87f4Bambu4.JPG', '2018-02-15 16:28:05', '1', '0'),
(1271, '1519656410', '48ef1eb6Bavaro1.JPG', '2018-02-26 14:56:53', '1', '1'),
(1272, '1519656410', 'f942ecd0Bavaro2.JPG', '2018-02-26 14:56:56', '1', '0'),
(1273, '1519656410', '08c5745bBavaro3.JPG', '2018-02-26 14:57:03', '1', '0'),
(1274, '1519656410', '107c0525Bavaro4.JPG', '2018-02-26 14:57:06', '1', '0'),
(1275, '1519671416', '60eba3d7SAMAN2.jpg', '2018-02-26 21:12:53', '1', '0'),
(1276, '1519671416', '9d0d4a7cSAMAN3.jpg', '2018-02-26 21:12:56', '1', '0'),
(1277, '1519671416', 'd06fadbeSAMAN1.jpg', '2018-02-26 21:12:56', '1', '0'),
(1278, '1519671416', '15e9c2f6SAMAN4.jpg', '2018-02-26 21:13:02', '1', '0'),
(1279, '1519671416', '8a6923b3SAMAN5.jpg', '2018-02-26 21:13:04', '1', '0'),
(1280, '1519671416', '687e7fe9SAMAN6.jpg', '2018-02-26 21:13:14', '1', '1'),
(1281, '1519738163', 'cc3dca24PLCP1.jpg', '2018-02-27 14:01:47', '1', '0'),
(1282, '1519738163', '6b470438PLCP2.jpg', '2018-02-27 14:01:55', '1', '1'),
(1283, '1519738163', 'd30d4635PLCP3.jpg', '2018-02-27 14:02:00', '1', '0'),
(1284, '1519738163', '509b2870PLCP4.jpg', '2018-02-27 14:02:05', '1', '0'),
(1292, '1519744804', '1a1b8092tinajero 3.jpg', '2018-02-27 15:23:52', '1', '0'),
(1291, '1519744804', 'f9fef580tinajero 2.jpg', '2018-02-27 15:23:47', '1', '0'),
(1290, '1519744804', 'b02fc65dTinajero 1.jpg', '2018-02-27 15:23:10', '1', '1'),
(1288, '1519744804', '2fb91321tinajero 4.jpg', '2018-02-27 15:22:04', '1', '0'),
(1289, '1519744804', '922e0457tinajero 4.jpg', '2018-02-27 15:22:21', '1', '0'),
(1293, '1519849317', 'b59ed155aua1.jpg', '2018-02-28 20:25:35', '1', '1'),
(1294, '1519849317', 'f1574368aua2.jpg', '2018-02-28 20:25:46', '1', '0'),
(1295, '1519849317', 'd9f9359aaua3.jpg', '2018-02-28 20:25:51', '1', '0'),
(1296, '1519849317', '63bdc0d4aua4.jpg', '2018-02-28 20:25:58', '1', '0'),
(1297, '1519931444', '7b31880fAura1.jpg', '2018-03-01 19:49:17', '1', '0'),
(1298, '1519931444', '6c41503dAura2.jpg', '2018-03-01 19:49:19', '1', '0'),
(1299, '1519931444', 'e28088c2Aura3.jpg', '2018-03-01 19:49:25', '1', '0'),
(1300, '1519931444', 'b77755abAura4.jpg', '2018-03-01 19:49:32', '1', '1'),
(1301, '1519931444', 'f25c3d73Aura5.jpg', '2018-03-01 19:49:35', '1', '0'),
(1302, '1519931444', 'd7492451Aura6.jpg', '2018-03-01 19:49:43', '1', '0'),
(1303, '1519931444', '8064e2f3Aura7.jpg', '2018-03-01 19:49:45', '1', '0'),
(1304, '1519931444', '06adaab7Aura8.jpg', '2018-03-01 19:49:51', '1', '0'),
(1305, '1519934313', 'b06cba8cHotel _ Casa Canabal 31.jpg', '2018-03-01 20:05:02', '1', '0'),
(1306, '1519934313', '19179441fachada 4.JPG', '2018-03-01 20:09:57', '1', '1'),
(1307, '1519934313', 'd85cf98fCANABAL 19 DE FEB 2014-66.jpg', '2018-03-01 20:13:19', '1', '0'),
(1308, '1519934313', '01ce058aBaÃ±o hab estandar.jpg', '2018-03-01 20:14:00', '1', '0'),
(1309, '1519934313', '16dd7e5bPiscina1.jpg', '2018-03-01 20:14:33', '1', '0'),
(1310, '1520257961', '4bac50ebartilleria1.jpg', '2018-03-05 14:00:31', '1', '0'),
(1311, '1520257961', '5dd7d17bartilleria2.jpg', '2018-03-05 14:00:35', '1', '0'),
(1312, '1520257961', '9b89fe59artilleria3.jpg', '2018-03-05 14:00:42', '1', '0'),
(1313, '1520257961', '6e283a8dartilleria4.jpg', '2018-03-05 14:00:47', '1', '0'),
(1314, '1520257961', '8e9d32c8artilleria5.jpg', '2018-03-05 14:00:54', '1', '0'),
(1315, '1520257961', '027304c3artilleria6.jpg', '2018-03-05 14:00:57', '1', '0'),
(1316, '1520257961', '18d80e4bartilleria7.jpg', '2018-03-05 14:01:47', '1', '1'),
(1317, '1520622438', '8f9bca35lm1.jpg', '2018-03-09 19:17:06', '1', '0'),
(1318, '1520622438', '7323e992lm3.jpg', '2018-03-09 19:17:16', '1', '0'),
(1319, '1520622438', '8762fac6lm2.jpg', '2018-03-09 19:17:16', '1', '1'),
(1320, '1520622438', '5468aa16lm4.jpg', '2018-03-09 19:17:24', '1', '0'),
(1321, '1520622438', '83090cb4lm5.JPEG', '2018-03-09 19:17:24', '1', '0'),
(1322, '1520622438', 'e66daef2lm6.jpg', '2018-03-09 19:17:32', '1', '0'),
(1323, '1520622438', '6f31e3d8lm7.jpg', '2018-03-09 19:17:36', '1', '0'),
(1324, '1520623266', 'd3d39c09BBB1.jpg', '2018-03-09 19:32:56', '1', '1'),
(1325, '1520623266', 'c202c470BBB2.jpg', '2018-03-09 19:32:57', '1', '0'),
(1326, '1520623266', '3f998fd8BBB3.jpg', '2018-03-09 19:33:03', '1', '0'),
(1327, '1520623266', 'fc6c57faBBB4.jpg', '2018-03-09 19:33:05', '1', '0'),
(1328, '1520623266', '5fc9a027BBB5.jpg', '2018-03-09 19:33:18', '1', '0'),
(1329, '1520623266', '29966ef8BBB6.jpg', '2018-03-09 19:33:25', '1', '0'),
(1330, '1520624059', '258cb6caBBP1.jpg', '2018-03-09 19:42:06', '1', '1'),
(1331, '1520624059', '13e83d50BBP2.jpg', '2018-03-09 19:42:07', '1', '0'),
(1332, '1520624059', 'c572ae60BBP3.jpg', '2018-03-09 19:42:14', '1', '0'),
(1333, '1520624059', '9b4f81c3BBP4.jpg', '2018-03-09 19:42:16', '1', '0'),
(1334, '1520624059', '6b10ce0cBBP5.JPEG', '2018-03-09 19:42:25', '1', '0'),
(1335, '1522163414', '0632298eLinconl suites 1.jpg', '2018-03-27 15:26:18', '1', '0'),
(1336, '1522163414', 'aabc4db6Linconl suites 2.jpg', '2018-03-27 15:26:24', '1', '0'),
(1337, '1522163414', '8c0efb55Linconl suites 3.jpg', '2018-03-27 15:26:26', '1', '1'),
(1338, '1522163414', '1e1915b9Linconl suites 4.jpg', '2018-03-27 15:26:33', '1', '0'),
(1339, '1522182409', '6ebf2bdaplayab1.jpg', '2018-03-27 20:36:25', '1', '0'),
(1340, '1522182409', '0019798bplayab2.jpg', '2018-03-27 20:36:30', '1', '0'),
(1341, '1522182409', 'd086b72aplayab3.jpg', '2018-03-27 20:36:35', '1', '0'),
(1342, '1522182409', 'f1f17983playab4.jpg', '2018-03-27 20:36:41', '1', '0'),
(1343, '1522182409', '00eee653playab5.jpg', '2018-03-27 20:36:46', '1', '1'),
(1344, '1522182409', 'aef97f28playab6.jpg', '2018-03-27 20:36:52', '1', '0'),
(1345, '1522182409', '6bc22426playab7.jpg', '2018-03-27 20:36:57', '1', '0'),
(1346, '1522183097', 'b60fc0b7paradise1.jpg', '2018-03-27 20:50:21', '1', '1'),
(1347, '1522183097', '1c0be3fbparadise2.jpg', '2018-03-27 20:50:25', '1', '0'),
(1348, '1522183097', 'b1b8d192paradise3.jpg', '2018-03-27 20:50:30', '1', '0'),
(1349, '1522183097', 'b5216cefparadise4.jpg', '2018-03-27 20:50:34', '1', '0');
INSERT INTO `hk_fotos` (`id`, `id_solicitud`, `foto`, `fecha`, `estatus`, `posicion`) VALUES
(1351, '1522183097', '56c8deeeparadise5.jpg', '2018-03-27 20:50:56', '1', '0'),
(1352, '1522183097', '86f41b06paradise6.jpg', '2018-03-27 20:51:00', '1', '0'),
(1353, '1522183097', '6680cb7dparadise7.jpg', '2018-03-27 20:51:03', '1', '0'),
(1354, '1522183097', '5bb634bbparadise8.jpg', '2018-03-27 20:51:09', '1', '0'),
(1355, '1522183097', 'b2a72c4eparadise9.jpg', '2018-03-27 20:51:12', '1', '0'),
(1356, '1522184017', '6c57735bRiuP1.jpg', '2018-03-27 20:55:59', '1', '1'),
(1357, '1522184017', '0092d6c3RiuP2.jpg', '2018-03-27 20:56:05', '1', '0'),
(1358, '1522184017', '7aee581cRiuP3.jpg', '2018-03-27 20:56:13', '1', '0'),
(1359, '1522184017', 'd24dfb54RiuP4.jpg', '2018-03-27 20:56:16', '1', '0'),
(1360, '1522702251', 'e1235aa0plaza pry6.jpg', '2018-04-02 20:59:25', '1', '0'),
(1361, '1522702251', 'c0ccbb2fPlaza PTY1.jpg', '2018-04-02 20:59:29', '1', '0'),
(1362, '1522702251', '906058efPlaza PTY2.jpg', '2018-04-02 20:59:33', '1', '1'),
(1363, '1522702251', 'b304fedfPlaza PTY3.jpg', '2018-04-02 20:59:37', '1', '0'),
(1364, '1522702251', '8bfd2d45plaza pty4.jpg', '2018-04-02 20:59:42', '1', '0'),
(1365, '1522702251', '00447cc1plaza pty5.jpg', '2018-04-02 20:59:47', '1', '0'),
(1366, '1522702251', '37b1c673plaza pty6.jpg', '2018-04-02 20:59:53', '1', '0'),
(1369, '1556299236', '3daab6e2costa1.JPG', '2019-04-26 17:42:35', '1', '1'),
(1370, '1556299236', 'ee30aa67costa2.JPG', '2019-04-26 17:42:39', '1', '0'),
(1371, '1556299236', 'da3915cacosta3.JPG', '2019-04-26 17:42:42', '1', '0'),
(1372, '1556299236', '35e07d70costa4.JPG', '2019-04-26 17:42:43', '1', '0'),
(1373, '1556299236', '2b36c42dcosta5.JPG', '2019-04-26 17:42:55', '1', '0'),
(1374, '1556299236', '3b79ae5bcosta6.JPG', '2019-04-26 17:42:57', '1', '0'),
(1375, '1556299236', '5d8b6069costa7.JPG', '2019-04-26 17:42:58', '1', '0'),
(1376, '1556300861', 'f039a4camochima5.JPG', '2019-04-26 18:31:23', '1', '0'),
(1377, '1556300861', '42b06d98mochima6.JPG', '2019-04-26 18:31:30', '1', '0'),
(1378, '1556300861', '76ca804emochima7.JPG', '2019-04-26 18:31:31', '1', '0'),
(1379, '1556300861', 'e36cd1d5mochimal1.jpg', '2019-04-26 18:31:39', '1', '1'),
(1380, '1556300861', '5dcd2310mochimal2.jpg', '2019-04-26 18:31:41', '1', '0'),
(1381, '1556300861', '0ac0bf3bmochimal3.jpg', '2019-04-26 18:31:42', '1', '0'),
(1382, '1556300861', '4da521eemochimal4.jpg', '2019-04-26 18:31:46', '1', '0'),
(1383, '1556303999', 'c09f1b6bEL PASEO 01.jpg', '2019-04-26 19:05:35', '1', '0'),
(1384, '1556303999', '63bd1aa6EL PASEO 02.jpg', '2019-04-26 19:05:39', '1', '0'),
(1385, '1556303999', 'fab04edaEL PASEO 03.jpg', '2019-04-26 19:05:43', '1', '1'),
(1386, '1556303999', '8813335fel paseo 3.jpg', '2019-04-26 19:05:49', '1', '0'),
(1387, '1556303999', '9282dbfdEL PASEO 04.jpeg', '2019-04-26 19:05:53', '1', '0'),
(1388, '1556303999', '546afdbfel paseo 4.jpg', '2019-04-26 19:05:57', '1', '0'),
(1389, '1556303999', '6848402cel paseo1.JPG', '2019-04-26 19:05:59', '1', '0'),
(1390, '1556303999', '44d728dfel paseo2.JPG', '2019-04-26 19:06:01', '1', '0'),
(1391, '1556299236', '2739f278Atardecer Piscina.jpg', '2019-04-26 20:26:12', '1', '0'),
(1392, '1556299236', 'dab4a914Habitacion 1.JPG', '2019-04-26 20:28:20', '1', '0'),
(1393, '1558367577', 'e0f11c44RiuP.JPG', '2019-05-20 18:38:43', '1', '0'),
(1394, '1558367577', '31459b60RiuP2.JPG', '2019-05-20 18:38:49', '1', '0'),
(1395, '1558367577', 'b622a392RiuP3.JPG', '2019-05-20 18:38:54', '1', '0'),
(1396, '1558367577', 'd21e8851RiuP4.JPG', '2019-05-20 18:38:55', '1', '0'),
(1397, '1558367577', '369858acRiuP5.jpg', '2019-05-20 18:38:58', '1', '0'),
(1398, '1558367577', 'fca0488fRiuP6.jpg', '2019-05-20 18:39:07', '1', '1'),
(1399, '1558367577', '81c682c1Riup7.jpg', '2019-05-20 18:39:10', '1', '0'),
(1401, '1560264005', 'd4a9bb04Eco 2.jpg', '2019-06-11 15:11:14', '1', '0'),
(1411, '1560459285', 'a7ac8e69eCOB1.jpeg', '2019-06-13 21:03:11', '1', '1'),
(1403, '1560264005', 'b377aae2Eco 3.jpg', '2019-06-11 15:11:16', '1', '0'),
(1404, '1560264005', 'ddff72ffEco 4.jpg', '2019-06-11 15:11:20', '1', '0'),
(1405, '1560264005', '6eae240bEco 5.jpg', '2019-06-11 15:11:27', '1', '0'),
(1406, '1560264005', '1188549aEco 6.jpg', '2019-06-11 15:11:33', '1', '0'),
(1407, '1560264005', '3af8f1e9Eco 7.jpg', '2019-06-11 15:11:37', '1', '1'),
(1408, '1560264005', '80c8ada8Eco 9.jpg', '2019-06-11 15:11:39', '1', '0'),
(1409, '1560264005', '31ef0460Eco 10.jpg', '2019-06-11 15:11:45', '1', '0'),
(1410, '1560264005', '960fbeeaEco 8.jpg', '2019-06-11 15:11:45', '1', '0'),
(1412, '1560459285', 'fd3f84d9eCOB2.jpg', '2019-06-13 21:03:13', '1', '0'),
(1413, '1560459285', '1ce4f1b5Ecob3.jpg', '2019-06-13 21:03:16', '1', '0'),
(1414, '1560459285', '6a35918dEcob4.jpg', '2019-06-13 21:03:20', '1', '0'),
(1415, '1560461489', '0f75fb5dTamanaco1.JPG', '2019-06-13 21:42:17', '1', '1'),
(1416, '1560461489', 'aabb0d7fTamanaco2.JPG', '2019-06-13 21:42:23', '1', '0'),
(1417, '1560461489', '496ea14aTamanaco3.JPG', '2019-06-13 21:42:27', '1', '0'),
(1418, '1560461489', '7e4e963eTamanaco4.jpg', '2019-06-13 21:42:28', '1', '0'),
(1419, '1560461489', '194d3959Tamanaco5.jpg', '2019-06-13 21:42:31', '1', '0'),
(1420, '1560461489', '2b69e8bbTamanaco6.jpg', '2019-06-13 21:42:35', '1', '0'),
(1421, '1560462494', 'e24712bfalex1.jpg', '2019-06-13 21:55:34', '1', '0'),
(1422, '1560462494', 'a39f9384alex2.jpg', '2019-06-13 21:55:37', '1', '0'),
(1423, '1560462494', 'da39b564alex3.jpg', '2019-06-13 21:55:41', '1', '1'),
(1424, '1560462494', 'fa6c0f42alex4.jpg', '2019-06-13 21:55:43', '1', '0'),
(1425, '1560462494', '06f9f357alex5.jpg', '2019-06-13 21:55:53', '1', '0'),
(1426, '1560540259', 'caef6667jirahara1.jpg', '2019-06-14 19:47:15', '1', '0'),
(1427, '1560540259', '553fdbcdjirahara2.jpg', '2019-06-14 19:47:17', '1', '0'),
(1428, '1560540259', '57997e6bjirahara3.jpg', '2019-06-14 19:47:21', '1', '0'),
(1429, '1560540259', 'c341955ejirahara4.jpg', '2019-06-14 19:47:24', '1', '1'),
(1430, '1561560547', '81fd6cfaChile 1.jpg', '2019-06-26 20:12:38', '1', '1'),
(1431, '1561560547', 'b2720823Chile 2.jpg', '2019-06-26 20:12:43', '1', '0'),
(1432, '1561560547', 'e85ecddeChile 5.jpg', '2019-06-26 20:12:56', '1', '0'),
(1433, '1561560547', '5476768aChile 6.jpg', '2019-06-26 20:12:56', '1', '0'),
(1434, '1561560547', '80a25193Chile 3.jpg', '2019-06-26 20:12:57', '1', '0'),
(1435, '1561560547', '9b8bfcb2Chile 7.jpg', '2019-06-26 20:13:02', '1', '0'),
(1436, '1561560547', '802a535cChile 4.jpg', '2019-06-26 20:13:08', '1', '0'),
(1437, '1562072964', 'e8ef570fChacao 1.jpg', '2019-07-02 13:24:57', '1', '0'),
(1438, '1562072964', '186b8cf7chacao2.jpg', '2019-07-02 13:25:01', '1', '0'),
(1439, '1562072964', '9568bbc6chacao3.jpg', '2019-07-02 13:25:03', '1', '0'),
(1440, '1562072964', '63bb3f44chacao4.jpg', '2019-07-02 13:25:07', '1', '0'),
(1442, '1562074227', '3f48b2baChacao 1.jpg', '2019-07-02 13:34:37', '1', '0'),
(1443, '1562074227', '6912b32eCHACAO4.jpg', '2019-07-02 13:34:38', '1', '1'),
(1444, '1562074227', '90ae1466chacao2.jpg', '2019-07-02 13:34:42', '1', '0'),
(1445, '1562074227', 'dbe0bf5dchacao3.jpg', '2019-07-02 13:34:43', '1', '0'),
(1446, '1562075666', '6876afa8ccs1.jpg', '2019-07-02 13:58:55', '1', '0'),
(1447, '1562075666', '832f9b8accs2.jpg', '2019-07-02 13:59:01', '1', '0'),
(1448, '1562075666', '488d9f8bccs3.jpg', '2019-07-02 13:59:36', '1', '1'),
(1449, '1562075666', '97a80807ccs4.jpg', '2019-07-02 13:59:37', '1', '0'),
(1450, '1562076223', '54a75eedMaracaibo 1.jpg', '2019-07-02 14:29:03', '1', '1'),
(1451, '1562076223', '5e34c43cmaracaibo 2.jpg', '2019-07-02 14:29:09', '1', '0'),
(1452, '1562076223', '6be9880cMaracaibo 3.jpg', '2019-07-02 14:29:09', '1', '0'),
(1453, '1562076223', '4d7ba6f9Maracaibo 4.jpg', '2019-07-02 14:29:13', '1', '0'),
(1454, '1562078595', '35ba72femiranda1.jpg', '2019-07-02 14:48:35', '1', '1'),
(1455, '1562078595', '31c982b2miranda2.jpg', '2019-07-02 14:48:39', '1', '0'),
(1456, '1562078595', '33692410miranda3.jpg', '2019-07-02 14:48:43', '1', '0'),
(1457, '1562078595', 'ee17eb21miranda4.jpg', '2019-07-02 14:48:47', '1', '0'),
(1458, '1567630475', 'd4f08335cct1.jpg', '2019-09-04 21:14:43', '1', '1'),
(1459, '1567630475', 'ccd7e7aeCCT2.jpg', '2019-09-05 14:47:42', '1', '0'),
(1460, '1567630475', '87d19603CCT3.jpg', '2019-09-05 14:47:47', '1', '0'),
(1461, '1567630475', '750ca271CCT4.jpg', '2019-09-05 14:47:56', '1', '0'),
(1462, '1567630475', 'ad3eb108CCT5.jpg', '2019-09-05 14:48:07', '1', '0'),
(1463, '1567630475', 'b5d622e0CCT6.jpg', '2019-09-05 14:48:12', '1', '0'),
(1464, '1567630475', '6a9853e7CCT7.jpg', '2019-09-05 14:48:16', '1', '0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_grado`
--

CREATE TABLE `hk_grado` (
  `id` int(11) NOT NULL,
  `descripcion` varchar(100) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_grado`
--

INSERT INTO `hk_grado` (`id`, `descripcion`) VALUES
(1, 'Primaria Completa'),
(2, 'Primaria Incompleta'),
(5, 'Secundaria Completa'),
(6, 'Secundaria Incompleta'),
(7, 'Superior Tecnica Completa'),
(8, 'Superior Tecnica Incompleta'),
(9, 'Universitaria Completa'),
(10, 'Universitaria Incompleta'),
(11, 'Postgrado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_habitaciones`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_habitaciones' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_habitaciones`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_habitaciones_plan`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_habitaciones_plan' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_habitaciones_plan`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_habitaciones_tipo`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_habitaciones_tipo' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_habitaciones_tipo`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_hotel`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_hotel' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_hotel`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_idioma`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_idioma' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_idioma`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_inmueble`
--

CREATE TABLE `hk_inmueble` (
  `id` int(11) NOT NULL,
  `nombres` varchar(128) DEFAULT NULL,
  `apellidos` varchar(128) DEFAULT NULL,
  `cedula` varchar(128) DEFAULT NULL,
  `tipo_inmueble` varchar(128) DEFAULT NULL,
  `tlf` varchar(128) DEFAULT NULL,
  `tlf_movil` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `estado` varchar(128) DEFAULT NULL,
  `ciudad` varchar(128) DEFAULT NULL,
  `direccion` varchar(2048) DEFAULT NULL,
  `nombre_inmueble` varchar(128) DEFAULT NULL,
  `edad_inmueble` varchar(128) DEFAULT NULL,
  `construccion_inmueble` varchar(128) DEFAULT NULL,
  `terreno_inmueble` varchar(128) DEFAULT NULL,
  `tenencia_inmueble` varchar(128) DEFAULT NULL,
  `uso_bien` varchar(128) DEFAULT NULL,
  `topografia` varchar(128) DEFAULT NULL,
  `mejoras` varchar(128) DEFAULT NULL,
  `conservacion` varchar(128) DEFAULT NULL,
  `condiciones_fisicas` varchar(128) DEFAULT NULL,
  `acabados` varchar(128) DEFAULT NULL,
  `pisos` varchar(128) DEFAULT NULL,
  `habitaciones` varchar(128) DEFAULT NULL,
  `banos` varchar(128) DEFAULT NULL,
  `puestos` varchar(128) DEFAULT NULL,
  `sala` varchar(128) DEFAULT NULL,
  `rodapie` varchar(128) DEFAULT NULL,
  `cable` varchar(128) DEFAULT NULL,
  `piso_de_granito` varchar(128) DEFAULT NULL,
  `rodapie_ceramica` varchar(128) DEFAULT NULL,
  `kictchenette` varchar(128) DEFAULT NULL,
  `EstacTechado` varchar(128) DEFAULT NULL,
  `BanodeServicio` varchar(128) DEFAULT NULL,
  `SemiAmoblado` varchar(128) DEFAULT NULL,
  `Porcelanato` varchar(128) DEFAULT NULL,
  `RodapieMadera` varchar(128) DEFAULT NULL,
  `CavaCuarto` varchar(128) DEFAULT NULL,
  `RejaSeguridad` varchar(128) DEFAULT NULL,
  `Parquet` varchar(128) DEFAULT NULL,
  `ParedesCeramica` varchar(128) DEFAULT NULL,
  `Alfombra` varchar(128) DEFAULT NULL,
  `VentanasRomanilla` varchar(128) DEFAULT NULL,
  `GasDirecto` varchar(128) DEFAULT NULL,
  `HabDeServicio` varchar(128) DEFAULT NULL,
  `Lavadero` varchar(128) DEFAULT NULL,
  `PuertaMadEntamb` varchar(128) DEFAULT NULL,
  `TerrazaCubierta` varchar(128) DEFAULT NULL,
  `Paredesfrisoesponj` varchar(128) DEFAULT NULL,
  `Maletero` varchar(128) DEFAULT NULL,
  `AscensordeCarga` varchar(128) DEFAULT NULL,
  `AguasServidas` varchar(128) DEFAULT NULL,
  `Paredesdefrisorustico` varchar(128) DEFAULT NULL,
  `TanquedeAgua` varchar(128) DEFAULT NULL,
  `Ventanafijaaluminio` varchar(128) DEFAULT NULL,
  `PisodeCeramica` varchar(128) DEFAULT NULL,
  `Ventanascorrederas` varchar(128) DEFAULT NULL,
  `EstructuraConcreto` varchar(128) DEFAULT NULL,
  `Banera` varchar(128) DEFAULT NULL,
  `EstructuraMetalica` varchar(128) DEFAULT NULL,
  `EstructuraenMamposteria` varchar(128) DEFAULT NULL,
  `ParedesMachiembrado` varchar(128) DEFAULT NULL,
  `EstructuraenMadera` varchar(128) DEFAULT NULL,
  `PinturaCaucho` varchar(128) DEFAULT NULL,
  `PuertadeAluminio` varchar(128) DEFAULT NULL,
  `CieloRaso` varchar(128) DEFAULT NULL,
  `PuertaHierro` varchar(128) DEFAULT NULL,
  `AireAcondicionado` varchar(128) DEFAULT NULL,
  `TerrazaDescubierta` varchar(128) DEFAULT NULL,
  `AguasBlancas` varchar(128) DEFAULT NULL,
  `Vestier` varchar(128) DEFAULT NULL,
  `LineaTelefonica` varchar(128) DEFAULT NULL,
  `Estudio` varchar(128) DEFAULT NULL,
  `SistemascontraIncendios` varchar(128) DEFAULT NULL,
  `SaladeConferencias` varchar(128) DEFAULT NULL,
  `AcometidaElectrica` varchar(128) DEFAULT NULL,
  `ParqueInfantil` varchar(128) DEFAULT NULL,
  `Gimnasio` varchar(128) DEFAULT NULL,
  `Cancha` varchar(128) DEFAULT NULL,
  `SaladeFiestas` varchar(128) DEFAULT NULL,
  `CallePrivada` varchar(128) DEFAULT NULL,
  `VigilanciaPrivada` varchar(128) DEFAULT NULL,
  `Jardines` varchar(128) DEFAULT NULL,
  `areadeBBQ` varchar(128) DEFAULT NULL,
  `EscalerasdeEmergencia` varchar(128) DEFAULT NULL,
  `Caminerias` varchar(128) DEFAULT NULL,
  `CastBombonasgas` varchar(128) DEFAULT NULL,
  `AscensorPrivado` varchar(128) DEFAULT NULL,
  `Piscina` varchar(128) DEFAULT NULL,
  `Sauna` varchar(128) DEFAULT NULL,
  `observacion_bien` varchar(2048) DEFAULT NULL,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` varchar(50) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hk_inmueble`
--

INSERT INTO `hk_inmueble` (`id`, `nombres`, `apellidos`, `cedula`, `tipo_inmueble`, `tlf`, `tlf_movil`, `email`, `estado`, `ciudad`, `direccion`, `nombre_inmueble`, `edad_inmueble`, `construccion_inmueble`, `terreno_inmueble`, `tenencia_inmueble`, `uso_bien`, `topografia`, `mejoras`, `conservacion`, `condiciones_fisicas`, `acabados`, `pisos`, `habitaciones`, `banos`, `puestos`, `sala`, `rodapie`, `cable`, `piso_de_granito`, `rodapie_ceramica`, `kictchenette`, `EstacTechado`, `BanodeServicio`, `SemiAmoblado`, `Porcelanato`, `RodapieMadera`, `CavaCuarto`, `RejaSeguridad`, `Parquet`, `ParedesCeramica`, `Alfombra`, `VentanasRomanilla`, `GasDirecto`, `HabDeServicio`, `Lavadero`, `PuertaMadEntamb`, `TerrazaCubierta`, `Paredesfrisoesponj`, `Maletero`, `AscensordeCarga`, `AguasServidas`, `Paredesdefrisorustico`, `TanquedeAgua`, `Ventanafijaaluminio`, `PisodeCeramica`, `Ventanascorrederas`, `EstructuraConcreto`, `Banera`, `EstructuraMetalica`, `EstructuraenMamposteria`, `ParedesMachiembrado`, `EstructuraenMadera`, `PinturaCaucho`, `PuertadeAluminio`, `CieloRaso`, `PuertaHierro`, `AireAcondicionado`, `TerrazaDescubierta`, `AguasBlancas`, `Vestier`, `LineaTelefonica`, `Estudio`, `SistemascontraIncendios`, `SaladeConferencias`, `AcometidaElectrica`, `ParqueInfantil`, `Gimnasio`, `Cancha`, `SaladeFiestas`, `CallePrivada`, `VigilanciaPrivada`, `Jardines`, `areadeBBQ`, `EscalerasdeEmergencia`, `Caminerias`, `CastBombonasgas`, `AscensorPrivado`, `Piscina`, `Sauna`, `observacion_bien`, `fecha`, `estatus`) VALUES
(1486030914, 'Johan Enrique', 'Quintero Urdaneta', '13532558', 'Apartamento', '', '04122116692', 'johanenri_1@hotmail.com', 'miranda', 'los teques', 'Sector el tanque vÃ­a lagunetica urb los durazno calle D casa Ãngela punto de referencia calle ciega muro de piedra ', 'angela', '3', '70,38', '70,38', 'Propia', 'Residencial', 'Plana', 'Sustanciales', 'Excelente', 'Nuevo', 'Muy Bueno', '1', '2', '1', '', 'Sala / Comedor', NULL, 'Tv por Cable', NULL, 'Rodapie Ceramica', NULL, NULL, NULL, NULL, 'Porcelanato', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Puerta Mad. Entamb', 'Terraza Cubierta', 'Paredes friso esponj ', NULL, NULL, 'Aguas Servidas', 'Paredes de friso rustico', 'Tanque de Agua ', 'Ventana fija aluminio ', 'Piso de CerÃ¡mica ', 'Ventanas correderas ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Aguas Blancas ', 'Vestier', NULL, 'Estudio', NULL, NULL, NULL, 'Parque Infantil ', NULL, NULL, 'Sala de Fiestas', 'Calle Privada ', 'Vigilancia Privada', 'Jardines', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Urb privada con portÃ³n elÃ©ctrico el agua es de sub terranea propia de la urb condominio econÃ³mico clima de montaÃ±a el apartamento es un anexo ', '2017-02-02 06:32:58', '1'),
(1485396131, 'Albero', 'Prato', 'E-916535', 'Galpon', '0212-6732369', '04142434104', 'velezpolitos@gmail.com', 'Miranda', 'Carrizal', 'Calle las industrias, automotris carrizal, carrizal.', 'sin nombre', '4', '200 m2', '200', 'No Aplica', 'Industrial', 'Plana', 'Sin Mejoras', 'Malo', 'Reparaciones Importantes', 'Malo', '1', '1', '1', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Estructura MetÃ¡lica', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Aguas Blancas ', NULL, NULL, NULL, NULL, NULL, 'Acometida ElÃ©ctrica', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'no tiene areas comunes', '2017-01-25 22:16:09', '1'),
(1505036255, 'JimmiNu', '94229', 'whKTqyueak', 'Quinta', 'MAFKnSiubheEMz', 'rsjYiDCUjmT', 'ec12342vtv@hotmail.com', 'fHXMLGqBbZZrVbEp', 'NY', 'xJhCKz http://www.FyLitCl7Pf7ojQdDUOLQOuaxTXbj5iNG.com', 'JimmiNu', '20', 'rGFJXxiPARyWy', 'DRokbVlT', 'Inti', 'Comercial', 'Plana', 'Sustanciales', 'Excelente', 'Nuevo', 'Muy Malo', '8', '9', 'TXWxPN8H', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'xJhCKz http://www.FyLitCl7Pf7ojQdDUOLQOuaxTXbj5iNG.com', '2017-09-10 09:37:38', '1'),
(1508573870, 'JimmiNi', '781072', 'blCUoxQyeaDBHwqH', 'Casa', 'AWXvSIkSdBGzSjj', 'zTnhGJbeyda', 'jimosa4ccf2@hotmail.com', 'ttpIkpcsWQuIkn', 'NY', 'sLPXVM http://www.FyLitCl7Pf7ojQdDUOLQOuaxTXbj5iNG.com', 'JimmiNi', '81', 'gSgaAIBJxKmDpaufmW', 'izuEScIjxLpu', 'Baldio', 'Industrial', 'Inclinada', 'Sustanciales', 'Regular', 'Regular', 'Muy Bueno', '4', '2', 'VJMvkZa9', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'sLPXVM http://www.FyLitCl7Pf7ojQdDUOLQOuaxTXbj5iNG.com', '2017-10-21 08:17:51', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_marcas`
--

CREATE TABLE `hk_marcas` (
  `id` int(11) NOT NULL,
  `titulo` varchar(512) CHARACTER SET latin1 NOT NULL,
  `descripcion` varchar(1024) CHARACTER SET latin1 NOT NULL,
  `imagen` varchar(512) CHARACTER SET latin1 NOT NULL,
  `estatus` varchar(12) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_marcas`
--

INSERT INTO `hk_marcas` (`id`, `titulo`, `descripcion`, `imagen`, `estatus`) VALUES
(1478952481, 'Chocolates La Mucuy, C.A.', 'Chocolates La Mucuy, C.A.', 'assets/img/034e17dcf1f5890e46c2f234f701009c.png', '1'),
(1478952538, 'Aluvima, C.A.', 'Aluvima, C.A.', 'assets/img/be9807c17d7a065b866421defa1b39ef.png', '1'),
(1478952556, 'Grupo Vasallo, C.A.', 'Grupo Vasallo, C.A.', 'assets/img/f75f95a8306356cb9c3fae61b0bc45f7.png', '1'),
(1478952574, 'Graficas El Portatitulos, C.A.', 'Graficas El Portatitulos, C.A.', 'assets/img/467c9faa9c2b01abaf3578f4f30ce559.png', '1'),
(1478952596, 'Caja de Ahorros y PrÃ©stamos de los Jueces de Venezuela', 'Caja de Ahorros y PrÃ©stamos de los Jueces de Venezuela', 'assets/img/108d77ad39b746a61a0505404b031300.png', '1'),
(1478952620, 'Unidad Educativa Instituto Victegui, C.A.', 'Unidad Educativa Instituto Victegui, C.A.', 'assets/img/f24aa87acb58fcd74f70d756a18ac5ed.png', '1'),
(1478952640, 'Transeguro, C.A., de Seguros', 'Transeguro, C.A., de Seguros', 'assets/img/c03665cec75f57b114bfb69cb96b314f.png', '1'),
(1478952668, 'FÃ¡brica de Calzados Volmes, C.A.', 'FÃ¡brica de Calzados Volmes, C.A.', 'assets/img/3adba8fde47800c3b4eb68b463f88774.png', '1'),
(1478952707, 'Zisne FÃ¡brica de Chocolate, C.A.', 'Zisne FÃ¡brica de Chocolate, C.A.', 'assets/img/9833f757200c94a831a07593ba2e6b63.png', '1'),
(1478952745, 'Buhito, C.A.', 'Buhito, C.A.', 'assets/img/b4e0e771b38c70ad9af73bada0f65eea.png', '1'),
(1483378863, 'Sistemas Delta P, C.A.', 'Sistemas Delta P, C.A.', 'assets/img/55e94e921591c0f27231a8fdc4239926.jpg', '1'),
(1483378972, 'Nextcom Systems', 'Nextcom Systems', 'assets/img/8a571b6ba29087d6634f31de8e72818c.jpg', '1'),
(1484848902, 'UE Dr. JosÃ© MarÃ­a Vargas', 'UE Dr. JosÃ© MarÃ­a Vargas', 'assets/img/7600d4cef35cdee9bec316d77005a548.jpg', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_modulos`
--

CREATE TABLE `hk_modulos` (
  `id` int(11) NOT NULL,
  `titulo` varchar(512) CHARACTER SET latin1 NOT NULL,
  `descripcion` varchar(1024) CHARACTER SET latin1 NOT NULL,
  `imagen` varchar(512) CHARACTER SET latin1 NOT NULL,
  `estatus` varchar(12) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_modulos`
--

INSERT INTO `hk_modulos` (`id`, `titulo`, `descripcion`, `imagen`, `estatus`) VALUES
(1478985493, 'VALORACIÃ“N ONLINE', 'En DiseÃ±os y Proyectos Tirex 2021, ponemos a su disposiciÃ³n nuestro sistema de ValoraciÃ³n Online. Obtenga un valor orientativo de sus bienes.\r\n\r\n\r\n<div class=\"row text-center\">\r\n\r\n <a class=\"btn btn-primary  \" href=\"/solicitud/mueble\" >  Muebles  </a>\r\n\r\n<a class=\"btn btn-primary \" href=\"/solicitud/inmueble\" >  Inmuebles  </a>\r\n\r\n  </div>\r\n', 'assets/img/7fb4b04d4c764d6b1491dc8a9c9ec4db.png', '1'),
(1478985833, 'VALIDADOR DE INFORME', 'A travÃ©s de nuestra pÃ¡gina web, usted podrÃ¡ comprobar la autenticidad de los Informes TÃ©cnicos de AvalÃºos emitidos por DiseÃ±os y Proyectos Tirex 2021.\r\n\r\n\r\n<div class=\"row text-center\">\r\n\r\n <a class=\"btn btn-primary  \" href=\"/solicitud/validador\" >  Validar Informe</a>\r\n\r\n\r\n  </div>\r\n', 'assets/img/3db990dd5c6b8f544969c2ce991ed121.png', '1'),
(1478985878, 'AUDITOR DE TASACIONES', 'Obtenga una segunda OpiniÃ³n de Valor de un AvalÃºo realizado por otra Empresa o Avaluador, que sea de decisiÃ³n importante para usted.\r\n\r\n<div class=\"row text-center\">\r\n\r\n <a class=\"btn btn-primary  \" href=\"/solicitud/auditar\" >  Auditar</a>\r\n\r\n  </div>', 'assets/img/6d0b8b9ddbc0b962d4852dbfd3ad038d.png', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_muebles`
--

CREATE TABLE `hk_muebles` (
  `id` int(11) NOT NULL,
  `nombres` varchar(256) NOT NULL,
  `apellidos` varchar(256) NOT NULL,
  `cedula` varchar(128) NOT NULL,
  `tlf` varchar(128) NOT NULL,
  `tlf_movil` varchar(128) NOT NULL,
  `email` varchar(512) NOT NULL,
  `cantidad` varchar(12) NOT NULL,
  `estado` varchar(128) NOT NULL,
  `ciudad` varchar(128) NOT NULL,
  `direccion` varchar(1024) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` varchar(12) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_noticia`
--

CREATE TABLE `hk_noticia` (
  `id` int(11) NOT NULL,
  `id_autor` varchar(12) CHARACTER SET latin1 NOT NULL,
  `titulo` varchar(512) CHARACTER SET latin1 NOT NULL,
  `noticia` varchar(5000) CHARACTER SET latin1 NOT NULL,
  `foto` varchar(256) CHARACTER SET latin1 NOT NULL,
  `date_add` date NOT NULL,
  `date_upd` date NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_noticia`
--

INSERT INTO `hk_noticia` (`id`, `id_autor`, `titulo`, `noticia`, `foto`, `date_add`, `date_upd`, `estatus`) VALUES
(1483973995, '8', 'Â¿Por quÃ© contratar un AvalÃºo?', '</p>Valuar o Avaluar significa obtener el justo valor de las cosas, en un contexto y tiempo determinado, evitando asÃ­ la especulaciÃ³n que puede no beneficiar tanto a la parte compradora como la parte vendedora, bien sea porque el activo se vende a menor precio de lo que realmente estÃ¡ en el mercado o por el contrario el cliente debe pagar mÃ¡s dinero de lo que realmente cuesta ese activo. En tal sentido, es necesario la intervenciÃ³n de alguien no interesado que estime el justo valor de dicho bien. Esto es el profesional Avaluador, Tasador o Perito.</p>\r\n<br>\r\n</p>Es necesario que se entienda que los AvalÃºos deben contener informaciÃ³n idÃ³nea y justificable en cualquier aspecto, ya que los diferentes activos tienen caracterÃ­sticas econÃ³micas muy particulares, que lo distinguen y diferencian de otros, tales como ofertas de mercado, caracterÃ­sticas, ubicaciÃ³n, estado de conservaciÃ³n y mantenimiento, operatividad y funcionamiento, asÃ­ como tambiÃ©n las polÃ­ticas econÃ³micas y sociales que adoptan los gobiernos, generando en ocasiones fluctuaciones de notable variaciÃ³n en los activos.</p>\r\n<br>\r\n</p>En la actualidad, el avaluador no solo debe conocer procedimientos matemÃ¡ticos y tÃ©cnicas de ingenierÃ­a, tambiÃ©n debe conocer teorÃ­as econÃ³micas y financieras, leyes del derecho urbanÃ­stico e inmobiliario, estadÃ­sticas e informÃ¡tica como herramientas de anÃ¡lisis, asÃ­ como estar al tanto de los mercados y sus tendencias, acorde con las nuevas y cada vez mayores exigencias de un entorno social, econÃ³mico, empresarial y tecnolÃ³gico cambiante.</p>\r\n<br>\r\n</p>La determinaciÃ³n del valor real de un activo tiene gran importancia no solo por el hecho de determinar con certeza su valor, sino por ser una herramienta de gran utilidad en materia comercial, contable y fiscal.  Un avalÃºo puede ser requerido por diversas causas, algunas por simple curiosidad de conocer el valor real del bien que se posee; pero en general, puede ser requerido cuando:</p>\r\n<br>\r\n</p>1. Se compra o se vende y se quiere determinar su justo precio</p>\r\n</p>2. Se va a otorgar un crÃ©dito con garantÃ­a hipotecaria se hace necesario para el acreedor conocer el valor real de la garantÃ­a, su estado y limitaciones.</p>\r\n</p>3. Se va a elaborar inventarios o para reajustar el valor de los bienes de una empresa o entidad financiera.</p>\r\n</p>4. Se fija el monto de una indemnizaciÃ³n en casos de siniestros. EstimaciÃ³n precisa, detallada e informativa del precio justo de sus bienes.</p>\r\n</p>5. Se va a realizar la reparticiÃ³n de una herencia.</p>\r\n<br>\r\n</p>En tal sentido los avalÃºos deben ser justos, proporcionando al usuario un documento con informaciÃ³n confiable y que garantice la exclusividad del propietario; es decir que cumpla con estÃ¡ndares nacionales e internacionales.\r\n\r\n			', 'assets/img/ba162715c1a540cd8b2f926f184ae564.jpg', '2017-01-09', '2017-01-09', 1),
(1481410021, '7', 'Un AvalÃºo Muy Personal', '<p>Como buen Ingeniero, tengo la habilidad para razonar y resolver problemas matemÃ¡ticos. Los nÃºmeros son parte de mi vida profesional e incluso mi vida social, ya que tengo la costumbre de analizar todo cuanto pasa a mÃ­ alrededor.<p>\r\n<br>\r\n<p>Siempre he tenido la facilidad para dominar la ciencia de los nÃºmeros, tanto que hace algunos aÃ±os incursionÃ© en el Ã¡rea de la docencia. Con el pasar de los aÃ±os me fui enamorando de Ã©sta profesiÃ³n y sobre todo cuando me llamaban â€œEl Profesor de MatemÃ¡ticasâ€. DÃ­a tras dÃ­a aprendÃ­ a apreciar el sentimiento de los estudiantes; fue entonces cuando decidÃ­ obsequiarles un Profesor de MatemÃ¡ticas diferente, estricto pero amigo, sobre todo amigo. EnseÃ±arles a razonar, no que sepan cuanto es la raÃ­z cuadrada de 25, sino el porquÃ© del resultadoâ€¦ Atacar los problemas y no huir de ellos, era el gran aprendizaje.<p>\r\n<br>\r\n<p>Hoy han pasado trece (13) aÃ±os desde que dejÃ© la docencia para dedicarme a mi pasiÃ³nâ€¦ La IngenierÃ­a. Gracias a Ã©sta, comencÃ© estudios de especializaciÃ³n en el Ã¡rea de AvalÃºos Inmobiliarios y Mobiliarios, el cual me ha servido para conocer muchas personas, algunas muy exitosas y otras menos exitosas. Peroâ€¦ Â¿Por quÃ© hay personas que triunfan en la vida y otras no? Â¿Porque unas se baÃ±an en dinero y otras en deudas?<p>\r\n<br>\r\n<p>Me di la tarea de indagar un poco el porquÃ© de esta situaciÃ³n y me di cuenta que los clientes exitosos pasaban sus horas del dÃ­a enfocÃ¡ndose en lo que querÃ­an, en el Ã©xito, en la abundancia, accionaban y trabajaban por alcanzarlos sin dejarse dominar por las circunstancias. Los otros clientes, los menos exitosos, desde que me reunÃ­a con ellos no hacÃ­an otra cosa sino quejarse de la situaciÃ³n, que en este paÃ­s no se puede trabajar, que las deudas los van arruinar, que el gobierno de turno no sirve, que la delincuencia nos va acabar, etc; lo que no saben es que el fracaso es un embustero con un satÃ­rico sentido de la ironÃ­a y la malicia, se deleita en hacernos caer cuando el Ã©xito estÃ¡ casi en nuestras manos.<p>\r\n<br>\r\n<p>Un dÃ­a conversando con mi madre, le pregunte por una amiga que querÃ­a ver y que tenÃ­a mÃ¡s de 20 aÃ±os sin saber de ella. SÃ³lo pasaron dos dÃ­as cuando me sorprendÃ­ topÃ¡ndome con mi amiga en un automercado que no frecuento con regularidad. Esto me puso a pensar y reflexionar Â¿AsÃ­ como atraje con mis pensamientos y sentimientos a mi amiga, puedo atraer otras cosas si me enfoco con la misma pasiÃ³n? La respuesta es SI. Fue entonces cuando entendÃ­ el error que habÃ­a cometido al enseÃ±arles a mis ex alumnosâ€¦ â€œAtacar los problemas y no huir de ellosâ€. La respuesta, no es atacar las dificultades, es enfocarnos en lo que queremos realmente. Sentir que somos exitosos, que vivimos en la abundancia, que somos felices, y con nuestro accionar los problemas por si sÃ³lo se esfumarÃ¡n. Cuando nos enfocamos con pasiÃ³n en las cosas que queremos y las que no, las atraemos a nuestras vidas.<p>\r\n<br>\r\n<p>Hay una campaÃ±a de una ONG Internacional muy conocida que dice â€œBasta de Balasâ€, el enfoque sigue siendo las cosas que no queremos LAS BALAS y por consiguiente las atraemos. Ejemplos hay muchos: No mÃ¡s Drogas, No mÃ¡s Guerras, No mÃ¡s Hambre, No mÃ¡s Colas, No mÃ¡s Deudas, entre otros. Basta mirar a nuestro alrededor y darnos cuenta que todo lo que no queremos, de acuerdo a esas campaÃ±as, se han incrementado. Si no quieres deudas no pienses en ellas, busca una manera de pagarlas cuando corresponda, enfÃ³cate en adquirir abundancia. Le damos tanto poder a lo que no queremos que dejamos de pensar y sentir para accionar en lo que si queremos: PAZ, EXITO, ABUNDANCIA, PROSPERIDAD, SALUD, FELICIDAD, AMOR, agreguen lo que quierenâ€¦<p>\r\n<br>\r\n<p>Hoy estÃ¡ muy de moda los famosos Coaching y su desarrollo de habilidades para la vida. No nos damos cuenta, que las habilidades las desarrollamos nosotros cuando empecemos a pensar, visualizar y sentir lo que queremos ser. Accionar para convertir los pensamientos en realidad, escribir nuestras vidasâ€¦ SiÃ©ntase exitosos y verÃ¡n cÃ³mo les van a llegar las ideas para hacer prÃ³speras sus vidas.<p>', 'assets/img/424c6f94de7d6627db89ac442a19ef13.jpg', '2016-12-10', '2017-01-10', 1),
(1481411638, '7', '7 factores clave que generan plusvalÃ­a en un inmueble', '					<p>Una propiedad es el cimiento del patrimonio familiar, ademÃ¡s de ser buena forma de dar certidumbre al futuro pues los inmuebles generalmente aumentan su valor a lo largo del tiempo. </p>\r\n<br>\r\n\r\n<p>A este incremento de valor a travÃ©s del tiempo se le llama plusvalÃ­a. Antes de tomar la decisiÃ³n de comprar un inmueble analiza los siguientes factores pues serÃ¡n determinantes para aumentar el valor de la propiedad a lo largo del tiempo: </p>\r\n<br>\r\n\r\n<ol>\r\n<li>UbicaciÃ³n del inmueble, es decir, si estÃ¡ en un lugar cÃ©ntrico o estÃ¡ alejado de zonas comerciales y de oficinas. Es importante considerar que la ubicaciÃ³n cerca de una zona de riesgo, como una gasolinera o un canal de aguas negras, tiende a devaluar la propiedad. Considera tambiÃ©n si es una zona de transformaciÃ³n, es importante estar cerca de oficinas y comercios.</li>\r\n\r\n<li>Accesibilidad, las vÃ­as de comunicaciÃ³n necesarias para trasladarse a diferentes zonas de la ciudad. La falta de accesibilidad a vÃ­as rÃ¡pidas y de transporte urbano impactarÃ¡n el valor de la vivienda.</li>\r\n\r\n<li>Competitividad de la zona. Para la mayorÃ­a de las personas es importante que su casa o departamento se ubique cerca de servicios que facilitan el dÃ­a a dÃ­a como centros comerciales, supermercados, hospitales, parques, escuelas y restaurantes, asÃ­ como ofertas de trabajo. Sin embargo no genera plusvalÃ­a estar ubicado muy cerca de cementerios, escuelas, vÃ­as del tren, tianguis o vendedores ambulantes.</li>\r\n\r\n<li>Planes de desarrollo urbano del Ã¡rea. Es importante no comprar una propiedad basÃ¡ndose en promesas de construcciÃ³n de infraestructura urbana que todavÃ­a no han iniciado, pues podrÃ­an pasar aÃ±os antes de que realmente se construyan. Los proyectos de movilidad urbana como MetrobÃºs, Metro, Ecobicis pueden generar valor a una zona..</li>\r\n\r\n<li>Mantenimiento y funcionalidad del inmueble. Aspectos del inmueble como el nÃºmero de habitaciones y baÃ±os, su distribuciÃ³n, tamaÃ±o, estacionamientos y Ã¡reas verdes influyen en el valor del inmueble. Si es una casa o departamento usado serÃ¡ fundamental considerar su estado. Otros aspectos que puede afectar la plusvalÃ­a son los problemas entre vecinos o con el desarrollador, que no se respeten los lineamientos de imagen urbana (tendederos, cuartos de servicios improvisados) y que se hayan hecho en la propiedad modificaciones estructurales sin supervisiÃ³n profesional.</li>\r\n\r\n<li>Seguridad. La seguridad de la zona donde se encuentra la vivienda es un factor muy importante para el valor de la misma. Las zonas mÃ¡s seguras tienen una mejor plusvalÃ­a ya que tienen una mayor demanda y le ofrecen a las familias protecciÃ³n y tranquilidad..</li>\r\n\r\n</ol>						', 'assets/img/be7a26ef3da3cbe8afd6a3903f246634.png', '2016-12-10', '2016-12-20', 1),
(1483640375, '8', 'Crisis en el Sector Inmobiliario', '					</p>No existe duda de la creciente crisis econÃ³mica que sufrimos en Venezuela y de ella no escapa el mercado inmobiliario. Se observa una alta oferta de inmuebles, pero en muchos casos es debido a la emigraciÃ³n de los propietarios a otros paÃ­ses.</p>\r\n<br>\r\n</p>Muchos de nuestros clientes al solicitar un AvalÃºo Inmobiliario en nuestro portal www.tirex2021.com son personas que estÃ¡n vendiendo para irse del paÃ­s, sin embargo a los seis (6) meses solicitan una actualizaciÃ³n del avalÃºo, ya que pocas personas estÃ¡n comprando, ya sea porque los crÃ©ditos hipotecarios estÃ¡n paralizados, el valor del inmueble es muy alto y no pueden comprar de contado, y en muchos casos buscan tranzar en moneda extranjera para recuperar la inversiÃ³n de sus bienes.</p>\r\n<br>\r\n</p>Lo mismo sucede en las empresas, donde sus dueÃ±os solicitan el avalÃºo en nuestro portal www.tirex2021.com para vender los bienes, ya que la rentabilidad es poca atractiva. </p>\r\n<br>\r\n</p>Un aspecto a considerar es que la venta de los bienes inmuebles y muebles se estÃ¡ realizando en moneda extranjera y si el vendedor acepta bolÃ­vares el comprador debe pagar su equivalente a la moneda extranjera en que se estÃ¡ vendiendo. </p>\r\n<br>\r\n</p>Haciendo Ã©nfasis en el sector inmobiliario, desde algÃºn tiempo no se registra construcciÃ³n de viviendas por parte del sector privado, ya que los precios de los materiales han crecido al ritmo de la inflaciÃ³n y los costos de las viviendas serian muy alto, esto sin tomar en cuenta la baja producciÃ³n de materia prima por parte de las empresas bÃ¡sicas. Motivado a esto, no hay oferta de viviendas nuevas para la venta lo que ha generado que los inmuebles del mercado secundario se revaloricen.</p>\r\n<br>\r\n</p>Por otra parte, tenemos la desinformaciÃ³n del Banco Central de Venezuela en cuanto a cifras oficiales, ya que los Ã­ndices de inflaciÃ³n que este emite no son publicados. Dichos indices son utilizados para calcular los ajustes que se deben realizar en materia de insumos de construcciÃ³n. AsÃ­ mismo, existen materiales de construcciÃ³n que se encuentran regulados, pero estos no se consiguen a precios de regulaciÃ³n, sino por el contrario se consiguen a precios muy superiores.</p>\r\n<br>\r\n</p>En fin, estamos pasando por momentos difÃ­ciles en donde es importante rescatar la confianza en nuestro paÃ­s, en las instituciones pÃºblicas y en las autoridades. </p>\r\n<br>\r\n</p>En DiseÃ±os y Proyectos Tirex 2021, C.A., somos optimistas y estamos seguros que con el aporte de todos reactivaremos la economÃ­a para que se desarrollen accesibles proyectos habitacionales para todos los sectores y lograremos efectos importantes para el desarrollo de nuestro paÃ­s.</p>\r\n						', 'assets/img/b3012a2445f708d096f784d69a47073b.jpg', '2017-01-05', '2017-01-19', 1),
(1482283485, '8', '7 Ingredientes para el Ã©xito', '																														</p>Se han preguntado alguna vez Â¿Porque unas personas tienen mas Ã©xitos que otras?. He averiguado que este es unos de los temas que mas se estudia en el mundo, que hoy hay mas informaciÃ³n al respecto que nunca. Podemos decir, que el Ã©xito se encuentra a la vuelta de la esquina. Hoy se puede lograr el Ã©xito con mayor velocidad si se hacen dos cosas elementales: </p>\r\n<br>\r\n\r\nTomar la decisiÃ³n de tener Ã©xito y lograr sus metas sean las que sean.\r\nAprender a tener Ã©xito.\r\n</p>\r\n<br>\r\n</p>Primero debemos saber, que el Ã©xito no tiene relaciÃ³n con la suerte, la educaciÃ³n, la inteligencia o el aspecto fÃ­sico. Cuando observamos la diferencia entre los animales y los seres humanos nos damos cuenta que los animales estÃ¡n dotados por un instinto de supervivencia, buscan un lugar donde dormir, reproducir su especie y cazan para tener comida suficiente. Los seres humanos tenemos lo que llamamos el instinto del Ã©xito, esto significa que contamos con un impulso que nos lleva hasta el Ã©xito, ese impulso es tan natural como respirar, es parte esencial de nuestro ser y hace que nunca estemos satisfechos mientras vivimosâ€¦ Siempre queremos mas, siempre queremos hacer mas.</p>\r\n<br>\r\n</p>Â¿Porque entonces hay pocas personas que logran el Ã©xito en la vida?</p>\r\n<br>\r\n</p>A mi manera de ver las cosas, existen 7 ingredientes que deben ser agregados a nuestras vidas para alcanzar el Ã©xito:</p>\r\n</p>\r\n<br>\r\n</p>Serenidad: Significa tranquilidad, que uno se encuentra en paz y satisfecho en nuestro interior. Serenidad quiere decir que uno se encuentra en libertad, libre de medio, de ansiedad, de carencia, de estrÃ©s, de culpas, de emociones negativas, etc. La serenidad es el ingrediente mas importante del Ã©xito, ya que si se alcanza todo lo demÃ¡s en la vida pero no tenemos serenidad no se podrÃ¡ disfrutar del resto.</p>\r\n<br>\r\n</p>Salud: Significa que estamos sanos, estamos libres de enfermedad, de dolor, de trastornos, etc. Debemos estar sanos para poder enfocarnos en el Ã©xito.</p>\r\n<br>\r\n</p>Relaciones afectivas: Significa que tenemos la capacidad de mantener relaciones afectivas con otros seres humanos, es una caracterÃ­stica esencial de nuestra naturaleza. Se ha comprobado que un alto porcentaje de nuestros Ã©xitos estÃ¡ determinado por lo bien que uno se lleve con los demÃ¡s. Si nuestras relaciones son estables, nuestra salud es estable y gozamos de serenidad.</p>\r\n<br>\r\n</p>Libertad EconÃ³mica: Significa que tenemos suficiente dinero para no tener que preocuparnos por el dinero. El dinero es importante para tener las cosas buenas de la vida, ya que al tenerlo nos podemos ocupar de otras cosas para lograr el Ã©xito tales como, nuestras relaciones, nuestra salud, nuestra mente, la expansiÃ³n de nuestra vida emocional y espiritual.</p>\r\n</p>\r\n</p>Metas: Significa que tenemos que establecer objetivos. Todos los seres humanos necesitamos unos objetivos para saber que se estÃ¡ haciendo algo importante, que existen motivos para levantarnos todos los dÃ­as en la maÃ±ana, creer en algo, tener entusiasmo por la vida.</p>\r\n<br>\r\n</p>Autoconocimiento: Saber y comprenderse uno mismo, tener la honradez para ver las cosas buenas y malas de nuestra personalidad, conocerse a uno mismo.</p>\r\n<br>\r\n</p>AutorealizaciÃ³n: Es la sensaciÃ³n de que uno se estÃ¡ convirtiendo en todo lo que es capaz de ser. Se produce por el resultado de metas, de tener relaciones afectivas, de contar con un alto nivel de conocimiento de uno mismo que generan salud y energÃ­a, serenidad, etc.</p>\r\n<br>\r\n</p>Muchas personas no logran llevar a plenitud estos 7 ingredientes porque trabajan muy por debajo de sus capacidades. Â¿Pero a que se debe esto?  el primer motivo es que llegamos al mundo sin un manual de instrucciones y nos pasamos la vida intentando descifrar como hacer las cosas. El segundo motivo es que a lo largo de nuestras etapas escolares nunca se nos enseÃ±Ã³ nada acerca del Ã©xito, en tal sentido no tenemos la costumbre de pensar y actuar como una persona exitosa. El como lograr el Ã©xito deberÃ­a enseÃ±arse a todo nivel educativo, incluso en nuestros lugares de trabajo.</p>\r\n<br>\r\n</p>La clave del Ã©xito es tener fe en uno mismo, comprensiÃ³n y aprender hacer las cosas bien, basÃ¡ndonos en estos 7 ingredientes y utilizando la comprensiÃ³n multiplicada por el esfuerzo para obtener los resultados esperados.</p>\r\n<br>\r\n\r\n</p>Pueden ser o hacer lo que quieran, si logran comprender y ejecutar estos 7 ingredientes para el Ã©xito.</p>																				', 'assets/img/a7e7aa795bea25b38b37569711a08520.jpg', '2016-12-20', '2016-12-20', 1),
(1483377884, '8', 'Las Tasaciones en aumento', '</p>Lamentablemente la noticia tiene que ver con EspaÃ±a y no con Venezuela. Sin embargo, seguimos sumando oportunidades en el sector tasaciÃ³n del paÃ­s y asÃ­ lograr un equilibrio en nuestra economÃ­a.</p>\r\n<br>\r\n</p>De acuerdo a una noticia publicada por www.elmundo.es y a los datos de las 23 Sociedades de TasaciÃ³n que forman la AsociaciÃ³n EspaÃ±ola de AnÃ¡lisis de Valor (AEV), que llevan a cabo en torno al 90% de las tasaciones del sector, indican que el nÃºmero de tasaciones completas llevadas a cabo durante el primer semestre del aÃ±o 2016 es un 20% mayor que el de las gestionadas en el mismo periodo de 2015, con un total de 487.716 valoraciones por un importe total de 160.010 millones de euros, lo que representa un crecimiento del 14% en valor.</p>\r\n<br>\r\n</p>El 56% de las valoraciones realizadas durante el primer semestre del 2016 ha tenido como finalidad la garantÃ­a de potenciales para prÃ©stamos hipotecarios, lo que supone un crecimiento anual del 19% tanto en nÃºmero como en valor.</p>\r\n<br>\r\n</p>Resultan muy significativos los aumentos del 97% y 110%, respectivamente, de las tasaciones para edificios en proyecto y de edificios en construcciÃ³n o rehabilitaciÃ³n (donde los importes de las valoraciones subieron, tambiÃ©n respectivamente, un 61% y un 50% respecto a 2015).</p>\r\n<br>\r\n</p>Los datos del primer semestre de 2016 dejan atrÃ¡s la reducciÃ³n que las nuevas tasaciones de edificios terminados sufrieron el primer trimestre, con una subida del 5%, consecuencia probablemente del leve aumento de viviendas de nueva construcciÃ³n. Lo que sÃ­ continÃºa en consonancia con los datos del primer trimestre es la tasa de crecimiento de las valoraciones de edificios destinados a segunda residencia.</p>\r\n<br>\r\n</p>Pese a continuar representando un pequeÃ±o porcentaje del total, supera el 229% de crecimiento respecto a aÃ±os anteriores, un dato que puede dar respuesta a la tendencia apreciada durante los Ãºltimos meses en los que se nota que el aumento de tasaciones de viviendas estaba centrado en grandes urbes y en zonas costeras, siendo las litorales las Ã¡reas donde mayor nÃºmero de viviendas de segunda residencia se registran.</p>\r\n<br>\r\n</p>Por segundo trimestre consecutivo, las valoraciones completas dirigidas a entidades bancarias por razones contables han resultado ser el tipo de tasaciones que mÃ¡s ha aumentado, con un incremento del 86%, cuando el crecimiento del primer semestre del aÃ±o pasado respecto al del ejercicio 2014 fue de sÃ³lo el 2%. Se trata de valoraciones que las entidades financieras normalmente usan bien para conocer el valor razonable de sus activos.</p>\r\n<br>\r\n</p>Las valoraciones masivas guiadas por modelos estadÃ­sticos (los llamados modelos automÃ¡ticos de valoraciÃ³n -AVM por sus siglas en inglÃ©s-) han aumentado un 49% en nÃºmero y un 50% en valor, hasta alcanzar casi los 2 millones de inmuebles valorados en el semestre (con un importe agregado de 235.000 millones de euros).</p>\r\n<br>\r\n</p>Este instrumento tambiÃ©n se utiliza por las entidades bancarias para valorar las garantÃ­as e inmuebles de su cartera que pueden ser objeto de tratamiento estadÃ­stico masivo, como son, entre otros, las viviendas, inmuebles comerciales o industriales.</p>', 'assets/img/2bb83bf68790c21674339b0dd9badb49.jpg', '2017-01-02', '2017-01-02', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_pagina`
--

CREATE TABLE `hk_pagina` (
  `id` int(11) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_spanish_ci NOT NULL,
  `pais` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `corta` varchar(1024) COLLATE utf8_spanish_ci NOT NULL,
  `corta_ingles` varchar(1024) COLLATE utf8_spanish_ci NOT NULL,
  `larga` mediumtext COLLATE utf8_spanish_ci,
  `larga_ingles` mediumtext COLLATE utf8_spanish_ci,
  `gmaps` varchar(5000) COLLATE utf8_spanish_ci NOT NULL,
  `idioma` varchar(50) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1',
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` varchar(12) CHARACTER SET latin1 NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_pagina`
--

INSERT INTO `hk_pagina` (`id`, `nombre`, `pais`, `estado`, `corta`, `corta_ingles`, `larga`, `larga_ingles`, `gmaps`, `idioma`, `fecha`, `estatus`) VALUES
(1486892594, 'Maracay ', '237', '4023', 'Maracay la ciudad jardin', '', '<div>Debido a que la poblaciÃ³n o densidad demogrÃ¡fica de Maracay, se ubicÃ³ originalmente dentro de lo que hoy es legalmente el municipio Girardot del estado Aragua, por lo general tiende a confundirse los lÃ­mites de la ciudad con los lÃ­mites del municipio, razÃ³n por la cual es importante resaltar que la ciudad no posee personalidad jurÃ­dica como tal, puesto que el nombre de Maracay, no pertenece a ningÃºn municipio ni parroquia del asentamiento poblacional dentro de la entidad federal, sino que mÃ¡s bien la ciudad responde en la actualidad al crecimiento poblacional o densidad demogrÃ¡fica y desarrollo urbano compartida entre la poblaciÃ³n del municipio Girardot y las poblaciones-lÃ­mites de los municipios que se mencionan a continuaciÃ³n:</div><div><br></div><div>Norte: poblaciÃ³n de ChoronÃ­ y caserÃ­os de la cordillera de la Costa, en el mismo municipio Girardot.</div><div><br></div><div>Sur: poblaciÃ³n de Palo Negro en el municipio Libertador, poblaciÃ³n y urbanidad de Santa Rita en el municipio Francisco Linares AlcÃ¡ntara y la poblaciÃ³n Turmero en el municipio Santiago MariÃ±o.</div><div><br></div><div>Este: poblaciones de El MÃ¡caro, La Julia, La Morita y Turmero en el municipio Santiago MariÃ±o.</div><div><br></div><div>Oeste: poblaciÃ³n de CaÃ±a de AzÃºcar, El Paseo y El LimÃ³n en el municipio Mario BriceÃ±o Iragorry, poblaciÃ³n de Ocumare de la Costa en el municipio Ocumare de la Costa de Oro, caserÃ­os y urbanidades de Boca de RÃ­o y La Cabrera en el municipio Girardot, y con la poblaciÃ³n de Mariara en el estado Carabobo.</div><div><br></div>', '<br>', 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xOCExbTEyITFtMyExZDEyNTk1Ni45MzYxMDE2NjMwMyEyZC02Ny42NzU0NTEwMTA4NTA1MiEzZDEwLjI2NzE3NjM5MTc2MzA1NyEybTMhMWYwITJmMCEzZjAhM20yITFpMTAyNCEyaTc2OCE0ZjEzLjEhM20zITFtMiExczB4OGU4MDNjOTg5Mzc3ZmU4NyUzQTB4YjVmZjUyNGRhZGFlNWI3NCEyc01hcmFjYXklMkMrQXJhZ3VhITVlMSEzbTIhMXNlcyEyc3ZlITR2MTQ4NzcwMDg2MjAyMyIgd2lkdGg9IjQwMCIgaGVpZ2h0PSIzMDAiIGZyYW1lYm9yZGVyPSIwIiBzdHlsZT0iYm9yZGVyOjAiIGFsbG93ZnVsbHNjcmVlbj48L2lmcmFtZT4=', '', '2017-02-12 09:43:41', '1'),
(1486892621, 'Valencia', '237', '4026', 'Valencia ciudad industrial del Centro Occidente', 'Valencia industrial city of the West Center', '<div>Valencia una ciudad de Venezuela, capital y ciudad mÃ¡s poblada del Estado Carabobo, situada en la RegiÃ³n Central del paÃ­s.</div><div>La ciudad de Valencia es conocida como Capital Industrial de Venezuela debido a que alberga una cantidad de zonas Industriales del paÃ­s. De igual forma, se ha convertido en un centro de inversiÃ³n, objeto de proyectos como el World Trade Center Valencia y el Complejo Isla Multiespacio. El PIB de la ciudad se ubica alrededor de los USD$24.443 millones, un per cÃ¡pita nominal de 11825$ y un pib ppa per cÃ¡pita de 20470 $. En 2015 el GaWC (Globalization and World Cities), incluyÃ³ a la ciudad de Valencia en su listado, siendo Ã©sta y Caracas las Ãºnicas ciudades de Venezuela en el ranking, clasificÃ¡ndola como una Ciudad de Suficiencia.â€‹</div><div>SegÃºn un estudio del Instituto Nacional de EstadÃ­sticas (INE) la ciudad posee una poblaciÃ³n para el 2013 de 870.000 habitantes en su municipio homÃ³nimo, mientras que toda el Ã¡rea metropolitana posee una poblaciÃ³n estimada 2.140.000 de habitantes, convirtiÃ©ndose en la ciudad mÃ¡s poblada de la RegiÃ³n Central, la tercera mÃ¡s poblada de Venezuela.</div><div>La ciudad se compone de cinco municipios autÃ³nomos distintos: Municipio Valencia, Municipio Naguanagua, Municipio San Diego, Municipio Libertador y Municipio Los Guayos, teniendo cada uno de esos sus respectivos alcaldes con atribuciones administrativas propias. La legislaciÃ³n local establece que las autoridades propiciarÃ¡n una iniciativa para crear un distrito metropolitano si se llegase a elevar la Parroquia Miguel PeÃ±a a nivel de municipio.â€‹ La ciudad fue capital de Venezuela en tres ocasiones, desde 1812 hasta 1830.</div><div>FUENTE: Wikipedia</div>', '<br><div><span style=\"color: rgb(33, 33, 33); font-family: arial, sans-serif; font-size: 16px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">Valencia is a city in Venezuela, capital and most populated city of Carabobo State, located in the Central Region of the country.\r\nThe city of Valencia is known as the Industrial Capital of Venezuela because it houses a number of industrial zones in the country. In the same way, it has become an investment center, object of projects such as the World Trade Center Valencia and the Isla Multiespacio Complex. The city\'s GDP is around USD $ 24,443 million, a nominal per capita of $ 11825 and a GDP per capita of $ 20470. In 2015 the GaWC (Globalization and World Cities), included the city of Valencia in its list, being this and Caracas the only cities in Venezuela in the ranking, classifying it as a Sufficiency City.\r\nAccording to a study by the National Institute of Statistics (INE) the city has a population of 870,000 in 2013 in its homonymous municipality, while the entire metropolitan area has an estimated population of 2,140,000 inhabitants, making it the most populated city in the country. the Central Region, the third most populated of Venezuela.\r\nThe city is made up of five distinct autonomous municipalities: Municipality of Valencia, Municipality of Naguanagua, Municipality of San Diego, Municipality of Libertador and Municipality of Los Guayos, each having its respective mayors with their own administrative powers. Local legislation establishes that the authorities will promote an initiative to create a metropolitan district if it were to elevate the Miguel PeÃ±a Parish at the municipal level.The city was the capital of Venezuela three times, from 1812 to 1830.</span><br></div>', 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xOCExbTEyITFtMyExZDYyOTk3LjIxNjc4ODA0MjMyITJkLTY4LjAyOTI0MzQ5ODg2Nzc2ITNkMTAuMTcyNTgwMDM5NTcxNTg1ITJtMyExZjAhMmYwITNmMCEzbTIhMWkxMDI0ITJpNzY4ITRmMTMuMSEzbTMhMW0yITFzMHg4ZTgwNjc4YmNlNjE5MjhmJTNBMHg5Zjc4ZTllZWE4YzM0MjcyITJzVmFsZW5jaWElMkMrQ2FyYWJvYm8hNWUxITNtMiExc2VzITJzdmUhNHYxNDg3NzAwODg3OTYxIiB3aWR0aD0iNDAwIiBoZWlnaHQ9IjMwMCIgZnJhbWVib3JkZXI9IjAiIHN0eWxlPSJib3JkZXI6MCIgYWxsb3dmdWxsc2NyZWVuPjwvaWZyYW1lPg==', 'Espaï¿½ol', '2017-02-12 09:46:46', '1'),
(1486892806, 'Barquisimeto', '237', '4032', 'Barquisimeto la ciudad musical y de los crepÃºsculos', '', '<div>Su Ã¡rea metropolitana estÃ¡ compuesta por 7 parroquias del Municipio Iribarren y el Municipio Palavecino en su totalidad. Su principal actividad econÃ³mica es el comercio y la industria lo cual estÃ¡ ligado a ser punto de convergencia de las principales vÃ­as terrestres del occidente del paÃ­s. Por ubicarse en un altiplano, la ciudad de Barquisimeto es una de las pocas ciudades venezolanas con un plano urbano hipodÃ©rmico.</div><div><br></div><div>Barquisimeto es conocida por ser uno de los importantes nÃºcleos artÃ­sticos y culturales de Venezuela al ser cuna de numerosos artistas y mÃºsicos de trayectoria nacional y mundial. Es considerada como una ciudad universitaria, debido al gran nÃºmero de oportunidades de estudios que ofrecen distintas instituciones que hacen vida en la ciudad entre las cuales destacan la Universidad Centro Occidental Lisandro Alvarado, la Universidad PedagÃ³gica Experimental Libertador (Instituto PedagÃ³gico de Barquisimeto) , la Universidad Nacional Experimental PolitÃ©cnica Antonio JosÃ© de Sucre y la Universidad PolitÃ©cnica Territorial AndrÃ©s Eloy Blanco posicionadas entre las mejores del paÃ­s.</div><div><br></div><div>Los Cardenales de Lara, uno de los equipos de bÃ©isbol de mayor tradiciÃ³n en Venezuela, tiene su sede en Barquisimeto en el estadio Antonio Herrera GutiÃ©rrez asÃ­ como tambiÃ©n el club deportivo Lara con sede en el estadio metropolitano ubicado al sur de la ciudad.</div><div><br></div><div>La ciudad es terminal del sistema ferroviario centro occidental SimÃ³n BolÃ­var asÃ­ como tambiÃ©n posee un aeropuerto internacional que sirve a toda la regiÃ³n. Dispone de un sistema de buses de transito rÃ¡pido los cuales son el principal sistema de transporte de la ciudad.</div><div><br></div>', '<br>', 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xOCExbTEyITFtMyExZDEyNjAzNy44ODY1Mzk4NzA1MSEyZC02OS40MzI4ODg0MTI2NTMxMyEzZDEwLjA2MTg1ODk4NTYxNTY5NyEybTMhMWYwITJmMCEzZjAhM20yITFpMTAyNCEyaTc2OCE0ZjEzLjEhM20zITFtMiExczB4OGU4NzY3MWQ3ODNlODY3MSUzQTB4OTcyZmUxZTExNTE5YzNkYiEyc0JhcnF1aXNpbWV0byszMDAxJTJDK0xhcmEhNWUxITNtMiExc2VzITJzdmUhNHYxNDg3NzAwOTEwOTE3IiB3aWR0aD0iNDAwIiBoZWlnaHQ9IjMwMCIgZnJhbWVib3JkZXI9IjAiIHN0eWxlPSJib3JkZXI6MCIgYWxsb3dmdWxsc2NyZWVuPjwvaWZyYW1lPg==', 'Espaï¿½ol', '2017-02-12 09:49:30', '1'),
(1487573259, 'Choroni', '237', '4023', 'ChoronÃ­ es una localidad costeÃ±a en el extremo norte del municipio Girardot, estado Aragua, Venezuela, ubicada en las faldas de la cordillera de la Costa, lo que le aÃ±ade los elementos naturales peculiares de la faja litoral del Parque Nacional Henri Pittier', '', '<div><br></div>', NULL, 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xOCExbTEyITFtMyExZDE2MjAxLjUwOTk5MTU4OTg3OSEyZC02Ny42MTQ0NDA1MDE1MjM2OSEzZDEwLjQ5NDQ4MTk3NzE2NjMwMiEybTMhMWYwITJmMCEzZjAhM20yITFpMTAyNCEyaTc2OCE0ZjEzLjEhM20zITFtMiExczB4OGU4MDMyZDk1OGM3ZDdmZCUzQTB4ZDMzMjM5ZThjMjA0ZGNlMCEyc0Nob3JvbmklMkMrQXJhZ3VhITVlMSEzbTIhMXNlcyEyc3ZlITR2MTQ4NzcwMDkzOTc3OCIgd2lkdGg9IjQwMCIgaGVpZ2h0PSIzMDAiIGZyYW1lYm9yZGVyPSIwIiBzdHlsZT0iYm9yZGVyOjAiIGFsbG93ZnVsbHNjcmVlbj48L2lmcmFtZT4=', 'Español', '2017-02-20 06:47:44', '0'),
(1487586047, 'CUMANA / MOCHIMA', '237', '4025', 'El Parque Nacional Mochima se encuentra a 600 m. sobre el nivel del mar, al noreste de Venezuela, entre las ciudades de Barcelona, Puerto la Cruz y Cumana, y se extiende a lo largo de la costa a lo largo de un Ã¡rea de 94,935 hectÃ¡reas. ', '', 'El Parque Nacional Mochima se encuentra a 600 m. sobre el nivel del mar, al noreste de Venezuela, entre las ciudades de Barcelona, Puerto la Cruz y CumanaÃ‚Â¡, y se extiende a lo largo de la costa a lo largo de un Ã¡rea de 94,935 hectÃ¡reas. Su belleza es diversa: playas espectaculares como el Golfo de Santa Fe, el Archipielago de la Borracha, la Peninsula de Manare y la Bahia de Mochima, mÃºltiples islas, muchas de ellas prÃ¡cticamente viÂ­rgenes, como las Islas Caracas y las Islas Chimana, y una singular combinacion de montanas que besan el mar. &nbsp;A todas estas maravillas se puede llegar con el servicio de botes del area.En la zona alta, la vegetacion es exuberante y abundan los helechos y las orquidÃ¡ceas, la flor Nacional Venezolana. Dentro de la zona baja, se consiguen iguanas, conejos, lagartos negros, serpientes, pelÃ­canos y pajaros bobos o tijeretas. &nbsp;El Parque Nacional &nbsp;Mochima es uno de los destinos turÃ­sticos mas deseados de Venezuela. Playa Colorada es mundialmente famosa por su fina arena de color dorado, casi rojizo, que da nombre al lugar.Fauna: aÂreas de nidificacion y alimentaciÃ³n de varias especies de tortugas marinas; gran numero de aves marinas residentes y migratorias. Enorme diversidad de invertebrados marinos y gran riqueza de peces de importancia para la pesca artesanal. Entre los mamÃ­feros: venado caramerudo (Odocoileus virginianus), ballena arenquera (Balaenoptera edem), delfin comun (Delphinus delphisFlora: Mangle (Rhizophora mangle) y especies costeras como hierba de vidrio (Salicornia fruticosa, Sesuvium portulacastrum y Batis maritima), batatilla de playa (Ipomoea pescaprae), varias cactuceas como los cardones (Pilosocereus sp. y Subpilocereus sp.) y el guamacho (Pereskia guamacho), leguminosas como el cujiÃ‚Â­ yaque (Prosopis juliflora) y el dividive (Caesalpinia coriaria).Este parque es ideal para practicar el submarinismo y otras actividades acuaticas ya que casi el 80% de su extension se encuentra en aguas del mar caribe<br>', NULL, 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xOCExbTEyITFtMyExZDM5MzUuNjE3MTI2MDg2NzMwNCEyZC02NC41NjM3OTU0ODUxOTU5MSEzZDEwLjMxMDI1MDM5MjYzODg5ITJtMyExZjAhMmYwITNmMCEzbTIhMWkxMDI0ITJpNzY4ITRmMTMuMSEzbTMhMW0yITFzMHg4YzJkODc1OTBmZmZmZmZmJTNBMHhlODQ0MGNkYTQxODM5YTQyITJzUGFycXVlK05hY2lvbmFsK01vY2hpbWEhNWUxITNtMiExc2VzITJzdmUhNHYxNDg3NzAwOTcwMDIxIiB3aWR0aD0iNDAwIiBoZWlnaHQ9IjMwMCIgZnJhbWVib3JkZXI9IjAiIHN0eWxlPSJib3JkZXI6MCIgYWxsb3dmdWxsc2NyZWVuPjwvaWZyYW1lPg==', 'Español', '2017-02-20 10:20:55', '0'),
(1487595202, 'Higuerote', '237', '4034', 'Higuerote es la ciudad capital del Municipio BriÃ³n (Miranda, Venezuela).', '', 'El Parque Nacional Mochima se encuentra a 600 m. sobre el nivel del mar, al noreste de Venezuela, entre las ciudades de Barcelona, Puerto la Cruz y Cumana, y se extiende a lo largo de la costa a lo largo de un Ã¡rea de 94,935 hectÃ¡reas. Su belleza es diversa: playas espectaculares como el Golfo de Santa Fe, el Archipielago de la Borracha, la Peninsula de Manare y la BahÃ­a de Mochima, mÃºltiples islas, muchas de ellas prÃ¡cticamente virgeses, como las Islas Caracas y las Islas Chimana, y una singular combinaciÃ³n de montanas que besan el mar. &nbsp;A todas estas maravillas se puede llegar con el servicio de botes del Ã¡rea. En la zona alta, la vegetaciÃ³n es exuberante y abundan los helechos y las orquiÃ‚Â­deas, la flor Nacional Venezolana. Dentro de la zona baja, se consiguen iguanas, conejos, lagartos negros, serpientes, peliÂ­canos y pÃ¡jaros bobos o tijeretas. &nbsp;El Parque Nacional &nbsp;Mochima es uno de los destinos turiÂ­sticos mas deseados de Venezuela. Playa Colorada es mundialmente famosa por su fina arena de color dorado, casi rojizo, que da nombre al lugar.Fauna: aeÂreas de nidificacion y alimentaciÃ³n de varias especies de tortugas marinas; gran numero de aves marinas residentes y migratorias. Enorme diversidad de invertebrados marinos y gran riqueza de peces de importancia para la pesca artesanal. Entre los mamÃ­feros: venado caramerudo (Odocoileus virginianus), ballena arenquera (Balaenoptera edem), delfin comun (Delphinus delphisFlora: Mangle (Rhizophora mangle) y especies costeras como hierba de vidrio (Salicornia fruticosa, Sesuvium portulacastrum y Batis maritima), batatilla de playa (Ipomoea pescaprae), varias cactuceas como los cardones (Pilosocereus sp. y Subpilocereus sp.) y el guamacho (Pereskia guamacho), leguminosas como el Â­ yaque (Prosopis juliflora) y el dividive (Caesalpinia coriaria).Este parque es ideal para practicar el submarinismo y otras actividades acuÃ¡ticas ya que casi el 80% de su extensiÃ³n se encuentra en aguas del mar caribe<br>', NULL, 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xOCExbTEyITFtMyExZDYyOTM4LjU4NTYyNjEyNzU1ITJkLTY2LjEzODk0MDU5ODIyNjc1ITNkMTAuNDY1NTk4MDg4MzUyMjkhMm0zITFmMCEyZjAhM2YwITNtMiExaTEwMjQhMmk3NjghNGYxMy4xITNtMyExbTIhMXMweDhjMmI5Yzc4MjlmZDNhMWQlM0EweGNlY2JjZDgzYWM5ODhhNSEyc0hpZ3Vlcm90ZSUyQytNaXJhbmRhITVlMSEzbTIhMXNlcyEyc3ZlITR2MTQ4NzcwMDI3NjU0MiIgd2lkdGg9IjQwMCIgaGVpZ2h0PSIzMDAiIGZyYW1lYm9yZGVyPSIwIiBzdHlsZT0iYm9yZGVyOjAiIGFsbG93ZnVsbHNjcmVlbj48L2lmcmFtZT4=', 'Español', '2017-02-20 12:53:26', '0'),
(1487595474, 'LA GRAN SABANA', '237', '4025', 'La Gran Sabana esta dentro del parque Nacional Canaima, al Sur del Estado Bolivar en Venezuela', '', 'La Gran Sabana esta dentro del parque Nacional Canaima, al Sur del Estado Bolivar en Venezuela. El lugar ofrece paisajes unicos en todo el mundo, Por cada nuevo kilometro recorrido una nueva maravilla por conocer: hay rios de aguas mansas y oscuras o mas bien rojizas; furiosos y turbulentos raudales; refrescantes pozas y toboganes naturales, delicia de quienes la visitan; pequenos saltos de agua e imponentes cascadas; el intenso azul de un cielo infinito y el verdor de la sabana sin fin. Y como telon de fondo para tan soberbio espectaculo, la sempiterna presencia de los formidables tepuyes.<br>', NULL, 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xOCExbTEyITFtMyExZDIwMzk0NzUuNjk5NDEzOTU5NCEyZC02Mi44MDgyMzU2MjQ5NjQwOSEzZDUuMjYxOTE2OTA1MDc0MzYxITJtMyExZjAhMmYwITNmMCEzbTIhMWkxMDI0ITJpNzY4ITRmMTMuMSEzbTMhMW0yITFzMHg4ZGMwOTRiYWNkYjRhMjY5JTNBMHg5YjZmNWQ2MDAxMDZiMGZiITJzTGErR3JhbitTYWJhbmElMkMrQm9sJUMzJUFEdmFyITVlMSEzbTIhMXNlcyEyc3ZlITR2MTQ4NzcwMTAxNDkyMSIgd2lkdGg9IjQwMCIgaGVpZ2h0PSIzMDAiIGZyYW1lYm9yZGVyPSIwIiBzdHlsZT0iYm9yZGVyOjAiIGFsbG93ZnVsbHNjcmVlbj48L2lmcmFtZT4=', 'Espaï¿½ol', '2017-02-20 12:58:55', '1'),
(1487595535, 'LA TORTUGA', '237', '4029', 'La isla La Tortuga le dijimos adiÃ³s a los peÃ±eros, ahora en lanchas para mayor comodidad, seguridad y confort.', '', '<div>La isla La Tortuga pertenece a Venezuela, estando incluida dentro de las Dependencias Federales Venezolanas.</div><div>EstÃ¡ en el sur del Mar Caribe a los 65Âº 18Â´ oeste y 10Âº 55Â´ norte, a unos 85 km de RÃ­o Chico (Miranda), estado de Miranda; a 170 km de Caracas; a 140 km de la Isla de Margarita; a unos 85 km de la laguna de Unare en el estado de AnzoÃ¡tegui y a 120 km del estado de Nueva Esparta. La forma de la isla es similar a una elipse de 12 km medidos de Norte a Sur y 25 km de Este a Oeste. Posee junto con los cayos adyacentes aproximadamente 156,60 kmÂ² (15.660 hectÃ¡reas) de extensiÃ³n, lo que la convierte en la segunda mÃ¡s grande en cuanto a tamaÃ±o de Venezuela, despuÃ©s de Margarita.</div><div><br></div><div>La Tortuga conforma un grupo de islas que incluye:</div><div><br></div><div>Islas Los Tortuguillos, con las islas de Tortuguillo del Este y Tortuguillo del Oeste;</div><div>Cayo Herradura</div><div>Incluye ademÃ¡s otros bajos o formaciones:</div><div><br></div><div>Bajo de los Los Palanquines</div><div>Cayos de Ã‘o MartÃ­n</div><div>Islote El Vapor</div><div>Cayos de Punta de Ranchos</div><div>Es muy visitada por aves migratorias y pescadores que frecuentan la isla entre los meses de septiembre y abril. Durante esta Ã©poca se pueden capturar especies como corocoros, rayas, meros, pargos y langostas.</div><div><br></div><div>Es una isla desÃ©rtica: sus Ãºnicos habitantes temporales son pescadores margariteÃ±os y mirandinos; existen planes recientes del gobierno central para construir un aeropuerto, una planta desalinizadora, carreteras y vÃ­as de acceso y un conjunto de hoteles con la intenciÃ³n de atraer el turismo.</div><div><br></div><div>NOTA IMPORTANTE JUNIO 2017:</div><div>A partir de Junio 2017 ofrecemos 3 versiones del mismo servicio: STANDARD traslados en PeÃ±eros de fibra de vidrio el mas econÃ³mico, SUPERIOR traslados en lancha tipo Panga con mas comodidad, VIP el tope de la linea con traslados en lancha Intermarine 350.</div><div><br></div><div>FUENTE: Wikipedia</div><div><br></div>', '<br>', 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xOCExbTEyITFtMyExZDEyNTM1NC45NDAwODM1MjYxITJkLTY1LjM4MDQzODk1NDcxNDYzITNkMTAuOTM3NTI2ODI0ODE1NzkhMm0zITFmMCEyZjAhM2YwITNtMiExaTEwMjQhMmk3NjghNGYxMy4xITNtMyExbTIhMXMweDhjMmMyNjQwYTcyNTVkZDklM0EweDY1NmVmZGJiNjg1MzU2MmYhMnNJc2xhK0xhK1RvcnR1Z2EhNWUwITNtMiExc2VzITJzdmUhNHYxNDg3Njk5MzM0NzY4IiB3aWR0aD0iNDAwIiBoZWlnaHQ9IjMwMCIgZnJhbWVib3JkZXI9IjAiIHN0eWxlPSJib3JkZXI6MCIgYWxsb3dmdWxsc2NyZWVuPjwvaWZyYW1lPg==', '', '2017-02-20 12:59:41', '1'),
(1491875517, 'Canaima Parque Nacional', '237', '4025', 'Parque Nacional Canaima las tierras mas antiguas del planeta', '', 'Es sin duda uno de los destinos mas solicitados en Venezuela por su cercanÃ­a al impresionante Salto Ãngel, el mayor salto de agua en el mundo, con una altura de 1007mts aunado esto a sus hermosos rÃ­os y lagunas, su exuberante flora y fauna Ãºnica en el paÃ­s. Si eres amante de las excursiones y el turismo de aventura este es el destino indicado para tus vacaciones ya que conjuga todos los elementos de una fascinante excursiÃ³n a la selva con la asesorÃ­a de guÃ­as experimentados y los paisajes mas hermosos de Venezuela. Conkhep pone a tu disposiciÃ³n las siguientes alternativas para tu viaje a Canaima con total Comodidad y Seguridad.<br>', '<br>', 'YUhSMGNITTZMeTkzZDNjdVoyOXZaMnhsTG1OdkxuWmxMMjFoY0hNdmNHeGhZMlV2UTJGdVlXbHRZU3dyUW05c0pVTXpKVUZFZG1GeUwwQTJMakkwTlRNMU1ESXNMVFl5TGpnMU9EZzROemtzTVRWNkwyUmhkR0U5SVRSdE5TRXpiVFFoTVhNd2VEaGtZelF5T1RobE9XVTROR1pqWkRjNk1IZzJNMkV3Wm1Ga016bGlPVFExTlRNM0lUaHRNaUV6WkRZdU1qTTVNREU1TkNFMFpDMDJNaTQ0TlRJNU16WXo=', '', '2017-04-11 02:52:47', '1'),
(1488922929, 'Merida', '237', '4033', 'Merida la Ciudad de los Caballeros', 'Merida the City of Knights', '<div>MÃ©rida, es la capital del Municipio Libertador y del estado MÃ©rida. MÃ©rida es una de las principales localidades de los Andes venezolanos. Se encuentra ubicada sobre una meseta en medio de la regiÃ³n, entre las sierras montaÃ±osas de La Culata y Nevada y los parques nacionales homÃ³nimos, dicha condiciÃ³n geogrÃ¡fica la posiciona como un importante centro turÃ­stico. AsÃ­ mismo el prestigio de su principal universidad y la vasta variedad de institutos de enseÃ±anza la han situado a nivel nacional como destino predilecto para el turismo y la poblaciÃ³n estudiantil. Posee gran movimiento nocturno; una cultura rica y diversa, es un importante centro deportivo y posee un notable desarrollo comercial y tecnolÃ³gico.</div><div><br></div><div>La poblaciÃ³n de la ciudad en el aÃ±o 2013 fue de 330.287 habitantes, representando el 34,4% de la poblaciÃ³n total del estado, mientras que el Ã¡rea Metropolitana (ConurbaciÃ³n de los municipios Libertador, Campo ElÃ­as, Sucre y Santos Marquina) poseÃ­a 516.217 habitantes,2 ocupando la dÃ©cimo segunda posiciÃ³n entre las ciudades venezolanas mÃ¡s pobladas y la dÃ©cima posiciÃ³n entre las Ã¡reas metropolitanas de acuerdo a su poblaciÃ³n.</div><div><br></div><div>Es sede de la Universidad de Los Andes, la cual se perfila como la 1ra Universidad del paÃ­s, la nÃºmero 48 de LatinoamÃ©rica y la 1308 en el mundo, en ella se emplazan igualmente la ArquidiÃ³cesis de MÃ©rida y el Seminario de San Buenaventura de los Caballeros de MÃ©rida, ostenta el telefÃ©rico mÃ¡s alto y el Segundo mÃ¡s largo del mundo; Un nuevo y atractivo Sistema de transporte masivo TrolebÃºs de MÃ©rida, el cual se expone como un medio de transporte turÃ­stico, al igual que un muy contemporÃ¡neo sistema de transporte masivo Trolcable, el cual conecta el centro de la ciudad con los suburbios del valle del Chama.</div><div><br></div><div>La localidad de MÃ©rida se encuentra situada a una altitud de 1.600 msnm, asentÃ¡ndose sobre una meseta enclavada en el valle medio del rÃ­o Chama, delimitada por el mismo y que extiende a lo largo de su cuenca; Como telÃ³n de fondo sobresale en el horizonte merideÃ±o las cinco cumbres mÃ¡s elevadas de la naciÃ³n, entre ellas el pico BolÃ­var con 4.978 m.s.n.m.6</div><div><br></div><div>El Sistema de Transporte TurÃ­stico TelefÃ©rico de MÃ©rida o simplemente TelefÃ©rico de MÃ©rida es un Sistema telefÃ©rico que opera en Venezuela. Ya se encuentra en total funcionamiento. Es el telefÃ©rico mÃ¡s alto y segundo mÃ¡s largo del mundo por sÃ³lo 500 metros, pero se encuentra en el primer lugar por ser el Ãºnico que combina tanta altura con tantos kilÃ³metros de largo. El telefÃ©rico de MÃ©rida posee 12,5 KilÃ³metros de trayecto, alcanzando una altura de 4.765 m.s.n.m, haciÃ©ndolo una obra de ingenierÃ­a que fue tradicionalmente Ãºnica en su tipo y con mÃ¡s de 50 aÃ±os de historia. Va desde la ciudad de MÃ©rida hasta la cima del pico Espejo dentro del Parque nacional Sierra Nevada en los Andes venezolanos, especÃ­ficamente en el estado MÃ©rida, Venezuela. Fue cerrado en 2008 para su modernizaciÃ³n y fue reinaugurado el 29 de abril de 2016, en estapa pre-comercial, para finalmente abrir a todo el pÃºblico el 7 de octubre de 2016.</div><div><br></div><div>FUENTE: Wikipedia</div><div><br></div>', '<br><div><span style=\"color: rgb(33, 33, 33); font-family: arial, sans-serif; font-size: 16px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">Merida, is the capital of Libertador Municipality and the state of Merida. MÃ©rida is one of the main towns of the Venezuelan Andes. It is located on a plateau in the middle of the region, between the mountain ranges of La Culata and Nevada and the homonymous national parks, this geographical condition positions it as an important tourist center. Likewise, the prestige of its main university and the vast variety of educational institutes have placed it at a national level as a favorite destination for tourism and the student population. It has great night movement; a rich and diverse culture, is an important sports center and has a remarkable commercial and technological development.\r\n\r\nThe population of the city in 2013 was 330,287 inhabitants, representing 34.4% of the total population of the state, while the Metropolitan Area (Conurbation of the municipalities Libertador, Campo ElÃ­as, Sucre and Santos Marquina) had 516,217 inhabitants , 2 occupying the twelfth position among the most populated Venezuelan cities and the tenth position among the metropolitan areas according to their population.\r\n\r\nIt is the seat of the University of Los Andes, which is shaping up as the 1st University in the country, number 48 in Latin America and 1308 in the world. It also includes the Archdiocese of MÃ©rida and the San Buenaventura de los Caballeros Seminary. of MÃ©rida, it boasts the highest cable car and the second longest in the world; A new and attractive mass transport system Trolleybus of MÃ©rida, which is exposed as a means of tourist transport, as well as a very contemporary mass transport system Trolcable, which connects the city center with the suburbs of the Chama Valley .\r\n\r\nThe town of MÃ©rida is located at an altitude of 1,600 meters above sea level, settling on a plateau located in the middle valley of the Chama river, delimited by it and extending along its basin; As a backdrop, the five highest summits in the nation stand out in the Merida skyline, among them the BolÃ­var peak with 4,978 m.s.m.</span><br></div><div><span style=\"color: rgb(33, 33, 33); font-family: arial, sans-serif; font-size: 16px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\"><br></span></div><div><span style=\"background-color: rgb(255, 255, 255);\"><font color=\"#212121\" face=\"arial, sans-serif\"><span style=\"font-size: 16px; white-space: pre-wrap;\">The MÃ©rida Cable Car Tourist Transport System or simply MÃ©rida Cable Car is a cable car system that operates in Venezuela. It is already in full operation. It is the highest and second longest cable car in the world for only 500 meters, but it is in the first place because it is the only one that combines so much height with so many kilometers in length. The MÃ©rida cable car has a distance of 12.5 kilometers, reaching an altitude of 4,765 meters above sea level, making it an engineering work that was traditionally unique in its type and with more than 50 years of history. It goes from the city of MÃ©rida to the peak of the Espejo peak within the Sierra Nevada National Park in the Venezuelan Andes, specifically in the state of Merida, Venezuela. It was closed in 2008 for its modernization and was reopened on April 29, 2016, in pre-commercial stage, to finally open to the public on October 7, 2016.\r\n\r\nSOURCE: Wikipedia</span></font><br></span></div><div><span style=\"color: rgb(33, 33, 33); font-family: arial, sans-serif; font-size: 16px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\"><br></span></div><div><pre class=\"tw-data-text tw-ta tw-text-small\" data-placeholder=\"TraducciÃ³n\" id=\"tw-target-text\" data-fulltext=\"\" dir=\"ltr\" style=\"unicode-bidi: isolate; background-color: rgb(255, 255, 255); border-width: initial; border-style: none; border-color: initial; padding: 0px 0.14em 0px 0px; position: relative; margin-bottom: 0px; resize: none; overflow: hidden; width: 276px; height: 648px; line-height: 24px !important;\"><font color=\"#212121\" face=\"inherit\"><span style=\"font-size: 16px !important; white-space: pre-wrap;\"><br></span></font></pre></div>', 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xOCExbTEyITFtMyExZDM5NDUuMDQzOTUxMzQzNjA4NiEyZC03MS4xNDM3NzQyODUyMDkxOCEzZDguNTkxNzcyODkzODI0OTYhMm0zITFmMCEyZjAhM2YwITNtMiExaTEwMjQhMmk3NjghNGYxMy4xITNtMyExbTIhMXMweDhlNjQ4NmUzYWJmM2I4MzMlM0EweGFkYjdiYjUwNDJiNWM4ZDchMnNFc3RhY2klQzMlQjNuK2RlK1RlbGVmJUMzJUE5cmljbytCYXJpbml0YXMhNWUwITNtMiExc2VzITJzdmUhNHYxNDg5MDA5NjU4OTIyIiB3aWR0aD0iNDAwIiBoZWlnaHQ9IjMwMCIgZnJhbWVib3JkZXI9IjAiIHN0eWxlPSJib3JkZXI6MCIgYWxsb3dmdWxsc2NyZWVuPjwvaWZyYW1lPg==', '', '2017-03-07 22:12:48', '1'),
(1489008119, 'Bogota', '47', '789', 'Bogota 2.600 metros mas cerca de las estrellas', '', '<div>BogotÃ¡, oficialmente BogotÃ¡, Distrito Capital, abreviado BogotÃ¡, D. C. (durante la Ã©poca de dominio espaÃ±ol y desde 1991 hasta 2000 llamada nuevamente SantafÃ© de BogotÃ¡) es la capital de la RepÃºblica de Colombia y del departamento de Cundinamarca. EstÃ¡ administrada como Distrito Capital, y goza de autonomÃ­a para la gestiÃ³n de sus intereses dentro de los lÃ­mites de la ConstituciÃ³n y la ley. A diferencia de los demÃ¡s distritos de Colombia, BogotÃ¡ es una entidad territorial de primer orden, con las atribuciones administrativas que la ley le confiere a los departamentos. EstÃ¡ constituida por 20 localidades y es el epicentro polÃ­tico, econÃ³mico, administrativo, industrial, artÃ­stico, cultural, deportivo y turÃ­stico del paÃ­s.</div><div><br></div><div>EstÃ¡ ubicada en el centro de Colombia, en la regiÃ³n natural conocida como la sabana de BogotÃ¡, que hace parte del altiplano cundiboyacense, formaciÃ³n ubicada en la cordillera Oriental de los Andes. Es la tercera capital mÃ¡s alta en AmÃ©rica del Sur (despuÃ©s de La Paz y Quito), a un promedio de 2625 metros sobre el nivel del mar.</div><div><br></div><div>Tiene una longitud de 33 km de sur a norte, y 16 km de oriente a occidente. Como capital, alberga los organismos de mayor jerarquÃ­a de la rama ejecutiva (Presidencia de la RepÃºblica), legislativa (Congreso de Colombia) y judicial (Corte Suprema de Justicia, Corte Constitucional, Consejo de Estado y el Consejo Superior de la Judicatura).</div><div><br></div><div>SegÃºn The Economist, en el plano econÃ³mico, BogotÃ¡ se destaca por su fortaleza econÃ³mica asociada al tamaÃ±o de su producciÃ³n, las facilidades para crear empresas y hacer negocios, la madurez financiera, la atracciÃ³n de empresas globales y la calidad de su capital humano. Es el principal mercado de Colombia y de la RegiÃ³n Andina, y el primer destino de la inversiÃ³n extranjera directa que llega a Colombia (70 %). Tiene el mayor PIB nominal y per cÃ¡pita del paÃ­s, aportando la mayor parte al total nacional (24,5 %), y es la sÃ©ptima ciudad por tamaÃ±o del PIB en LatinoamÃ©rica (de unos 92 917 millones USD), igualmente, es la plataforma empresarial mÃ¡s grande de Colombia en donde ocurren la mayorÃ­a de los emprendimientos de alto impacto.</div><div><br></div><div>El aeropuerto de la ciudad transporta el mayor volumen de carga en LatinoamÃ©rica, y es el segundo en cantidad de personas. Es la ciudad de Colombia con el mayor nÃºmero de universidades (114) y centros de investigaciÃ³n. Cuenta con una amplia oferta cultural representada en una gran cantidad de museos, teatros y bibliotecas, que le ha otorgado el reconocimiento de la \"Atenas Suramericana.\" La ciudad se encuentra en el puesto 55 del Ã­ndice Global Cities de 2012,28 y es considerada una ciudad global tipo Beta+ por el GaWC.</div><div><br></div><div>FUENTE: Wikipedia</div><div><br></div>', '<br>', 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xOCExbTEyITFtMyExZDI1NDUwOC41MTY0MTA3NTQzNSEyZC03NC4yNDc4OTM3NzQyMDI5NiEzZDQuNjQ4MjgzNzE3MzQ4NjgyITJtMyExZjAhMmYwITNmMCEzbTIhMWkxMDI0ITJpNzY4ITRmMTMuMSEzbTMhMW0yITFzMHg4ZTNmOWJmZDJkYTZjYjI5JTNBMHgyMzlkNjM1NTIwYTMzOTE0ITJzQm9nb3QlQzMlQTElMkMrQ29sb21iaWEhNWUwITNtMiExc2VzITJzdmUhNHYxNDg5MDA5ODY2NjAwIiB3aWR0aD0iNDAwIiBoZWlnaHQ9IjMwMCIgZnJhbWVib3JkZXI9IjAiIHN0eWxlPSJib3JkZXI6MCIgYWxsb3dmdWxsc2NyZWVuPjwvaWZyYW1lPg==', '', '2017-03-08 21:40:58', '1'),
(1489092186, 'Isla de Margarita', '237', '4036', 'Isla de Margarita .......... La Perla del Caribe', '', '<div>La isla de Margarita, llamada la Perla del Caribe,1 estÃ¡ ubicada al sureste del mar Caribe, noreste venezolano, al norte de la penÃ­nsula de Araya del estado Sucre. Junto a las islas de Coche y Cubagua, constituye el Ãºnico estado insular de Venezuela, denominado Nueva Esparta. La isla desempeÃ±Ã³ un papel importante en la historia de independencia de Venezuela.</div><div><br></div><div>La Isla de Margarita constituye el atractivo turÃ­stico mÃ¡s importante de Venezuela con mÃ¡s de 2.711.000 turistas en el aÃ±o 2009. Posee playas con condiciones para el surf, submarinismo, windsurf, kitesurf y otros deportes acuÃ¡ticos, asÃ­ como pueblos coloniales histÃ³ricos. En los Ãºltimos aÃ±os se ha previsto la realizaciÃ³n de varios proyectos para impulsar el turismo, como el Puerto de Cruceros de Puerto la Mar, la ampliaciÃ³n del Aeropuerto Internacional del Caribe Santiago MariÃ±o, el Faro de Punta Ballena (en cooperaciÃ³n con la Armada de Venezuela) entre otros. En la isla se encuentran varias fortificaciones espaÃ±olas antiguas (castillos, fortines y fortalezas), que se consideran patrimonio nacional.</div><div><br></div><div>Playas mÃ¡s importantes:</div><div>Playa el Agua, Playa La Pared, Puerto Cruz, Playa El Humo, Playa Puerto Viejo, Playa La Restinga, Playa Juan Griego, La Punta del Guamache (Playa ParaÃ­so), Playa El Yaque, Playa El Agua, Playa Parguito, Playa Guacuco, Playa El Tirano, Playa la Galera, Playa Caribe, Playa Manzanillo, Playa Zaragoza (Pedro GonzÃ¡lez), hacia la parte norte las playas son de tipo oceÃ¡nico mientras que al sur son de tipo caribeÃ±o. Las de la parte noreste suelen tener oleaje fuerte, en tanto que las de la parte sur suelen tener un oleaje mÃ¡s calmado.</div><div><br></div><div><a href=\"https://www.youtube.com/watch?v=Cjgw6wUSPpk&amp;feature=em-subs_digest#t=90.389216\" title=\"Video Playa Parguito\" target=\"\">javascript:nicTemp();</a><br></div><div><br></div><div>FUENTE: Wikipedia</div><div><br></div>', '<br>', 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xNCExbTghMW0zITFkMzEzMzEuNTExODQwNTc5ODghMmQtNjMuODQ5OTg5ITNkMTEuMDA1NjUzNyEzbTIhMWkxMDI0ITJpNzY4ITRmMTMuMSEzbTMhMW0yITFzMHg4YzMxOGViMTVlODhmYWNiJTNBMHg1NzJjOWYzMWUzM2M2OTUxITJzUG9ybGFtYXIlMkMrTnVldmErRXNwYXJ0YSE1ZTAhM20yITFzZXMhMnN2ZSE0djE0OTUyNzI1MjcxNTgiIHdpZHRoPSI0MDAiIGhlaWdodD0iMzAwIiBmcmFtZWJvcmRlcj0iMCIgc3R5bGU9ImJvcmRlcjowIiBhbGxvd2Z1bGxzY3JlZW4+PC9pZnJhbWU+', '', '2017-03-09 20:56:13', '1'),
(1495043214, 'Los Roques', '237', '4121', 'Los Roques ................ Un paraiso a 45 minutos de Caracas', '', '<div>El ArchipiÃ©lago Los Roques es una dependencia Federal que agrupa un conjunto de islas y cayos en las Antillas menores pertenecientes a Venezuela que poseen una superficie estimada en 40,61 kmÂ² y que estÃ¡n ubicados entre el archipiÃ©lago Las Aves (al oeste) y la isla de La Orchila (al este) a 176 km al norte de la ciudad de Caracas y que representa uno de los principales atractivos turÃ­sticos del paÃ­s, forma parte del Territorio Insular Francisco de Miranda, es parque nacional y segÃºn estimaciones en el aÃ±o 2014 contaba con 3.100 habitantes fijos (siendo la dependencia federal mÃ¡s poblada). Tiene una superficie aproximada de 221.120 hectÃ¡reas entre espacios marÃ­timos y terrestres, y es considerado el parque marino mÃ¡s grande de AmÃ©rica Latina</div><div>El pueblo del Gran Roque es la principal localidad del archipiÃ©lago, se trata de un pequeÃ±o poblado que consta de calles de arena (las principales son calle MarÃ¡o, Calle Tortuga, Calle CojinÃºa y Calle Chucho). Es la sede de la autoridad Ãºnica de gobierno, hay ademÃ¡s un puesto de INPARQUES, una comisarÃ­a, un puesto de la Guardia Nacional de Venezuela, un pequeÃ±o estadio, escuela primaria y una iglesia catÃ³lica, un Comando de Guardacosta de la Armada de Venezuela, la Superintendencia de Instituto Nacional de Parques, y otros organismos pÃºblicos privados. El aeropuerto estÃ¡ ubicado al borde del mar, a escasos metros de la playa, estÃ¡ separado del pueblo por una Laguna ubicada al este y norte del poblado.</div><div>Las Actividades turÃ­sticas han tenido un auge durante los Ãºltimos aÃ±os estimuladas por sus Playas de arenas blancas, navegaciÃ³n recreacional en diversos tipos de embarcaciones como kayak, velero, bote de remo y catamarÃ¡n (en todo el parque se impone un lÃ­mite de 80 pies de eslora); windsurf, buceo o submarinismo sin tanques, pesca deportiva con caÃ±a, observaciÃ³n de aves y excursiones a pie. Inparques, el organismo estatal encargado del cuidado y manejo de los parque nacionales de Venezuela, tiene designados varios lugares para acampar.</div><div>Otros atractivos turÃ­sticos son la festividad de la Virgen del Valle, anualmente en la segunda semana de septiembre, y el Festival de la Langosta en noviembre, cuando comienza la temporada de pesca.</div><div><br></div><div>FUENTE: wikipedia</div><div><br></div>', '<br>', 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xNCExbTghMW0zITFkNzgwNi43NTcwOTUzMTExOTQ1ITJkLTY2LjY3ODU5MjEhM2QxMS45NDgyNzE5ITNtMiExaTEwMjQhMmk3NjghNGYxMy4xITNtMyExbTIhMXMweDhjMjg1Nzc4NmE1Zjk1YzMlM0EweGRmOWE4MmE4ZDYyYTMyODIhMnNHcmFuK1JvcXVlJTJDK0RlcGVuZGVuY2lhcytGZWRlcmFsZXMrVmVuZXpvbGFuYXMhNWUwITNtMiExc2VzITJzdmUhNHYxNDk1MTEwMzY5ODU3IiB3aWR0aD0iNjAwIiBoZWlnaHQ9IjQ1MCIgZnJhbWVib3JkZXI9IjAiIHN0eWxlPSJib3JkZXI6MCIgYWxsb3dmdWxsc2NyZWVuPjwvaWZyYW1lPg==', '', '2017-05-17 18:15:05', '1'),
(1495410798, 'Caracas', '237', '4034', 'Caracas La Sucursal del Cielo y La de la Eterna Primavera', '', '<div>Caracas, oficialmente Santiago de LeÃ³n de Caracas, es la ciudad capital de la RepÃºblica Bolivariana de Venezuela, y principal centro administrativo, financiero, polÃ­tico, comercial y cultural de la naciÃ³n venezolana. Se encuentra ubicada en la zona centro-norte costera del paÃ­s, a 15 km de la costa del mar Caribe y se sitÃºa dentro de un valle montaÃ±oso a una altitud promedio de 900 msnm. El Parque nacional Waraira Repano, conocido como el cerro Ãvila, es su mayor pulmÃ³n vegetal y accidente geogrÃ¡fico que separa la ciudad del litoral central, con el cual se conecta a travÃ©s de la autopista Caracas-La Guaira, que conduce al estado Vargas asÃ­ como al principal aeropuerto internacional y al segundo puerto del paÃ­s a orillas del mar Caribe.</div><div>Caracas posee una importante cultura gastronÃ³mica y culinaria, esto debido a la influencia de las corrientes migratorias; por ello es frecuente encontrar las especialidades culinarias de las diversas regiones venezolanas, conjuntamente con la de muchos paÃ­ses.</div><div>Existe una gran variedad de restaurantes franceses, italianos, espaÃ±oles, portugueses, libaneses, indios, chinos, japoneses, thailandeses, mexicanos, peruanos, entre otros. La zona de La Candelaria es muy conocida por los restaurantes espaÃ±oles, ya que en esta zona se concentraron gran parte de los inmigrantes gallegos, canarios y sus descendientes llegados a Venezuela a mediados del siglo XX, contribuyendo asÃ­ a la riqueza gastronÃ³mica de la ciudad. Otras zonas de la ciudad como Las Mercedes, Altamira, Sebucan, La Castellana y Los Palos Grandes se caracterizan por una gran cantidad de restaurantes especializados en cocina internacional y gourmet.</div><div>Tambien encontraran gran variedad de alojamientos desde 1 hasta 5 estrellas, pasando por Hoteles Boutique.</div>', '<br>', 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xMCExbTghMW0zITFkMTU2OTIuNTQ3MzczMzQ1NDczITJkLTY2Ljg3MzcyNDAwMDAwMDAxITNkMTAuNDg5ODc3MDQ5OTk5OTk5ITNtMiExaTEwMjQhMmk3NjghNGYxMy4xITVlMCEzbTIhMXNlbiEyc3ZlITR2MTQ5NTU2ODY3MTQ4NiIgd2lkdGg9IjYwMCIgaGVpZ2h0PSI0NTAiIGZyYW1lYm9yZGVyPSIwIiBzdHlsZT0iYm9yZGVyOjAiIGFsbG93ZnVsbHNjcmVlbj48L2lmcmFtZT4=', 'Espaï¿½ol', '2017-05-22 00:09:28', '1'),
(1496867624, 'Aruba', '12', '244', 'One Happy Island', '', '<div>Nuestras playas de arenas blancas, vientos frescos y templados, gente cÃ¡lida y amable, son sÃ³lo algunas de las razones porque tantas personas vuelven a Aruba aÃ±o tras aÃ±o. Elija una de nuestras vacaciones y comience a descubrir todo lo que nos convierte en una isla feliz.</div><div><br></div><div>NUESTRA ISLA HERMOSA</div><div>Situada a 25 kilÃ³metros al norte de Venezuela en las aguas cÃ¡lidas del sur del Caribe, Aruba es el hogar de hermosas playas de arena blanca, con dÃ­as de 27 grados centÃ­grados, una de las poblaciones mÃ¡s cÃ¡lidas del mundo y tambien una de las islas mÃ¡s felices.</div><div><br></div><div>Nuestra isla tiene 32 kilÃ³metros de longitud y casi 10 kilÃ³metros de ancho en su punto mÃ¡s amplio, con una superficie total de aproximadamente 180 kilÃ³metros cuadrados.</div><div><br></div><div>Estamos situados justo debajo de la zona de huracanes, a diferencia de muchas islas del Caribe, el clima es seco, por lo que rara vez llueve.</div><div><br></div><div>En el sur y oeste de Aruba, estÃ¡ Oranjestad, nuestra ciudad capital y kilÃ³metros de playas, algunas nombradas entre las mejores del mundo. AquÃ­, encontrarÃ¡s la mayorÃ­a de los hoteles y resorts todo incluido en Aruba y el Aeropuerto Internacional Reina Beatriz (AUA).</div><div><br></div><div>En el interior de la isla, se encuentra el Parque Nacional Arikok, un desierto donde se preserva una gran variedad de vida silvestre, cactus y espectaculares formaciones rocosas. TambiÃ©n encontrarÃ¡s algunas de las vistas mÃ¡s impresionantes de Aruba, donde el mar se estrella contra la costa rugosa.</div><div><br></div><div>En la costa noreste, a lo largo de la costa de barlovento, podrÃ¡s ver los sÃ­mbolos oficiales de nuestra isla: los Ã¡rboles fofoti. Los vientos alisios constantes y permanentes han esculpido en formas grÃ¡ciles en el paisaje.&nbsp;</div><div><br></div>', '<br>', 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xOCExbTEyITFtMyExZDI0OTI3OC42NzQ1NTk0NTMyNyEyZC03MC4xMDQ5ODUxODU0Mjg2OSEzZDEyLjUxNzUzMDg2NDA2NTc4NSEybTMhMWYwITJmMCEzZjAhM20yITFpMTAyNCEyaTc2OCE0ZjEzLjEhM20zITFtMiExczB4OGU4NTM4Y2ZlMjVhNzdkYiUzQTB4ZjE2YThhM2U4OTgxOGMyZiEyc0FydWJhITVlMCEzbTIhMXNlcyEyc3ZlITR2MTQ5NzM4MTc3NDI3MCIgd2lkdGg9IjYwMCIgaGVpZ2h0PSI0NTAiIGZyYW1lYm9yZGVyPSIwIiBzdHlsZT0iYm9yZGVyOjAiIGFsbG93ZnVsbHNjcmVlbj48L2lmcmFtZT4=', '', '2017-06-07 20:47:48', '1'),
(1498775087, 'Litoral Central Edo. Vargas', '237', '4041', 'Litoral Central las playas mas cercanas a Caracas', '', '<div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">El Litoral Central es el nombre que recibe una regiÃ³n del paÃ­s suramericano de Venezuela, que abarca la mitad del Municipio Vargas del Estado Vargas. Hasta 1998 formaba parte del extinto Distrito Federal. El nombre hace referencia a la cantidad de playas y asentamientos de montaÃ±a ubicados en Ã©l, estÃ¡ bordeado al norte por el Mar Caribe y al sur por la Cordillera de La Costa. Inicia al oeste en el poblado de Chichiriviche de La Costa y finaliza al este en Chuspa, casi llegando al estado Miranda, en la zona se ubica el Aeropuerto de Caracas, el nÃºcleo Litoral de la Universidad SimÃ³n BolÃ­var y el Camposanto Carmen de Uria, la zona fue gravemente afectada por el Terremoto de 1967 y por la Tragedia de Vargas, varia poblaciones como Caraballeda y Los Corales quedaron bajo el lodo y otras como Carmen de Uria y Los Caracas fueron olvidadas. El Litoral es visitado por la mayorÃ­a de los habitantes de Caracas cada fÃ­n de semana y forma parte de la Gran Caracas, tres de sus ciudades (Catia La Mar, La Guaira y Maiquetia) son ciudades dormitorio de Caracas.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\"><br></span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Poblaciones del Litoral[editar]</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Chichiriviche de La Costa:Balneario.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Playa Oricao: Balneario, UrbanizaciÃ³n y Club TurÃ­stico.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Playa Arrecife:Balneario.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Mamo:CaserÃ­o de Pescadores y Balneario.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Catia La Mar:Ciudad EconÃ³mica del Litoral.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Balneario Marina Grande.Balneario y Club TurÃ­stico</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">San SebastiÃ¡n de MaiquetÃ­a:Pueblo Colonial y Aeropuerto Principal de Venezuela.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">La Guaira: Principal Ciudad de Litoral.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Macuto:Poblado Urbano y Balneario.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">CamurÃ­ Chico.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Playa AlÃ­ BabÃ¡: Balneario.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">BahÃ­a de Los NiÃ±os:BahÃ­a y Balneario seguro para toda la familia.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Caraballeda:Centro Residencial del Litoral, aquÃ­ se halla el Club de Golf Caraballeda.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Playa Carrilito.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Playa Escondida.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Carmen de Uria:Camposanto del Litoral.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Tanaguarena:Poblado y Club TurÃ­stico.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">NaiguatÃ¡:Poblado Comercial y Balneario.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Playa Los Ãngeles.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">CamurÃ­ Grande: Poblado Residencial, aquÃ­ se ubica el NÃºcleo Litoral de USB.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Playa Pantaleta: Balneario conocido por los surfistas.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Playa PelÃºa: Balneario conocido por los surfistas.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Ciudad Vacacional Los Caracas: Antigua Ciudad Vacacional, hoy en estado de recuperaciÃ³n, zona de posadas y Balnearios (La Punta, Los Pescadores y Mirador Rey Mar).</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Todasana: CaserÃ­o TurÃ­stico y Balneario de rÃ­o y mar.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Caruao: CaserÃ­o de Pescadores y Balneario.</span></font></div><div><font face=\"Tahoma, sans-serif\"><span style=\"font-size: 13.3333px;\">Chuspa: CaserÃ­o de Pescadores y Balnearios. Ãšltimo pueblo antes de llegar a Miranda.</span></font></div><div><br></div>', '<br>', 'aHR0cHM6Ly9nb28uZ2wvbWFwcy9HSkJ3SkNiWWRMSzI=', 'Espaï¿½ol', '2017-06-29 22:31:04', '1');
INSERT INTO `hk_pagina` (`id`, `nombre`, `pais`, `estado`, `corta`, `corta_ingles`, `larga`, `larga_ingles`, `gmaps`, `idioma`, `fecha`, `estatus`) VALUES
(1499456338, 'Peru', '172', '2825', 'Descubra lo mÃ­stico e histÃ³rico, con la modernidad', '', '<div>NOTA IMPORTANTE 24 Jul 2017:</div><div><p class=\"MsoNormal\" style=\"line-height:150%\"><span style=\"font-size:10.5pt;\r\nline-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\ncolor:#202020\">Ante la declaraciÃ³n del Estado de Emergencia por 30 dÃ­as en\r\nalgunos distritos de las regiones de Cusco y Puno, el Ministerio de Comercio\r\nExterior y Turismo (MINCETUR) comunica lo siguiente:</span><span style=\"font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;\r\ncolor:#202020\"> <o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-top:7.5pt;margin-right:0cm;margin-bottom:7.5pt;\r\nmargin-left:0cm;text-align:justify;line-height:150%\"><span style=\"font-family:\r\n&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#202020\"><br>\r\n</span><span style=\"font-size:10.5pt;line-height:150%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;\r\ncolor:#202020\">1. La declaratoria se ha realizado como medida preventiva para\r\nasegurar el orden pÃºblico y evitar actos de violencia que puedan afectar la\r\nintegridad fÃ­sica de las personas â€“nacionales y extranjerosâ€“ y de la propiedad\r\npÃºblica y privada.</span><span style=\"font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;\r\ncolor:#202020\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-top:7.5pt;margin-right:0cm;margin-bottom:7.5pt;\r\nmargin-left:0cm;text-align:justify;line-height:150%;-ms-text-size-adjust: 100%;\r\n-webkit-text-size-adjust: 100%\"><span style=\"font-size:10.5pt;line-height:150%;\r\nfont-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#202020\">&nbsp;</span><span style=\"font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#202020\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-top:7.5pt;margin-right:0cm;margin-bottom:7.5pt;\r\nmargin-left:0cm;text-align:justify;line-height:150%;-ms-text-size-adjust: 100%;\r\n-webkit-text-size-adjust: 100%\"><span style=\"font-size:10.5pt;line-height:150%;\r\nfont-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#202020\">2. La medida tiene efecto en el\r\ndistrito de Juliaca en Puno; en los distritos de Wanchaq, San SebastiÃ¡n y Cusco\r\nde la provincia de Cusco, y en los distritos de Machupicchu y Ollantaytambo de\r\nla provincia de Urubamba.</span><span style=\"font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;\r\ncolor:#202020\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-top:7.5pt;margin-right:0cm;margin-bottom:7.5pt;\r\nmargin-left:0cm;text-align:justify;line-height:150%;-ms-text-size-adjust: 100%;\r\n-webkit-text-size-adjust: 100%\"><span style=\"font-size:10.5pt;line-height:150%;\r\nfont-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#202020\">&nbsp;</span><span style=\"font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#202020\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-top:7.5pt;margin-right:0cm;margin-bottom:7.5pt;\r\nmargin-left:0cm;text-align:justify;line-height:150%;-ms-text-size-adjust: 100%;\r\n-webkit-text-size-adjust: 100%\"><span style=\"font-size:10.5pt;line-height:150%;\r\nfont-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#202020\">3. El Gobierno Nacional se\r\nencuentra trabajando para garantizar el libre trÃ¡nsito de vehÃ­culos y personas\r\nen los referidos distritos. </span><span style=\"font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;\r\ncolor:#202020\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-top:7.5pt;margin-right:0cm;margin-bottom:7.5pt;\r\nmargin-left:0cm;text-align:justify;line-height:150%;-ms-text-size-adjust: 100%;\r\n-webkit-text-size-adjust: 100%\"><span style=\"font-size:10.5pt;line-height:150%;\r\nfont-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#202020\">&nbsp;</span><span style=\"font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#202020\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-top:7.5pt;margin-right:0cm;margin-bottom:7.5pt;\r\nmargin-left:0cm;text-align:justify;line-height:150%;-ms-text-size-adjust: 100%;\r\n-webkit-text-size-adjust: 100%\"><span style=\"font-size:10.5pt;line-height:150%;\r\nfont-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#202020\">4. La Red de ProtecciÃ³n al\r\nTurista, en trabajo conjunto con el Ministerio del Interior y todas las\r\nentidades competentes, ha tomado las medidas necesarias para velar por la seguridad\r\nde los visitantes.</span><span style=\"font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;\r\ncolor:#202020\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-top:7.5pt;margin-right:0cm;margin-bottom:7.5pt;\r\nmargin-left:0cm;text-align:justify;line-height:150%;-ms-text-size-adjust: 100%;\r\n-webkit-text-size-adjust: 100%\"><span style=\"font-size:10.5pt;line-height:150%;\r\nfont-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#202020\">&nbsp;</span><span style=\"font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#202020\"><o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" style=\"margin-top:7.5pt;margin-right:0cm;margin-bottom:7.5pt;\r\nmargin-left:0cm;text-align:justify;line-height:150%;-ms-text-size-adjust: 100%;\r\n-webkit-text-size-adjust: 100%\"><span style=\"font-size:10.5pt;line-height:150%;\r\nfont-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#202020\">5. El MINCETUR a travÃ©s de\r\nPROMPERÃš viene ofreciendo apoyo e informaciÃ³n oportuna a los turistas\r\nnacionales y extranjeros a travÃ©s de las oficinas descentralizadas de iPerÃº,\r\nubicadas en las diferentes regiones del paÃ­s.</span><span style=\"font-family:\r\n&quot;Helvetica&quot;,&quot;sans-serif&quot;;color:#202020\"><o:p></o:p></span></p>\r\n\r\n<span style=\"font-size:12.0pt;font-family:&quot;Helvetica&quot;,&quot;sans-serif&quot;;mso-fareast-font-family:\r\nCalibri;mso-fareast-theme-font:minor-latin;color:#202020;mso-ansi-language:\r\nES-VE;mso-fareast-language:ES-VE;mso-bidi-language:AR-SA\"><br>\r\n</span><span style=\"font-size:10.5pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;\r\nmso-fareast-font-family:Calibri;mso-fareast-theme-font:minor-latin;color:#202020;\r\nmso-ansi-language:ES-VE;mso-fareast-language:ES-VE;mso-bidi-language:AR-SA\">El\r\nMINCETUR invoca a los turistas nacionales y extranjeros seguir las\r\nrecomendaciones de seguridad de las entidades involucradas que velan por\r\nmantener el orden.</span><br></div><div><br></div><div>El PerÃº es una suma de tradiciones, culturas, lenguas. El PerÃº es como el rÃ­o Amazonas, que nace de la uniÃ³n de varios afluentes hasta lograr su inmensidad. El PerÃº naciÃ³ asÃ­, en los peruanos fluye sangre indÃ­gena, europea, africana y oriental. Este mestizaje es su fortaleza y orgullo.</div><div>Las culturas prehispÃ¡nicas y los Incas lograron convivir en armonÃ­a con la agreste geografÃ­a peruana. Dominaron con sabidurÃ­a un mar rico en peces y sobre las arenas del desierto levantaron pirÃ¡mides que retan al tiempo. En los Andes hicieron caminos y puentes que aÃºn son usados y construyeron con su avanzada ingenierÃ­a ciudades de piedras. En la selva se adaptaron a la naturaleza y desarrollaron civilizaciones gracias a la protecciÃ³n de la Pachamama.</div><div>Parte de estas culturas milenarias viven hoy en cada pueblo y se descubren en las costumbres de la gente. Llegar al PerÃº es viajar a travÃ©s del tiempo y reencontrar las huellas ancestrales de los Incas, los Chancas, los Chachapoyas, los Mochicas y los Wari; asÃ­ como sus grandes obras arquitectÃ³nicas, su arte, sus fiestas, las raÃ­ces de su fortaleza social y la energÃ­a de sus hombres.</div><div>Recorrer el PerÃº es descubrir mundos con paisajes, sonidos, colores y sabores propios. Viaja a travÃ©s del tiempo por civilizaciones milenarias y comparte con los peruanos su gran herencia cultural. Busca informaciÃ³n y disfruta de innumerables destinos con emociones difÃ­ciles de olvidar. Experiencias y recuerdos que siempre llevarÃ¡s contigo.</div><div>Caral, la primera civilizaciÃ³n de AmÃ©rica, culturas prehispÃ¡nicas, el Imperio Inca, la fusiÃ³n inca con el mundo hispÃ¡nico. PerÃº y sus desiertos, montaÃ±as, selvas y mar. Paisajes inolvidables. Flora, fauna y una gran variedad de manifestaciones culturales.</div><div>El PerÃº es reconocido en el mundo por su alta cocina. La abundancia de productos naturales, la fusiÃ³n de tÃ©cnicas y culturas, forjaron en siglos una gastronomÃ­a con identidad mestiza. Las cocinas regionales mantienen su identidad, calidad y variedad de sabores. Por la excelente calidad y pasiÃ³n de los cocineros peruanos, Lima ha sido nombrada Capital GastronÃ³mica de LatinoamÃ©rica.</div><div>PerÃº, un paÃ­s de aventura, historia y hospitalidad. PerÃº, un mundo de sensaciones.</div><div>FUENTE: Wikipedia</div><div><br></div>', '<br>', 'aHR0cHM6Ly9nb28uZ2wvbWFwcy9mR1B0TnFaN2RRbg==', '', '2017-07-07 20:20:21', '1'),
(1515758186, 'Puerto Ordaz', '237', '4025', 'Puerto Ordaz Ciudad entre dos rios', 'Puerto Ordaz City between two rivers', '<p style=\"margin-top: 0.5em; margin-bottom: 0.5em; line-height: inherit; color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);\"><b>Puerto Ordaz</b>&nbsp;es una localidad venezolana que se encuentra en el oeste del&nbsp;<a href=\"https://es.wikipedia.org/wiki/Municipio_Caron%C3%AD\" title=\"Municipio CaronÃ­\" style=\"color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Municipio CaronÃ­</a>&nbsp;y conforma junto a&nbsp;<a href=\"https://es.wikipedia.org/wiki/San_F%C3%A9lix_(Venezuela)\" title=\"San FÃ©lix (Venezuela)\" style=\"color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">San FÃ©lix</a>&nbsp;la&nbsp;<a href=\"https://es.wikipedia.org/wiki/Ciudad_Guayana\" title=\"Ciudad Guayana\" style=\"color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Ciudad Guayana</a>. Puerto Ordaz es una localidad del&nbsp;<a href=\"https://es.wikipedia.org/wiki/Bol%C3%ADvar_(Venezuela)\" class=\"mw-redirect\" title=\"BolÃ­var (Venezuela)\" style=\"color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">estado BolÃ­var</a>, en el este de&nbsp;<a href=\"https://es.wikipedia.org/wiki/Venezuela\" title=\"Venezuela\" style=\"color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Venezuela</a>. Fundada en&nbsp;<a href=\"https://es.wikipedia.org/wiki/1952\" title=\"1952\" style=\"color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">1952</a>&nbsp;como puerto de exportaciÃ³n minera a orillas del&nbsp;<a href=\"https://es.wikipedia.org/wiki/R%C3%ADo_Caron%C3%AD\" title=\"RÃ­o CaronÃ­\" style=\"color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">rÃ­o CaronÃ­</a>, en el punto donde Ã©ste fluye al&nbsp;<a href=\"https://es.wikipedia.org/wiki/R%C3%ADo_Orinoco\" title=\"RÃ­o Orinoco\" style=\"color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">rÃ­o Orinoco</a>; Puerto Ordaz es sede de empresas mineras e hidroelÃ©ctricas. Su aeropuerto sirve de enlace entre los pequeÃ±os aeropuertos en la zona selvÃ¡tica del Estado BolÃ­var y el resto del paÃ­s.</p><p style=\"margin-top: 0.5em; margin-bottom: 0.5em; line-height: inherit; color: rgb(34, 34, 34); font-family: sans-serif; font-size: 14px; background-color: rgb(255, 255, 255);\">Junto con los sectores de Matanzas (Zona Industrial y Residencial), Unare, Alta Vista y la localidad de&nbsp;<a href=\"https://es.wikipedia.org/wiki/San_F%C3%A9lix_(Venezuela)\" title=\"San FÃ©lix (Venezuela)\" style=\"color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">San FÃ©lix</a>, forman una aglomeraciÃ³n urbana que desde el 2 de julio de&nbsp;<a href=\"https://es.wikipedia.org/wiki/1961\" title=\"1961\" style=\"color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">1961</a>&nbsp;recibe oficialmente el nombre de&nbsp;<a href=\"https://es.wikipedia.org/wiki/Ciudad_Guayana\" title=\"Ciudad Guayana\" style=\"color: rgb(11, 0, 128); background-image: none; background-position: initial; background-size: initial; background-repeat: initial; background-attachment: initial; background-origin: initial; background-clip: initial;\">Ciudad Guayana</a>&nbsp;tiene una poblaciÃ³n de 852.000 habitantes.</p>', '<br><div><span style=\"color: rgb(33, 33, 33); font-family: arial, sans-serif; font-size: 16px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">Puerto Ordaz is a Venezuelan town located in the west of the CaronÃ­ Municipality and, together with San Felix, the Ciudad Guayana. Puerto Ordaz is a town in BolÃ­var state, in eastern Venezuela. Founded in 1952 as a mining export port on the banks of the CaronÃ­ River, at the point where it flows to the Orinoco River; Puerto Ordaz is the headquarters of mining and hydroelectric companies. Its airport serves as a link between the small airports in the jungle area of â€‹â€‹Bolivar State and the rest of the country.\r\n\r\nTogether with the sectors of Matanzas (Industrial and Residential Zone), Unare, Alta Vista and the town of San Felix, form an urban agglomeration that since July 2, 1961 officially receives the name of Ciudad Guayana has a population of 852,000 inhabitants.</span><br></div>', 'PGlmcmFtZSBzcmM9Imh0dHBzOi8vd3d3Lmdvb2dsZS5jb20vbWFwcy9lbWJlZD9wYj0hMW0xOCExbTEyITFtMyExZDEyNjM0NS4zNTk5OTU0NTg0NCEyZC02Mi44MzMzMDIzODQyOTc3OSEzZDguMjczNjU4MjUzNjc3NjI0ITJtMyExZjAhMmYwITNmMCEzbTIhMWkxMDI0ITJpNzY4ITRmMTMuMSEzbTMhMW0yITFzMHg4ZGNiZjkzMDk1N2RkYTNmJTNBMHhhN2JlMThhMmQ3Y2MzZDIyITJzUHVlcnRvK09yZGF6JTJDK0NpdWRhZCtHdWF5YW5hKzgwNTAlMkMrQm9sJUMzJUFEdmFyITVlMCEzbTIhMXNlcyEyc3ZlITR2MTUxNTg2MDc3MzA1NiIgd2lkdGg9IjYwMCIgaGVpZ2h0PSI0NTAiIGZyYW1lYm9yZGVyPSIwIiBzdHlsZT0iYm9yZGVyOjAiIGFsbG93ZnVsbHNjcmVlbj48L2lmcmFtZT4=', '', '2018-01-12 12:00:53', '1'),
(1516292997, 'Argentina', '10', '208', 'Argentina tierra de Tangos y Gauchos', '', '<div>La RepÃºblica Argentina, conocida simplemente como Argentina,â€‹ es un paÃ­s soberano de AmÃ©rica del Sur, ubicado en el extremo sur y sudeste de dicho subcontinente. Adopta la forma de gobierno republicana, democrÃ¡tica, representativa y federal.</div><div><br></div><div>La Argentina estÃ¡ organizada como un Estado federal descentralizado, integrado por un Estado nacional y veinticuatro estados jurisdiccionales con constituciÃ³n propia y autonomÃ­a polÃ­tica -veintitrÃ©s provincias y una ciudad autÃ³noma-.14â€‹15â€‹ Las 23 provincias se reservan todos los poderes no delegados al Estado nacional.â€‹ Los municipios son autÃ³nomos y su capital federal es la Ciudad de Buenos Aires. La ConstituciÃ³n reconoce la preexistencia Ã©tnica y cultural de los pueblos indÃ­genas, garantizando sus derechos a la identidad, a una educaciÃ³n bilingÃ¼e e intercultural y a la propiedad comunitaria de sus tierras.</div><div><br></div><div>Integra el Mercosur â€”bloque del que fue fundador en 1991â€”, la UniÃ³n de Naciones Sudamericanas (Unasur), la Comunidad de Estados Latinoamericanos y CaribeÃ±os (CELAC) y la OrganizaciÃ³n de Estados Americanos (OEA).</div><div><br></div><div>En 2016, su Ãndice de Desarrollo Humano fue del 0,827 y se encuentra en el puesto 45,17â€‹ en el grupo de paÃ­ses de desarrollo humano muy alto. Su Ãndice de Desarrollo Humano ajustado por la desigualdad es de 0,698; con un coeficiente de Gini de 42,7.18â€‹ En educaciÃ³n, posee una inversiÃ³n del 5,1 % del PBI,â€‹ teniendo las mÃ¡s altas tasas de matrÃ­cula en toda la regiÃ³n, con una poblaciÃ³n casi al tope del alfabetismo. SegÃºn el Banco Mundial, su PIB nominal es el 21.Âº del mundo.â€‹</div><div><br></div><div>Debido a su importancia geopolÃ­tica y econÃ³mica, es uno de los tres estados soberanos latinoamericanos que forma parte del denominado Grupo de los 20 e integra ademÃ¡s el grupo de los NIC o nuevos paÃ­ses industrializados.</div><div><br></div><div>Su capacidad tecnolÃ³gica y cientÃ­fica le ha permitido diseÃ±ar y producir satÃ©lites,â€‹ construir reactores nucleares y ser una potencia, aunque en los Ãºltimos aÃ±os, su influencia se ha visto reducida debido a las transiciones internas que estÃ¡ sufriendo. Argentina ha brindado una creciente cooperaciÃ³n nuclear a paÃ­ses de AmÃ©rica Latina, el Magreb, el Golfo PÃ©rsico, el sudeste asiÃ¡tico y OceanÃ­a, a partir de las capacidades desarrolladas por la ComisiÃ³n Nacional de EnergÃ­a AtÃ³mica (CNEA) y por la empresa estatal INVAP.22â€‹ Es el paÃ­s latinoamericano que mÃ¡s premios Nobel ha ganado â€”cinco en totalâ€”, tres de ellos vinculados con la ciencia.</div><div><br></div><div>Con una superficie de 2 780 400 kmÂ²,5â€‹ es el paÃ­s hispanohablante mÃ¡s extenso del planeta, el segundo mÃ¡s grande de AmÃ©rica Latina, y octavo en el mundo, si se considera solo la superficie continental sujeta a soberanÃ­a efectiva. Su plataforma continental, reconocida por la ONU en 2016, alcanza los 6 581 500 kmÂ²23â€‹ convirtiÃ©ndose en una de las mÃ¡s grandes del mundo,â€‹ extendiÃ©ndose desde el continente americano hasta el Polo Sur en la AntÃ¡rtida, a travÃ©s de AtlÃ¡ntico Sur. Si se cuentan las islas Malvinas, Georgias del Sur, Sandwich del Sur y otras numerosas islas menores (administradas por el Reino Unido pero de soberanÃ­a en litigio), mÃ¡s una porciÃ³n del Ã¡rea antÃ¡rtica llamada AntÃ¡rtida Argentina al sur del paralelo 60Â° S, sobre la cual Argentina reclama soberanÃ­a, la superficie se eleva a 3 761 274 kmÂ².25â€‹ Es uno de los veinte paÃ­ses que tienen presencia permanente en la AntÃ¡rtida, siendo entre ellos el que tiene mayor cantidad de bases permanentes, con seis bases en total.</div><div>FUENTE: Wikipedia</div><div><br></div>', '<br>', 'YUhSMGNITTZMeTluYjI4dVoyd3ZiV0Z3Y3k5TGRUVkVXbGxRVWpOMlZEST0=', '', '2018-01-18 16:49:27', '1'),
(1516296163, 'Parque Nacional Morrocoy', '237', '4030', 'Morrocoy famoso por sus cayos', '', '<div>El Parque nacional Morrocoy es un parque nacional ubicado en el litoral mÃ¡s oriental del estado FalcÃ³n y en el noroeste del Golfo Triste; en la costa centro occidental venezolana, cerca de las poblaciones de Boca de Aroa, Tucacas, Sanare, Chichiriviche, Flamenco y Tocuyo de la Costa. Fue declarado parque nacional el 26 de mayo de 1974 y consta de 32.090 ha.</div><div>El parque se extiende tanto por zonas terrestres como acuÃ¡ticas del Golfo Triste, contiene una zona de manglares y gran cantidad de islotes o cayos entre los cuales se encuentran Borracho, Muerto, Sombrero, Sal, Las Animas y Peraza, entre otros, con playas espectaculares de arenas blancas como playa Mero, PaiclÃ¡s, Punta Brava, Mayorquina, Playuela, Playuelita, Sur (conocida popularmente como azul por el azul de sus aguas), Norte, Boca Seca, Playa Muerto y muchas mÃ¡s; asÃ­ como bajos (sin orilla) como Bajo CaimÃ¡n, Tucupido y Los Juanes.</div><div>En el relieve destacan el cerro Morrocoy, con elevaciones de 250 msnm; el conjunto de ensenadas y manglares internos y los cayos e islas.</div><div><br></div><div>FUENTE: Wikipedia</div><div><br></div>', '<br>', 'aHR0cHM6Ly9nb28uZ2wvbWFwcy9kYVJqTHdLVjJkODI=', 'Espaï¿½ol', '2018-01-18 18:04:37', '1'),
(1516369201, 'Puerto Cabello', '237', '4026', 'Puerto Cabello', '', '<div>Puerto Cabello es una Ciudad de Venezuela, capital del municipio Puerto Cabello en el Estado Carabobo, en cuyas inmediaciones se halla el puerto marÃ­timo mÃ¡s importante y de mayor valor econÃ³mico del paÃ­s debido a su gran actividad de importaciÃ³n de materias primas para el sector industrial venezolano, que normalmente se trasladan hacia Valencia y Maracay. Le siguen en importancia los puertos de La Guaira y Guanta. EstÃ¡ ubicado al norte en las costas del Estado Carabobo.</div><div>Su poblaciÃ³n es de 209.080 habitantes (Censo 2011), lo que la convierte en la segunda ciudad mÃ¡s importante y poblada del Estado Carabobo y ocupa el puesto nÃºmero 15 de ciudades mÃ¡s pobladas de Venezuela. Desde julio de 1946, en esta ciudad se encuentra la Base Naval AgustÃ­n Armario y desde 1960 el Comando de la Armada, razÃ³n por la cual este puerto se ha convertido en una de las bases navales de la Marina de Guerra mÃ¡s importantes de Venezuela.<br></div><div><br></div><div>Frente a Puerto Cabello hay una serie de pequeÃ±as islas:</div><div>Isla Larga: es la de mayor dimensiÃ³n, con 1855 m de longitud. Actualmente forma parte del Parque Nacional San Esteban.<br></div><div>Isla Santo Domingo: es una isla de 463 m a unos 928 m al oeste de Isla Larga.<br></div><div>Isla RatÃ³n: es un islote rodeado de corales a 1390 m al suroeste de la Isla Santo Domingo y a 230 m de la costa continental.<br></div><div>Isla del Rey: con 463 m de longitud, se halla rodeada de corales y arrecifes.<br></div><div><br></div><div>FUENTE: Wikipedia</div><div><br></div>', '<br>', 'YUhSMGNITTZMeTluYjI4dVoyd3ZiV0Z3Y3k5eU9FMVpiVTFUZFdWSGJ3PT0=', 'Espaï¿½ol', '2018-01-19 13:50:02', '1'),
(1516370868, 'MATURIN', '237', '4035', 'Maturin estado Monagas', '', '<div>MaturÃ­n es una ciudad venezolana de la regiÃ³n nor-oriental, capital del estado Monagas y del municipio homÃ³nimo. EstÃ¡ situada a 122 msnm, junto al rÃ­o Guarapiche, a 416 km de Caracas en lÃ­nea recta.</div><div>MaturÃ­n, fundada el 7 de diciembre de 1760 por el fraile capuchino Lucas de Zaragoza, ha cobrado auge en los Ãºltimos aÃ±os como consecuencia de la actividad petrolera en sus cercanÃ­as. La ciudad es el principal centro polÃ­tico, administrativo, financiero, comercial y de servicios del estado Monagas.<br></div><div><br></div>', '<br>', 'aHR0cHM6Ly9nb28uZ2wvbWFwcy8yUVZOeXBaTUdRUDI=', 'Espaï¿½ol', '2018-01-19 14:14:30', '1'),
(1516394576, 'Punto Fijo', '237', '4030', 'Punto Fijo Peninsula de Paraguana', '', '<div>Punto Fijo es una ciudad de Venezuela, capital del Municipio Carirubana y ciudad mÃ¡s poblada del Estado FalcÃ³n. EstÃ¡ ubicada al suroeste de la penÃ­nsula de ParaguanÃ¡. Su Ã¡rea metropolitana abarca las parroquias urbanas Norte, Carirubana, Punta CardÃ³n y la parroquia Judibana del municipio Los Taques. SegÃºn un estudio del Instituto Nacional de EstadÃ­sticas (INE) la ciudad posee una poblaciÃ³n para el 2011 de 287558 habitantes, es la 14.Âº ciudad mÃ¡s poblada en Venezuela y la mÃ¡s grande en el estado de FalcÃ³n.â€‹ Punto Fijo tiene una altitud promedio de 29 metros sobre el nivel del mar.2</div><div><br></div><div>Punto Fijo resalta por ser el centro urbano mÃ¡s grande, poblado y el principal eje econÃ³mico del Estado FalcÃ³n, puesto que existe un gran movimiento comercial, turÃ­stico e industrial en la zona, siendo cuna de los principales complejos refinadores no solo del paÃ­s sino tambiÃ©n de LatinoamÃ©rica y del mundo; ademÃ¡s de una zona libre de inversiÃ³n turÃ­stica libre de impuestos Ãºnica en la regiÃ³n. Cabe destacar que dicha ciudad ha sido incluida aÃ±o tras aÃ±o como unas de las ciudades mÃ¡s atractivas para invertir en Venezuela.3</div><div><br></div><div>FUENTE: Wikipedia</div><div><br></div>', '<br>', 'YUhSMGNITTZMeTluYjI4dVoyd3ZiV0Z3Y3k5d05tSnBXWEZhZUdkaVF6ST0=', 'Espaï¿½ol', '2018-01-19 21:09:18', '1'),
(1518012191, 'San Felipe Estado Yaracuy', '237', '4042', 'Yaracuy tierras de magia y esoterismo', 'Yaracuy lands of magic and esotericism', '<div>Yaracuy es uno de los veintitrÃ©s estados que, junto con el Distrito Capital y las Dependencias Federales, forman la RepÃºblica Bolivariana de Venezuela. Su capital es San Felipe. EstÃ¡ ubicado en la regiÃ³n centro-Norte del paÃ­s, limitando al norte con FalcÃ³n, al noreste con el golfo Triste (mar Caribe, ocÃ©ano AtlÃ¡ntico), al este con Carabobo, al sur con Cojedes y al oeste con Lara. Con 7100 kmÂ² es el quinto estado menos extenso por delante de Aragua, Carabobo, Vargas y Nueva Esparta, el menos extenso y con 600 000 habs. en 2011.</div><div><br></div><div>Posee 14 municipios autÃ³nomos y 21 parroquias civiles. Sus principales ciudades son: San Felipe, Yaritagua, Chivacoa, Nirgua, Cocorote, Urachiche y Aroa.</div><div>FUENTE: Wikipedia</div>', '<br><div><span style=\"color: rgb(33, 33, 33); font-family: arial, sans-serif; font-size: 16px; white-space: pre-wrap; background-color: rgb(255, 255, 255);\">Yaracuy is one of the twenty-three states that, together with the Capital District and the Federal Dependencies, form the Bolivarian Republic of Venezuela. Its capital is San Felipe. It is located in the center-north region of the country, bordering on the north with FalcÃ³n, on the northeast with Golfo Triste (Caribbean Sea, Atlantic Ocean), on the east with Carabobo, on the south with Cojedes and on the west with Lara. With 7100 kmÂ² it is the fifth least extensive state ahead of Aragua, Carabobo, Vargas and Nueva Esparta, the least extensive and with 600 000 inhabitants. in 2011.\r\n\r\nIt has 14 autonomous municipalities and 21 civil parishes. Its main cities are: San Felipe, Yaritagua, Chivacoa, Nirgua, Cocorote, Urachiche and Aroa.\r\nSOURCE: Wikipedia</span><br></div>', 'WVVoU01HTklUVFpNZVRsdVlqSTRkVm95ZDNaaVYwWjNZM2s1TTFrd05UTk9WR2cwVGtjNU5VNUVTVDA9', 'Espaï¿½ol', '2018-02-07 14:22:43', '1'),
(1518710392, 'Republica Dominicana', '61', '990', 'PUNTA CANA unas de las mejores playas del caribe', '', '<div>Punta Cana es una localidad situada al este de la RepÃºblica Dominicana, en la provincia de La Altagracia. En esta localidad se ubican varios complejos hoteleros, cuya superficie total es de unos 420 000 mÂ² (equivalentes a 42 hectÃ¡reas o 0,42 kmÂ²).</div><div><br></div><div>En Punta Cana se encuentra el Aeropuerto Internacional Punta Cana (PUJ), el principal aeropuerto del paÃ­s, situado a unos 30 km hacia el interior, en la carretera que lleva desde HigÃ¼ey hasta La Romana. Este aeropuerto recibe el 64% de todos los vuelos que llegan al paÃ­s, por lo que recibe mÃ¡s pasajeros que el Aeropuerto Internacional de Las AmÃ©ricas, situado en Santo Domingo.</div><div><br></div><div>Administrativamente, Punta Cana es un distrito municipal perteneciente al municipio de HigÃ¼ey bajo el nombre de Distrito Municipal TurÃ­stico VerÃ³n Punta Cana.â€‹ Este distrito tenÃ­a segÃºn el censo de 2010 una poblaciÃ³n de 43 982 habitantes, de los cuales 37 286 eran del Ã¡rea urbana y 6696 del Ã¡rea rural.</div><div>La zona de Punta Cana se inicia al norte con la Playa de Arena Gorda, siguiendo despuÃ©s la famosa Playa BÃ¡varo, Playa Uvero Alto, Playa Lavacama, Playa Macao y la Playa de El Cortecito. Cuando se inicia la vuelta de la punta se encuentran las playas de Cabeza de Toro, Cabo EngaÃ±o, Punta Cana y Juanillo. El litoral de la zona de Punta Cana se extiende 50 km de costa. La parte de playa mÃ¡s extensa es la de BÃ¡varo, considerado por el gobierno de RepÃºblica Dominicana como tesoro nacional por la riqueza de su flora y fauna y por las bellezas naturales que allÃ­ se aprecian.</div><div><br></div><div>Las playas son de arena blanca y fina, y el mar de un suave color azul verdoso, sin alcanzar el turquesa de las playas caribeÃ±as situadas mÃ¡s al sur. El mar presenta generalmente oleaje y la playa se hunde muy rÃ¡pidamente en el mar. El agua siempre es transparente y abundan las algas en algunas Ã¡reas.</div><div><br></div><div>La zona de Punta Cana es el 23302 (CÃ³digo Postal), cada regiÃ³n se identifica con el primer dÃ­gito, en el caso de RepÃºblica Dominicana, estÃ¡ dividido en nueve regiones postales.</div><div><br></div><div>La principal atracciÃ³n de Punta Cana es el turismo, ofreciendo una gran variedad de complejos hoteleros. Estos reciben visitas tanto de locales como de internacionales que arriban desde el Aeropuerto Internacional de Punta Cana o el Aeropuerto Internacional La Romana.&nbsp;</div><div>Algunos de estos complejos son:</div><div>Gran Bahia Principe Punta Cana<br></div><div>Gran Bahia Principe Bavaros</div><div>Gran Bahia Principe FantasÃ­a</div><div>The Westin Puntacana Resort &amp; Club</div><div>Four Points by Sheraton Puntacana Village</div><div>Tortuga Bay Hotel Puntacana Resort &amp; Club</div><div>Hard Rock Hotel &amp; Casino Punta Cana</div><div>Sunscape BÃ¡varo Beach</div><div>Sunscape Dominican Beach</div><div>BarcelÃ³ Palace Deluxe</div><div>Paradisus Punta Cana</div><div>AlSol Tiara Cap Cana</div><div>Iberostar Grand Hotel Bavaro</div><div>Zoetry Agua Punta Cana</div><div>Sanctuary Cap Cana by AlSol</div><div>Secrets Royal Beach Punta Cana</div><div>AlSol Luxury Village</div><div>Excellence Punta Cana</div><div>Dreams Punta Cana Resort &amp; Spa</div><div>Paradisus Palma Real Golf &amp; Spa Resort</div><div>Breathless Punta Cana Resort &amp; Spa</div><div>Majestic Mirage</div><div>Majestic Elegance</div><div>Majestic Colonial</div><div>Royalton</div><div><br></div><div>FUENTE: Wikipedia</div><div><br></div>', '<br>', 'aHR0cHM6Ly9nb28uZ2wvbWFwcy9ldGM4QmtFckd6NTI=', '', '2018-02-15 16:19:05', '1'),
(1561560547, 'Chile', '43', '728', '', '', '<br>', '<br>', '', '', '2019-06-26 14:49:53', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_pagos`
--

CREATE TABLE `hk_pagos` (
  `id` int(11) NOT NULL,
  `id_pedido` varchar(128) CHARACTER SET latin1 NOT NULL,
  `codigo` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `tipo_pago` varchar(128) CHARACTER SET latin1 NOT NULL,
  `monto` float(10,2) NOT NULL,
  `pdf` varchar(512) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` varchar(12) CHARACTER SET latin1 NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Mayor de Pagos ';

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_paquetes`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_paquetes' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_paquetes`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_pdf`
--

CREATE TABLE `hk_pdf` (
  `id` int(11) NOT NULL,
  `id_solicitud` varchar(50) NOT NULL DEFAULT '0',
  `pdf` varchar(512) NOT NULL DEFAULT '0',
  `descripcion` varchar(512) NOT NULL DEFAULT '0',
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` varchar(50) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `hk_pdf`
--

INSERT INTO `hk_pdf` (`id`, `id_solicitud`, `pdf`, `descripcion`, `fecha`, `estatus`) VALUES
(91, '1486889728', '58a5dcd9210 marye.pdf', '0', '2017-02-12 08:57:01', '1'),
(92, '1486889913', 'a447dd84l2.jpg', '0', '2017-02-12 08:59:03', '1'),
(93, '1486890071', '227b67cd210 marye.pdf', '0', '2017-02-12 09:01:34', '1'),
(94, '1486890143', '77a7b46e210 marye.pdf', '0', '2017-02-12 09:02:46', '1'),
(95, '1486890203', 'f9314189210 marye.pdf', '0', '2017-02-12 09:03:38', '1'),
(96, '1486890203', 'cafc5f27210 marye.pdf', '0', '2017-02-12 09:03:46', '1'),
(97, '1486890962', '7feae0a0210 marye.pdf', '0', '2017-02-12 09:17:52', '1'),
(98, '1486892020', '148460cdm6.pdf', '0', '2017-02-12 09:40:57', '1'),
(99, '1486892621', '50fd97ebvd.pdf', 'Que es valencia', '2017-02-12 09:46:41', '1'),
(105, '1488791761', 'd47d3e64vd.pdf', 'vd.pdf', '2017-03-06 09:16:38', '1'),
(161, '1516296163', '520633b7Plan Carnavales  2018 morrocoy paraiso azul si.pdf', 'Plan Carnavales  2018 morrocoy Posada Paraiso Azul ', '2018-01-18 18:04:01', '1'),
(103, '1487573259', '793f53a3210 marye.pdf', 'Fin de Semana en Familia', '2017-02-20 06:49:34', '1'),
(104, '1487573259', 'e691c2fd2000 wilfredo.pdf', '5 Dias en Pareja', '2017-02-20 06:49:36', '1'),
(107, '1488920698', '8ef0e504Uruguay - Wikipedia, la enciclopedia libre.pdf', 'Uruguay - Wikipedia, la enciclopedia libre.pdf', '2017-03-07 21:12:31', '1'),
(157, '1487595535', '93278601La Tortuga Enero 2018.pptx', 'La Tortuga Enero 2018', '2018-01-17 17:37:44', '1'),
(176, '1516292997', 'a69d353eFlyer concierto de chayanne.jpg', 'Chayanne en Concierto en Buenos Aires', '2019-06-14 19:20:28', '1'),
(177, '1561560547', '29d69479requisitos_visa_de_turismo_ciudadanos_venezolanos.pdf', 'requisitos visa de turismo ciudadanos venezolanos', '2019-06-26 14:49:49', '1'),
(116, '1487595474', 'e93e43c0ITINERARIO GRAN SABANA 20177.pdf', 'ITINERARIO GRAN SABANA 2017', '2017-03-09 20:41:21', '1'),
(162, '1491875517', '944a1c6eCANAIMA Campamento Waku Lodge BAJA 2018.pdf', 'CANAIMA Campamento Waku Lodge BAJA 2018', '2018-01-22 11:37:16', '1'),
(129, '1497380418', '30704a37TARIFARIO CARACAS-MAIQUETIA 19.05.2017.pdf', 'prueba017.pdf', '2017-06-13 19:00:42', '1'),
(130, '1497380418', 'd25bbd46TARIFARIO MARGARITA 19.05.2017.pdf', 'TARIFARIO MARGARITA 19.05.2017.pdf', '2017-06-13 19:01:18', '1'),
(178, '1489008119', '26e945bfFlyer Bogota visa americana.jpg', 'VISA AMERICANA EN BOGOTA', '2019-08-19 16:47:14', '1'),
(144, '1499456338', 'dba90294PerÃº econÃ³mico Jun 2017.pdf', 'PerÃº econÃ³mico Jul 2017', '2017-07-07 20:20:04', '1'),
(145, '1499976136', 'f65fa71efranjas.png', 'franjas.png', '2017-07-13 20:03:20', '1'),
(173, '1488922929', '18ec5f07MERIDA PLAN SEMANA SANTA 2018.pdf', 'MERIDA PLAN SEMANA SANTA 2018', '2018-03-09 16:36:30', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_pedidos`
--

CREATE TABLE `hk_pedidos` (
  `id` int(11) NOT NULL,
  `id_user` varchar(12) CHARACTER SET latin1 NOT NULL,
  `id_cliente` varchar(12) CHARACTER SET latin1 NOT NULL,
  `id_empleado` varchar(128) CHARACTER SET latin1 NOT NULL,
  `total` float(12,2) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `descripcion` varchar(128) CHARACTER SET latin1 NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_pedidos`
--

INSERT INTO `hk_pedidos` (`id`, `id_user`, `id_cliente`, `id_empleado`, `total`, `fecha`, `descripcion`, `status`) VALUES
(3, '7', '0', '1', 8200.00, '2016-10-20 21:15:23', 'Mesa2', 2),
(4, '7', '0', '2', 2200.00, '2016-10-20 21:17:46', 'Mesa3', 2),
(5, '7', '0', '1', 400.00, '2016-10-20 21:18:57', 'mesa4', 2),
(6, '7', '0', '2', 6900.00, '2016-10-20 21:19:03', 'mesa5', 2),
(7, '7', '0', '1', 1200.00, '2016-10-20 21:19:12', 'Luis', 2),
(8, '7', '0', '1', 7200.00, '2016-10-20 21:19:19', 'Jose', 2),
(9, '7', '0', '1', 4800.00, '2016-10-22 05:51:47', 'Mesa1', 2),
(10, '7', '0', '1', 800.00, '2016-10-22 05:53:29', 'Mesa1', 2),
(11, '7', '0', '1', 7400.00, '2016-10-22 05:54:14', 'Angel', 2),
(12, '7', '0', '2', 5100.00, '2016-10-22 05:55:20', 'TEst1', 2),
(13, '7', '0', '1', 18900.00, '2016-10-22 16:48:41', 'Marylin', 2),
(14, '7', '0', '2', 6000.00, '2016-10-22 19:24:58', 'lllll', 2),
(15, '7', '0', '1', 21100.00, '2016-10-23 13:59:10', 'Miguel', 2),
(16, '7', '0', '2', 7600.00, '2016-10-23 15:14:48', 'Mesa1', 2),
(17, '7', '0', '1', 5600.00, '2016-10-23 15:14:59', 'Mesa2', 2),
(18, '7', '0', '2', 8400.00, '2016-10-23 15:15:11', 'Mesa3', 2),
(19, '7', '0', '1', 7600.00, '2016-10-23 15:15:23', 'Terraza', 2),
(20, '7', '0', '1', 1200.00, '2016-10-23 15:17:04', 'luis', 2),
(21, '7', '0', '1', 10400.00, '2016-10-23 16:51:20', 'Uno', 2),
(22, '7', '0', '1', 6600.00, '2016-10-23 17:08:17', 'Dos', 2),
(23, '7', '0', '1', 13300.00, '2016-10-23 17:27:07', 'Uno', 2),
(24, '7', '0', '2', 4400.00, '2016-10-23 17:27:13', 'dos', 2),
(25, '7', '0', '2', 6000.00, '2016-10-23 17:27:18', 'tres', 2),
(26, '7', '0', '2', 5600.00, '2016-10-23 17:27:26', 'cuatro', 2),
(27, '7', '0', '1', 8800.00, '2016-10-23 17:27:31', 'cinco', 2),
(28, '7', '0', '2', 6300.00, '2016-10-23 17:27:36', 'seis', 2),
(29, '7', '0', '1', 2100.00, '2016-10-23 22:40:34', 'mesa1', 2),
(30, '7', '0', '1', 5000.00, '2016-10-24 21:22:48', 'mesa1', 2),
(31, '7', '0', '1', 7600.00, '2016-10-24 21:25:04', 'mesa2', 2),
(32, '7', '0', '1', 5000.00, '2016-10-26 08:25:41', '', 2),
(33, '7', '2', '2', 6400.00, '2016-10-26 08:27:24', 'Uno', 2),
(34, '7', '5', '2', 12800.00, '2016-10-26 08:34:19', 'dos', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_personal`
--

CREATE TABLE `hk_personal` (
  `id` int(11) NOT NULL,
  `nombre` varchar(512) CHARACTER SET latin1 NOT NULL,
  `descripcion` varchar(2048) CHARACTER SET latin1 NOT NULL,
  `url` varchar(512) CHARACTER SET latin1 NOT NULL,
  `imagen` varchar(512) CHARACTER SET latin1 NOT NULL,
  `estatus` varchar(12) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Mayor de personal';

--
-- Volcado de datos para la tabla `hk_personal`
--

INSERT INTO `hk_personal` (`id`, `nombre`, `descripcion`, `url`, `imagen`, `estatus`) VALUES
(1476156132, 'Gabriela Hidalfo', 'STAFF DE  SUPERVISORAS M&M Internatinal Group LTD', '', 'assets/img/8a24cace05e5c751368973c3b68c9fd0.png', '1'),
(1476156166, 'Elainer Dominguez', 'STAFF DE SUPERVISORAS M&M Internatinal Group LTD', '', 'assets/img/fd1e48885a7be1ed9b5d29d92253c12c.png', '1'),
(1476156199, 'Hilda Ariza', 'STAFF DE SUPERVISORAS M&M Internatinal Group LTD', '', 'assets/img/f9691d15229d7e4f90fe605c799f34bc.png', '1'),
(1476156218, 'Marianni Espinoza', 'STAFF DE SUPERVISORAS M&M Internatinal Group LTD', '', 'assets/img/c659d1a96cf8f989bbbf090ab8469d55.png', '1'),
(1476156248, 'Marlene Espinoza', 'STAFF DE SUPERVISORAS M&M Internatinal Group LTD', '', 'assets/img/276334c6137c1a1fb061613eabe41965.png', '1'),
(1476156270, 'Natalia Valderrama', 'STAFF DE SUPERVISORAS M&M Internatinal Group LTD', '', 'assets/img/95d5b6fe0903eac86ab79ca783222518.png', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_portafolio`
--

CREATE TABLE `hk_portafolio` (
  `id` int(11) NOT NULL,
  `titulo` varchar(512) CHARACTER SET latin1 NOT NULL,
  `descripcion` varchar(2048) CHARACTER SET latin1 NOT NULL,
  `url` varchar(512) CHARACTER SET latin1 NOT NULL,
  `imagen` varchar(512) CHARACTER SET latin1 NOT NULL,
  `estatus` varchar(12) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Mayor de portafolios';

--
-- Volcado de datos para la tabla `hk_portafolio`
--

INSERT INTO `hk_portafolio` (`id`, `titulo`, `descripcion`, `url`, `imagen`, `estatus`) VALUES
(1479189707, 'AvalÃºo Agrofinca Santa Barbara', 'AvalÃºo Agrofinca Santa Barbara', '0', 'assets/img/462e66f8c6dd6176622b70458de21aa7.png', '1'),
(1479189753, 'AvalÃºo Maquinarias y Equipos propiedad de Zisne Fabrica de Chocolates, C.A.', 'AvalÃºo Maquinarias y Equipos propiedad de Zisne Fabrica de Chocolates, C.A.', '0', 'assets/img/49cf699f7da58e5b6444fc36fc0b46f6.png', '1'),
(1479189772, 'AvalÃºo de Apartamento en San Bernardino - Caracas', 'AvalÃºo de Apartamento en San Bernardino - Caracas', '0', 'assets/img/44d3407c7164c5ed8029ee8075f5861a.png', '1'),
(1479190858, 'AvalÃºo de Locales Comerciales propiedad de Aluvima, C.A.', 'AvalÃºo de Locales Comerciales propiedad de Aluvima, C.A.', '0', 'assets/img/e1774adc95d1231d2aa4885e6b866e7b.png', '1'),
(1479190884, 'AvalÃºo de Bienes Inmuebles propiedad de la U.E. Dr. JosÃ© MarÃ­a Vargas', 'AvalÃºo de Bienes Inmuebles propiedad de la U.E. Dr. JosÃ© MarÃ­a Vargas', '0', 'assets/img/1092d7d0fc985248f5798d2bbb970168.png', '1'),
(1479190899, 'AvalÃºo de Edificaciones de la empresa Buhito,C.A.', 'AvalÃºo de Edificaciones de la empresa Buhito,C.A.', '0', 'assets/img/44d0c9116c02d0d515a72d4ea9ce1816.png', '1'),
(1482007876, 'AvalÃºo Bienes Muebles e Inmuebles de la U.E. Instituto Victegui', 'AvalÃºo Bienes Muebles e Inmuebles de la U.E. Instituto Victegui', '0', 'assets/img/ff78d3af887697ce4c47aa3aa7deeaf1.png', '1'),
(1482008925, 'AvalÃºo Mobiliarios y Equipos de Oficina propiedad de Sistemas Delta P, C.A.', 'AvalÃºo Mobiliarios y Equipos de Oficina propiedad de Sistemas Delta P, C.A.', '0', 'assets/img/2cb709d45d7a0e29c0adea1c38001fed.png', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_presupuesto`
--

CREATE TABLE `hk_presupuesto` (
  `id` int(11) NOT NULL,
  `nombres` varchar(512) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(512) COLLATE utf8_spanish_ci NOT NULL,
  `tlf` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `tlf_movil` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `empresa` varchar(512) COLLATE utf8_spanish_ci NOT NULL,
  `rif` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `ciudad` varchar(512) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(512) COLLATE utf8_spanish_ci NOT NULL,
  `cantidad` varchar(128) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(4096) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` varchar(12) COLLATE utf8_spanish_ci NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='mayor de presupuestos';

--
-- Volcado de datos para la tabla `hk_presupuesto`
--

INSERT INTO `hk_presupuesto` (`id`, `nombres`, `apellidos`, `tlf`, `tlf_movil`, `email`, `empresa`, `rif`, `ciudad`, `tipo`, `cantidad`, `descripcion`, `fecha`, `estatus`) VALUES
(1485397061, '', '', '', '', '', 'Cooperativa el guerrero che', 'Rif j-31472251-4', 'Carrizal', 'galpon', '200m2', 'El costo del alquiler en la actualidad es de 100 mil bolivares fuertes', '2017-01-25 22:22:04', '1'),
(1485350104, 'deilsa ', 'peÃ±apeÃ±a', '3224375', '04129628342', 'deilsalisboa@hotmail.com', 'multiservicios el tambor mf,ca', 'j-306152393', 'los teques', 'terreno y extrutura', '1', 'terreno de 400 metro cuadrado construido y tiene un apartamento en la parte  de arriba', '2017-01-25 09:21:30', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_privileges`
--

CREATE TABLE `hk_privileges` (
  `id` int(11) NOT NULL,
  `privilege` varchar(100) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_privileges`
--

INSERT INTO `hk_privileges` (`id`, `privilege`) VALUES
(1, 'index'),
(2, 'add'),
(3, 'edit'),
(4, 'del'),
(5, 'approve'),
(6, 'reject'),
(7, 'pending'),
(8, 'export'),
(9, 'detail'),
(10, 'notificacion'),
(11, 'list');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_productos`
--

CREATE TABLE `hk_productos` (
  `id` int(11) NOT NULL,
  `codigo` varchar(128) CHARACTER SET latin1 NOT NULL,
  `descripcion` varchar(256) CHARACTER SET latin1 NOT NULL,
  `precio` float(10,2) NOT NULL,
  `estatus` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_productos`
--

INSERT INTO `hk_productos` (`id`, `codigo`, `descripcion`, `precio`, `estatus`) VALUES
(6, '001', 'Cerveza PequeÃ±a', 400.00, 1),
(7, 'hw', 'Hora Wake', 4000.00, 1),
(8, '002', 'Vaso con Hielo', 100.00, 1),
(9, '003', 'Refresco', 500.00, 1),
(10, '004', 'Perro Caliente', 1000.00, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_professions`
--

CREATE TABLE `hk_professions` (
  `id` int(11) NOT NULL,
  `profession` varchar(100) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_professions`
--

INSERT INTO `hk_professions` (`id`, `profession`) VALUES
(1, 'Abogado'),
(2, 'Administrador'),
(3, 'Arquitecto'),
(4, 'Biólogo'),
(5, 'Bioquímico'),
(6, 'Botánico'),
(7, 'Carpintero'),
(8, 'Chef'),
(9, 'Cirujano'),
(10, 'Diseñador Gráfico'),
(11, 'Economista'),
(12, 'Enfermero'),
(13, 'Farmacéutico'),
(14, 'Filósofo'),
(15, 'Físico'),
(16, 'Fotógrafo'),
(17, 'Geólogo'),
(18, 'Gerente'),
(19, 'Historiador'),
(20, 'Informático'),
(21, 'Ingeniero'),
(22, 'Juez'),
(23, 'Licenciado'),
(24, 'Locutor'),
(25, 'Matemático'),
(26, 'Médico'),
(27, 'Modelo'),
(28, 'Músico'),
(29, 'Nutricionista'),
(30, 'Odontólogo'),
(31, 'Pediatra'),
(32, 'Periodista'),
(33, 'Pintor'),
(34, 'Profesor'),
(35, 'Psicólogo'),
(36, 'Psiquiatra'),
(37, 'Publicista'),
(38, 'Químico'),
(39, 'Sociólogo'),
(40, 'Técnico'),
(41, 'Teólogo'),
(42, 'Terapeuta'),
(43, 'Traductor'),
(44, 'Veterinario'),
(45, 'Electricista'),
(46, 'Mecánico'),
(47, 'Comerciante'),
(48, 'Obrero'),
(49, 'Actor'),
(50, 'Cantante'),
(51, 'Deportista'),
(52, 'Diseñador de moda'),
(53, 'Contador'),
(54, 'Estudiante');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_reserva`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_reserva' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_reserva`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_reserva_detalle`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_reserva_detalle' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_reserva_detalle`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_resources`
--

CREATE TABLE `hk_resources` (
  `id` int(11) NOT NULL,
  `resource` varchar(100) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_resources`
--

INSERT INTO `hk_resources` (`id`, `resource`) VALUES
(1, 'index'),
(2, 'users'),
(3, 'members'),
(4, 'estudiantes'),
(5, 'certificados'),
(6, 'cursos'),
(7, 'categoria'),
(8, 'noticias'),
(9, 'sliders'),
(10, 'modulo'),
(11, 'portafolio'),
(12, 'pagina'),
(13, 'personal'),
(14, 'pedidos'),
(15, 'productos'),
(16, 'clientes'),
(17, 'empleados'),
(19, 'marcas'),
(21, 'servicios'),
(22, 'contacto'),
(23, 'solicitud'),
(26, 'noticia'),
(27, 'presupuesto'),
(28, 'inmueble'),
(29, 'mueble'),
(30, 'administrador'),
(31, 'validador'),
(32, 'auditoria'),
(33, 'blog'),
(36, 'destinos'),
(37, 'aerolineas'),
(38, 'hoteles'),
(39, 'tarifa'),
(40, 'paquetes'),
(41, 'agencias'),
(42, 'nosotros'),
(43, 'terminos'),
(44, 'politicas'),
(45, 'faqs'),
(46, 'privacidad'),
(47, 'reserva');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_roles`
--

CREATE TABLE `hk_roles` (
  `id` int(11) NOT NULL,
  `role` varchar(50) CHARACTER SET latin1 NOT NULL,
  `description` varchar(50) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_roles`
--

INSERT INTO `hk_roles` (`id`, `role`, `description`) VALUES
(1, 'administrador', 'Administrador'),
(2, 'secretario', 'Secretario'),
(3, 'superadmin', 'Super Administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_servicios`
--

CREATE TABLE `hk_servicios` (
  `id` int(11) NOT NULL,
  `titulo` varchar(512) CHARACTER SET latin1 NOT NULL,
  `descripcion` varchar(4096) CHARACTER SET latin1 NOT NULL,
  `imagen` varchar(512) CHARACTER SET latin1 NOT NULL,
  `estatus` varchar(12) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_servicios`
--

INSERT INTO `hk_servicios` (`id`, `titulo`, `descripcion`, `imagen`, `estatus`) VALUES
(1478957764, 'AVALÃšOS INMOBILIARIOS', '<p class=\"serv-p2\">Son informes destinados a establecer el valor de los bienes inmuebles, utilizando metodologÃ­as universalmente reconocidas y normativas vigentes.  En este tipo de informe, el Tasador refleja aspectos relevantes de un inmueble, tales como la ubicaciÃ³n, superficie, uso, tipo, edad, vida Ãºtil, conservaciÃ³n y mantenimiento, considerando las condiciones de mercado y zona que le rodea, entre otros, su valor actual.</p> \r\n<p class=\"serv-p2\">El Tasador realiza la visita al inmueble con la finalidad de realizar las tomas fotogrÃ¡ficas, levantamiento de plano, levantamiento de las caracterÃ­sticas y condiciones fÃ­sicas. Se verificarÃ¡ la documentaciÃ³n de propiedad, catastro, etc. Todos estos datos son parte esencial del Informe de AvalÃºo, junto al valor actual del inmueble.</p> \r\n<p class=\"serv-p2\">Entre los bienes inmuebles que valora la empresa se encuentran los terrenos, apartamentos, casas, quintas, town house, apartamentos, edificios, locales comerciales, centros comerciales, galpones, entre otros. </p>\r\n\r\n<a href=\"/solicitud/presupuesto\" class=\"btn btn-default btn-large\"> Solicitar Presupuesto</a>', 'assets/img/2e42bf2845a4615d3ca69811a78f5b31.png', '1'),
(1478957800, 'AVALÃšOS DE MAQUINARIAS Y EQUIPOS', '<p class=\"serv-p2\">Es la estimaciÃ³n de valor fundamentado en la utilizaciÃ³n, manejo, implementaciÃ³n  y  ejecuciÃ³n  de  criterios  y  metodologÃ­as  de  valoraciÃ³n  universalmente aceptados y con  estricto cumplimiento  de  normas  y  procedimientos desarrollados por DiseÃ±os y Proyectos Tirex 2021, C.A., tomando como referencia los conocimientos prÃ¡cticos de los diferentes tipos de  procesos  industriales,  maquinarias vinculadas  a  estos  procesos,  y  funcionamiento  de las plantas  industriales  en  todas  sus  Ã¡reas  </p>\r\n\r\n <p class=\"serv-p2\">En este tipo de informe, el tasador refleja los aspectos relevantes de las maquinarias y equipos, mediante un proceso metodolÃ³gico  de  depreciaciÃ³n  tÃ©cnica, que  incluye la ubicaciÃ³n, uso, tipo, edad, vida Ãºtil, conservaciÃ³n y mantenimiento, grado de obsolescencia funcional o tecnolÃ³gica y condiciones de operatividad y funcionamiento</p> \r\n\r\n<p class=\"serv-p2\">El Tasador realiza la visita a los bienes con la finalidad de realizar las tomas fotogrÃ¡ficas, levantamiento de las caracterÃ­sticas y condiciones fÃ­sicas. Todos estos datos son parte esencial del Informe de AvalÃºo, junto al valor actual de los bienes </p>\r\n\r\n<a href=\"/solicitud/presupuesto\" class=\"btn btn-default btn-large\"> Solicitar Presupuesto</a>', 'assets/img/ba6c2b79533f9570724c453afdc2e364.png', '1'),
(1478957841, 'AVALÃšOS INDUSTRIALES', '<p class=\"serv-p2\">Son informes destinados a establecer el valor actual de los activos de una industria, con el objeto de que cuente con informaciÃ³n financiera y permita hacer una evaluaciÃ³n realista de su verdadera situaciÃ³n econÃ³mica. </p>\r\n\r\n <p class=\"serv-p2\">En este tipo de informe el Tasador, realiza una valuaciÃ³n de los inmuebles, maquinarias, equipos, instalaciones, herramientas, entre otros; determinando el Valor Actual o de Mercado de los mismos; reflejando el mayor y mejor uso de un activo, asÃ­ como la ubicaciÃ³n, uso, tipo, edad, vida Ãºtil, conservaciÃ³n y mantenimiento, grado de obsolescencia funcional o tecnolÃ³gica y condiciones de operatividad y funcionamiento, entre otros factores  </p> \r\n\r\n<p class=\"serv-p2\">De igual manera, el Tasador realiza la conciliaciÃ³n fÃ­sica versus los registros contables, ya que en muchas ocasiones se encuentran activos que ya han sido dados de baja, ya sea por deterioro o por haber alcanzado su vida Ãºtil, por usarlos como partes y piezas para otros activos o por haber sido vendidos, con la finalidad de corroborar lo que existe fÃ­sicamente con lo que estÃ¡ registrado en los libros contables y asÃ­ poder identificar las diferencias existentes..</p>\r\n\r\n<a href=\"/solicitud/presupuesto\" class=\"btn btn-default btn-large\"> Solicitar Presupuesto</a>', 'assets/img/bdadd7582790ed4fe06936b3207549e5.png', '1'),
(1478957866, 'AVALÃšOS  AGRICOLAS', '<p class=\"serv-p2\">Son informes donde se establece el valor actual de un bien con caracterÃ­sticas rurales, tales como fincas, haciendas, hatos, fundos, entre otros; constituidos por diversos tipos de construcciones, instalaciones, maquinarias y equipos, implementos, herramientas, utensilios, semovientes y cultivos, dedicados a la agricultura, producciÃ³n de forrajes, etc; utilizando metodologÃ­as universalmente reconocidas y normativas vigentes </p>\r\n\r\n <p class=\"serv-p2\">El Tasador realiza la visita a los bienes con la finalidad de realizar las tomas fotogrÃ¡ficas, levantamiento de las caracterÃ­sticas y condiciones fÃ­sicas, poniendo especial atenciÃ³n en los factores que influyen en la productividad y explotaciÃ³n actual y futura del predio, tomando en consideraciÃ³n la superficie del predio, calidad y fertilidad de la tierra, la profundidad de la  capa arable, el nivel de permeabilidad, textura, topografÃ­a, vegetaciÃ³n, tipo de pasto, la fauna, yacimientos y desechos minerales, uso actual del suelo, la capacidad de carga animal, clima de la regiÃ³n, la intensidad y frecuencia de los vientos, tormentas, comportamiento con respecto a fenÃ³menos  meteorolÃ³gicos, las colindancias de la regiÃ³n, la infraestructura existente y maquinarias y equipos existentes.. </p>\r\n\r\n<a href=\"/solicitud/presupuesto\" class=\"btn btn-default btn-large\"> Solicitar Presupuesto</a>', 'assets/img/cbde7877c110bb989e91f5819303d9e0.png', '1'),
(1478957888, 'AVALÃšOS DE VEHICULOS USADOS', '<p class=\"serv-p2\">En este tipo de informe se establece el  valor actual de VehÃ­culos Terrestres, VehÃ­culos AÃ©reos, VehÃ­culos MarÃ­timos y VehÃ­culos Desincorporados; fundamentado en la informaciÃ³n de los Documentos de Propiedad, ubicaciÃ³n, uso, tipo, edad, vida Ãºtil, conservaciÃ³n, mantenimiento, obsolescencia, desgaste, operatividad y funcionamiento los bienes.</p>\r\n\r\n <p class=\"serv-p2\">El Tasador realiza la visita a los bienes con la finalidad de realizar las tomas fotogrÃ¡ficas y levantamiento de los diferentes componentes  y partes tales como el sistema motriz, transmisiÃ³n, propulsiÃ³n, sistema diferencial, frenos, suspensiÃ³n, direcciÃ³n, carrocerÃ­a, sistema elÃ©ctrico y accesorios.</p>\r\n\r\n <p class=\"serv-p2\">DiseÃ±os y Proyectos Tirex 2021, C.A., a los fines de establecer metodologÃ­as a cada vehÃ­culo los clasificÃ³, de acuerdo a su uso, de la siguiente manera:</p> \r\n\r\n <ul class=\"lista_texto\">\r\n<li>VehÃ­culos  Terrestres: Particular, colectivo, industrial o de carga.</li>\r\n<li>VehÃ­culos  MarÃ­timos: DiversiÃ³n, carga y transporte, pasajeros.</li>\r\n<li>VehÃ­culos  AÃ©reo: Pasajeros y carga..</li>\r\n<li>VehÃ­culos  Desincorporados: Chatarras.</li>\r\n</ul>\r\n\r\n<a href=\"/solicitud/presupuesto\" class=\"btn btn-default btn-large\"> Solicitar Presupuesto</a>', 'assets/img/0b912e39d48edaa7fc5a810f7c6cdbc2.png', '1'),
(1478957936, 'AVALÃšOS DE INMUEBLES NO TERMINADOS (OBRAS INCONCLUSAS)', '<p class=\"serv-p2\">Son informes que tienen como finalidad establecer un estudio valorativo de inmuebles que no han sido culminados, considerando a la Obra Inconclusa como un inmueble especial que demandarÃ¡ tÃ©cnicas valuatorias particulares, tomando como base las cantidades de obras ejecutadas y los documentos existentes (si los hubieran), tales como planos aprobados por la autoridad municipal, permisos de construcciÃ³n, presupuesto original de la obra, contabilidad de costos de la obra, copia del libro de obra, planos modificados, actas de inicio y paralizaciÃ³n, copias de las valuaciones aprobadas, corte de cuenta o cuadro de aumento y disminuciÃ³n, entre otros. . </p>\r\n\r\n <p class=\"serv-p2\">El Tasador realiza la visita a los bienes con la finalidad de realizar las tomas fotogrÃ¡ficas y levantamiento de informaciÃ³n para determinar la cantidades de obras ejecutadas, estimar que porcentaje de cada partida de obra han sido ejecutadas, medir aquellas partidas que puedan ser mensurables en sitio, deducir que obras extra adolece la construcciÃ³n o cuales son las partidas destinadas a disminuciÃ³n. AsÃ­ mismo, se verificarÃ¡ la ubicaciÃ³n del inmueble, su superficie, conservaciÃ³n y mantenimiento, considerando las condiciones de mercado y zona que le rodea, entre otros.</p>\r\n\r\n\r\n<a href=\"/solicitud/presupuesto\" class=\"btn btn-default btn-large\"> Solicitar Presupuesto</a>', 'assets/img/a08bc0cbab1d0f9365c80c38560a61ec.png', '1'),
(1478957955, 'AVALÃšOS ESPECIALES', '<p class=\"serv-p2\"> En estos tipos de informes se establece el valor actual de bienes inmuebles con caracterÃ­sticas especiales, tales como Hoteles, Posadas, Teatros, ClÃ­nicas, Hospitales, entre otros. Se valora los activos tangibles e intangibles, incluyendo terrenos, bienhechurÃ­as, mejoras, maquinarias, mobiliarios, accesorios y equipos; ingresos por operaciÃ³n, rentas por arrendamientos, good will y valor en marcha. Es decir, se considera a los bienes como una unidad operacional</p>\r\n\r\n <p class=\"serv-p2\"> El Tasador realiza la visita a los bienes con la finalidad de realizar las tomas fotogrÃ¡ficas y levantamiento de informaciÃ³n, tales como la ubicaciÃ³n, superficie, uso, tipo, edad, vida Ãºtil, conservaciÃ³n y mantenimiento, operatividad, funcionamiento, obsolescencia, potencialidad y los registros de operaciÃ³n que estÃ©n disponibles</p>\r\n\r\n<a href=\"/solicitud/presupuesto\" class=\"btn btn-default btn-large\"> Solicitar Presupuesto</a>', 'assets/img/169dd395f5310abff16700707ddb5789.png', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_sliders`
--

CREATE TABLE `hk_sliders` (
  `id` int(11) NOT NULL,
  `titulo` varchar(256) CHARACTER SET latin1 NOT NULL,
  `descripcion` varchar(512) CHARACTER SET latin1 NOT NULL,
  `imagen` varchar(512) CHARACTER SET latin1 NOT NULL,
  `estatus` varchar(12) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_sliders`
--

INSERT INTO `hk_sliders` (`id`, `titulo`, `descripcion`, `imagen`, `estatus`) VALUES
(1489066520, 'Acarigua Araure', 'Acarigua', 'assets/img/f1c669937390c733440ab88333beb7b0.jpg', '1'),
(1489066206, 'Barinas', 'Rafting', 'assets/img/428a89b9572bd0762ac0275c041af598.jpg', '1'),
(1489065660, 'Gran Sabana', 'Gran Sabana', 'assets/img/7bc5de5b80f2e85451c9eafcf26edc8d.jpg', '1'),
(1489065593, 'Canaima', 'Canaima', 'assets/img/b47fbd48fc601c31df5c5c34e62b6bcd.jpg', '1'),
(1489065176, 'Merida', 'Merida Ciudad de Caballeros', 'assets/img/51080394d4e36b3857fbd9bf4391e5ee.jpg', '1'),
(1489065513, 'Bogota', '2600 metros mas cerca de las estrellas', 'assets/img/e998a59baa0f68b95a3c53defb1f2e0d.jpg', '1'),
(1489066581, 'Barquisimeto', 'Ciudad de los crepusculos', 'assets/img/8698c2dd4d1b86d75cd9ef427071f1c9.jpg', '1'),
(1489075352, 'Caracas', 'La ciudad de la eterna primavera', 'assets/img/50d4a6ea3d828d407b19af5420f3e43e.jpg', '1'),
(1489075773, 'Choroni', 'Playas del Estado Aragua', 'assets/img/00bfea916d250c7136d611686bb9830b.jpg', '1'),
(1489076205, 'Colonia Tovar', 'Villa Alemana enclavada en las montaÃ±as del Parque Henty Pittier', 'assets/img/a0fa62e51355903023bfdc8992f4fd52.jpg', '1'),
(1489076361, 'Mochima', 'Parque Nacional Mochima Edo. Sucre', 'assets/img/3c7927a44ba3c06322be421d452e9d43.jpg', '1'),
(1489076398, 'Medanos de Coro', 'Estado Falcon', 'assets/img/1b5b6d0befa79576d9511490f2c6c2ad.jpg', '1'),
(1489076531, 'Playa Caracolito', 'Higuerote', 'assets/img/d6e0299108837dfad3254712345f7df1.jpg', '1'),
(1489076647, 'Isla de Margarita', 'La perla del caribe', 'assets/img/5e5652ab62b6439f3383679a906ba73a.jpg', '1'),
(1489077102, 'Isla la Tortuga', 'La segunda mas grande despues de Isla Margarita', 'assets/img/6ee537280dfb5ac73cd43002bd8309b4.jpg', '1'),
(1489077163, 'Litoral Central', 'Estado Vargas', 'assets/img/8e561e5c31966083e64dfc164916f1fb.jpg', '1'),
(1489077617, 'Los Roques', 'A solo 45 minutos en avion de Caracas', '0', '1'),
(1489077684, 'Maracaibo', 'La Tierra del Sol Amada y del Catatumbo', 'assets/img/5f8ddc057694cfb74dd4c97c818c7fa7.jpg', '1'),
(1489077914, 'Catatumbo', 'Patrimonio de la humanidad por la Unesco', 'assets/img/5cbf465868557b1c8b0ec7c42afabbd7.jpg', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_states`
--

CREATE TABLE `hk_states` (
  `id` int(11) NOT NULL,
  `state` varchar(100) CHARACTER SET utf8 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `hk_states`
--

INSERT INTO `hk_states` (`id`, `state`) VALUES
(1, 'Amazonas'),
(2, 'Anzoátegui'),
(3, 'Apure'),
(4, 'Aragua'),
(5, 'Barinas'),
(6, 'Bolívar'),
(7, 'Carabobo'),
(8, 'Cojedes'),
(9, 'Delta Amacuro'),
(10, 'Falcón'),
(11, 'Guárico'),
(12, 'Lara'),
(13, 'Mérida'),
(14, 'Miranda'),
(15, 'Monagas'),
(16, 'Nueva Esparta'),
(17, 'Portuguesa'),
(18, 'Sucre'),
(19, 'Táchira'),
(20, 'Trujillo'),
(21, 'Vargas'),
(22, 'Yaracuy'),
(23, 'Zulia'),
(24, 'Distrito Capital'),
(25, 'Dependencias Federales');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_tarifa`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_tarifa' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_tarifa`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_validador`
--

CREATE TABLE `hk_validador` (
  `id` int(22) NOT NULL,
  `dpt` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `cvt` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `estatus` varchar(12) COLLATE utf8_unicode_ci NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Mayor de solicitudes de validacion de informe';

--
-- Volcado de datos para la tabla `hk_validador`
--

INSERT INTO `hk_validador` (`id`, `dpt`, `cvt`, `email`, `fecha`, `estatus`) VALUES
(1505036229, 'pfyPsWLCyxCZXt', 'PVWcbQvaN', 'ec12342vtv@hotmail.com', '2017-09-10 09:37:11', '1'),
(1508573862, 'hkNILCwYNXbAqp', 'EjMPgloHazLGduqqo', 'jimosa4ccf2@hotmail.com', '2017-10-21 08:17:43', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_video`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_video' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_video`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hk_views`
--
-- en uso(#1932 - Table 'conkhcom_cms.hk_views' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`hk_views`' en la linea 1)

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `states`
--
-- en uso(#1932 - Table 'conkhcom_cms.states' doesn't exist in engine)
-- Error leyendo datos: (#1064 - Algo está equivocado en su sintax cerca 'FROM `conkhcom_cms`.`states`' en la linea 1)

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `hk_access`
--
ALTER TABLE `hk_access`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `role_id` (`role_id`,`resource_id`,`privilege_id`),
  ADD KEY `FK_acl_access_acl_resources` (`resource_id`),
  ADD KEY `FK_acl_access_acl_privileges` (`privilege_id`);

--
-- Indices de la tabla `hk_admin_users`
--
ALTER TABLE `hk_admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `usuario` (`username`),
  ADD KEY `FK_admin_users_acl_roles` (`role_id`);

--
-- Indices de la tabla `hk_asistencia`
--
ALTER TABLE `hk_asistencia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_auditoria`
--
ALTER TABLE `hk_auditoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_audit_trail`
--
ALTER TABLE `hk_audit_trail`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_bienes`
--
ALTER TABLE `hk_bienes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_categoria`
--
ALTER TABLE `hk_categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_cities`
--
ALTER TABLE `hk_cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_estado` (`state_id`);

--
-- Indices de la tabla `hk_clientes`
--
ALTER TABLE `hk_clientes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cedula` (`cedula`);

--
-- Indices de la tabla `hk_cursos`
--
ALTER TABLE `hk_cursos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`);

--
-- Indices de la tabla `hk_detalle_pedido`
--
ALTER TABLE `hk_detalle_pedido`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_empleados`
--
ALTER TABLE `hk_empleados`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `hk_estatus_pedido`
--
ALTER TABLE `hk_estatus_pedido`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_estudiantes`
--
ALTER TABLE `hk_estudiantes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cedula` (`cedula`);

--
-- Indices de la tabla `hk_estudiante_curso`
--
ALTER TABLE `hk_estudiante_curso`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD KEY `id` (`id`,`id_estudiante`,`id_curso`),
  ADD KEY `id_estudiante` (`id_estudiante`),
  ADD KEY `id_curso` (`id_curso`);

--
-- Indices de la tabla `hk_etiquetas`
--
ALTER TABLE `hk_etiquetas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_fotos`
--
ALTER TABLE `hk_fotos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_grado`
--
ALTER TABLE `hk_grado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_inmueble`
--
ALTER TABLE `hk_inmueble`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_marcas`
--
ALTER TABLE `hk_marcas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_modulos`
--
ALTER TABLE `hk_modulos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_muebles`
--
ALTER TABLE `hk_muebles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_noticia`
--
ALTER TABLE `hk_noticia`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_pagina`
--
ALTER TABLE `hk_pagina`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_pagos`
--
ALTER TABLE `hk_pagos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_pdf`
--
ALTER TABLE `hk_pdf`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_pedidos`
--
ALTER TABLE `hk_pedidos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_personal`
--
ALTER TABLE `hk_personal`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_portafolio`
--
ALTER TABLE `hk_portafolio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_presupuesto`
--
ALTER TABLE `hk_presupuesto`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_privileges`
--
ALTER TABLE `hk_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_productos`
--
ALTER TABLE `hk_productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_professions`
--
ALTER TABLE `hk_professions`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_resources`
--
ALTER TABLE `hk_resources`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_roles`
--
ALTER TABLE `hk_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_servicios`
--
ALTER TABLE `hk_servicios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_sliders`
--
ALTER TABLE `hk_sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_states`
--
ALTER TABLE `hk_states`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `hk_validador`
--
ALTER TABLE `hk_validador`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `hk_access`
--
ALTER TABLE `hk_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT de la tabla `hk_admin_users`
--
ALTER TABLE `hk_admin_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `hk_asistencia`
--
ALTER TABLE `hk_asistencia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `hk_auditoria`
--
ALTER TABLE `hk_auditoria`
  MODIFY `id` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1485455439;
--
-- AUTO_INCREMENT de la tabla `hk_audit_trail`
--
ALTER TABLE `hk_audit_trail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=415;
--
-- AUTO_INCREMENT de la tabla `hk_bienes`
--
ALTER TABLE `hk_bienes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `hk_categoria`
--
ALTER TABLE `hk_categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT de la tabla `hk_cities`
--
ALTER TABLE `hk_cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=523;
--
-- AUTO_INCREMENT de la tabla `hk_clientes`
--
ALTER TABLE `hk_clientes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `hk_cursos`
--
ALTER TABLE `hk_cursos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `hk_detalle_pedido`
--
ALTER TABLE `hk_detalle_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=244;
--
-- AUTO_INCREMENT de la tabla `hk_empleados`
--
ALTER TABLE `hk_empleados`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `hk_estatus_pedido`
--
ALTER TABLE `hk_estatus_pedido`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `hk_estudiantes`
--
ALTER TABLE `hk_estudiantes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `hk_estudiante_curso`
--
ALTER TABLE `hk_estudiante_curso`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `hk_etiquetas`
--
ALTER TABLE `hk_etiquetas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=190;
--
-- AUTO_INCREMENT de la tabla `hk_fotos`
--
ALTER TABLE `hk_fotos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1465;
--
-- AUTO_INCREMENT de la tabla `hk_grado`
--
ALTER TABLE `hk_grado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `hk_inmueble`
--
ALTER TABLE `hk_inmueble`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1508573871;
--
-- AUTO_INCREMENT de la tabla `hk_marcas`
--
ALTER TABLE `hk_marcas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1484848903;
--
-- AUTO_INCREMENT de la tabla `hk_modulos`
--
ALTER TABLE `hk_modulos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1478985879;
--
-- AUTO_INCREMENT de la tabla `hk_muebles`
--
ALTER TABLE `hk_muebles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1485457478;
--
-- AUTO_INCREMENT de la tabla `hk_noticia`
--
ALTER TABLE `hk_noticia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1483973996;
--
-- AUTO_INCREMENT de la tabla `hk_pagina`
--
ALTER TABLE `hk_pagina`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1561560548;
--
-- AUTO_INCREMENT de la tabla `hk_pagos`
--
ALTER TABLE `hk_pagos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `hk_pdf`
--
ALTER TABLE `hk_pdf`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;
--
-- AUTO_INCREMENT de la tabla `hk_pedidos`
--
ALTER TABLE `hk_pedidos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT de la tabla `hk_portafolio`
--
ALTER TABLE `hk_portafolio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1482008926;
--
-- AUTO_INCREMENT de la tabla `hk_presupuesto`
--
ALTER TABLE `hk_presupuesto`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1485455635;
--
-- AUTO_INCREMENT de la tabla `hk_privileges`
--
ALTER TABLE `hk_privileges`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `hk_productos`
--
ALTER TABLE `hk_productos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `hk_professions`
--
ALTER TABLE `hk_professions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT de la tabla `hk_resources`
--
ALTER TABLE `hk_resources`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;
--
-- AUTO_INCREMENT de la tabla `hk_roles`
--
ALTER TABLE `hk_roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `hk_servicios`
--
ALTER TABLE `hk_servicios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1478957956;
--
-- AUTO_INCREMENT de la tabla `hk_sliders`
--
ALTER TABLE `hk_sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1489077915;
--
-- AUTO_INCREMENT de la tabla `hk_states`
--
ALTER TABLE `hk_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT de la tabla `hk_validador`
--
ALTER TABLE `hk_validador`
  MODIFY `id` int(22) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1508573863;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
